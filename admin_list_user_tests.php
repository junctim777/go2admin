<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function printUserTests($uid) {
    $query = "SELECT * FROM go2stuko_test_examination te, go2stuko_test t WHERE t.tid = te.tid AND te.uid = " . $uid;
    $test_examinations = Database::getDatasetFromQuery($query);
    $out = '<table>';
    $out .= '<tr>';
    $out .= '<td align="left" valign="top">';
    $out .= '<b>TID</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Test</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Start</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Ende</b>';
    $out .= '</td>';
    $out .= '</tr>';
    foreach ($test_examinations as $test_examination) {
        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= $test_examination->tid;
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
        $out .= $test_examination->description;
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
        $out .= $test_examination->starting_time;
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
        $out .= $test_examination->ending_time;
        $out .= '</td>';
        $out .= '</tr>';
    }
    $out .= '</table>';
    $out .= '<br>';
    $out .= '<a href="administration.php">Zur&uuml;ck</a>';
    return $out;
}

?>