<html>
<body>
Sehr [sex] [surname],<br/><br/>

wir freuen uns über Ihr Interesse an unserer Online-Software für Sprachschulen.<br/><br/>

Anbei schicken wir Ihnen unsere aktuelle Preisliste für Partnersprachschulen. Alle Preise sind inkl. MwSt. angegeben.<br/><br/>

Bei den aufgeführten Paketen handelt es sich um Musterangebote. Gerne machen wir Ihnen
ein individuelles Angebot, bei dem Sie sowohl die Dauer als auch die Anzahl der Prüfungen pro Woche vorgeben können.<br/><br/>

Für Fragen zu Software und Vertragsbedingungen stehen wir Ihnen gerne zur Verfügung.<br/><br/>

Mit freundlichen Grüßen,<br/>
Jonathan Berroth<br/><br/>

- - - - - - - - - - - - - - - - - - - - - - - - - - - - -<br/><br/>
Go2Studienkolleg GbR<br/>
Theo-Prosel-Weg 16<br/>
DE-80797 München<br/><br/>

Phone: 0 (+49) 89 / 2155 0525<br/><br/>

Email: info@Go2Studienkolleg.de<br/>
Internet: www.Go2Studienkolleg.de<br/><br/><br/><br/></body>
</html>
