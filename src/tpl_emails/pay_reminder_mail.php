<html>
<body>
<table width="650"><tbody><tr><td colspan="2">
            <table id="table1" border="0" cellspacing="0" cellpadding="0" width="100%" height="145"><tbody><tr><td valign="top" width="50%" nowrap="nowrap"><font size="2" face="Arial"><b><img src="https://www.go2studienkolleg.de/src/imgs/logo/logo_small.png"/></b></font></td>
                    <td valign="top" width="50%" nowrap="nowrap" align="right">
                        <p><br/><br/><b>Go2Studienkolleg GbR</b><br/>Theo-Prosel-Weg. 16<br/>D-80797 M&uuml;nchen<br/>Email: <a href="mailto:info@go2studienkolleg.de">info@go2studienkolleg.de</a><br/>Tel: 089-21550525</p></td></tr></tbody></table></td></tr><tr><td valign="top">
            <p><br/><span style="LINE-HEIGHT: 115%; FONT-FAMILY: 'Times New Roman','serif'; FONT-SIZE: 7.5pt; mso-fareast-font-family: 'Times New Roman'; mso-fareast-language: DE; mso-ansi-language: DE; mso-bidi-language: AR-SA"><u>Rechnungsanschrift <br/></u></span><span style="LINE-HEIGHT: 115%; FONT-FAMILY: 'Times New Roman','serif'; FONT-SIZE: 7.5pt; mso-fareast-font-family: 'Times New Roman'; mso-fareast-language: DE; mso-ansi-language: DE; mso-bidi-language: AR-SA">[prename] [surname]<br/>[payer_street] [payer_house_number]<br/>[payer_plz] [payer_city]</span><br/><br/></p></td>
        <td valign="top" align="right"/></tr><tr><td valign="top">
            <p><font face="Arial"><font size="4">Mahung bez&uuml;glich Rechnung Nr.: [booking_id] <br/></font></font></p></td>
        <td valign="top" align="right"><font size="4" face="Arial">[date]</font></td></tr><tr><td colspan="7">
            <p><font size="2" face="Arial"><strong><br/>Sehr [sex] [surname],<br/><br /></strong>leider konnten wir bis zum heutigen Datum keinen Zahlungseingang auf unserem Konto verzeichnen.<br/><br/>[reminderMessage]
                    <br /><br />Bitte &uuml;berweisen Sie den f&auml;lligen Betrag innerhalb der n&auml;chsten 10 Tage auf unser Konto (Bankdaten siehe unten). Falls Sie die &Uuml;berweisung bereits get&auml;tigt haben betrachten Sie dieses Schreiben bitte als gegenstandslos.<br /></font></p></td></tr></tbody></table><table width="650"><tbody><tr><td align="right"><font size="2" face="Arial"><b>Kunden-Nr.: [user_id]</b></font></td></tr></tbody></table><table cellpadding="0" width="650"><tbody><tr><td colspan="7">
            <hr size="1"/></td></tr><tr><td width="5%" align="right"></td>
        <td width="10%" align="left"><font size="1" face="Arial"><b>Anz</b></font></td>
        <td width="29%" align="left"><font size="1" face="Arial"><b>Leistung</b></font></td>
        <td width="5%" align="right"><font size="1" face="Arial"><b/></font></td>
        <td width="19%" align="right"><font size="1" face="Arial"><b/>Dauer</font></td>
        <td width="19%" align="right"><font size="1" face="Arial"><b/></font></td>
        <td width="13%" align="right"><font size="1" face="Arial"><b>Entgelt pro Lizenz</b></font></td></tr><tr><td colspan="7">
            <hr size="1"/></td></tr><tr><td valign="top" nowrap="nowrap" align="left"><font color="#000000" size="1" face="Arial"/></td>
        <td valign="top" nowrap="nowrap" align="left"><font color="#000000" face="Arial"><strong>1</strong></font></td>
        <td valign="top" align="left"><font color="#000000" size="2" face="Arial"><b>Leistungsumfang:</b><br/>
                - Original Pr&uuml;fungsoptik<br/>
                - individuelle &Uuml;bungseinheiten<br/>
                - w&ouml;chentliche Musterpr&uuml;fungen<br/>
                - Auto-Korrektur<br/>
                - t&auml;glich neue &Uuml;bungen<br/>
                - L&ouml;sungshinweis<br/><br/><b>Fragetypen Wochenpr&uuml;fungen:</b><br/>
                - Satzbildung<br/>
                - L&uuml;ckentexte<br/>
                - Fragebildung<br/>
                - Leseverstehen<br/>
                - Passivbildung<br/>
                - Bild- &amp; Grafikbeschreibung<br/>
                - Fehlersuche<br/></font></td>
        <td valign="top" nowrap="nowrap" align="right"><font color="#000000" face="Arial"/></td>
        <td valign="top" nowrap="nowrap" align="right"><font color="#000000" size="1" face="Arial"><font size="1">[package_duration] Woche(n)</font></font></td>
        <td valign="top" nowrap="nowrap" align="right"><font color="#000000" size="1" face="Arial"/></td>
        <td valign="top" nowrap="nowrap" align="right"><font color="#000000" size="1" face="Arial">[price_brutto] &euro;</font></td></tr>
            <hr size="1"/></td></tr><tr><td colspan="6" align="right"><font size="2" face="Arial">Gesamt</font></td>
        <td nowrap="nowrap" align="right"><font color="#000000" size="2" face="Arial">[price_brutto] &euro;</font></td></tr><tr><td colspan="4"/>
        <td colspan="3"/></tr><td colspan="4"/>
    <td colspan="3"/>
    <tr><td colspan="7" align="right"><font size="1" face="Arial">umsatzsteuerbefreit (nach &sect; 19 UstG) </font></td></tr><tr><td colspan="4">
        </td>
        <td colspan="3">
            <hr size="1"/></td></tr><td colspan="7" align="left">
        <table border="0" cellspacing="0" cellpadding="2"><tbody><tr><td colspan="7" width="100%" nowrap="nowrap"><br/><br/>
                    <TABLE cellPadding=0 width=650>
                        <TR>
                            <TD colspan="2"><FONT size=2 face=Arial>Bitte &uuml;berweisen Sie den Gesamtbetrag umgehend auf folgendes Konto:</FONT><br></TD>
                        </TR>
                        <TR>
                            <TD><FONT size=2 face=Arial>Beg&uuml;nstigter:</FONT></TD>
                            <TD style="padding-left: 10px;"><FONT size=2 face=Arial>Go2Studienkolleg GbR</FONT></TD>
                        </TR>
                        <TR>
                            <TD><FONT size=2 face=Arial>IBAN:</FONT></TD>
                            <TD style="padding-left: 10px;"><FONT size=2 face=Arial>DE18 7004 0041 0665 2994 00</FONT></TD>
                        </TR>
                        <TR>
                            <TD><FONT size=2 face=Arial>BIC:</TD>
                            <TD style="padding-left: 10px;"><FONT size=2 face=Arial>COBADEFF</FONT></TD>
                        </TR>
                        <TR>
                            <TD><FONT size=2 face=Arial>Verwendungszweck 1:</FONT></TD>
                            <TD style="padding-left: 10px;"><FONT size=2 face=Arial>[booking_id]</TD>
                        </TR>
                        <TR>
                            <TD><FONT size=2 face=Arial>Verwendungszweck 2:</TD>
                            <TD style="padding-left: 10px;"><FONT size=2 face=Arial>[prename] [surname]</FONT></TD>
                        </TR>
                    </TABLE>
                </td>
            </tr><tr/></tbody></table></td></tbody></table><table cellpadding="0" width="650"><tbody><tr><td colspan="7">
            <p/>
            <p><font size="2" face="Arial">Mit freundlichen Gr&uuml;&szlig;en, <br/><span style="LINE-HEIGHT: 115%; FONT-FAMILY: 'Arial','sans-serif'; FONT-SIZE: 10pt; mso-fareast-font-family: 'Times New Roman'; mso-fareast-language: DE; mso-ansi-language: DE; mso-bidi-language: AR-SA">Marco Armbruster</span></font></p><font face="Arial"><small><font size="1"><strong>
                        </strong></font></small></font></td>
    </tr><tr><td><hr size="1"/></td>
    </tr></tbody></table><table cellpadding="0" width="650"><tbody><tr><td colspan="2" valign="top" align="left" width="20"></td>
        <td valign="top" align="left"><font size="1" face="Arial">Go2Studienkolleg GbR<br/>Theo-Prosel-Weg 16<br/>80797 M&uuml;nchen</font></td>
        <td valign="top" align="left"><font size="1" face="Arial">Tel.: +49 89 / 2155 0525<br/>info@go2studienkolleg.de<br/>www.Go2Studienkolleg.de</font></td>
        <td valign="top" align="left"><font size="1" face="Arial">Bankverbindung:<br/>Go2Studienkolleg GbR<br/>IBAN: DE18 7004 0041 0665 2994 00<br/>BIC: COBADEFFXXX<br/>Commerzbank M&uuml;nchen</font></td>
        <td colspan="2" valign="top" align="left"><font size="1" face="Arial">Steuer-Nr.:<br/>144/232/21284<br/>Gesch&auml;ftsf&uuml;hrer:<br/>Marco Armbruster<br/>Simon Holzmann<br/></font></td>
    </tr></tbody></table></body>
</html>
