<html>
<body>
Sehr [sex] [surname],<br/><br/>

wie besprochen schicke ich Ihnen die Unterlagen und Zugangsdaten zum Angebot von Go2Studienkolleg.de.<br/><br/>

Anbei finden Sie eine Beschreibung unserer Leistungen und einen kurzen Ausblick auf die weitere Entwicklung unserer Software.<br/><br/>

Wir bieten Ihnen die Möglichkeit unsere Online-Software 14 Tage lang kostenlos zu testen.<br/>
Username: [username]<br/>
Passwort: demo<br/>
Direktlink der Lernumgebung:  http://campus.go2studienkolleg.de <br/>
Selbstverständlich können Sie die Zugangsdaten auch an Ihre Lehrer und Schüler weitergeben.<br/><br/>

Am Beispiel unserer Referenzsprachschule "Axioma" sehen Sie die Integration unserer Software in das Angebot unserer Partnersprachschulen.<br/>
Designanpassung:  http://axioma.go2studienkolleg.de <br/>
Schülerlogin: http://www.deutschkurse-in-muenchen.com/modelltest-studienkolleg<br/><br/>

Wir wünschen Ihnen viel Spaß beim ausgiebigen Testen und werden uns in ca. einer Woche wieder bei Ihnen melden.<br/><br/>

Für Fragen zu Software und Vertragsbedingungen stehen wir Ihnen gerne zur Verfügung.<br/><br/>

Mit freundlichen Grüßen,<br/>
Jonathan Berroth<br/><br/>

- - - - - - - - - - - - - - - - - - - - - - - - - - - - -<br/><br/>
Go2Studienkolleg GbR<br/>
Theo-Prosel-Weg 16<br/>
DE-80797 München<br/><br/>

Phone: 0 (+49) 89 / 2155 0525<br/><br/>

Email: info@Go2Studienkolleg.de<br/>
Internet: www.Go2Studienkolleg.de<br/><br/><br/><br/></body>
</html>
