<html>
<body>
[sex] [prename] [surname],<br/><br/>

es sind jetzt noch <b>[weeks_to_next_exam] Wochen</b> bis zur n&auml;chsten Aufnahmepr&uuml;fung am
[date_next_exam] an Deinem Studienkolleg in [stk_city]. Doch diese vergehen
erfahrungsgem&auml;&szlig; schneller als gedacht.<br/>
Um die Zeit optimal zu nutzen und Dich bereits jetzt perfekt vorzubereiten, stehen Dir von <b>Go2Studienkolleg</b>
kosteng&uuml;nstige <a href="http://www.go2studienkolleg.de/software/produktinformationen.html"><b><i>Trainings-Angebote</i></b></a> zur Verf&uuml;gung:<br/><div style="padding-left: 30px;">
    <ul><li><a href="http://www.go2studienkolleg.de/produkte.html"><b> 4-Wochen</b></a> (4 Pr&uuml;fungen; 30 Training-Einheiten; 370 Verb-&Uuml;bungen): <font color="red"><b>nur 19,99&euro;</b></font></li>
        <li><a href="http://www.go2studienkolleg.de/produkte.html"><b> 8-Wochen</b></a> (8 Pr&uuml;fungen; 60 Training-Einheiten; 370 Verb-&Uuml;bungen): <font color="red"><b>nur 35,99&euro;</b></font></li>
        <li><a href="http://www.go2studienkolleg.de/produkte.html"><b>12-Wochen</b></a> (12 Pr&uuml;fungen; 60 Training-Einheiten; 370 Verb-&Uuml;bungen): <font color="red"><b>nur 49,99&euro;</b></font></li>
        <li><a href="http://www.go2studienkolleg.de/produkte.html"><b>16-Wochen</b></a> (16 Pr&uuml;fungen; 60 Training-Einheiten; 370 Verb-&Uuml;bungen): <font color="red"><b>nur 63,99&euro;</b></font></li>
    </ul></div>

Unsere Studienkolleg-Experten haben die Pr&uuml;fungsaufgaben an Deinem Studienkolleg genau analysiert
und &auml;hnliche Aufgaben erstellt. So kannst Du mit unseren <a href="http://www.go2studienkolleg.de/software/produktinformationen.html"><b>Studienkolleg-Online-Kursen</b></a> authentische
Pr&uuml;fungssituationen simulieren und Dich optimal auf die echte Aufnahmepr&uuml;fung vorbereiten.<br/>
In den 4- und 8-Wochen-Angeboten wird jede Woche eine neue Musterpr&uuml;fung freigeschalten.<br/>
In den 12- und 16-Wochen-Angeboten werden alle Tests sofort nach der Buchung freigeschalten.<br/><br/>

Dass die Go2-Studienkolleg Online-Kurse wirklich f&uuml;r die Pr&uuml;fung helfen, zeigt unsere <a href="http://www.go2studienkolleg.de/software/zahlenundfakten.html"><b>Kundenumfrage</b></a>:<br/><br/><div style="padding-left: 30px;">
    <ul><li><b><font color="orange">81%</font></b> der Go2-Nutzer half die Online-Software <font color="orange"><b>sehr viel</b></font>.</li>
        <li><b><font color="orange">71%</font></b> der regelm&auml;&szlig;igen Go2-Nutzer haben die Aufnahmepr&uuml;fung <font color="orange"><b>bestanden</b></font>.</li>
    </ul></div>

Wir w&uuml;rden uns sehr freuen auch Dich bald bei Go2Studienkolleg begr&uuml;&szlig;en zu d&uuml;rfen und
w&uuml;nschen Dir viel Erfolg auf Deinem Weg ins Studienkolleg!<br/><br/>

Mit freundlichen Gr&uuml;&szlig;en,<br/>
Das Go2-Team<br/>
- - - - - - - - - - - - - - - - - - - - - - - - - - - - -<br/>
Go2Studienkolleg GbR<br/>
Theo-Prosel-Weg 16<br/>
DE-80797 M&uuml;nchen<br/><br/>

Phone: 0 (+49) 89 / 2155 0525<br/><br/>

Email: info@Go2Studienkolleg.de<br/><a href="http://www.go2studienkolleg.de">www.Go2Studienkolleg.de</a><br/><br/>
Um Dich aus unserem Newsletter auszutragen klicke auf folgenden Link:<br/><a href="http://www.go2studienkolleg.de/unsubscribe.php?uid=[uid]">Newsletter abbestellen</a>
<br/><br/></body>
</html>
