<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    include('database/Database.php');
    include('classes/User.php');
    include('classes/utils/DynamicFormElements.php');
    
    include('admin_list_studienkollegs.php');
    include('admin_list_language_schools.php');
    include('admin_list_bookings.php');
    include('admin_list_discount_codes.php');
    include('admin_list_survey.php');
    include('admin_list_feedback.php');
    include('admin_list_logins.php');
    include('admin_list_user_tests.php');
    include('admin_show_user_details.php');
    include('admin_show_booking.php');
    include('admin_list_users.php');
    include('admin_list_logging.php');

    Database::establishConnection();
    
    include('templates/core/tpl_header.php');
    include('templates/tpl_administration.php');
    include('templates/core/tpl_footer.php');
    
    Database::closeConnection();
    

    function validateUser(){
        $uid = $_GET['uid'];
        $query = "UPDATE go2stuko_user SET validated = NOW() WHERE uid = " . $uid;
        mysql_query($query);
    }

    function acceptTermsOfUse(){
        $bid = $_GET['bid'];
        $query = "UPDATE go2stuko_booking SET terms_of_use_accepted = 1, starting_date = NOW(), ending_date = NOW() + INTERVAL 12 WEEKS WHERE bid = " . $bid;
        mysql_query($query);
    }
    
    function resetBooking(){
        $bid = $_GET['bid'];
        
        $query = "DELETE FROM go2stuko_test_examination " .
        "WHERE " . 
        "uid = (SELECT b.uid FROM go2stuko_booking b WHERE b.bid = " . $bid . ") " .
        "AND ".
        "tid IN (SELECT ptl.tid FROM go2stuko_package_test_link ptl, go2stuko_booking b WHERE ptl.tpid = b.tpid AND b.bid = " . $bid . ")";
        echo $query;
        mysql_query($query);
    }
    
?>
