<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function printUserSurvey() {
    $query = "SELECT * FROM go2stuko_survey s, go2stuko_user u WHERE s.uid = u.uid ORDER BY inserted DESC";
    $survey_datas = Database::getDatasetFromQuery($query);
    $out = '<table>';
    $out .= '<tr>';
    $out .= '<td align="left" valign="top">';
    $out .= '<b>User</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Text</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Datum</b>';
    $out .= '</td>';
    $out .= '</tr>';
    foreach ($survey_datas as $survey_data) {
        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= $survey_data->forename . " " . $survey_data->surname;
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
        $out .= $survey_data->user_text;
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
        $out .= $survey_data->inserted;
        $out .= '</td>';
        $out .= '</tr>';
    }
    $out .= '</table>';
    $out .= '<br>';
    $out .= '<a href="administration.php">Zur&uuml;ck</a>';
    return $out;
}

?>