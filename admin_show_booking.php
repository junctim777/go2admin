<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//error_reporting(E_ALL);
//ini_set("display_errors",1);

include('send_global_mail.php');


function printBooking($bid) {

    //require_once('/send_global_mail.php');

    if (isset($_GET['acceptTermsOfUse'])) {
        acceptTermsOfUse();
    }
    if (isset($_GET['resetBooking'])) {
        resetBooking();
    }
    $query = "SELECT * FROM go2stuko_booking b, go2stuko_user u, go2stuko_student_information si, go2stuko_studienkolleg sk, go2stuko_address a, go2stuko_offer o, go2stuko_payer_data pd WHERE b.uid = u.uid AND u.uid = si.uid AND si.skid = sk.skid AND a.aid = b.paid AND b.pdid = pd.pdid AND b.oid = o.oid AND b.bid = " . $bid;
    $booking_data_tmp = Database::getDatasetFromQuery($query);
    $booking_data_tmp = $booking_data_tmp[0];
    $out = '<table>';
    $out .= '<tr>';
    $out .= '<td align="left" valign="top">';
    $out .= '<b>BID</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
    $out .= $booking_data_tmp->bid;
    $out .= '</td>';
    $out .= '</tr>';
    $out .= '<tr>';
    $out .= '<td align="left" valign="top">';
    $out .= '<b>Angebot</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
    $out .= $booking_data_tmp->offer_name . "<br>"
            . $booking_data_tmp->offer_description . "<br>" . $booking_data_tmp->redemption_notice;
    $out .= '</td>';
    $out .= '</tr>';
    $out .= '<tr>';
    $out .= '<td align="left" valign="top">';
    $out .= '<b>Paket</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
    $out .= '<form action="' . $_SERVER['PHP_SELF'] . '?showUserBookings=true&changePackageOfBooking=true&bid=' . $booking_data_tmp->bid . '" method="post" name="choose_tpid"><table><tr><td>' . DynamicFormElements::getPackageTypes(
                    'tpid', $booking_data_tmp->tpid, ($errors['tpid'] != null ? "register_error" : "")) . ' <br clear="all"><input class="" type="submit" name="submit" value="OK"/></td></tr></table></form>';
    $out .= "<br /><a href=\"" . $_SERVER['PHP_SELF'] . "?resetBooking=true&bid=" . $booking_data_tmp->bid . " \">Bearbeitung zur&uuml;cksetzen</a>";
    $out .= '</td>';
    $out .= '</tr>';
    if ($booking_data_tmp->payment_method != "automatic_debit_transfer") {
        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= '<b>Rechnungsperson</b>';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
        $out .= $booking_data_tmp->sex . " "
                . $booking_data_tmp->forename . " " . $booking_data_tmp->surname . "<br>"
                . $booking_data_tmp->email_address;
        $out .= '</td>';
        $out .= '</tr>';
        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= '<b>Rechnungsaddresse</b>';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
        $out .= $booking_data_tmp->road . " "
                . $booking_data_tmp->house_number . "<br>" . $booking_data_tmp->postal_code . "<br>"
                . $booking_data_tmp->city;
        $out .= '</td>';
        $out .= '</tr>';
        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= '<b>Zahlungsart</b>';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
        $out .= $booking_data_tmp->payment_method . "<br>";
        $out .= '</td>';
        $out .= '</tr>';
    } else {
        $query = "SELECT *, AES_DECRYPT(ad.account_number, 'Achtung!!!DasIstEineUnsichereVerschluesselungsMethode7') AS bank_code_decrypted, AES_DECRYPT(ad.iban, 'Achtung!!!DasIstEineUnsichereVerschluesselungsMethode7') AS iban_decrypted FROM go2stuko_account_data ad WHERE ad.bid = " . $bid;
        $account_data_tmp = Database::getDatasetFromQuery($query);
        $account_data_tmp = $account_data_tmp[0];
        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= '<b>Zahlungsart</b>';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
        $out .= $booking_data_tmp->payment_method . "<br>";
        $out .= '</td>';
        $out .= '</tr>';
        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= '<b>Zahlungspflichtiger</b>';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
        $out .= $booking_data_tmp->forename . " " . $booking_data_tmp->surname . " (" . $booking_data_tmp->sex . ")<br>";
        $out .= '</td>';
        $out .= '</tr>';
        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= '<b>IBAN:</b>';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
        $out .= $account_data_tmp->iban_decrypted . "<br>";
        $out .= '</td>';
        $out .= '</tr>';
        $out .= '<tr>';
        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= '<b>BIC:</b>';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
        $out .= $account_data_tmp->bic . "<br>";
        $out .= '</td>';
        $out .= '</tr>';
        $out .= '<td align="left" valign="top">';
        $out .= '<b>Konto-Nr:</b>';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
        $out .= $account_data_tmp->bank_code_decrypted . "<br>";
        $out .= '</td>';
        $out .= '</tr>';
        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= '<b>BLZ:</b>';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
        $out .= $account_data_tmp->bank_code . "<br>";
        $out .= '</td>';
        $out .= '</tr>';
        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= '<b>Betrag:</b>';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
        $out .= str_replace("&euro;", "", str_replace("€", "", $booking_data_tmp->total_sum)) . "<br>";
        $out .= '</td>';
        $out .= '</tr>';
        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= '<b>Verwendungszweck 1-1:</b>';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
        if (strpos($booking_data_tmp->offer_name, "+") !== false) {
            $test_cnt_tmp = explode(" ", $booking_data_tmp->offer_name);
            if ($test_cnt_tmp[1] == 1) {
                $out .= "+" . $test_cnt_tmp[1] . " GO2-MUSTER-TEST<br>";
            } else {
                $out .= "+" . $test_cnt_tmp[1] . " GO2-MUSTER-TESTS<br>";
            }
        } else {
            $out .= $booking_data_tmp->number_of_weeks . " WO. GO2-ONLINE-TRAINING<br>";
        }
        $out .= '</td>';
        $out .= '</tr>';
        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= '<b>Verwendungszweck 1-2:</b>';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
        $stk_tmp = $booking_data_tmp->studienkolleg_name;
        $stk_tmp = str_replace("&uuml;", "UE", $stk_tmp);
        $stk_tmp = str_replace("&Uuml;", "UE", $stk_tmp);
        $stk_tmp = str_replace("&ouml;", "OE", $stk_tmp);
        $stk_tmp = str_replace("&Ouml;", "OE", $stk_tmp);
        $stk_tmp = str_replace("&auml;", "AE", $stk_tmp);
        $stk_tmp = str_replace("&Auml;", "AE", $stk_tmp);
        $stk_tmp = str_replace("&szlig;", "SS", $stk_tmp);
        $out .= strtoupper($stk_tmp) . "<br>";
        $out .= '</td>';
        $out .= '</tr>';
        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= '<b>Verwendungszweck 2-1:</b>';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
        $out .= "RECHNUNGSNO. " . (10000 + $booking_data_tmp->bid) . "<br>";
        $out .= '</td>';
        $out .= '</tr>';

        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= '<b>SEPA Verwendungszweck:</b>';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
        if (strpos($booking_data_tmp->offer_name, "+") !== false) {
            $test_cnt_tmp = explode(" ", $booking_data_tmp->offer_name);
            if ($test_cnt_tmp[1] == 1) {
                $out .= "+" . $test_cnt_tmp[1] . " GO2-MUSTER-TEST, ";
            } else {
                $out .= "+" . $test_cnt_tmp[1] . " GO2-MUSTER-TESTS, ";
            }
        } else {
            $out .= $booking_data_tmp->number_of_weeks . " WO. GO2-ONLINE-TRAINING, ";
        }
        $out .= strtoupper($stk_tmp). ", RECHNUNGSNO. " . (10000 + $booking_data_tmp->bid) . "<br>";
        $out .= '</td>';
        $out .= '</tr>';

        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= '<b>Lastschriftmandat:</b>';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
        $out .= 'GO2' . ($booking_data_tmp->bid + 10000) . ($booking_data_tmp->uid + 45234);
        $out .= '</td>';
        $out .= '</tr>';

        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= '<b>GläubigerID:</b>';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
        $out .= 'DE36ZZZ00001222006<br>';
        $out .= '</td>';
        $out .= '</tr>';

        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= '<b>Rechnungsaddresse</b>';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
        $out .= $booking_data_tmp->road . " "
                . $booking_data_tmp->house_number . "<br>" . $booking_data_tmp->postal_code . "<br>"
                . $booking_data_tmp->city . "<br>" . $booking_data_tmp->email_address;
        $out .= '</td>';
        $out .= '</tr>';
    }
    $out .= '<tr>';
    $out .= '<td align="left" valign="top">';
    $out .= '<b>Nutzungsbedingungen</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
    $out .= ($booking_data_tmp->terms_of_use_accepted == 1 ? "akzeptiert" : "<u>nicht</u> akzeptiert (Buchung nicht abgeschlossen!)");

    if ($booking_data_tmp->terms_of_use_accepted != 1) {
        $out .= "<br /><a href=\"" . $_SERVER['PHP_SELF'] . "?acceptTermsOfUse=true&bid=" . $booking_data_tmp->bid . " \">manuell akzeptieren</a>";
    }

    $out .= '</td>';
    $out .= '</tr>';
    $out .= '<tr>';
    $out .= '<td align="left" valign="top">';
    $out .= '<b>Account aktiv?</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
    $out .= ($booking_data_tmp->validated == 'NULL' ? "noch nicht" : "ja");
    $out .= '</td>';
    $out .= '</tr>';

    $out .= '<tr>';
    $out .= '<td align="left" valign="top">';
    $out .= '<b>Erinnerungs-Mail:</b>';
    $out .= '</td>';

    $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';


    if($booking_data_tmp->first_pay_reminder_mail != null){
        $out .= "1. Mahnung versendet am: <b>" . $booking_data_tmp->first_pay_reminder_mail . "</b><br /><br />";
    }

    if ($booking_data_tmp->second_pay_reminder_mail != null){
        $out .= "2. Mahnung versendet am: <b>" . $booking_data_tmp->second_pay_reminder_mail . "</b><br /><br />";
    }

    if ($booking_data_tmp->third_pay_reminder_mail != null){
        $out .= "3. Mahnung versendet am: <b>" . $booking_data_tmp->third_pay_reminder_mail . "</b><br /><br />";
    }

    if (!isset($_GET['sendPayReminder'])){

        //if($booking_data_tmp->third_pay_reminder_mail == null) {
            $out .= '<form action="' . $_SERVER['PHP_SELF'] . '?sendPayReminder=true&bid=' . $booking_data_tmp->bid . '" method="post" name="send_pay_reminder">';
            $out .= 'Kommentar: <br /><textarea name="reminderMessage" cols="50" rows="5"></textarea><br /><br />';
            $out .= 'Mahnung mit Gebühr Senden: <br /><input type="text" name="reminderCharge" value="5" style="margin: 5px 3px 5px 0px;  width:25px; text-align: right; " />&euro;';
            $out .= '<input name="payChargeReminder" type="submit" value="Mahnung senden" style="padding: 3px; margin: 5px 20px;"  /><br /><br />';
            $out .= 'Zahlungserinnerung ohne Gebühr Senden: <br /><input name="payReminder" type="submit" value="Zahlungsaufforderung senden" style="padding: 3px; margin: 5px 0px;"   /><br />';
            $out .= '</form>';
        //}
    }
    if (isset($_GET['sendPayReminder'])){
    //} elseif ($booking_data_tmp->third == null) {


        $user_mail_data = $booking_data_tmp;

        $mail_subject = 'Zahlungserinnerung';
        $mail_body = '';

        if(!isset($_POST['payChargeReminder'])){
            $mail_body_tmp = file("src/tpl_emails/pay_reminder_mail.php");
        }else{
            $mail_body_tmp = file("src/tpl_emails/pay_reminder_charge_mail.php");
        }

        foreach ($mail_body_tmp as $mail_body_line) {
            $mail_body .= $mail_body_line;
        }

        $total_sum = $booking_data_tmp->total_sum;
        $total_sum = str_replace("&euro;", "", $total_sum);
        $total_sum = str_replace("€", "", $total_sum);
        $total_sum = str_replace(",", ".", $total_sum);

        if($booking_data_tmp->sex == 'Herr'){
            $mail_body = str_replace('[sex]', 'geehrter ' . $booking_data_tmp->sex, $mail_body);
        }else{
            $mail_body = str_replace('[sex]', 'geehrte ' . $booking_data_tmp->sex, $mail_body);
        }


        if(isset($_POST['payChargeReminder'])){
            $reminderCharge = $_POST['reminderCharge'];
            $mail_body = str_replace('[reminder_charge]', number_format($reminderCharge,2,',','.'), $mail_body);
        }

        $reminderMessage = $_POST['reminderMessage'];
        $reminderMessage = str_replace('ö', '&ouml;', $reminderMessage);
        $reminderMessage = str_replace('ä', '&auml;', $reminderMessage);
        $reminderMessage = str_replace('ü', '&uuml;', $reminderMessage);

        $reminderMessage = str_replace('Ö', '&Ouml;', $reminderMessage);
        $reminderMessage = str_replace('Ä', '&Auml;', $reminderMessage);
        $reminderMessage = str_replace('Ü', '&Uuml;', $reminderMessage);

        $reminderMessage = str_replace('ß', '&szlig;', $reminderMessage);


        $mail_body = str_replace('[reminderMessage]', $reminderMessage, $mail_body);

        $mail_body = str_replace('[prename]', $booking_data_tmp->forename, $mail_body);
        $mail_body = str_replace('[surname]', $booking_data_tmp->surname, $mail_body);
        $mail_body = str_replace('[payer_street]', $booking_data_tmp->road, $mail_body);
        $mail_body = str_replace('[payer_house_number]', $booking_data_tmp->house_number, $mail_body);
        $mail_body = str_replace('[payer_plz]', $booking_data_tmp->postal_code, $mail_body);
        $mail_body = str_replace('[payer_city]', $booking_data_tmp->city, $mail_body);
        $mail_body = str_replace('[booking_id]', ($booking_data_tmp->bid + 10000), $mail_body);
        $mail_body = str_replace('[user_id]', ($booking_data_tmp->uid + 45234), $mail_body);
        $mail_body = str_replace('[date]', date('d.m.Y'), $mail_body);
        $mail_body = str_replace('[package_name]', $booking_data_tmp->offer_name, $mail_body);
        $mail_body = str_replace('[package_duration]', ($booking_data_tmp->number_of_weeks), $mail_body);
        $mail_body = str_replace('[price_netto]', round((0.81 * $total_sum), 2), $mail_body);
        $mail_body = str_replace('[price_brutto]', number_format($total_sum,2,',','.'), $mail_body);
        $mail_body = str_replace('[rest_netto]', round((0.19 * $total_sum),2), $mail_body);
        $mail_body = str_replace('[price_total]', number_format(($total_sum + $reminderCharge),2,',','.'), $mail_body);

        if(isset($_POST['payReminder'])){
            echo '<b>Zahlungsaufforderung wurde gesendet</b><br /><br />';
            $out .= '<b>Zahlungsaufforderung wurde gesendet</b><br /><br />';
        }
        if(isset($_POST['payChargeReminder'])){
            echo '<b>Mahnung wurde gesendet</b><br /><br />';
            $out .= '<b>Mahnung wurde gesendet</b><br /><br />';
        }

        if($booking_data_tmp->first_pay_reminder_mail == null){
            $query = "UPDATE go2stuko_booking SET first_pay_reminder_mail = NOW() WHERE bid =" . $booking_data_tmp->bid ;
        }

        if ($booking_data_tmp->first_pay_reminder_mail != null && $booking_data_tmp->second_pay_reminder_mail == null){
            $query = "UPDATE go2stuko_booking SET second_pay_reminder_mail = NOW() WHERE bid =" . $booking_data_tmp->bid ;
        }

        if ($booking_data_tmp->first_pay_reminder_mail != null && $booking_data_tmp->second_pay_reminder_mail != null && $booking_data_tmp->third_pay_reminder_mail == null){
            $query = "UPDATE go2stuko_booking SET third_pay_reminder_mail = NOW() WHERE bid =" . $booking_data_tmp->bid ;
        }

        $success = mysql_query($query);

        if($success) {
            globalMailSend($mail_subject, $mail_body, $user_mail_data);
        }

    }

    //} else {
        //$out .= '<b>3. Mahnung wurde bereits gesendet!</b>';
    //}

    $out .= '</td>';
    $out .= '</tr>';
    $out .= '<tr>';

    $out .= '<tr>';
    $out .= '<td align="left" valign="top">';
    $out .= '<a href="administration.php">Zur&uuml;ck</a>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
    $out .= '</td>';
    $out .= '</tr>';
    $out .= '</table>';
    return $out;
}

?>