<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function printUserBookings() {

    if (isset($_GET['changePackageOfBooking']) && $_GET['changePackageOfBooking'] == true) {
        $bid = $_GET['bid'];
        $tpid = $_POST['tpid'];
        $query = "UPDATE go2stuko_booking SET tpid = " . $tpid . " WHERE bid = " . $bid;
        $success = mysql_query($query);
    }

    if (isset($_GET['setPayed']) && $_GET['setPayed'] == 'true') {
        $bid = $_GET[bid];
        $query = "UPDATE go2stuko_booking set cash_receipt_date = NOW() WHERE bid = " . $bid;
        $success = mysql_query($query);
        $query = "SELECT * FROM go2stuko_booking b WHERE bid = " . $bid;
        $booking_info = Database::getDatasetFromQuery($query);
        $booking_info = $booking_info[0];
        $query = "SELECT * FROM go2stuko_offer WHERE oid = " . $booking_info->oid;
        $offer_data_tmp = Database::getDatasetFromQuery($query);
        $offer_data = $offer_data_tmp[0];
        if ($booking_info->payment_method == "prepayment") {
            $query = "UPDATE go2stuko_booking set starting_date = NOW(), ending_date = DATE_ADD(NOW(), INTERVAL " . $offer_data->number_of_weeks . " WEEK) WHERE bid = " . $bid;
            $success = mysql_query($query);
            if ($success) {
                // SEND EMAIL
            }
        }
        header("Location: administration.php?showUserBookings=true");
    }
    
    if (isset($_GET['setPayed']) && $_GET['setPayed'] == 'false') {
        $bid = $_GET[bid];
        $query = "UPDATE go2stuko_booking set cash_receipt_date = NULL WHERE bid = " . $bid;
        $success = mysql_query($query);
        $query = "SELECT * FROM go2stuko_booking b WHERE bid = " . $bid;
        $booking_info = Database::getDatasetFromQuery($query);
        $booking_info = $booking_info[0];
        $query = "SELECT * FROM go2stuko_offer WHERE oid = " . $booking_info->oid;
        $offer_data_tmp = Database::getDatasetFromQuery($query);
        $offer_data = $offer_data_tmp[0];
        if ($booking_info->payment_method == "prepayment") {
            $query = "UPDATE go2stuko_booking set starting_date = NULL, ending_date = NULL WHERE bid = " . $bid;
            $success = mysql_query($query);
            if ($success) {
                // SEND EMAIL
            }
        }
        header("Location: administration.php?showUserBookings=true");
    }
    
    if (isset($_GET['deleteBooking']) && $_GET['deleteBooking'] == true) {
        $bid = $_GET[bid];
        $query = "DELETE FROM go2stuko_booking WHERE bid = " . $bid;
        $success = mysql_query($query);
        header("Location: administration.php?showUserBookings=true");
    }
    if (isset($_GET['setAsDemo']) && $_GET['setAsDemo'] == true) {
        $bid = $_GET[bid];
        $query = "UPDATE go2stuko_booking set is_demo = 1 WHERE bid = " . $bid;
        $success = mysql_query($query);
        header("Location: administration.php?showUserBookings=true");
    }
    if (isset($_GET['setAsNoDemo']) && $_GET['setAsNoDemo'] == true) {
        $bid = $_GET[bid];
        $query = "UPDATE go2stuko_booking set is_demo = 0 WHERE bid = " . $bid;
        $success = mysql_query($query);
        header("Location: administration.php?showUserBookings=true");
    }
    if (isset($_GET['activateAllTests']) && $_GET['activateAllTests'] == true) {
        $bid = $_GET[bid];
        $query = "UPDATE go2stuko_booking set all_tests_instantly = 1 WHERE bid = " . $bid;
        $success = mysql_query($query);
        header("Location: administration.php?showUserBookings=true");
    }
    if (isset($_GET['deactivateAllTests']) && $_GET['deactivateAllTests'] == true) {
        $bid = $_GET[bid];
        $query = "UPDATE go2stuko_booking set all_tests_instantly = 0 WHERE bid = " . $bid;
        $success = mysql_query($query);
        header("Location: administration.php?showUserBookings=true");
    }
    $query = "SELECT *, si.skid AS si_skid, tp.skid AS tp_skid, b.all_tests_instantly AS all_tests FROM go2stuko_booking b, go2stuko_user u, go2stuko_offer o, go2stuko_test_package tp, go2stuko_student_information si, go2stuko_studienkolleg sk WHERE u.uid = si.uid AND si.skid = sk.skid AND b.tpid = tp.tpid AND b.oid = o.oid AND b.uid = u.uid ORDER BY booking_date DESC";
    $bookings = Database::getDatasetFromQuery($query);
    $query = "SELECT * FROM go2stuko_booking WHERE booking_date < NOW() AND ending_date > NOW()";
    $active_bookings = Database::getDatasetFromQuery($query);
    $userCount = 1;
    $out = '<table>';
    $out .= '<tr>';
    $out .= '<td align="left" valign="top">';
    $out .= '<b>Nr</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px; width: 250px; height: 30px;">';
    $out .= '<b>User</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Buchung/Zahlung/<br>Start/Ende</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Paket</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>STK</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Preis</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Aktiv</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Tests instantan</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Demo</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Ref-ID</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '';
    $out .= '</td>';
    $out .= '</tr>';
    $total_sum = 0;
    $total_sum_real = 0;
    foreach ($bookings as $user_booking) {
        $price = $user_booking->total_sum;
        $price = str_replace("€", "", $price);
        $price = str_replace(",", ".", $price);
        $activeBooking = 'Nein';
        $bookingDate = new DateTime($user_booking->booking_date);
        $startingDate = new DateTime($user_booking->starting_date);
        $endingDate = new DateTime($user_booking->ending_date);
        $tr_font_size = "";
        $tr_color = "";
        $td_color = "";
        $td_color_stk = "";
        $is_demo = '<a href="' . $_SERVER['PHP_SELF'] . "?showUserBookings=true&bid=" . $user_booking->bid . '&setAsDemo=true">NEIN</a>';
        $all_tests = '<a href="' . $_SERVER['PHP_SELF'] . "?showUserBookings=true&bid=" . $user_booking->bid . '&activateAllTests=true">NEIN</a>';

        foreach ($active_bookings as $active_booking) {
            if ($active_booking->uid == $user_booking->uid) {
                $activeBooking = 'JA';
                $tr_color = "green";
            }
        }
        if ($user_booking->is_demo) {
            $tr_font_size = "10px";
            $is_demo = '<a href="' . $_SERVER['PHP_SELF'] . "?showUserBookings=true&bid=" . $user_booking->bid . '&setAsNoDemo=true">JA</a>';
        }
        if ($user_booking->all_tests) {
            $all_tests = '<a href="' . $_SERVER['PHP_SELF'] . "?showUserBookings=true&bid=" . $user_booking->bid . '&deactivateAllTests=true">JA</a>';
        }
        if ($user_booking->terms_of_use_accepted && $user_booking->is_demo == 0) {
            $total_sum += $price;
        }
        if ($user_booking->si_skid != $user_booking->tp_skid) {
            $td_color_stk = ' style="background-color:orange;" ';
        }
        if ($user_booking->cash_receipt_date == null) {
            if ($user_booking->terms_of_use_accepted) {
                $paying_hint = 'ausstehend';
                $td_color = ' style="background-color:red;" ';
            } else {
                $paying_hint = 'ung&uuml;ltig.';
            }
        } else {
            $receiptDate = new DateTime($user_booking->cash_receipt_date);
            if ($user_booking->is_demo == 0) {
                $total_sum_real += $price;
            }
        }
        $out .= '<tr style="background-color: ' . $tr_color . ';font-size: ' . $tr_font_size . ';">';
        $out .= '<td align="left" valign="">';
        $out .= $userCount++;
        $out .= '</td>';
        $out .= '<td align="left" valign="" style="padding-left: 20px; height: 30px;">';
        $out .= "<a href=\"" . $_SERVER['PHP_SELF'] . "?bid=" . $user_booking->bid . "\">" . $user_booking->forename . ' ' . $user_booking->surname . "</a>";
        $out .= '</td>';
        $out .= '<td ' . $td_color . ' align="left" valign="" style="padding-left: 20px;">';
        $out .= $bookingDate->format("d.m.y") . "/";
        if ($user_booking->cash_receipt_date == null) {
            $out .= $paying_hint;
        } else {
            $out .= '<a href="administration.php?showUserBookings=true&bid=' . $user_booking->bid . '&setPayed=false">' . $receiptDate->format("d.m.y") . '</a>';
        }
        $out .= '<br>';
        if ($user_booking->cash_receipt_date == null) {
            if ($user_booking->terms_of_use_accepted) {
                $out .= '<a href="administration.php?showUserBookings=true&bid=' . $user_booking->bid . '&setPayed=true">verzeichnen</a>';
            } else {
                $out .= 'Nutzungsbedingungen!';
            }
            $out .= '<br>';
        }
        $out .= $startingDate->format("d.m.y") . "/"
                . $endingDate->format("d.m.y") . "<br>";

        $out .= '</td>';
        $out .= '<td ' . $td_color_stk . ' align="left" valign="" style="padding-left: 20px;">';
        $package_first_word = split(" ", $user_booking->description);
        $out .= $user_booking->tpid . ' (' . $package_first_word[0] . ')';
        $out .= '</td>';
        $out .= '<td ' . $td_color_stk . ' align="left" valign="" style="padding-left: 20px;">';
        $out .= $user_booking->studienkolleg_name;
        $out .= '</td>';
        $out .= '<td align="left" valign="" style="padding-left: 20px;">';
        $out .= $user_booking->total_sum;
        $out .= '</td>';
        $out .= '<td align="left" valign="" style="padding-left: 20px;">';
        $out .= $activeBooking;
        $out .= '</td>';
        $out .= '<td align="left" valign="" style="padding-left: 20px;">';
        $out .= $all_tests;

        $out .= '</td>';
        $out .= '<td align="left" valign="" style="padding-left: 20px;">';
        $out .= $is_demo;
        $out .= '</td>';
        $out .= '<td align="left" valign="" style="padding-left: 20px;">';
        $out .= $user_booking->ref_id;
        $out .= '</td>';
        $out .= '<td align="left" valign="" style="padding-left: 20px;">';
        $out .= '<a href="' . $_SERVER['PHP_SELF'] . "?showUserBookings=true&bid=" . $user_booking->bid . '&deleteBooking=true"><img src="/src/imgs/delete.gif"></a>';
        $out .= '</td>';
        $out .= '</tr>';
    }
    $out .= '<tr><td align="right" colspan="6" style="padding-top:30px;"><b>Summe - SOLL:</b></td><td colspan="2" style="padding-left: 20px; padding-top:30px;">' . $total_sum . '€ (' . round((0.81 * $total_sum), 2) . ' €)</td><td colspan="3"></td></tr>';
    $out .= '<tr><td align="right" colspan="6" style="padding-top:10px;"><b>Summe - IST:</b></td><td colspan="2" style="padding-left: 20px; padding-top:10px;">' . $total_sum_real . '€ (' . round((0.81 * $total_sum_real), 2) . ' €)</td><td colspan="3"></td></tr>';
    $out .= '</table>';
    $out .= '<br>';
    $out .= '<a href="administration.php">Zur&uuml;ck</a>';
    return $out;
}

?>