<?php

include("./database/Database.php");
include("./classes/Exercise.php");
include("./classes/utils/Helper.php");

$extype = $_GET['extype'];
$action = $_GET['action'];
$eid = $_GET['eid'];
$qid = $_POST['qid'];
$ssid = $_POST['ssid'];
$eeid = $_GET['eeid'];
$word = $_GET['word'];
$wordno = $_GET['wordno'];
$charno = $_GET['charno'];
$action = $_GET['action'];
$new_slot = $_GET['new_slot'];
$update_slot = $_GET['update_slot'];
$exmodus = $_GET['exmodus'];
$newSolution = $_POST['new_solution'];
$has_content_point = $_POST['has_content_point'];
 

if (empty($eid) && $exmodus != "test" && $exmodus != "training") {
    $errors[modus] = "Es ist kein korrekter &Uuml;bungsmodus gesetzt (Training oder Test)";
}
if (empty($eid) && $extype != "Ctest") {
    $errors[type] = "Es ist kein korrekter &Uuml;bungstyp gesetzt (Ctest)";
}

Database::establishConnection();

if(!empty($action)){
    if($action == "deleteQuestion"){
        if(!empty($qid)){
            $query = "DELETE FROM go2stuko_question WHERE qid = " . $qid;
            
        }
    }
    if($action == "deleteExerciseElement"){
        if(!empty($eeid)){
            $query = "DELETE FROM go2stuko_exercise_element WHERE eeid = " . $eeid;
            mysql_query($query);
            header("Location: " . $_SERVER['PHP_SELF'] . "?eid=" . $eid);
        }
    }
}

if (count($errors) == 0) {
    if (!empty($action) && isset($wordno) && !empty($eeid)) {
        if ($action == 'addAsQuestion') {
            if (!empty($word)) {
                include('./templates/content/add_exercise/ctest/dynamic_addWordAsQuestion.php');
            }
        }
        if ($action == 'deleteAsQuestion') {
            include('./templates/content/add_exercise/ctest/dynamic_deleteWordAsQuestion.php');
        }
    }
}

if (isset($_POST['submit_base_data']) || isset($_POST['update_base_data'])) {
    $heading = addslashes(strip_tags(trim($_POST['heading'])));
    $introduction_text = addslashes(nl2br(trim($_POST['introduction_text'])));
    if (empty($heading)) {
        $errors[heading] = "Die Ueberschrift muss korrekt eingegeben werden!";
    }
    if (empty($introduction_text)) {
        $errors[introduction_text] = "Der Aufgabentext muss korrekt eingegeben werden!";
    }
}

if (isset($_POST['submit_text_data']) || isset($_POST['update_text_data'])) {
    $lueckentext = addslashes(nl2br(strip_tags(trim($_POST['lueckentext']))));
    $lueckentext = str_replace("<br>", "", $lueckentext);
    $lueckentext = str_replace("<br />", "", $lueckentext);
    $lueckentext = preg_replace('/\s{2,}/sm', ' ', $lueckentext);
    if (empty($lueckentext)) {
        $errors[heading] = "Der Luecken-Text (Aufgaben-Satz) muss korrekt eingegeben werden!";
    }
}

if (count($errors) == 0) {
    if (isset($_POST['submit_base_data'])) {
        include('./templates/content/add_exercise/dynamic_submit_base_data.php');
    } else if (isset($_POST['update_base_data']) && isset($eid)) {
        include('./templates/content/add_exercise/dynamic_update_base_data.php');
    }
    if (isset($_POST['submit_text_data'])) {
        include('./templates/content/add_exercise/ctest/dynamic_submit_text_data.php');
    }
    if (isset($_POST['update_text_data'])) {
        include('./templates/content/add_exercise/ctest/dynamic_update_element_data.php');
    }
    if (!empty($newSolution) && !empty($qid)){
        include('./templates/content/add_exercise/ctest/dynamic_addNewSolution.php');
    }
    if (isset($has_content_point) && !empty($ssid) && !empty($qid)){
        include('./templates/content/add_exercise/ctest/dynamic_updateContentPoints.php');
    }
}

if (!empty($eid)) {
    $exercise = new Exercise($eid);
    if (!isset($_POST['update_base_data']) || $update_slot != 1) {
        $heading = $exercise->getHeading();
        $introduction_text = $exercise->getIntroductionText();
    }
} else{
    if($extype == "Ctest"){
        $introduction_text = '<b>Setzen Sie bitte in jede Lücke <u>eine</u> Silbe ein, die grammatikalisch und inhaltlich passt!</b>
<i>Beachten Sie, dass es sich um einen zusammenhängenden Text handelt.</i>';
    }
}

Database::closeConnection();

if ($extype != null) {
    $extype_str = "- " . $extype;
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include('./templates/core/tpl_header.php');

include('./templates/content/add_exercise/tpl_add_exercise_header.php');
$slot_no = 1;
include('./templates/content/add_exercise/tpl_new_base_data.php');
echo "<br clear=\"all\"><h3>Inhalt</h3>";

if (!empty($eid)) {
    if (count($exercise->exerciseElements) == 0) {
        include('./templates/content/add_exercise/ctest/tpl_new_text.php');
    }
    foreach ($exercise->exerciseElements as $exerciseElement) {
        $slot_no++;
        $eeid = $exerciseElement->getEeid();
        $element_text = $exerciseElement->getElementText();
        $sentence_no = $exerciseElement->getExercisePosition();
        $words = split(" ", $element_text);
        $sentence = '';
        $cnt = 0;
        $question_cnt = 0;
        foreach ($words as $word) {
            if (isWordAQuestion($cnt, $exerciseElement->questions)) {
                $questions = $exerciseElement->questions;
                $question_text= $questions[$question_cnt++]->getQuestionText();
                $solution_length = split("-", $question_text);
                $solution_length = $solution_length[1];
                for($i=0; $i<$solution_length; $i++){
                    $sentence .= '<a href="' . $_SERVER['PHP_SELF'] . '?eid=' . $eid
                            . '&eeid=' . $eeid . '&wordno=' . $cnt . '&charno=' . $i 
                            . '&action=deleteAsQuestion#' . $eeid .'">';
                    $sentence .= $word[$i];
                }
                $sentence .= '______ ';
                $sentence .= '</a>';
            } else {
                for($i=0; $i<strlen($word); $i++){
                    $sentence .= '<a href="' . $_SERVER['PHP_SELF'] . '?eid=' . $eid
                            . '&eeid=' . $eeid . '&wordno=' . $cnt . '&charno=' . $i 
                            . '&word=' . Helper::quote2entities($word) . '&action=addAsQuestion#' . $eeid .'">';
                    $sentence .= utf8_encode($word[$i]);
                    $sentence .= '</a>';
                }
            }            
            $sentence .= ' ';
            $cnt++;
        }
        $question_list = '<ol>';
        foreach($exerciseElement->questions as $question){
            $question_list .= '<li>';
               $question_list .= '<table width="200" cellpadding="0" cellspacing="0">'; 
               $sol_cnt = 0; 
               foreach($question->sampleSolutions as $sampleSolution){
                    $solution_word = $sampleSolution->wordByWordSolutions[0];
                    $question_list .= '<tr>'; 
                    $question_list .= '<td valign="top" align="left" width="180">'; 
                    
                    $wordCnt = $question->getQuestionText();
                    $wordCnt = split("-", $wordCnt);
                    $wordCnt = $wordCnt[0];
                    
                    $question_list .= '<a href="' . $_SERVER['PHP_SELF'] . '?eid=' . $eid
                                                            . '&eeid=' . $eeid . '&wordno=' . $wordCnt  
                                                            . '&action=deleteAsQuestion#' . $eeid .'">-</a>&nbsp;&nbsp;';
                    if($sampleSolution->getIsBestAnswer()){
                        $question_list .= '' . trim($sampleSolution->getSolutionText()) . ' ';
                    } else{
                        $question_list .= '<a href="">' . trim($sampleSolution->getSolutionText()) . '</a> ';
                    } 
                    $question_list .= '</td><td valign="top" align="left" width="20" style="padding-left: 5px;">'; 
                    $question_list .= '</td></tr>'; 
                }
                $question_list .= '</form>';
            $question_list .= '</td></tr></table>';
            $question_list .= '</li>';
        }
        $question_list .= '</ol>';
        if(isset($update_slot) && $update_slot == $slot_no){
            $lueckentext = $element_text;
            include('./templates/content/add_exercise/ctest/tpl_new_text.php');
        } else{
            include('./templates/content/add_exercise/ctest/tpl_show_text.php');
        }
    }
}

include('./templates/content/add_exercise/tpl_add_exercise_footer.php');
include('./templates/core/tpl_footer.php');

function isWordAQuestion($word_index, $questions) {
    foreach ($questions as $question) {
        if ($question->getExerciseElementPosition() == $word_index)
            return true;
    }
    return false;
}

?>
