<?php

    include("./database/Database.php");
    include("./classes/Exercise.php");
    include("./classes/utils/Helper.php");
    
    $extype = $_GET['extype'];
    $eid = $_GET['eid'];
    $new_slot = $_GET['new_slot'];
    $update_slot = $_GET['update_slot'];
    $exmodus = $_GET['exmodus'];
    
    
    if(empty($eid) && $exmodus != "test" && $exmodus != "training"){
        $errors[modus] = "Es ist kein korrekter &Uuml;bungsmodus gesetzt (Training oder Test)";
    }
    if(empty($eid) && $extype != "Satzbildung" && $extype != "Passivbildung"
            && $extype != "Fragenbildung" && $extype != "Fehlerfinden"){
        $errors[type] = "Es ist kein korrekter &Uuml;bungstyp gesetzt (Satzbildung, Passivbildung, Fragenbildung oder Fehlerfinden)";
    }
    
    Database::establishConnection();
    
    if(isset($_POST['submit_base_data']) || isset($_POST['update_base_data'])){
        $heading = addslashes(strip_tags(trim($_POST['heading'])));
        $introduction_text = addslashes(nl2br(trim($_POST['introduction_text'])));
        if(empty($heading)){
            $errors[heading] = "Die Ueberschrift muss korrekt eingegeben werden!";
        }
        if(empty($introduction_text)){
            $errors[introduction_text] = "Der Aufgabentext muss korrekt eingegeben werden!";
        }
    }
    
    if(isset($_POST['submit_element_data']) || isset($_POST['update_element_data'])){
        $element_text = addslashes(nl2br(strip_tags(trim($_POST['element_text']))));
        $question_text = addslashes(nl2br(strip_tags(trim($_POST['question_text']))));
        $solution_texts = $_POST['solution_texts'];
        $answer_note = addslashes(nl2br(trim($_POST['answer_note'])));
        $difficulty = nl2br(strip_tags(trim($_POST['difficulty'])));
        $estimated_time = nl2br(strip_tags(trim($_POST['estimated_time'])));
        $content_points = nl2br(strip_tags(trim($_POST['content_points'])));
        $linguistic_points = nl2br(strip_tags(trim($_POST['linguistic_points'])));
        if(isset($_POST['update_element_data'])){
            $update_ssids = $_POST['update_ssid'];
        }
        if(empty($question_text)){
            $errors[heading] = "Der Frage-Text (Aufgaben-Satz) muss korrekt eingegeben werden!";
        }
        if(Helper::isArrayEmpty($solution_texts)){
            $errors[solution_texts] = "Es muss zumindestens eine L&ouml;sung angegeben werden!";
        }
        if(empty($answer_note)){
            $answer_note = 'NULL';
            // $errors[answer_note] = "Es muss ein L&ouml;sungshinweis angegeben werden!";
        }
        if(($content_points + $linguistic_points) == 0){
            $errors[no_points] = "Es muss zumindestens 1 Inhalts- oder Sprachpunkt f&uuml;r diese Aufgabe vergeben werden!";
        }
    }
    
    if(count($errors) == 0){
        if(isset($_POST['submit_base_data'])){
            include('./templates/content/add_exercise/dynamic_submit_base_data.php');
        }
        else if(isset($_POST['update_base_data']) && isset($eid)){
            include('./templates/content/add_exercise/dynamic_update_base_data.php');
        }
        if(isset($_POST['submit_element_data'])){
            include('./templates/content/add_exercise/satzbildung/dynamic_submit_element_data.php');
        }
        if(isset($_POST['update_element_data'])){
            include('./templates/content/add_exercise/satzbildung/dynamic_update_element_data.php');
        }
    }
    
    if(!empty($eid)){
        $exercise = new Exercise($eid);
        if(!isset($_POST['update_base_data']) || $update_slot != 1){
            $heading = $exercise->getHeading();
            $introduction_text = $exercise->getIntroductionText();
        }
    } else{
        if($extype == "Satzbildung"){
            $introduction_text = '<b>Bilden Sie aus folgenden Stichwörtern einen sinnvollen Satz! Beachten Sie die <u>Verbzeit</u>!</b>
<i>(Artikel, Präpositionen und Konjunktionen müssen Sie selbst ergänzen.)</i>';
        } else if($extype == "Passivbildung"){
            $introduction_text = '<b>Setzen Sie bitte die <u>drei</u> folgenden Sätze ins <u>Passiv</u>!</b>
<i>Achten Sie darauf, dass dabei alle Informationen erhalten bleiben und dass die Verbzeit stimmt!</i>
Beispiel: In drei Tagen werden Freunde die Studentin am Bahnhof abholen.
Lösung: In drei Tagen wird die Studentin von Freunden am Bahnhof abgeholt werden.';
        } else if($extype == "Fragenbildung"){
            $introduction_text = '<b>Beantworten Sie die Fragen mit allen Informationen, die in Klammern angegeben sind.</b>
<i>Das Verb muss jeweils im <b>Perfekt</b> stehen! Grammatisch nötige Wörter (wie Artikel, Pronomen, Präpositionen, Konjunktionen) müssen Sie selbst ergänzen.</i>';
        }
    }
    
    Database::closeConnection();
    
    if($extype != null){
        $extype_str = "- " . $extype;
    }

    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */

    include('./templates/core/tpl_header.php');
    include('./templates/content/add_exercise/tpl_add_exercise_header.php');
    
    if(empty($eid) && $extype != "Satzbildung" && $extype != "Passivbildung"
        && $extype != "Fragenbildung" && $extype != "Fehlerfinden"){
          echo "<b>Es muss ein Aufgabentyp gewählt werden!</b><br><br>";
    }else
        include('./templates/content/add_exercise/tpl_new_base_data.php'); 
    
    $slot_no = 1;
    echo "<br clear=\"all\"><h3>Inhalt</h3>";
    if(!empty($eid)){
        foreach($exercise->exerciseElements as $exerciseElement){
            echo "<br clear=\"all\">";
            $slot_no++;
            $sentence_no = $exerciseElement->getExercisePosition();
            $question = $exerciseElement->getFirstQuestion();
            unset($solution_texts);
            if(!isset($_POST['update_element_data']) || $update_slot != $slot_no){
                $element_text = $exerciseElement->getElementText();
                $question_text = $question->getQuestionText();
                $solindex = 0;
                foreach($question->sampleSolutions as $sampleSolution){
                    $solution_texts[$solindex++] = $sampleSolution->getSolutionText();
                }
                $solindex = 0;
                $answer_note = $question->getAnswerNote();
                $difficulty = $question->getDifficulty();
                $estimated_time = $question->getEstimatedTime();
                $content_points = $question->getContentPoints();
                $linguistic_points = $question->getLinguisticPoints();
            }
            if(isset($update_slot) && $update_slot == $slot_no)
                include('./templates/content/add_exercise/satzbildung/tpl_new_element.php');
            else
                include('./templates/content/add_exercise/satzbildung/tpl_show_element.php');
        }
        if(!$update_slot){
            $slot_no++;
            $sentence_no = $slot_no-1;
            if(isset($_POST['submit_element_data']) && ($new_slot == $slot_no)){
                $element_text = nl2br(strip_tags(trim($_POST['element_text'])));
                $question_text = nl2br(strip_tags(trim($_POST['question_text'])));
                $solution_texts = $_POST['solution_texts'];
                $answer_note = nl2br(strip_tags(trim($_POST['answer_note'])));
                $difficulty = nl2br(strip_tags(trim($_POST['difficulty'])));
                $estimated_time = nl2br(strip_tags(trim($_POST['estimated_time'])));
                $content_points = nl2br(strip_tags(trim($_POST['content_points'])));
                $linguistic_points = nl2br(strip_tags(trim($_POST['linguistic_points'])));
                
            } else{
                $element_text = null;
                $question_text = null;
                $solution_texts = null;
                $answer_note = null;
                $difficulty = null;
                $estimated_time = null;
                $content_points = null;
                $linguistic_points = null;
            }
            echo "<br clear=\"all\">";
            include('./templates/content/add_exercise/satzbildung/tpl_new_element.php');
        }
    }
    include('./templates/content/add_exercise/tpl_add_exercise_footer.php');
    include('./templates/core/tpl_footer.php');
    
?>
