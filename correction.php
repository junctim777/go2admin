<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    include('database/Database.php');
    include('classes/SampleSolution.php');
    
    $eid = $_GET['eid'];
    $etid = $_GET['etid'];

    if(!empty($eid) && !empty($etid)){
        redirectToExercise($eid, $etid);
    }
    
    include('templates/core/tpl_header.php');
    include('templates/tpl_correction.php');
    include('templates/core/tpl_footer.php');
    
    function printUserVetos(){
        $out = '<table>';
        $out .= '<tr>';
            $out .= '<td align="left" valign="top">';
                $out .= '<b>UID</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>SUVID</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>QID</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>&Uuml;bung</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>Unsere L&ouml;sung</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>Veto-Nachricht</b>';
            $out .= '</td>';
        $out .= '</tr>';
        Database::establishConnection();
        $query = "SELECT * FROM go2stuko_solution_user_veto uv, go2stuko_user u, go2stuko_user_contact_details ucd WHERE uv.uid = u.uid AND u.study_cdid = ucd.cdid "
                ."ORDER BY uv.created DESC";
        $user_vetos_tmp = Database::getDatasetFromQuery($query);
        foreach($user_vetos_tmp as $user_veto_tmp){
            $query = "SELECT * FROM go2stuko_sample_solution "
                ."WHERE ssid = " . $user_veto_tmp->ssid;
            $dbSampleSolution = Database::getDatasetFromQuery($query);
            $sampleSolution = new SampleSolution($dbSampleSolution[0]);
            $query = "SELECT * FROM go2stuko_sample_solution ss, go2stuko_question q, go2stuko_exercise_element ee , go2stuko_exercise e  "
                ."WHERE ss.ssid = " . $user_veto_tmp->ssid . " AND ss.qid = q.qid AND q.eeid = ee.eeid AND e.eid = ee.eid";
            $exercise = Database::getDatasetFromQuery($query);
            $exercise = $exercise[0];
            $out .= '<tr>';
                $out .= '<td align="left" valign="top">';
                    $out .= '<a href="administration.php?showUserDetails=true&uid=' . $user_veto_tmp->uid . '">' . $user_veto_tmp->uid . '</a>';
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $user_veto_tmp->suvid;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= '<a href="correction.php?eid=' . $exercise->eid . '&etid=' . $exercise->etid . '" target="_blank">' . $sampleSolution->getQid() . '</a>';
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $exercise->heading;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $sampleSolution->getSolutionText();
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= nl2br($user_veto_tmp->veto_message);
                $out .= '</td>';
            $out .= '</tr>';
        }
        Database::closeConnection();
        $out .= '</table>';
        return $out;
    }
    
    function redirectToExercise($eid, $etid){
        switch($etid){
            case(5):
                $url = 'content_add_exercise_bildbeschreibung.php';
                break;
            case(6):
                $url = 'content_add_exercise_lueckentext.php';
                break;
            case(7):
                $url = 'content_add_exercise_leseverstehen.php';
                break;
            default:
                $url = 'content_add_exercise_multi.php';
        }
        Header ('Location: ' . $url . '?eid=' . $eid);
    }
    
?>
