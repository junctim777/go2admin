<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    include('database/Database.php');
    include('classes/User.php');
    require_once './classes/utils/class.phpmailer.php';
    require_once './classes/utils/class.smtp.php';
    include('classes/utils/DynamicFormElements.php');
    
    $lsid = $_GET['lsid'];
    $mail_type = $_GET['mail_type'];
    $school_email = $_POST['school_email'];
    $email_subject = $_POST['email_subject'];
    $email_body = $_POST['email_body'];
    
    Database::establishConnection();
    
    $query = "SELECT * FROM go2stuko_language_school WHERE lsid = " . $lsid;
    $language_school = Database::getDatasetFromQuery($query);
    $language_school = $language_school[0];
    
    $query = "SELECT ucd.* FROM go2stuko_language_school ls, go2stuko_user_contact_details ucd WHERE ls.cdid = ucd.cdid AND ls.lsid = " . $lsid;
    $contact_details = Database::getDatasetFromQuery($query);
    $contact_details = $contact_details[0];
    
    $query = "SELECT u.*, ld.* FROM go2stuko_language_school ls, go2stuko_login_data ld, go2stuko_user u WHERE u.uid = ld.uid AND ls.headofschool_uid = u.uid AND ls.lsid = " . $lsid;
    $headofschool = Database::getDatasetFromQuery($query);
    $headofschool = $headofschool[0];
    
    $query = "SELECT * FROM go2stuko_language_school_comment WHERE lsid = " . $lsid . " ORDER BY insert_date DESC";
    $comments = Database::getDatasetFromQuery($query);
    
    $query = "SELECT sk.* FROM go2stuko_language_school_studienkolleg_link lsskl, go2stuko_studienkolleg sk WHERE lsskl.lsid = " . $lsid . " AND lsskl.skid = sk.skid";
    $studienkollegs = Database::getDatasetFromQuery($query);
    
    $query = "SELECT * FROM go2stuko_language_school_user_link lsul, go2stuko_role r, go2stuko_user u WHERE lsul.uid = u.uid AND lsul.rid = r.rid AND lsul.lsid = " . $lsid;
    $linkedUsers = Database::getDatasetFromQuery($query);
    
    if(empty($email_subject)){
        if($mail_type == "promotion_mail"){
            $mail_subject = "Informationen und Testzugangsdaten - Go2-Lernsoftware";
        } else if($mail_type == "price_list_mail"){
            $mail_subject = "Preisliste Sommersemester 2012 - Go2-Lernsoftware";
        } else if($mail_type == "reminder_mail"){
            $mail_subject = "Verl&auml;ngerung Ihres Testaccounts - Go2-Lernsoftware";
        }
    } else{
        $mail_subject = $email_subject;
    }
    
    if(empty($email_body)){
        if($mail_type == "promotion_mail"){
            $mail_body_tmp = file("src/tpl_emails/promotion_mail.php");
        } else if($mail_type == "price_list_mail"){
            $mail_body_tmp = file("src/tpl_emails/price_list_mail.php");
        } else if($mail_type == "reminder_mail"){
            $mail_body_tmp = file("src/tpl_emails/reminder_mail.php");
        }
        foreach ($mail_body_tmp as $mail_body_line) {
            $mail_body .= $mail_body_line;
        }
        if($headofschool->sex == "Herr"){
            $mail_body = str_replace('[sex]', "geehrter Herr", $mail_body);
        } else if($headofschool->sex == "Frau") {
            $mail_body = str_replace('[sex]', "geehrte Frau", $mail_body);
        }
        $mail_body = str_replace('[prename]', $headofschool->forename, $mail_body);
        $mail_body = str_replace('[surname]', $headofschool->surname, $mail_body);
        $mail_body = str_replace('[schoolname]', $language_school->school_name, $mail_body);
        $mail_body = str_replace('[subdomain]', $language_school->subdomain, $mail_body);
        $mail_body = str_replace('[username]', $headofschool->login_nick_name, $mail_body);
        $mail_body = str_replace('[password]', $headofschool->login_password, $mail_body);
    } else{
        $mail_body = $email_body;
    }
    
    
    if(isset ($_POST['send_mail'])){
        //error_reporting(E_ALL); 
        //ini_set("display_errors", 1);
        $mail = new PHPMailer(true);
        try {
          $mail->CharSet = "UTF-8";
          $mail->IsSMTP();
          $mail->Host       = "ms-srv.com"; // SMTP server
          $mail->SMTPAuth   = true;                  // enable SMTP authentication
          $mail->Port       = 25;                    // set the SMTP port for the GMAIL server
          $mail->Username   = "berroth@go2studienkolleg.de"; // SMTP account username
          $mail->Password   = "okuts2goberroth";        // SMTP account password
          $mail->AddReplyTo('berroth@go2studienkolleg.de', 'Jonathan Berroth Go2Studienkolleg');
          $mail->AddAddress($contact_details->email_address);
          $mail->AddBCC('info@go2studienkolleg.de', 'Go 2 Studienkolleg');
          $mail->SetFrom('berroth@go2studienkolleg.de', 'Jonathan Berroth Go2Studienkolleg');
          $mail->Subject = $mail_subject;
          $mail->AltBody = 'Bitte nutzen Sie ein HTML-kompatibles Email-Programm um diese Nachricht zu lesen!'; // optional - MsgHTML will create an alternate automatically
          $mail->MsgHTML($mail_body);
          if($mail_type == "promotion_mail"){
              $mail->AddAttachment('src/tpl_emails/Beschreibung_Go2Lernsoftware.pdf');
          }
          else if($mail_type == "price_list_mail"){
              $mail->AddAttachment('src/tpl_emails/Preisliste_Sommersemester_2012.pdf');
          }
          else if($mail_type == "reminder_mail"){
              $mail->AddAttachment('src/tpl_emails/Preisliste_Sommersemester_2012.pdf');
          }
          $mail->Send();
          if($mail_type == "promotion_mail"){
                $query = "UPDATE go2stuko_language_school set mail = NOW() WHERE lsid = " . $lsid;
          }
          else if($mail_type == "price_list_mail"){
              $query = "UPDATE go2stuko_language_school set price_list_mail = NOW() WHERE lsid = " . $lsid;
          }
          else if($mail_type == "reminder_mail"){
              $query = "UPDATE go2stuko_language_school set reminder_mail = NOW() WHERE lsid = " . $lsid;
          }
          $success = mysql_query($query);
          header("Location: administration.php?adminLanguageSchools=true");
        } catch (phpmailerException $e) {
          echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
          echo $e->getMessage(); //Boring error messages from anything else!
        }
    }
    
    include('templates/core/tpl_header.php');
    include('templates/tpl_send_mail.php');
    include('templates/core/tpl_footer.php');
    
    Database::closeConnection();

?>
