<?php

function printLanguageSchools(){
        
        $skid = $_POST[skid];
        $lsid = $_GET[lsid];
        $sortBy = $_GET[sortBy];
        
        if(isset($_GET['setFirstContact']) && $_GET['setFirstContact'] == true)
        {
            $query = "UPDATE go2stuko_language_school set initial_contact = NOW() WHERE lsid = " . $lsid;
            $success = mysql_query($query);
            header("Location: administration.php?adminLanguageSchools=true");
        }  else if (isset($_GET['deleteFirstContact']) && $_GET['deleteFirstContact'] == true){
            $query = "UPDATE go2stuko_language_school set initial_contact = 0 WHERE lsid = " . $lsid;
            $success = mysql_query($query);
            header("Location: administration.php?adminLanguageSchools=true");
        } else if (isset($_GET['setSecondContact']) && $_GET['setSecondContact'] == true){ 
            $query = "UPDATE go2stuko_language_school set second_contact = NOW() WHERE lsid = " . $lsid;
            $success = mysql_query($query);
            header("Location: administration.php?adminLanguageSchools=true");
        } else if (isset($_GET['deleteSecondContact']) && $_GET['deleteSecondContact'] == true){
            $query = "UPDATE go2stuko_language_school set second_contact = 0 WHERE lsid = " . $lsid;
            $success = mysql_query($query);
            header("Location: administration.php?adminLanguageSchools=true");
        } else if (isset($_GET['setDeal']) && $_GET['setDeal'] == true){
            $query = "UPDATE go2stuko_language_school set deal = NOW() WHERE lsid = " . $lsid;
            $success = mysql_query($query);
            header("Location: administration.php?adminLanguageSchools=true");
        } else if (isset($_GET['deleteDeal']) && $_GET['deleteDeal'] == true){
            $query = "UPDATE go2stuko_language_school set deal = 0 WHERE lsid = " . $lsid;
            $success = mysql_query($query);
            header("Location: administration.php?adminLanguageSchools=true");
        } else if (isset($_GET['setPromotionMailSent']) && $_GET['setPromotionMailSent'] == true){
            $query = "UPDATE go2stuko_language_school set mail = NOW() WHERE lsid = " . $lsid;
            $success = mysql_query($query);
            header("Location: administration.php?adminLanguageSchools=true");
        }
        if(empty($sortBy)) $sortBy = "lsid";
        if(empty($skid)){
            $query = "SELECT * FROM go2stuko_language_school ORDER BY " . $sortBy . " DESC";
        } else{ 
            $query = "SELECT ls.* FROM go2stuko_language_school ls, go2stuko_language_school_studienkolleg_link lsskl WHERE ls.lsid = lsskl.lsid AND lsskl.skid = " . $skid . " ORDER BY " . $sortBy . " DESC";
        }
        $schools = Database::getDatasetFromQuery($query);
        $out = '<form action="' . $_SERVER['PHP_SELF'] . '?adminLanguageSchools=true" method="post" 
                name="choose_skid"><table><tr><td>Studienkolleg:</td><td style="padding-left:30px;">' 
            . DynamicFormElements::getStudienkollegs(
            'skid', NULL, 
            ($errors['skid'] != null ? "register_error" : ""))
            . '</td><td style="padding-left:20px;"><input class="" type="submit" name="submit" value="OK"/></td></tr></table></form><br>';
        $out .= '<a href="add_new_school.php">Neue Sprachschule hinzufügen</a>';
        $out .= ' | Anzahl an Sprachschulen: ' . count($schools) . '<br><br>';
        
        $out .= '<table border="1">';
        $out .= '<tr>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;">';
                $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?adminLanguageSchools=true&sortBy=school_name"><b>Sprachschule</b></a>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px; height: 30px;">';
                $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?adminLanguageSchools=true&sortBy=initial_contact"><b>Erster Kontakt</b></a>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;">';
                $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?adminLanguageSchools=true&sortBy=mail"><b>Mail Anfrage</b></a>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;">';
                $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?adminLanguageSchools=true&sortBy=second_contact"><b>Zweiter Kontakt</b></a>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;">';
                $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?adminLanguageSchools=true&sortBy=price_list_mail"><b>Preisliste versandt</b></a>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;">';
                $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?adminLanguageSchools=true&sortBy=reminder_mail"><b>Erinnerung versandt</b></a>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;">';
                $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?adminLanguageSchools=true&sortBy=deal"><b>Angebot gebucht</b></a>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;">';
                $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?adminLanguageSchools=true"><b>Getestet</b></a>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;">';
                $out .= '<b>Detail Ansicht</b>';
            $out .= '</td>';
        $out .= '</tr>';
        
        foreach ($schools as $school){
            $query = "SELECT te.* FROM go2stuko_language_school ls, go2stuko_user u, go2stuko_test_examination te WHERE te.uid = u.uid AND ls.headofschool_uid = u.uid AND ls.lsid = " . $school->lsid . " ORDER BY te.starting_time DESC";
            $test_examinations = Database::getDatasetFromQuery($query);
            $tested = (!empty ($test_examinations));
            $out .= '<tr>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;" height: 30px;">';
                $out .= $school->school_name;
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;" height: 30px;">';
                if($school->initial_contact == 0){
                    $out .= '<a href="administration.php?adminLanguageSchools=true&lsid='. $school->lsid .'&setFirstContact=true">Kontakt verzeichnen</a>';
                } else {
                    $oDate = new DateTime($school->initial_contact);
                    $out .= 'kontaktiert am:<br>' . $oDate->format("d.m.y H:i:s");
                    $out .= '<br><a href="administration.php?adminLanguageSchools=true&lsid='. $school->lsid .'&deleteFirstContact=true">löschen</a>';
                }
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;" height: 30px;">';
                if($school->mail == 0){
                    $out .= ' <a href="administration.php?adminLanguageSchools=true&lsid='. $school->lsid .'&setPromotionMailSent=true">-</a> ';
                } else {
                    $oDate = new DateTime($school->mail);
                    $out .= 'versendet am: <br>' . $oDate->format("d.m.y H:i:s");
                }
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;" height: 30px;">';
                if($school->second_contact == 0){
                    $out .= '<a href="administration.php?adminLanguageSchools=true&lsid='. $school->lsid .'&setSecondContact=true">Kontakt verzeichnen</a>';
                } else {
                    $oDate = new DateTime($school->second_contact);
                    $out .= 'kontaktiert am:<br>' . $oDate->format("d.m.y H:i:s");
                    $out .= '<br><a href="administration.php?adminLanguageSchools=true&lsid='. $school->lsid .'&deleteSecondContact=true">löschen</a>';
                }
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;" height: 30px;">';
                if($school->price_list_mail == 0){
                    $out .= ' - ';
                } else {
                    $oDate = new DateTime($school->price_list_mail);
                    $out .= 'versendet am: <br>' . $oDate->format("d.m.y H:i:s");
                }
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;" height: 30px;">';
                if($school->reminder_mail == 0){
                    $out .= ' - ';
                } else {
                    $oDate = new DateTime($school->reminder_mail);
                    $out .= 'versendet am: <br>' . $oDate->format("d.m.y H:i:s");
                }
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;">';
                if($school->deal == 0){
                    $out .= '<a href="administration.php?adminLanguageSchools=true&lsid='. $school->lsid .'&setDeal=true">Buchung verzeichnen</a>';
                } else {
                    $oDate = new DateTime($school->deal);
                    $out .= 'verbucht am:<br>' . $oDate->format("d.m.y H:i:s");
                    $out .= '<br><a href="administration.php?adminLanguageSchools=true&lsid='. $school->lsid .'&deleteDeal=true">löschen</a>';
                }
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;">';
                if(!$tested){
                    $out .= '-';
                } else {
                    $last_examination = $test_examinations[0];
                    $oDate = new DateTime($last_examination->starting_time);
                    $out .= 'zuletzt am:<br>' . $oDate->format("d.m.y H:i:s");
                }
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;" height: 30px;">';
                $out .= '<a href="show_school_details.php?lsid=' . $school->lsid . '">Sprachschul Details</a>';
            $out .= '</td>';
        $out .= '</tr>';
            
        }
        
        $out .= '</table>';
        $out .= '<br>';
        $out .= '<a href="administration.php">Zur&uuml;ck</a>';
        return $out;
    }
    
?>