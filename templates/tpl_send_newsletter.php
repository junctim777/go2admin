<A HREF="javascript:history.back()" onMouseOver="{window.status='Zurück'; return true;}"> Zur&uuml;ck </A>
<?php
if (isset($errors) && count($errors) > 0) {
    echo '<table class="" width="" align="LEFT" cellpadding="" cellspacing="" border="0">';
    foreach ($errors as $error) {
        ?>
        <tr>
            <td class="register_error_main_box" colspan=""><?php echo $error; ?></td>
        </tr>
    <?php } ?>
    <tr>
        <td class="register_error_main_box" colspan="">&nbsp;</td>
    </tr>
    </table>
    <br clear="all">
<?php } ?>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>?skid=<?php echo $skid;?>" method="post" name="send_newsletter">
    <table class="" width="800" align="LEFT" cellpadding="" cellspacing="" border="0">
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" style="padding-bottom: 5px; padding-top: 30px;">
                <h3>Sende Newsletter</h3>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br><u>Empf&auml;nger:</u><br><br>
                <?php 
                echo '<form action="' . $_SERVER['PHP_SELF'] . '" method="post" 
                    name="choose_stk"><table><tr><td>Studienkolleg:</td><td style="padding-left:30px;">' 
                    . DynamicFormElements::getStudienkollegs(
                    'skid', NULL, 
                    ($errors['skid'] != null ? "register_error" : "")
                    ) . '</td><td style="padding-left:20px;"><input class="" type="submit" name="submit" value="OK"/></td></tr></table></form><br>';
                if(!empty($next_exam)){
                    $next_exam_date = new DateTime($next_exam->exam_date);
                    $info_box .= '<u>Aufnahmepr&uuml;fung:</u><br>';
                    $info_box .= '<div style="padding-left: 30px;"><ul>';
                    $info_box .= '<li>N&auml;chste Pr&uuml;fung: ' . $next_exam_date->format("d.m.Y") . '</li>';
                    $info_box .= '</ul></div>';
                    $info_box .= '<div style="padding-top:10px; padding-bottom:10px; margin-left: 85px;"><b><font color="red">noch ' . $next_exam->days_to_exam . ' Tage ( ' . $weeks_to_next_exam . ' Wochen)</font></b></div>';
                }
                echo $info_box;
                ?>
            </td>
        </tr>
        <tr>
            <td class="" valign="top" align="LEFT" width="" style="padding-top: 5px;">
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>?skid=<?php echo $skid; ?>" method="post" 
                    name="others">
                    <input tabindex="" class="" type="submit" name="show_email_address" value="ANZEIGEN"/>
                </form>
            </td>
        </tr>
        <?php if(isset ($_POST['show_email_address'])){ ?>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>
                <?php echo $email_adresses; ?>
            </td>
        </tr>
        <?php } ?>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br><u>Betreff:</u><br><br>
            </td>
        </tr>
        <tr>
            <td>
                <input tabindex="" name="email_subject" size="100"
                       value="<?php echo (isset($_POST['email_subject']) ? 
                               $_POST['email_subject'] : $mail_subject); ?>"></input>
            <td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br><u>Mail-Text:</u><br><br>
                <textarea tabindex="" name="email_body" cols="100" rows="30"><?php 
                echo (isset($_POST['email_body']) ? 
                               $_POST['email_body'] : $mail_body); ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br><input tabindex="" class="" type="submit" name="show_mail_body" value="Formatierung anzeigen"/>
            </td>
        </tr>
        <?php if(isset ($_POST['show_mail_body'])){ ?>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>
                <?php echo $mail_body; ?>
            </td>
        </tr>
        <?php } ?>
            <td class="" valign="top" align="RIGHT" width="" style="padding-top: 20px;">
                <input tabindex="" class="" type="submit" name="send_test_mail" value="TEST-MAIL"/>
                <input tabindex="" class="" type="submit" name="send_newsletter_mail" value="SENDEN"/>
            </td>
        </tr>
    </table>
</form>
<br clear="all">