<?php include ("templates/content/tpl_exercises_menu.php"); ?>
<h1>Neue Aufgabe <?php echo $extype_str; ?> - <?php echo $exmodus; ?></h1>
<br>
<?php
if (isset($errors) && count($errors) > 0) {
    echo '<table class="" width="" align="LEFT" cellpadding="" cellspacing="" border="0">';
    foreach ($errors as $error) {
        ?>
        <tr>
            <td class="register_error_main_box" colspan=""><?php echo $error; ?></td>
        </tr>
    <?php } ?>
    <tr>
        <td class="register_error_main_box" colspan="">&nbsp;</td>
    </tr>
    </table>
    <br clear="all">
<?php } ?>