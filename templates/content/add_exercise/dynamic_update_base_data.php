<?php
$query = "UPDATE go2stuko_exercise" .
            " SET " .
            "heading = '" . $heading . "'," . 
            (empty($introduction_text) ? "" : "introduction_text = '" . $introduction_text . "',") .
            "changed = NOW()" . 
            " WHERE eid = " . $eid;
$success = mysql_query($query);
if(! $success){
    $errors['database_failure_update_base_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Update Basis-Daten - go2stuko_exercise)";
}

if(count($errors) == 0){
    header("Location: " . $_SERVER['PHP_SELF'] . "?eid=" . $eid);
}

?>
