<?php

$sentence_no_tmp = $update_slot-1;

$query = "UPDATE go2stuko_exercise_element" .
            " SET " .
            "element_text = '" . $element_text . "'," . 
            "changed = NOW()" . 
            " WHERE eeid = " . $_POST['eeid'];
$success = mysql_query($query);

if(! $success){
    $errors['database_failure_element_exercise_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe (Update) aufgetreten (Element-Daten - go2stuko_exercise_element)";
} else{
    $query = "UPDATE go2stuko_question" .
            " SET " .
            "difficulty = " . $difficulty . "," .
            "estimated_time = " . $estimated_time . "," .
            "question_text = '" . $question_text . "'," . 
            "content_points = " . $content_points . "," .
            "linguistic_points = " . $linguistic_points . "," .
            "answer_note = '" . $answer_note . "'," . 
            "changed = NOW()" . 
            " WHERE qid = " . $_POST['qid'];
    $success = mysql_query($query);
    if(! $success){
        $errors['database_failure_question_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe (Update) aufgetreten (Element-Daten - go2stuko_question)";
    } else{
        $solcnt == 0;
        foreach($solution_texts as $solution_text){
            if(!empty($solution_text)){
                ++$solcnt;
                if($solcnt == 1) $isBestAnswer = 1;
                else $isBestAnswer = 0;
                if($solcnt-1 < count($update_ssids)){
                   $update_ssid =  $update_ssids[$solcnt-1];
                   $query = "SELECT * FROM go2stuko_sample_solution WHERE ssid = " . $update_ssid;
                   $update_sample_solution = Database::getDatasetFromQuery($query);
                   $update_sample_solution = $update_sample_solution[0];
                   $update_sample_solution = new SampleSolution($update_sample_solution);
                   if($update_sample_solution->getSolutionText() == $solution_text){

                   } else{
                       $query = "DELETE FROM go2stuko_sample_solution" .
                            " WHERE ssid = " . $update_ssid;
                       $success = mysql_query($query);
                        $query = "INSERT INTO go2stuko_sample_solution" .
                        " (qid, is_best_answer, created)" .
                        " VALUES (" .
                        $_POST['qid'] . "," .
                        $isBestAnswer . "," .
                        "NOW()" .
                        ")";
                        $success = mysql_query($query);
                        $ssid = mysql_insert_id(Database::$link);
                        if(! $success){
                            $errors['database_failure_sample_solution_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Element-Daten - go2stuko_sample_solution)";
                        } else{
                            $words = split(" ", $solution_text);
                            $wordcnt = 0;
                            foreach($words as $word){
                                ++$wordcnt;
                                $query = "INSERT INTO go2stuko_word_by_word_solution" .
                                " (ssid, sentence_position, value)" .
                                " VALUES (" .
                                $ssid . "," .
                                $wordcnt . "," .
                                "'" . $word . "'" .
                                ")";
                                $success = mysql_query($query);
                                if(! $success){
                                    $errors['database_failure_sample_word_by_word_solution_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Element-Daten - go2stuko_word_by_word_solution)";
                                }
                            }
                        }
                   }
                } else{
                    $query = "INSERT INTO go2stuko_sample_solution" .
                    " (qid, is_best_answer, created)" .
                    " VALUES (" .
                    $_POST['qid'] . "," .
                    $isBestAnswer . "," .
                    "NOW()" .
                    ")";
                    $success = mysql_query($query);
                    $ssid = mysql_insert_id(Database::$link);
                    if(! $success){
                        $errors['database_failure_sample_solution_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Element-Daten - go2stuko_sample_solution)";
                    } else{
                        $words = split(" ", $solution_text);
                        $wordcnt = 0;
                        foreach($words as $word){
                            ++$wordcnt;
                            $query = "INSERT INTO go2stuko_word_by_word_solution" .
                            " (ssid, sentence_position, value)" .
                            " VALUES (" .
                            $ssid . "," .
                            $wordcnt . "," .
                            "'" . $word . "'" .
                            ")";
                            $success = mysql_query($query);
                            if(! $success){
                                $errors['database_failure_sample_word_by_word_solution_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Element-Daten - go2stuko_word_by_word_solution)";
                            }
                        }
                    }
                }
                
            }
        }
    }
}

if(count($errors) == 0){
    header("Location: " . $_SERVER['PHP_SELF'] . "?eid=" . $eid);
}

?>
