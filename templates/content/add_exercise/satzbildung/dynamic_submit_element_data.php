<?php

$sentence_no_tmp = $new_slot-1;
$query = "INSERT INTO go2stuko_exercise_element" .
        " (eid, element_type, exercise_position, element_text, created)" .
        " VALUES (" .
        $eid . "," .
        "'Test'," .
        "'" . $sentence_no_tmp . "'," .
        "'" . $element_text . "'," .
        "NOW()" .
        ")";
$success = mysql_query($query);
$eeid = mysql_insert_id(Database::$link);
if(! $success){
    $errors['database_failure_element_exercise_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Element-Daten - go2stuko_exercise_element)";
} else{
    $query = "INSERT INTO go2stuko_question" .
        " (eeid, qtid, exercise_element_position, difficulty, estimated_time," .
        " question_text, is_free_text_question, content_points, linguistic_points," .
        " answer_note, created)" .
        " VALUES (" .
        $eeid . "," .
        "1," .
        "1," .
        $difficulty . "," .
        $estimated_time . "," .
        "'" . $question_text . "'," .
        "0," .
        $content_points . "," .
        $linguistic_points . "," .
        "'" . $answer_note . "'," .
        "NOW()" .
        ")";
    $success = mysql_query($query);
    $qid = mysql_insert_id(Database::$link);
    if(! $success){
        $errors['database_failure_question_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Element-Daten - go2stuko_question)";
    } else{
        $solcnt == 0;
        foreach($solution_texts as $solution_text){
            if(!empty($solution_text)){
                ++$solcnt;
                if($solcnt == 1) $isBestAnswer = 1;
                else $isBestAnswer = 0;
                $query = "INSERT INTO go2stuko_sample_solution" .
                " (qid, is_best_answer, created)" .
                " VALUES (" .
                $qid . "," .
                $isBestAnswer . "," .
                "NOW()" .
                ")";
                $success = mysql_query($query);
                $ssid = mysql_insert_id(Database::$link);
                if(! $success){
                    $errors['database_failure_sample_solution_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Element-Daten - go2stuko_sample_solution)";
                } else{
                    $words = split(" ", $solution_text);
                    $wordcnt = 0;
                    foreach($words as $word){
                        ++$wordcnt;
                        $query = "INSERT INTO go2stuko_word_by_word_solution" .
                        " (ssid, sentence_position, value)" .
                        " VALUES (" .
                        $ssid . "," .
                        $wordcnt . "," .
                        "'" . $word . "'" .
                        ")";
                        $success = mysql_query($query);
                        if(! $success){
                            $errors['database_failure_sample_word_by_word_solution_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Element-Daten - go2stuko_word_by_word_solution)";
                        }
                    }
                }
            }
        }
    }
}

if(count($errors) == 0){
    header("Location: " . $_SERVER['PHP_SELF'] . "?eid=" . $eid);
}
?>
