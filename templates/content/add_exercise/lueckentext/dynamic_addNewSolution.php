<?php

$query = "INSERT INTO go2stuko_sample_solution" .
        " (qid, is_best_answer, created)" .
        " VALUES (" .
        $qid . "," .
        "0," .
        "NOW()" .
        ")";
$success = mysql_query($query);
$ssid = mysql_insert_id(Database::$link);
if (!$success) {
    $errors['database_failure_sample_solution_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Lueckentext-Daten - go2stuko_sample_solution)";
} else {
    $query = "INSERT INTO go2stuko_word_by_word_solution" .
            " (ssid, sentence_position, value, linguistic_points)" .
            " VALUES (" .
            $ssid . "," .
            "1," .
            "'" . $newSolution . "'," .
            "1" .
            ")";
    $success = mysql_query($query);
    echo $query;
    if (!$success) {
        $errors['database_failure_sample_word_by_word_solution_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Lueckentext-Daten - go2stuko_word_by_word_solution)";
    }
}

if (count($errors) == 0) {
    header("Location: " . $_SERVER['PHP_SELF'] . "?eid=" . $eid);
}
?>
