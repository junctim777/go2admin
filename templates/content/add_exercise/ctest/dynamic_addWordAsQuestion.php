<?php

if ($charno == 0){
    $errors['input_failure_question_data'] = "Der erste Buchstabe kann nicht gewählt werden!";
}
else{
    $query = "INSERT INTO go2stuko_question" .
            " (eeid, qtid, exercise_element_position, difficulty, estimated_time," .
            " question_text, is_free_text_question, content_points, linguistic_points," .
            " created)" .
            " VALUES (" .
            $eeid . "," .
            "3," .
            $wordno . "," .
            "2," .
            "1," .
            "'" . $wordno. "-" . $charno . "'," .
            "0," .
            "0," .
            "1," .
            "NOW()" .
            ")";
    echo $query;
    $success = mysql_query($query);
    $qid = mysql_insert_id(Database::$link);
}
if (!$success) {
    $errors['database_failure_question_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Lueckentext-Daten - go2stuko_question)";
} else {
    $query = "INSERT INTO go2stuko_sample_solution" .
            " (qid, is_best_answer, created)" .
            " VALUES (" .
            $qid . "," .
            "1," .
            "NOW()" .
            ")";
    $success = mysql_query($query);
    $ssid = mysql_insert_id(Database::$link);
    if (!$success) {
        $errors['database_failure_sample_solution_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Lueckentext-Daten - go2stuko_sample_solution)";
    } else {
        for ($i=0; $i<strlen($word); $i++){
            if ($i == $charno){
                $syllable = substr($word, $i);
            }
        }
        $query = "INSERT INTO go2stuko_word_by_word_solution" .
                " (ssid, sentence_position, value, linguistic_points)" .
                " VALUES (" .
                $ssid . "," .
                "1," .
                "'" . $syllable . "'," .
                "1" .
                ")";
        $success = mysql_query($query);
        echo $query;
        if (!$success) {
            $errors['database_failure_sample_word_by_word_solution_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Lueckentext-Daten - go2stuko_word_by_word_solution)";
        }
    }
}

if (count($errors) == 0) {
    header("Location: " . $_SERVER['PHP_SELF'] . "?eid=" . $eid);
}
?>
