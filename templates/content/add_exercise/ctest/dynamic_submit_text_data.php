<?php

$regEx = '/([.!?] )/';
$exercise_sentences = preg_split($regEx, $lueckentext, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
$cnt = 1;
for($i=0; $i<count($exercise_sentences); $i+=2){
    $exercise_sentence = $exercise_sentences[$i] . $exercise_sentences[$i+1];
    $query = "INSERT INTO go2stuko_exercise_element" .
            " (eid, element_type, exercise_position, element_text, created)" .
            " VALUES (" .
            $eid . "," .
            "'Test'," .
            "'" . $cnt++ . "'," .
            "'" . $exercise_sentence . "'," .
            "NOW()" .
            ")";
    $success = mysql_query($query);
    $eeid = mysql_insert_id(Database::$link);
    if(! $success){
        $errors['database_failure_element_exercise_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Lueckentext-Daten - go2stuko_exercise_element)";
    }
}

if(count($errors) == 0){
    header("Location: " . $_SERVER['PHP_SELF'] . "?eid=" . $eid);
}
?>
