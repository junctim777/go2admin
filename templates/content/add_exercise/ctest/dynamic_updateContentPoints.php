<?php

$query = "UPDATE go2stuko_word_by_word_solution" .
            " SET " .
            "content_points = " . $has_content_point . "" .
            " WHERE ssid = " . $ssid;
$success = mysql_query($query);

if(! $success){
    $errors['database_failure_word_by_word_solution_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe (Update) aufgetreten (Word-By-Word-Solution-Daten - go2stuko_question)";
} else{
    if($has_content_point == 1){
        $query = "UPDATE go2stuko_question" .
                " SET " .
                "content_points = " . $has_content_point . "" .
                " WHERE qid = " . $qid;
        $success = mysql_query($query);
        if(! $success){
            $errors['database_failure_word_by_word_solution_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe (Update) aufgetreten (Update content_points on Question-Daten - go2stuko_question)";
        }
    }
}

if(count($errors) == 0){
    header("Location: " . $_SERVER['PHP_SELF'] . "?eid=" . $eid);
}

?>
