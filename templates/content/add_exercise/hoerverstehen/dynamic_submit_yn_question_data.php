<?php
$xmlstr = <<<XML
<?xml version='1.0' standalone='yes'?>
<yn_question>
</yn_question>
XML;

$xml = new SimpleXMLElement($xmlstr);
$yn_question_cnt = 0;
foreach($yn_questions as $yn_question){
    if(!empty($yn_question)){
        $correct = (empty($is_correct[$yn_question_cnt++]) ? 'false' : 'true');
        $xml_question = $xml->addChild('question', $yn_question);
        $xml_question->addAttribute('correct', $correct);
    }
}
$query = "INSERT INTO go2stuko_question" .
        " (eeid, qtid, exercise_element_position, difficulty, estimated_time," .
        " question_text, is_free_text_question, content_points, linguistic_points," .
        " answer_note, created)" .
        " VALUES (" .
        $_POST['eeid'] . "," .
        "6," .
        "1," .
        $difficulty . "," .
        $estimated_time . "," .
        "'" . str_replace("'", "", $xml->asXML()) . "'," .
        "0," .
        $content_points . "," .
        $linguistic_points . "," .
        "'" . $answer_note . "'," .
        "NOW()" .
        ")";
    $success = mysql_query($query);
    if(! $success){
        $errors['database_failure_question_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Element-Daten - go2stuko_question)";
    }

if(count($errors) == 0){
    header("Location: " . $_SERVER['PHP_SELF'] . "?eid=" . $eid);
}
?>
