<form action="<?php echo $_SERVER['PHP_SELF']; ?>?extype=<?php echo $_GET['extype'];?>&new_slot=<?php echo $slot_no; ?>&update_slot=<?php echo $_GET['update_slot']; ?>&eid=<?php echo $eid; ?>" 
      method="post" enctype="multipart/form-data" name="content_add_exercise_<?php echo $extype; ?>_upload_image">
    <table class="" width="800" align="LEFT" cellpadding="" cellspacing="" border="0">
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" style="padding-bottom: 5px; padding-top: 30px;">
                <h3>Audiofile (mp3, wav)</h3>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" style="padding-bottom: 5px; padding-top: 30px;">
                <?php if(count($exercise->exerciseElements) > 0){ 
                    $exElement = $exercise->exerciseElements[0];
                    $exFile = $exElement->file;
                    echo '<b>Hochgeladenen Datei:</b> ' . $exFile->getDescription();
                } else{
                    echo '<input type="file" name="new_file">';
                }
                ?>              
            </td>
        </tr>

        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" style="padding-bottom: 5px; padding-top: 30px;">
                <h3>Neuer Text</h3>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <u>Audio als Text eingeben:</u>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input type="hidden" name="eeid" value="<?php echo isset($exerciseElement) ? $exerciseElement->getEeid() : ""; ?>">
                <textarea tabindex="<?php echo ++$tabindex; ?>" name="element_text" rows="12" 
                          cols="100"><?php echo isset($element_text) ? str_replace("<br />", "", $element_text) : $_POST['element_text']; ?></textarea>
            </td>
        </tr>
        <tr>
            <td class="" valign="top" align="LEFT" width="" style="padding-top: 20px;">
                <?php 
                if(count($exerciseElement->questions) > 0 && !isset($action)){ 
                            echo printQuestionsOfExerciseElement($exerciseElement); 
                 } if(!isset($action)){ ?>
                            <br><br>
                            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=new_mc_question&eid=<?php echo $eid; ?>&eeid=<?php echo $exerciseElement->dbExerciseElement->eeid; ?>">Neue MC-Frage</a><br>
                            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=new_yn_question&eid=<?php echo $eid; ?>&eeid=<?php echo $exerciseElement->dbExerciseElement->eeid; ?>">Neue Ja/Nein Frage</a>
                <?php } ?>            
            </td>
        </tr>
        <tr>
            <td class="" valign="top" align="RIGHT" width="" style="padding-top: 20px;">
                <?php if(count($exercise->exerciseElements) > 0){ ?>
                            <input tabindex="<?php echo ++$tabindex; ?>" class="" type="submit" name="update_element_data" 
                            value="UPDATE"/>
                <?php } else{ ?>
                            <input tabindex="<?php echo ++$tabindex; ?>" class="" type="submit" name="submit_element_data" 
                            value="UPLOAD"/>
                <?php } ?>
            </td>
        </tr>
    </table>
</form>