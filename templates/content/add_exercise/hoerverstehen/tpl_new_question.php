<form action="<?php echo $_SERVER['PHP_SELF']; ?>?extype=<?php echo $_GET['extype'];?>&new_slot=<?php echo $slot_no; ?>&update_slot=<?php echo $_GET['update_slot']; ?>&eid=<?php echo $eid; ?>&eeid=<?php echo $eeid; ?>&position=<?php echo $sentence_no; ?>" 
      method="post" name="content_add_exercise_<?php echo $extype; ?>_textproduktion">
    <table class="" width="800" align="LEFT" cellpadding="" cellspacing="" border="0">
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" style="padding-bottom: 5px; padding-top: 30px;">
                <h3>Neue Frage No. <?php echo $sentence_no; ?></h3>
            </td>
        </tr>
        <tr>
            <td colspan="" class="" valign="top" align="LEFT" width="" style="padding-top: 10px;">
                <u>Fragestellung</u>
            </td>
        </tr>
        <tr>
            <td colspan="" class="" valign="top" align="LEFT" width="">
                <input type="hidden" name="qid" value="<?php echo isset($question) ? $question->getQid() : ""; ?>">
                <textarea tabindex="<?php echo ++$tabindex; ?>" name="question_text" rows="2" 
                  cols="100"><?php echo isset($question_text) ? $question_text : $_POST['question_text']; ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="" class="" valign="top" align="LEFT" width="" style="padding-top: 10px;">
                <u>Musterl&ouml;sung</u>
            </td>
        </tr>
        <tr>
            <td colspan="" class="" valign="top" align="LEFT" width="">
                <textarea tabindex="<?php echo ++$tabindex; ?>" name="answer_note" rows="6" 
                  cols="100"><?php echo isset($answer_note) ? $answer_note : $_POST['answer_note']; ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="" class="" valign="top" align="LEFT" width="" style="padding-top: 10px;">
                <u>Aufgaben-Details</u>
            </td>
        </tr>
        <tr>
            <td colspan="" class="" valign="top" align="LEFT" width="">
                <table>
                    <tr>
                        <td>
                            Schwierigkeit: 
                        </td>
                        <td>
                            <select tabindex="<?php echo ++$tabindex; ?>" name="difficulty">
                                <option value="1" <?php echo $_POST['difficulty'] == 1 ? 'selected="selected"' : ($difficulty == 1 ? 'selected="selected"': 'selected="selected"'); ?>>Leicht</option>
                                <option value="2" <?php echo $_POST['difficulty'] == 2 ? 'selected="selected"' : ($difficulty == 2 ? 'selected="selected"': ''); ?>>Mittel</option>
                                <option value="3" <?php echo $_POST['difficulty'] == 3 ? 'selected="selected"' : ($difficulty == 3 ? 'selected="selected"': ''); ?>>Schwer</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Gesch&auml;tzte Bearbeitungszeit (mins): 
                        </td>
                        <td>
                            <select tabindex="<?php echo ++$tabindex; ?>" name="estimated_time">
                                <option value="1" <?php echo $_POST['estimated_time'] == 1 ? 'selected="selected"' : ($estimated_time == 1 ? 'selected="selected"': 'selected="selected"'); ?>>1</option>
                                <option value="2" <?php echo $_POST['estimated_time'] == 2 ? 'selected="selected"' : ($estimated_time == 2 ? 'selected="selected"': ''); ?>>2</option>
                                <option value="3" <?php echo $_POST['estimated_time'] == 3 ? 'selected="selected"' : ($estimated_time == 3 ? 'selected="selected"': ''); ?>>3</option>
                                <option value="4" <?php echo $_POST['estimated_time'] == 4 ? 'selected="selected"' : ($estimated_time == 4 ? 'selected="selected"': ''); ?>>4</option>
                                <option value="5" <?php echo $_POST['estimated_time'] == 5 ? 'selected="selected"' : ($estimated_time == 5 ? 'selected="selected"': ''); ?>>5</option>
                                <option value="6" <?php echo $_POST['estimated_time'] == 6 ? 'selected="selected"' : ($estimated_time == 6 ? 'selected="selected"': ''); ?>>6</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Inhaltspunkte: 
                        </td>
                        <td>
                            <select tabindex="<?php echo ++$tabindex; ?>" name="content_points">
                                <option value="0" <?php echo $_POST['content_points'] == 0 ? 'selected="selected"' : ($content_points == 0 ? 'selected="selected"': ((isset($content_points) || isset($_POST['content_points']))?'':'selected="selected"'));  ?>>0</option>
                                <option value="1" <?php echo $_POST['content_points'] == 1 ? 'selected="selected"' : ($content_points == 1 ? 'selected="selected"': ''); ?>>1</option>
                                <option value="2" <?php echo $_POST['content_points'] == 2 ? 'selected="selected"' : ($content_points == 2 ? 'selected="selected"': ''); ?>>2</option>
                                <option value="3" <?php echo $_POST['content_points'] == 3 ? 'selected="selected"' : ($content_points == 3 ? 'selected="selected"': ''); ?>>3</option>
                                <option value="4" <?php echo $_POST['content_points'] == 4 ? 'selected="selected"' : ($content_points == 4 ? 'selected="selected"': ''); ?>>4</option>
                                <option value="5" <?php echo $_POST['content_points'] == 5 ? 'selected="selected"' : ($content_points == 5 ? 'selected="selected"': ''); ?>>5</option>
                                <option value="6" <?php echo $_POST['content_points'] == 6 ? 'selected="selected"' : ($content_points == 6 ? 'selected="selected"': ''); ?>>6</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Sprachpunkte: 
                        </td>
                        <td>
                            <select tabindex="<?php echo ++$tabindex; ?>" name="linguistic_points">
                                <option value="0" <?php echo $_POST['linguistic_points'] == 0 ? 'selected="selected"' : ($linguistic_points == 0 ? 'selected="selected"': ((isset($linguistic_points) || isset($_POST['linguistic_points']))?'':'selected="selected"'));  ?>>0</option>
                                <option value="1" <?php echo $_POST['linguistic_points'] == 1 ? 'selected="selected"' : ($linguistic_points == 1 ? 'selected="selected"': ''); ?>>1</option>
                                <option value="2" <?php echo $_POST['linguistic_points'] == 2 ? 'selected="selected"' : ($linguistic_points == 2 ? 'selected="selected"': ''); ?>>2</option>
                                <option value="3" <?php echo $_POST['linguistic_points'] == 3 ? 'selected="selected"' : ($linguistic_points == 3 ? 'selected="selected"': ''); ?>>3</option>
                                <option value="4" <?php echo $_POST['linguistic_points'] == 4 ? 'selected="selected"' : ($linguistic_points == 4 ? 'selected="selected"': ''); ?>>4</option>
                                <option value="5" <?php echo $_POST['linguistic_points'] == 5 ? 'selected="selected"' : ($linguistic_points == 5 ? 'selected="selected"': ''); ?>>5</option>
                                <option value="6" <?php echo $_POST['linguistic_points'] == 6 ? 'selected="selected"' : ($linguistic_points == 6 ? 'selected="selected"': ''); ?>>6</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="" valign="top" align="RIGHT" width="" style="padding-top: 20px;">
                <?php if(isset($update_slot) && $update_slot == $slot_no){ ?>
                <input tabindex="<?php echo ++$tabindex; ?>" class="" type="submit" name="update_question_data" 
                       value="UPDATE"/>
                <?php } else{ ?>
                <input tabindex="<?php echo ++$tabindex; ?>" class="" type="submit" name="submit_question_data" 
                       value="WEITER"/>
                <?php } ?>
            </td>
        </tr>
    </table>
</form>