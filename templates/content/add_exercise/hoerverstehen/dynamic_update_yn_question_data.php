<?php

$xmlstr = <<<XML
<?xml version='1.0' standalone='yes'?>
<yn_question>
</yn_question>
XML;

$xml = new SimpleXMLElement($xmlstr);
$yn_question_cnt = 0;
foreach($yn_questions as $yn_question){
    if(!empty($yn_question)){
        $correct = (empty($is_correct[$yn_question_cnt++]) ? 'false' : 'true');
        $xml_question = $xml->addChild('question', $yn_question);
        $xml_question->addAttribute('correct', $correct);
    }
}

$query = "UPDATE go2stuko_question" .
            " SET " .
            "difficulty = " . $difficulty . "," .
            "estimated_time = " . $estimated_time . "," .
            "question_text = '" . str_replace("'", "", $xml->asXML()) . "'," . 
            "content_points = " . $content_points . "," .
            "linguistic_points = " . $linguistic_points . "," .
            "answer_note = '" . $answer_note . "'," . 
            "changed = NOW()" . 
            " WHERE qid = " . $_POST['qid'];
$success = mysql_query($query);
if(! $success){
    $errors['database_failure_question_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe (Update) aufgetreten (Element-Daten - go2stuko_question)";
}

if(count($errors) == 0){
    header("Location: " . $_SERVER['PHP_SELF'] . "?eid=" . $eid);
}

?>
