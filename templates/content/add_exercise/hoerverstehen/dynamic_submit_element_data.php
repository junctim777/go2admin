<?php

$audio_name_full = basename($_FILES['new_file']['name']);
$audio_name_tmp = explode(".", $audio_name_full); 
$audio_ending = array_pop($audio_name_tmp);

$query = "INSERT INTO go2stuko_file" .
        " (ending, file_type, description)" .
        " VALUES (" .
        "'" . $audio_ending . "'," .
        "'Audio'," .
        "'" . $audio_name_full . "'" .
        ")";
$success = mysql_query($query);
$fid = mysql_insert_id(Database::$link);
if (!$success) {
    $errors['database_failure_audio'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (File-Daten - go2stuko_exercise_file)";
} else {
    $query = "INSERT INTO go2stuko_exercise_element" .
        " (eid, element_type, exercise_position, element_text, fid, created)" .
        " VALUES (" .
        $eid . "," .
        "'Audio'," .
        "1," .
        "'" . $element_text . "'," .
        $fid . "," .
        "NOW()" .
        ")";
    $success = mysql_query($query);
    $eeid = mysql_insert_id(Database::$link);
    if (!$success) {
        $errors['database_failure_question_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Element-Daten - go2stuko_question)";
    }
    
    $success = move_uploaded_file($_FILES['new_file']['tmp_name'], "src/exercise_imgs/" . $fid . "." . $audio_ending);
    if (!$success) {
        $errors['upload_file'] = "Sorry. Es ist ein Problem beim Upload der Audio Datei aufgetreten";
    }
}

if(count($errors) == 0){
    header("Location: " . $_SERVER['PHP_SELF'] . "?eid=" . $eid);
}

?>
