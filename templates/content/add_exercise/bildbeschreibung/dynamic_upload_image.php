<?php

$image_name_full = basename($_FILES['new_image']['name']);
$image_name_tmp = explode(".", $image_name_full); 
$image_ending = array_pop($image_name_tmp);
$query = "INSERT INTO go2stuko_file" .
        " (ending, description)" .
        " VALUES (" .
        "'" . $image_ending . "'," .
        "'" . $image_name_full . "'" .
        ")";
$success = mysql_query($query);
$fid = mysql_insert_id(Database::$link);
if (!$success) {
    $errors['database_failure_picture'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Picture-Daten - go2stuko_exercise_picture)";
} else {
    $query = "INSERT INTO go2stuko_exercise_element" .
        " (eid, element_type, exercise_position, fid, created)" .
        " VALUES (" .
        $eid . "," .
        "'Bild/Grafik'," .
        "1," .
        $fid . "," .
        "NOW()" .
        ")";
    $success = mysql_query($query);
    $eeid = mysql_insert_id(Database::$link);
    if (!$success) {
        $errors['database_failure_question_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Element-Daten - go2stuko_question)";
    }
    
    $success = move_uploaded_file($_FILES['new_image']['tmp_name'], "src/exercise_imgs/" . $fid . "." . $image_ending);
    if (!$success) {
        $errors['upload_image'] = "Sorry. Es ist ein Problem beim Upload des Bildes aufgetreten";
    }
}

if(count($errors) == 0){
    header("Location: " . $_SERVER['PHP_SELF'] . "?eid=" . $eid);
}

?>
