<form action="<?php echo $_SERVER['PHP_SELF']; ?>?extype=<?php echo $_GET['extype'];?>&exmodus=<?php echo $exmodus; ?>&update_slot=<?php echo $_GET['update_slot'];?>&eid=<?php echo $eid; ?>" 
      method="post" name="content_add_exercise_<?php echo $extype; ?>_base_data">
    <table class="" width="800" align="LEFT" cellpadding="" cellspacing="" border="0">
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" style="padding-bottom: 5px;">
                <h3>Lektions-Informationen</h3>
            </td>
        </tr>
        <tr>
            <td class="" width="800" valign="top" align="LEFT" width="">
                <u>&Uuml;berschrift</u>
            </td>
        </tr>
        <tr>
            <td class="" width="800" valign="top" align="LEFT" width="">
                <?php if(empty($eid) || (isset($update_slot) && $update_slot == $slot_no)){?>
                <input tabindex="<?php echo ++$tabindex; ?>" class="" type="text" size="130"
                       maxlength="100" name="heading" 
                       value="<?php echo isset($heading) ? $heading : $_POST['heading']; ?>"/>
                <?php } else echo $heading; ?>
            </td>
        </tr>
        <?php if($extype != "Leseverstehen" && $exercise->dbExercise->etid != 7){?>
        <tr>
            <td class="" width="800" valign="top" align="LEFT" width="" style="padding-top: 10px;">
                <u>Aufgabentext</u>
            </td>
        </tr>
        <tr>
            <td class="" width="800" valign="top" align="LEFT" width="">
                <?php if(empty($eid) || (isset($update_slot) && $update_slot == $slot_no)){?>
                <textarea tabindex="<?php echo ++$tabindex; ?>" name="introduction_text" rows="4" 
                  cols="100"><?php echo isset($introduction_text) ? str_replace('<br />','', $introduction_text) : $_POST['introduction_text']; ?></textarea>
                <?php } else echo $introduction_text; ?>
            </td>
        </tr>
        <?php } ?>
        <tr>
            <td class="" width="800" valign="top" align="RIGHT" width="" style="padding-top: 10px; padding-bottom: 30px;">
                <?php if(empty($eid)){?>
                <input tabindex="<?php echo ++$tabindex; ?>" class="" type="submit" name="submit_base_data" 
                       value="WEITER"/>
                <?php } else if((isset($update_slot) && $update_slot == $slot_no)){ ?>
                <input tabindex="<?php echo ++$tabindex; ?>" class="" type="submit" name="update_base_data" 
                       value="UPDATE"/>
                <?php } else{ ?>
                <a href="<?php echo $_SERVER['PHP_SELF']; ?>?extype=<?php echo $_GET['extype'];?>&update_slot=<?php echo $slot_no; ?>&eid=<?php echo $eid; ?>">Bearbeiten</a>
                <?php } ?>
            </td>
        </tr>
    </table>
</form>