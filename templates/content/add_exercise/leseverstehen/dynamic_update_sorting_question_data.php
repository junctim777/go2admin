<?php

$query = "UPDATE go2stuko_question" .
            " SET " .
            "difficulty = " . $difficulty . "," .
            "estimated_time = " . $estimated_time . "," .
            "question_text = '" . str_replace("'", "", $question_introduction) . "'," . 
            "content_points = " . $content_points . "," .
            "linguistic_points = " . $linguistic_points . "," .
            "answer_note = '" . $answer_note . "'," . 
            "changed = NOW()" . 
            " WHERE qid = " . $_POST['qid'];
$success = mysql_query($query);
if(! $success){
    $errors['database_failure_question_data'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe (Update) aufgetreten (Element-Daten - go2stuko_question)";
}

if(count($errors) == 0){
    header("Location: " . $_SERVER['PHP_SELF'] . "?eid=" . $eid);
}

?>
