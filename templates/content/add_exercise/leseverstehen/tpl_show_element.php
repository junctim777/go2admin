<br clear="all">
<table width="800" class="" width="" align="LEFT" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td colspan="" width="20" class="" valign="top" align="LEFT" style="padding-bottom: 5px;">
            <b><?php echo $sentence_no; ?></b>
        </td>
        <td colspan="" width="380" class="" valign="top" align="LEFT" style="padding-left:10px; padding-bottom: 5px;">
            <?php echo $element_text; ?>
            <br><br>
            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?extype=<?php echo $_GET['extype'];?>&update_slot=<?php echo $slot_no; ?>&eid=<?php echo $eid; ?>">Bearbeiten</a>
        </td>
        <td colspan="" width="400" class="" valign="top" align="LEFT" style="padding-left:80px; padding-bottom: 5px;">
            <b>Fragen:</b><br>
                <?php echo printQuestionsOfExerciseElement($exerciseElement); ?>
            <br>
            <b>Neue Frage:</b><br>
            <ul>
                <li><a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=new_mc_question&eid=<?php echo $eid; ?>&eeid=<?php echo $exerciseElement->dbExerciseElement->eeid; ?>">MC-Frage</a></li>
                <li><a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=new_sorting_question&eid=<?php echo $eid; ?>&eeid=<?php echo $exerciseElement->dbExerciseElement->eeid; ?>">Reihenfolge sortieren</a></li>
            </ul>
        </td>
    </tr>
</table>