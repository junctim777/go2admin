<h1>Bearbeite Angebot</h1>
<br><br>
<form action="<?php echo $_SERVER['php_self']; ?>?oid=<?php echo $_GET['oid']; ?>" method="post" name="add_new_offer">
<b>Name:</b><br>
<input style="" class="" type="text" size="30" maxlength="100" name="offer_name" value="<?php echo $offer->offer_name; ?>"><br><br>

<b>Beschreibung:</b><br>
<textarea tabindex="" name="offer_description" rows="4" cols="40"><?php echo $offer->offer_description; ?></textarea><br><br>

<b>Laufzeit (Wochen):</b><br>
<select tabindex="" name="number_of_weeks">
    <option value="1" <?php echo ($offer->number_of_weeks == '1' ? 'selected="selected"':''); ?>>1</option>
    <option value="2" <?php echo ($offer->number_of_weeks == '2' ? 'selected="selected"':''); ?>>2</option>
    <option value="3" <?php echo ($offer->number_of_weeks == '3' ? 'selected="selected"':''); ?>>3</option>
    <option value="4" <?php echo ($offer->number_of_weeks == '4' ? 'selected="selected"':''); ?>>4</option>
    <option value="5" <?php echo ($offer->number_of_weeks == '5' ? 'selected="selected"':''); ?>>5</option>
    <option value="6" <?php echo ($offer->number_of_weeks == '6' ? 'selected="selected"':''); ?>>6</option>
    <option value="7" <?php echo ($offer->number_of_weeks == '7' ? 'selected="selected"':''); ?>>7</option>
    <option value="8" <?php echo ($offer->number_of_weeks == '8' ? 'selected="selected"':''); ?>>8</option>
    <option value="9" <?php echo ($offer->number_of_weeks == '9' ? 'selected="selected"':''); ?>>9</option>
    <option value="10" <?php echo ($offer->number_of_weeks == '10' ? 'selected="selected"':''); ?>>10</option>
    <option value="11" <?php echo ($offer->number_of_weeks == '11' ? 'selected="selected"':''); ?>>11</option>
    <option value="12" <?php echo ($offer->number_of_weeks == '12' ? 'selected="selected"':''); ?>>12</option>
    <option value="13" <?php echo ($offer->number_of_weeks == '13' ? 'selected="selected"':''); ?>>13</option>
    <option value="14" <?php echo ($offer->number_of_weeks == '14' ? 'selected="selected"':''); ?>>14</option>
    <option value="15" <?php echo ($offer->number_of_weeks == '15' ? 'selected="selected"':''); ?>>15</option>
    <option value="16" <?php echo ($offer->number_of_weeks == '16' ? 'selected="selected"':''); ?>>16</option>
</select><br><br>

<b>Tests pro Woche:</b><br>
<select tabindex="" name="tests_per_week">
    <option value="1" <?php echo ($offer->tests_per_week == '1' ? 'selected="selected"':''); ?>>1</option>
    <option value="2" <?php echo ($offer->tests_per_week == '2' ? 'selected="selected"':''); ?>>2</option>
    <option value="3" <?php echo ($offer->tests_per_week == '3' ? 'selected="selected"':''); ?>>3</option>
    <option value="4" <?php echo ($offer->tests_per_week == '4' ? 'selected="selected"':''); ?>>4</option>
    <option value="5" <?php echo ($offer->tests_per_week == '5' ? 'selected="selected"':''); ?>>5</option>
    <option value="6" <?php echo ($offer->tests_per_week == '6' ? 'selected="selected"':''); ?>>6</option>
    <option value="7" <?php echo ($offer->tests_per_week == '7' ? 'selected="selected"':''); ?>>7</option>
    <option value="8" <?php echo ($offer->tests_per_week == '8' ? 'selected="selected"':''); ?>>8</option>
    <option value="9" <?php echo ($offer->tests_per_week == '9' ? 'selected="selected"':''); ?>>9</option>
    <option value="10" <?php echo ($offer->tests_per_week == '10' ? 'selected="selected"':''); ?>>10</option>
    <option value="11" <?php echo ($offer->tests_per_week == '11' ? 'selected="selected"':''); ?>>11</option>
    <option value="12" <?php echo ($offer->tests_per_week == '12' ? 'selected="selected"':''); ?>>12</option>
    <option value="13" <?php echo ($offer->tests_per_week == '13' ? 'selected="selected"':''); ?>>13</option>
    <option value="14" <?php echo ($offer->tests_per_week == '14' ? 'selected="selected"':''); ?>>14</option>
</select><br><br>

<b>Gesamt-Summe:</b><br>
<input style="" class="" type="text" size="8" maxlength="8" name="total_sum" value="<?php echo $offer->total_sum; ?>"><br><br>

<b>K&uuml;ndigungs-Hinweis:</b><br>
<textarea tabindex="" name="redemption_notice" rows="4" cols="40"><?php echo $offer->redemption_notice; ?></textarea><br><br> 

<input style="" class="" type="submit" name="submit" value="OK"/>
</form>