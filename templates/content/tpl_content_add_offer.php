<h1>Neues Angebote</h1>
<br><br>
<form action="content_add_offer.php?action=add_new_offer" method="post" name="add_new_offer">
<b>Name:</b><br>
<input style="" class="" type="text" size="30" maxlength="100" name="offer_name" value="1 Monat - Studienkollegvorbereitung"><br><br>

<b>Beschreibung:</b><br>
<textarea tabindex="" name="offer_description" rows="4" cols="40">4 Wochen (4 originale Tests, 30 Trainingeinheiten)</textarea><br><br>

<b>Laufzeit (Wochen):</b><br>
<select tabindex="" name="number_of_weeks">
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4" selected="selected">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
    <option value="13">13</option>
    <option value="14">14</option>
    <option value="15">15</option>
    <option value="16">16</option>
</select><br><br>

<b>Tests pro Woche:</b><br>
<select tabindex="" name="tests_per_week">
    <option value="1" selected="selected">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
    <option value="13">13</option>
    <option value="14">14</option>
</select><br><br>

<b>Gesamt-Summe:</b><br>
<input style="" class="" type="text" size="8" maxlength="8" name="total_sum" value="19,99">&nbsp;€<br><br>

<b>K&uuml;ndigungs-Hinweis:</b><br>
<textarea tabindex="" name="redemption_notice" rows="4" cols="40">Die Laufzeit beträgt 4 Wochen mit automatischer Kündigung nach dem Ende der Laufzeit.</textarea><br><br> 

<input style="" class="" type="submit" name="submit" value="OK"/>
</form>