<A HREF="javascript:history.back()" onMouseOver="{window.status='Zurück'; return true;}"> Zur&uuml;ck </A>
<?php
if (isset($errors) && count($errors) > 0) {
    echo '<table class="" width="" align="LEFT" cellpadding="" cellspacing="" border="0">';
    foreach ($errors as $error) {
        ?>
        <tr>
            <td class="register_error_main_box" colspan=""><?php echo $error; ?></td>
        </tr>
    <?php } ?>
    <tr>
        <td class="register_error_main_box" colspan="">&nbsp;</td>
    </tr>
    </table>
    <br clear="all">
<?php } ?>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>?lsid=<?php echo $lsid; ?>&mail_type=<?php echo $mail_type; ?>" method="post" name="send_mail">
    <table class="" width="800" align="LEFT" cellpadding="" cellspacing="" border="0">
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" style="padding-bottom: 5px; padding-top: 30px;">
                <h3>Sende Email</h3>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br><u>Empf&auml;nger:</u><br><br>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="school_email" size="100"
                       value="<?php echo (isset($_POST['school_email']) ? 
                               $_POST['school_email'] : (!empty($contact_details) ? 
                               $contact_details->email_address : '')); ?>"></input>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br><u>Betreff:</u><br><br>
            </td>
        </tr>
        <tr>
            <td>
                <input tabindex="" name="email_subject" size="100"
                       value="<?php echo (isset($_POST['email_subject']) ? 
                               $_POST['email_subject'] : $mail_subject); ?>"></input>
            <td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br><u>Mail-Text:</u><br><br>
                <textarea tabindex="" name="email_body" cols="100" rows="30"><?php 
                echo (isset($_POST['email_body']) ? 
                               $_POST['email_body'] : $mail_body); ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br><input tabindex="" class="" type="submit" name="show_mail_body" value="Formatierung anzeigen"/>
            </td>
        </tr>
        <?php if(isset ($_POST['show_mail_body'])){ ?>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>
                <?php echo $mail_body; ?>
            </td>
        </tr>
        <?php } ?>
            <td class="" valign="top" align="RIGHT" width="" style="padding-top: 20px;">
                <input tabindex="" class="" type="submit" name="send_mail" value="SENDEN"/>
            </td>
        </tr>
    </table>
</form>
<br clear="all">