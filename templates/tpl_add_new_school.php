<?php
if (isset($errors) && count($errors) > 0) {
    echo '<table class="" width="" align="LEFT" cellpadding="" cellspacing="" border="0">';
    foreach ($errors as $error) {
        ?>
        <tr>
            <td class="register_error_main_box" colspan=""><?php echo $error; ?></td>
        </tr>
    <?php } ?>
    <tr>
        <td class="register_error_main_box" colspan="">&nbsp;</td>
    </tr>
    </table>
    <br clear="all">
<?php } ?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>?lsid=<?php echo $lsid; ?>" method="post" name="add_new_school">
    <table class="" width="800" align="LEFT" cellpadding="" cellspacing="" border="0">
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" style="padding-bottom: 5px; padding-top: 30px;">
                <h3>Neue Sprachschule</h3>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Sprachschulname:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="school_name" size="50"></input>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Stra&szlig;e / Hausnummer:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="school_adress_street" size="40"></input> / 
                <input tabindex="" name="school_adress_housenr" size="5"></input>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>PLZ / Ort:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="school_adress_plz" size="10"></input> / 
                <input tabindex="" name="school_adress_city" size="15"></input>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Subdomain:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="school_subdomain" size="20"></input> .go2studienkolleg.de
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Sprachsschul-Email:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="school_email" size="50"></input>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Sprachschul-Tel.:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="school_phone" size="50"></input>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Sprachschul-Homepage:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="school_url" size="50"></input>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Studienkolleg:<br>(weitere k&ouml;nnen in Detailansicht hinzugef&uuml;gt werden)
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <?php echo DynamicFormElements::getStudienkollegs(
            'skid', NULL, 
            ($errors['skid'] != null ? "register_error" : "")); ?>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br><u>Kontakperson:</u>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Anrede:
            </td>
        </tr>
        <tr>
            <td>
                <select name="contactPerson_sex" size="1">
                  <option <?php echo (isset($_POST['contactPerson_sex']) ? 
                               ($_POST['contactPerson_sex'] == "Frau" ? ' selected="selected"' : '') : (!empty($headofschool) ? 
                               ($headofschool->sex == "Frau" ? ' selected="selected"' : '') : '')); ?>>Frau</option>
                  <option <?php echo (isset($_POST['contactPerson_sex']) ? 
                               ($_POST['contactPerson_sex'] == "Herr" ? ' selected="selected"' : '') : (!empty($headofschool) ? 
                               ($headofschool->sex == "Herr" ? ' selected="selected"' : '') : '')); ?>>Herr</option>
                </select>
            <tdp>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Vorname:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="contactPerson_forename" size="50"></input>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Nachname:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="contactPerson_surname" size="50"></input>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br><u>Logindaten:</u>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Login-Name:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="loginData_username" size="50"></input>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Login-Passwort:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="loginData_password" size="50"></input>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br><u>Kommentar:</u>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <textarea tabindex="" name="school_comment" cols="100" rows="5"></textarea>
            </td>
        </tr>
        <tr>
            <td class="" valign="top" align="RIGHT" width="" style="padding-top: 20px;">
                <input tabindex="" class="" type="submit" name="submit_school_data" value="UPDATE"/>
            </td>
        </tr>
    </table>
</form>
<br clear="all">