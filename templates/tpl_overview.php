<h1>&Uuml;bersicht:</h1>
<a href="<?php echo $_SERVER['PHP_SELF']; ?>?showClassRooms=true">Zu den Kursr&auml;umen</a>
<br><br>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="choose_date_diff">
    <select name="date_choose" style="width: 155px;" onChange="javascript:document.choose_date_diff.submit()">
        <option value="">Bitte wählen:</option>
        <option value="today">Heute</option>
        <option value="7">Letzte 7 Tage</option>
        <option value="14">Letzte 14 Tage</option>
        <option value="30">Letzte 30 Tage</option>
        <option value="60">Letzte 60 Tage</option>
        <option value="90">Letzte 90 Tage</option>
    </select>
</form>
<br>
<div id="tests_chart_div"></div>

<?php //echo printUserInputs($date_choose); ?>
<br>
<b><font color="blue">T&auml;gliche-Training-Einheiten:</font></b>
<br>
<?php echo printTrainingInputs($date_choose); ?>
<br>
<b><font color="blue">Irregul&auml;re Verb-Trainings:</font></b>
<br>
<?php echo printIrregularVerbInputs($date_choose); ?>
<br>
<b><font color="blue">Pr&auml;position-Trainings:</font></b>
<br>
<?php echo printPrepositionInputs($date_choose); ?>
