<A HREF="javascript:history.back()" onMouseOver="{window.status='Zurück'; return true;}"> Zur&uuml;ck </A>
<?php
if (isset($errors) && count($errors) > 0) {
    echo '<table class="" width="" align="LEFT" cellpadding="" cellspacing="" border="0">';
    foreach ($errors as $error) {
        ?>
        <tr>
            <td class="register_error_main_box" colspan=""><?php echo $error; ?></td>
        </tr>
    <?php } ?>
    <tr>
        <td class="register_error_main_box" colspan="">&nbsp;</td>
    </tr>
    </table>
    <br clear="all"> 
<?php } ?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>?lsid=<?php echo $lsid; ?>" method="post" name="show_school_details">
    <table class="" width="800" align="LEFT" cellpadding="" cellspacing="" border="0">
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" style="padding-bottom: 5px; padding-top: 30px;">
                <h3>Sprachschule - Detail</h3> 
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Sprachschulname:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="school_name" size="50" 
                       value="<?php echo (isset($_POST['school_name']) ? $_POST['school_name'] : $language_school->school_name); ?>"></input>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Stra&szlig;e / Hausnummer:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="school_adress_street" size="40" 
                       value="<?php echo (isset($_POST['school_adress_street']) ? $_POST['school_adress_street'] : $address->road); ?>"></input> / 
                <input tabindex="" name="school_adress_housenr" size="5" 
                       value="<?php echo (isset($_POST['school_adress_housenr']) ? $_POST['school_adress_housenr'] : $address->house_number); ?>"></input>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>PLZ / Ort:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="school_adress_plz" size="10" 
                       value="<?php echo (isset($_POST['school_adress_plz']) ? $_POST['school_adress_plz'] : $address->postal_code); ?>"></input> / 
                <input tabindex="" name="school_adress_city" size="15" 
                       value="<?php echo (isset($_POST['school_adress_city']) ? $_POST['school_adress_city'] : $address->city); ?>"></input>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Subdomain:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="school_subdomain" size="20" 
                       value="<?php echo (isset($_POST['school_subdomain']) ? 
                               $_POST['school_subdomain'] : $language_school->subdomain); ?>"></input> .go2studienkolleg.de
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Sprachsschul-Email:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="school_email" size="50"
                       value="<?php echo (isset($_POST['school_email']) ? 
                               $_POST['school_email'] : (!empty($contact_details) ? 
                               $contact_details->email_address : '')); ?>"></input>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Sprachschul-Tel.:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="school_phone" size="50" 
                       value="<?php echo (isset($_POST['school_phone']) ? 
                               $_POST['school_phone'] : (!empty($contact_details) ? 
                               $contact_details->phone_number : '')); ?>"></input>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Sprachschul-Homepage:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="school_url" size="50" 
                       value="<?php echo (isset($_POST['school_url']) ? 
                               $_POST['school_url'] : (!empty($contact_details) ? 
                               $contact_details->url : '')); ?>"></input>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br><u>Studienkollegs:</u><br><br>
            </td>
        </tr>
        <?php foreach($studienkollegs as $studienkolleg){ ?>
            <tr>
                <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                    <?php echo $studienkolleg->studienkolleg_name; ?>
                    &nbsp; <a href="<?php echo $_SERVER['PHP_SELF']; ?>?lsid=<?php echo $lsid; ?>&action=deleteStudienkolleg&skid=<?php echo $studienkolleg->skid; ?>"><img src="src/imgs/delete.gif"></a>
                </td>
            </tr>
        <?php }?>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br><u>Weiteres Studienkolleg:</u><br><br>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <?php echo DynamicFormElements::getStudienkollegs(
            'skid', NULL, 
            ($errors['skid'] != null ? "register_error" : "")); ?>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br><u>Kontakperson:</u>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Anrede:
            </td>
        </tr>
        <tr>
            <td>
                <select name="contactPerson_sex" size="1">
                  <option <?php echo (isset($_POST['contactPerson_sex']) ? 
                               ($_POST['contactPerson_sex'] == "Frau" ? ' selected="selected"' : '') : (!empty($headofschool) ? 
                               ($headofschool->sex == "Frau" ? ' selected="selected"' : '') : '')); ?>>Frau</option>
                  <option <?php echo (isset($_POST['contactPerson_sex']) ? 
                               ($_POST['contactPerson_sex'] == "Herr" ? ' selected="selected"' : '') : (!empty($headofschool) ? 
                               ($headofschool->sex == "Herr" ? ' selected="selected"' : '') : '')); ?>>Herr</option>
                </select>
            <tdp>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Vorname:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="contactPerson_forename" size="50" 
                       value="<?php echo (isset($_POST['contactPerson_forename']) ? 
                               $_POST['contactPerson_forename'] : (!empty($headofschool) ? 
                               $headofschool->forename : '')); ?>"></input>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Nachname:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <input tabindex="" name="contactPerson_surname" size="50" 
                       value="<?php echo (isset($_POST['contactPerson_surname']) ? 
                               $_POST['contactPerson_surname'] : (!empty($headofschool) ? 
                               $headofschool->surname : '')); ?>"></input>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br><u>Logindaten:</u>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Login-Name:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <?php
                    if(count($linkedUsers) == 0){
                ?>
                <input tabindex="" name="loginData_username" size="50" 
                       value="<?php echo (isset($_POST['loginData_username']) ? 
                               $_POST['loginData_username'] : (!empty($headofschool) ? 
                               $headofschool->login_nick_name : '')); ?>"></input>
                <?php }else echo $headofschool->login_nick_name; ?>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br>Login-Passwort:
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <?php
                    if(count($linkedUsers) == 0){
                ?>
                <input tabindex="" name="loginData_password" size="50" 
                       value="<?php echo (isset($_POST['loginData_password']) ? 
                               $_POST['loginData_password'] : (!empty($headofschool) ? 
                               'demo' : '')); ?>"></input>
                <?php }else echo "nicht einsehbar"; ?>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br><?php
                if(count($linkedUsers) == 0){
                    echo '<a href="' . $_SERVER['PHP_SELF'] . '?lsid=' . $lsid . '&action=activateTeachersAccount">Aktivieren f&uuml;r Lehrerumgebung</a>';
                } else{
                    echo 'Account bereits aktiviert f&uuml;r Lehrerumgebung!<br>Login-Daten nicht mehr aenderbar!';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br><u>Kommentare:</u><br><br>
            </td>
        </tr>
        <?php foreach($comments as $comment){ ?>
            <tr>
                <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                    <?php echo $comment->insert_date; ?>:<br>
                    <?php echo nl2br($comment->comment_text); ?><br><br>
                </td>
            </tr>
        <?php }?>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <u>Neuer Kommentar:</u><br><br>
                <textarea tabindex="" name="school_comment" cols="100" rows="5"></textarea>
            </td>
        </tr>
        <?php if(empty($language_school->mail) || $language_school->mail == "0000-00-00 00:00:00"){ ?>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br><a href="send_mail.php?lsid=<?php echo $lsid; ?>&mail_type=promotion_mail"><u>Promotion-Mail versenden</u></a>
            </td>
        </tr>
        <?php } else{ ?>
        <tr>
            <td colspan="" width="800" class="" valign="top" align="LEFT" width="">
                <br><b>Promotion-Mail versandt am: <?php echo $language_school->mail;?></b><br>
                <br><a href="send_mail.php?lsid=<?php echo $lsid; ?>&mail_type=promotion_mail"><u>Promotion-Mail ERNEUT versenden</u></a><br>
                <?php if(empty($language_school->price_list_mail) || $language_school->price_list_mail == "0000-00-00 00:00:00"){ ?>
                    <br><a href="send_mail.php?lsid=<?php echo $lsid; ?>&mail_type=price_list_mail"><u>Preisliste versenden</u></a>
                <?php } else{ ?>
                    <br><b>Preisliste versandt am: <?php echo $language_school->price_list_mail;?></b><br>
                    <br><a href="send_mail.php?lsid=<?php echo $lsid; ?>&mail_type=price_list_mail"><u>Preisliste ERNEUT versenden</u></a><br>
                <?php } ?>
                <?php if(empty($language_school->reminder_mail) || $language_school->reminder_mail == "0000-00-00 00:00:00"){ ?>
                    <br><a href="send_mail.php?lsid=<?php echo $lsid; ?>&mail_type=reminder_mail"><u>Erinnerung versenden</u></a>
                <?php } else{ ?>
                    <br><b>Erinnerung versandt am: <?php echo $language_school->price_list_mail;?></b><br>
                    <br><a href="send_mail.php?lsid=<?php echo $lsid; ?>&mail_type=reminder_mail"><u>Erinnerung ERNEUT versenden</u></a><br>
                <?php } ?>
            </td>
        </tr>
        <?php } ?>
        <tr>
            <td class="" valign="top" align="RIGHT" width="" style="padding-top: 20px;">
                    <input tabindex="" class="" type="submit" name="submit_school_data" value="UPDATE"/>
            </td>
        </tr>
    </table>
</form>
    <br clear="all">