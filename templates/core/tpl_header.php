<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
  <META HTTP-EQUIV="content-type" CONTENT="text/html; charset=utf-8"> 
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <script language="JavaScript" src="js/animations.js"></script>
  <script language="JavaScript" src="js/submitOnEnter.js"></script>
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <?php echo $chart_js_code; ?>  
  <title>Online Training</title>
 </head>
 <body>
  <div class="wrapper">
   <div class="wrapper-header">
    <div class="container-box">
     <div class="logo">
      <img src="src/imgs/logo.png" alt="Logo">
     </div>
     <div class="wrapper-navigation">
      <div class="navigation">  
       <a href="overview.php">
        <ul class="main-menu one">
         <div class="menu-one-top">
         </div>
         <li class="menu-title">
          &Uuml;bersicht
         </li>
        </ul>
       </a>
       <script>
        $(".one").mouseover(function () {
        if($(".menu-one-top").is(":hidden")){
        $(".menu-one-top").slideDown(200,function(){
        $(".menu-title");
        }).delay(500);
        }
        });
        $(".one").mouseleave(function () {
        $(".menu-one-top").slideUp(200,function(){
        $(".menu-title");
        });
        });
       </script>   
       <a href="content.php">
        <ul class="main-menu two">
         <div class="menu-two-top">
         </div>
        <li class="menu-title">
         Inhalte
        </li>
        </ul>
       </a>
       <script>
        $(".two").mouseover(function () {
        if($(".menu-two-top").is(":hidden")){
        $(".menu-two-top").slideDown(200,function(){
        $(".menu-title");
        }).delay(500);
        }
        });
        $(".two").mouseleave(function () {
        $(".menu-two-top").slideUp(200,function(){
        $(".menu-title");
        });
        });
       </script>	  
       <a href="correction.php">
        <ul class="main-menu three">
         <div class="menu-three-top">
         </div>
         <li class="menu-title">
          Korrektur
         </li>
        </ul>
       </a>
       <script>
        $(".three").mouseover(function () {
        if($(".menu-three-top").is(":hidden")){
        $(".menu-three-top").slideDown(200,function(){
        $(".menu-title");
        }).delay(500);
        }
        });
        $(".three").mouseleave(function () {
        $(".menu-three-top").slideUp(200,function(){
        $(".menu-title");
        });
        });
       </script>
       <a href="administration.php">
        <ul class="main-menu four">
         <div class="menu-four-top">
         </div>
         <li class="menu-title">
          Verwaltung
         </li>
        </ul>
       </a>
       <script>
        $(".four").mouseover(function () {
        if($(".menu-four-top").is(":hidden")){
        $(".menu-four-top").slideDown(200,function(){
        $(".menu-title");
        }).delay(500);
        }
        });
        $(".four").mouseleave(function () {
        $(".menu-four-top").slideUp(200,function(){
        $(".menu-title");
        });
        });
       </script>   
       <a href="settings.php">
        <ul class="main-menu five">
         <div class="menu-five-top">
         </div>
         <li class="menu-title">
          Einstellungen
         </li>
        </ul>
       </a>
       <script>
        $(".five").mouseover(function () {
        if($(".menu-five-top").is(":hidden")){
        $(".menu-five-top").slideDown(200,function(){
        $(".menu-title");
        }).delay(500);
        }
        });
        $(".five").mouseleave(function () {
        $(".menu-five-top").slideUp(200,function(){
        $(".menu-title");
        });
        });
       </script>	   
      </div> 
     </div>
    </div>
   </div>
   <div class="wrapper-content">
    <div class="container-box">
     <div class="main-content">