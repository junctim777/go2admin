<h1>Verwaltung</h1>
<br><br>
<?php 

echo '<a href="administration.php">User</a> | ';
echo '<a href="administration.php?showUserBookings=true">Buchungen</a> | ';
echo '<a href="administration.php?showStudienkollegs=true">Studienkollegs</a> | ';
echo '<a href="administration.php?showFeedback=true">Feedback</a> | ';
echo '<a href="administration.php?showUserSurvey=true">User-Umfrage</a> | ';
echo '<a href="send_newsletter.php">Newsletter</a> | ';
echo '<a href="administration.php?adminLanguageSchools=true">Akquise</a> | ';
echo '<a href="administration.php?showCodes=true">Rabatt-Code Eingabe</a> | ';
echo '<a href="administration.php?showLogins=true">Logins</a>  | ';
echo '<a href="administration.php?showLogging=true&limit=25">Logs</a><br><br>';


if($_GET['showLogging']) {
    echo printLogging();
}
else if($_GET['adminLanguageSchools']){
   echo printLanguageSchools();  
}
else if($_GET['showUserBookings']){
   echo printUserBookings(); 
}
else if($_GET['showFeedback']){
    echo printFeedback();
}
else if($_GET['showStudienkollegs']){
    echo printStudienkollegs();
}
else if($_GET['showUserSurvey']){
    echo printUserSurvey();
} else{
    if($_GET['showUserDetails']){
        echo printUserDetails($_GET['uid']);
    } else{
        if($_GET['showLogins']){
            echo printUserLogins();
        } else{
            if($_GET['showTests']){
                echo printUserTests($_GET['uid']);
            } else{
                if($_GET['showCodes']){
                    echo printDiscountCodes();
                } else{
                    if($_GET['bid']){
                        echo printBooking($_GET['bid']);
                    } else{
                        echo printUsers(); 
                    } 
                }
            }
        }
    }
}
?>
