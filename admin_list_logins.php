<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    
    function printUserLogins(){
        $query = "SELECT * FROM go2stuko_user_session us, go2stuko_user u WHERE us.uid = u.uid ORDER BY login_date DESC";
        $user_session_datas = Database::getDatasetFromQuery($query);
        $out = '<table>';
        $out .= '<tr>';
            $out .= '<td align="left" valign="top">';
                $out .= '<b>User</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>IP-Adresse</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>Login-Herkunft</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>Login-Datum</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>Logout-Datum</b>';
            $out .= '</td>';
        $out .= '</tr>';
        foreach($user_session_datas as $user_session_data){
            $font = ( strchr ( $user_session_data->ip_address, '138.246.2.' ) == null ? '<font color="black">' : '<font size="1" color="grey">');
            $out .= '<tr>';
                $out .= '<td align="left" valign="top">';
                    $out .= $font . $user_session_data->forename . " " . $user_session_data->surname . '</font>';
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $font . $user_session_data->ip_address . '</font>';
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $font . $user_session_data->login_origin . '</font>';
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $font . $user_session_data->login_date . '</font>';
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $font . $user_session_data->logout_date . '</font>';
                $out .= '</td>';
            $out .= '</tr>';
        }
        $out .= '</table>';
        $out .= '<br>';
        $out .= '<a href="administration.php">Zur&uuml;ck</a>';
        return $out;
    }
    
    
?>