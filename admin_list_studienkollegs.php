<?php

function printStudienkollegs() {
    if (isset($_POST['addNewExam']) && is_numeric($_GET['skid'])) {
        $query = "INSERT INTO go2stuko_studienkolleg_exam (skid, exam_date) VALUES (" . $_GET['skid'] . ", '" . $_POST['exam_date'] . "')";
        mysql_query($query);
    }
    if (!empty($_GET['deleteExam']) && is_numeric($_GET['skexid'])) {
        $query = "DELETE FROM go2stuko_studienkolleg_exam WHERE skexid = " . $_GET['skexid'];
        mysql_query($query);
    }
    $query = "SELECT * FROM go2stuko_studienkolleg";
    $studienkollegs = Database::getDatasetFromQuery($query);
    $out = '<table>';
    $out .= '<tr>';
    $out .= '<td align="left" valign="top">';
    $out .= '<b>ID:</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Name:</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Letzter Newsletter:</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Tage bis Test:</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Pruefungen:</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Neu:</b>';
    $out .= '</td>';
    $out .= '</tr>';
    foreach ($studienkollegs as $studienkolleg) {
        $application_deadline_wise = $studienkolleg->application_deadline_wise_day . "." . $studienkolleg->application_deadline_wise_month . ".";
        $application_deadline_sose = $studienkolleg->application_deadline_sose_day . "." . $studienkolleg->application_deadline_sose_month . ".";
        $query = "SELECT *, DATEDIFF(exam_date, NOW()) AS days_to_exam FROM go2stuko_studienkolleg_exam ske WHERE ske.skid = " . $studienkolleg->skid . " AND ske.exam_date > date(NOW()) ORDER BY ske.exam_date";
        $stk_exams = Database::getDatasetFromQuery($query);
        $next_exam = null;
        if (count($stk_exams > 0)) {
            $next_exam = $stk_exams[0];
            if ($next_exam != null) {
                $query = "SELECT *, DATEDIFF(NOW(), sending_date) AS days_until_last_email FROM go2stuko_mail_log ml WHERE ml.skexid = " . $next_exam->skexid . " ORDER BY sending_date DESC LIMIT 1";
                $last_newsletter = Database::getDatasetFromQuery($query);
                if (count($last_newsletter > 0)) {
                    $last_newsletter = $last_newsletter[0];
                }
            }
        }

        $out .= '<tr>';
        $out .= '<td align="left" valign="top" style="padding-top: 5px; border-top-width:1px;border-top-style:solid;">';
        $out .= $studienkolleg->skid;
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-top: 5px; padding-left: 20px;border-top-width:1px;border-top-style:solid;">';
        $out .= $studienkolleg->studienkolleg_name;
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-top: 5px; padding-left: 20px;border-top-width:1px;border-top-style:solid;">';
        if (!empty($last_newsletter)) {
            $out .= 'vor ' . $last_newsletter->days_until_last_email . ' Tagen';
        }
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-top: 5px; padding-left: 20px;border-top-width:1px;border-top-style:solid;">';
        if (count($stk_exams > 0)) {
            $out .= $next_exam->days_to_exam;
        }
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-top: 5px; padding-left: 20px;border-top-width:1px;border-top-style:solid;">';
        $cnt = 0;
        foreach ($stk_exams as $stk_exam) {
            if ($cnt++ > 0)
                $out .= " | ";
            $next_exam_date = new DateTime($stk_exam->exam_date);
            $out .= $next_exam_date->format("d.m.Y") . ' <a href="' . $_SERVER['php_self'] . '?showStudienkollegs=true&deleteExam=true&skexid=' . $stk_exam->skexid . '"><img src="src/imgs/icons/date_delete.gif"></a>';
        }
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-top: 5px; padding-left: 20px;border-top-width:1px;border-top-style:solid;">';
        $out .= ' <form action="' . $_SERVER['php_self'] . '?showStudienkollegs=true&skid=' . $studienkolleg->skid . '" method="POST"><input name="exam_date" type="text" value="YYYY-MM-DD" style="width:100px;"><input type="submit" name="addNewExam" value="OK"></form>';
        $out .= '</td>';
        $out .= '</tr>';
    }
    $out .= '</table>';
    $out .= '<br>';
    $out .= '<a href="administration.php">Zur&uuml;ck</a>';
    return $out;
}
?>