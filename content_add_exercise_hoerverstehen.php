<?php

    include("./database/Database.php");
    include("./classes/Exercise.php");
    include("./classes/utils/Helper.php");
    
    $action = $_GET['action'];
    $extype = $_GET['extype'];
    $eid = $_GET['eid'];
    $new_slot = $_GET['new_slot'];
    $update_slot = $_GET['update_slot'];
    $exmodus = $_GET['exmodus'];
    
    
    if(empty($eid) && $exmodus != "test" && $exmodus != "training"){
        $errors[modus] = "Es ist kein korrekter &Uuml;bungsmodus gesetzt (Training oder Test)";
    }
    
    if(empty($eid) && $extype != "Hoerverstehen"){
        $errors[type] = "Es ist kein korrekter &Uuml;bungstyp gesetzt (Hoerverstehen)";
    }
    
    Database::establishConnection();
    
    if(isset($_POST['submit_base_data']) || isset($_POST['update_base_data'])){
        $heading = addslashes(strip_tags(trim($_POST['heading'])));
        $introduction_text = addslashes(nl2br(trim($_POST['introduction_text'])));
        if(empty($heading)){
            $errors[heading] = "Die Ueberschrift muss korrekt eingegeben werden!";
        }
        if(empty($introduction_text)){
            $errors[introduction_text] = "Der Aufgabentext muss korrekt eingegeben werden!";
        }
    }
    
    // HIER!!! Ueberpruefe Eckdaten des Files auf Konsistenz
    if(isset($_POST['update_element_data']) || isset($_POST['submit_element_data'])){        
        $element_text = addslashes(nl2br(strip_tags(trim($_POST['element_text']))));
        echo 'bla';
        if(isset($_POST['submit_element_data'])){
            $allowed = array("mp3", "wav");
            $filename  = $_FILES['new_file']['name'];
            $filesize = $_FILES['new_file']['size'];
            $parts = explode(".", $filename);
            if(!in_array($parts[count($parts)-1], $allowed)) {
                $errors[no_points] = "Bitte w&auml;hle ein g&uuml;ltiges Format aus (wav oder mp3)!";
            }
            if($filesize > 8000000){
                $errors[no_points] = "Das Audio-File darf nicht gr&ouml;&szlig;er als 8MB sein!";
            }
        } 
    }
    
    if(isset($_POST['submit_mc_question_data']) || isset($_POST['update_mc_question_data'])){
       
        $question_introduction = addslashes(nl2br(trim($_POST['question_introduction'])));
        $answers = $_POST['answers'];
        $is_correct = $_POST['is_correct'];
        $answer_note = addslashes(nl2br(strip_tags(trim($_POST['answer_note']))));
        $difficulty = nl2br(strip_tags(trim($_POST['difficulty'])));
        $estimated_time = nl2br(strip_tags(trim($_POST['estimated_time'])));
        $content_points = nl2br(strip_tags(trim($_POST['content_points'])));
        $linguistic_points = nl2br(strip_tags(trim($_POST['linguistic_points'])));
        if(empty($answer_note)){
            $answer_note = 'NULL';
        }
        if(($content_points + $linguistic_points) == 0){
            $errors[no_points] = "<b>!!!Es muss zumindestens 1 Inhalts- oder Sprachpunkt f&uuml;r diese Aufgabe vergeben werden!!!</b>";
        }
        if(empty($question_introduction)){
            $errors[question_introduction] = "Der Frage-Text (Aufgaben-Text) muss korrekt eingegeben werden!";
        }
        if(Helper::isArrayEmpty($answers)){
            $errors[solution_texts] = "Es muss zumindestens eine Antwort angegeben werden!";
        }
    } 
    
    if(isset($_POST['submit_yn_question_data']) || isset($_POST['update_yn_question_data'])){
        $yn_questions = $_POST['yn_questions'];
        $is_correct = $_POST['is_correct'];
        $answer_note = addslashes(nl2br(strip_tags(trim($_POST['answer_note']))));
        $difficulty = nl2br(strip_tags(trim($_POST['difficulty'])));
        $estimated_time = nl2br(strip_tags(trim($_POST['estimated_time'])));
        $content_points = nl2br(strip_tags(trim($_POST['content_points'])));
        $linguistic_points = nl2br(strip_tags(trim($_POST['linguistic_points'])));
        if(empty($answer_note)){
            $answer_note = 'NULL';
        }
        if(($content_points + $linguistic_points) == 0){
            $errors[no_points] = "<b>!!!Es muss zumindestens 1 Inhalts- oder Sprachpunkt f&uuml;r diese Aufgabe vergeben werden!!!</b>";
        }
        if(Helper::isArrayEmpty($yn_questions)){
            $errors[solution_texts] = "Es muss zumindestens eine Ja/Nein Frage angegeben werden!";
        }
    }
   
    
    if(isset($_POST['submit_question_data']) || isset($_POST['update_question_data'])){
        $question_text = addslashes(nl2br(strip_tags(trim($_POST['question_text']))));
        $answer_note = addslashes(nl2br(strip_tags(trim($_POST['answer_note']))));
        $difficulty = nl2br(strip_tags(trim($_POST['difficulty'])));
        $estimated_time = nl2br(strip_tags(trim($_POST['estimated_time'])));
        $content_points = nl2br(strip_tags(trim($_POST['content_points'])));
        $linguistic_points = nl2br(strip_tags(trim($_POST['linguistic_points'])));
        if(empty($question_text)){
            $errors[heading] = "Der Frage-Text (Aufgaben-Satz) muss korrekt eingegeben werden!";
        }
        if(empty($answer_note)){
            $errors[answer_note] = "Es muss ein L&ouml;sungshinweis angegeben werden!";
        }
        if(($content_points + $linguistic_points) == 0){
            $errors[no_points] = "Es muss zumindestens 1 Inhalts- oder Sprachpunkt f&uuml;r diese Aufgabe vergeben werden!";
        }
    }
    
    if(count($errors) == 0){
        if(isset($_POST['submit_base_data'])){
            include('./templates/content/add_exercise/dynamic_submit_base_data.php');
        }
        else if(isset($_POST['update_base_data']) && isset($eid)){
            include('./templates/content/add_exercise/dynamic_update_base_data.php');
        }
        if(isset($_POST['submit_element_data'])){
            include('./templates/content/add_exercise/hoerverstehen/dynamic_submit_element_data.php');
        }
        if(isset($_POST['update_element_data'])){
           include('./templates/content/add_exercise/hoerverstehen/dynamic_update_element_data.php');
        }
        if(isset($_POST['submit_mc_question_data'])){
            include('./templates/content/add_exercise/hoerverstehen/dynamic_submit_mc_question_data.php');
        }
        if(isset($_POST['update_mc_question_data'])){
            include('./templates/content/add_exercise/hoerverstehen/dynamic_update_mc_question_data.php');
        }
        if(isset($_POST['submit_yn_question_data'])){
            include('./templates/content/add_exercise/hoerverstehen/dynamic_submit_yn_question_data.php');
        }
        if(isset($_POST['update_yn_question_data'])){
            include('./templates/content/add_exercise/hoerverstehen/dynamic_update_yn_question_data.php');
        }
        
    }
    
    if(!empty($eid)){
        $exercise = new Exercise($eid);
        if(!isset($_POST['update_base_data']) || $update_slot != 1){
            $heading = $exercise->getHeading();
            $introduction_text = $exercise->getIntroductionText();
        }
    } else{
        if($extype == "Hoerverstehen"){
            $introduction_text = '<b>Hören Sie sich den Text <u>zweimal</u> an und beantworten Sie anschließend folgende Fragen dazu:</b>';
        }
    }
    
    Database::closeConnection();
    
    if($extype != null){
        $extype_str = "- " . $extype;
    }

    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */

    include('./templates/core/tpl_header.php');
    
    include('./templates/content/add_exercise/tpl_add_exercise_header.php');
    $slot_no = 1;
    include('./templates/content/add_exercise/tpl_new_base_data.php');
    echo "<br clear=\"all\"><h3>Inhalt</h3>";
    if(!empty($eid)){
        if(count($exercise->exerciseElements) > 0){
            $exerciseElement = $exercise->exerciseElements[0];
            $eeid = $exerciseElement->getEeid();
            $element_text = $exerciseElement->getElementText();
            include('./templates/content/add_exercise/hoerverstehen/tpl_new_element.php');
            
            if($action == "new_mc_question" || $action == "show_mc_question" || 
                    $action == "new_yn_question" || $action == "show_yn_question"){
                if($action == "new_mc_question"){
                    include('./templates/content/add_exercise/hoerverstehen/tpl_new_mc_question.php');
                } else if($action == "show_mc_question"){
                    
                    $question = $exerciseElement->getQuestionFromId($_GET['qid']);
                    $answer_note = $question->getAnswerNote();
                    $difficulty = $question->getDifficulty();
                    $estimated_time = $question->getEstimatedTime();
                    $content_points = $question->getContentPoints();
                    $linguistic_points = $question->getLinguisticPoints();
                
                    $xml = new SimpleXMLElement($question->getQuestionText());
                    $question_introduction = $xml->question_introduction;
                    $answer = $xml->answer;
                    
                    include('./templates/content/add_exercise/hoerverstehen/tpl_new_mc_question.php');
                }
                if($action == "new_yn_question"){
                    include('./templates/content/add_exercise/hoerverstehen/tpl_new_yn_question.php');
                } else if($action == "show_yn_question"){
                    
                    $question = $exerciseElement->getQuestionFromId($_GET['qid']);
                    $answer_note = $question->getAnswerNote();
                    $difficulty = $question->getDifficulty();
                    $estimated_time = $question->getEstimatedTime();
                    $content_points = $question->getContentPoints();
                    $linguistic_points = $question->getLinguisticPoints();
                
                    $xml = new SimpleXMLElement($question->getQuestionText());
                    $yn_question = $xml->question;
                    
                    include('./templates/content/add_exercise/hoerverstehen/tpl_new_yn_question.php');
                }
                
            }
            if(!$update_slot){
                $slot_no++;
                $sentence_no = $slot_no-1;
                if(isset($_POST['submit_question_data']) && ($new_slot == $slot_no)){
                    $question_text = nl2br(strip_tags(trim($_POST['question_text'])));
                    $answer_note = nl2br(strip_tags(trim($_POST['answer_note'])));
                    $difficulty = nl2br(strip_tags(trim($_POST['difficulty'])));
                    $estimated_time = nl2br(strip_tags(trim($_POST['estimated_time'])));
                    $content_points = nl2br(strip_tags(trim($_POST['content_points'])));
                    $linguistic_points = nl2br(strip_tags(trim($_POST['linguistic_points'])));

                } else{
                    $question_text = null;
                    $answer_note = null;
                    $difficulty = null;
                    $estimated_time = null;
                    $content_points = null;
                    $linguistic_points = null;
                }
                echo "<br clear=\"all\">";
                //include('./templates/content/add_exercise/hoerverstehen/tpl_new_question.php');
            }
        } else{
            include('./templates/content/add_exercise/hoerverstehen/tpl_new_element.php');
        }
    }
    include('./templates/content/add_exercise/tpl_add_exercise_footer.php');
    include('./templates/core/tpl_footer.php');
    
    function printQuestionsOfExerciseElement($exerciseElement){
        $out = '<ul>';
        foreach($exerciseElement->getQuestions() as $question){
            switch($question->getQtid()){
                case(4): $action = "show_mc_question"; break;
                case(6): $action = "show_yn_question"; break;
                ++$cnt;
            }
                $out .= '<li style="list-style-type:none;">';
                    $out .= ++$cnt .'. <a href="' . $_SERVER['PHP_SELF'] .'?action=' . $action . '&eid=' . $_GET['eid'] .'&eeid=' . $exerciseElement->dbExerciseElement->eeid .'&qid=' . $question->getQid() . '">Bearbeite ' . $question->getQtName() . ' Frage</a>';
                    //$out .= '&nbsp;&nbsp;&nbsp;&nbsp;<a href="' . $_SERVER['PHP_SELF'] .'?action=delete_question&eid=' . $_GET['eid'] .'&eeid=' . $exerciseElement->dbExerciseElement->eeid .'&qid=' . $question->getQid() . '">-</a>';
                $out .= '</li>';
        }
        $out .= '</ul>';
        return $out;
    }
    
?>
