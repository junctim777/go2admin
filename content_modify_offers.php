<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    include("./database/Database.php");
    include("./classes/utils/DynamicFormElements.php");

    Database::establishConnection();
    
    if(isset($_GET['action']) && $_GET['action'] == 'delete_offer' && !empty($_GET['oid'])){
        $query = "DELETE FROM go2stuko_offer WHERE oid = " . $_GET['oid'];
        mysql_query($query);
    }
    if(isset($_GET['action']) && $_GET['action'] == 'add_package_to_offer' && !empty($_GET['oid'])
            && !empty($_POST['tpid'])){
        $query = "INSERT INTO go2stuko_offer_package_link (oid, tpid) " . 
                    "VALUES (" . $_GET['oid'] . "," . $_POST['tpid'] . ")";
        mysql_query($query);
    }
    if(isset($_GET['action']) && $_GET['action'] == 'delete_offer_package_link' && !empty($_GET['oid'])
            && !empty($_GET['tpid'])){
        $query = "DELETE FROM go2stuko_offer_package_link WHERE oid = " . $_GET['oid'] 
        . " AND tpid = " . $_GET['tpid'];
        mysql_query($query);
    }
    if(isset($_GET['action']) && $_GET['action'] == 'add_language_school_to_offer' && !empty($_GET['oid'])
            && !empty($_POST['lsid'])){
        $query = "INSERT INTO go2stuko_offer_language_school_link (oid, lsid) " . 
                    "VALUES (" . $_GET['oid'] . "," . $_POST['lsid'] . ")";
        mysql_query($query);
    }
    if(isset($_GET['action']) && $_GET['action'] == 'delete_offer_language_school_link' && !empty($_GET['oid'])
            && !empty($_GET['lsid'])){
        $query = "DELETE FROM go2stuko_offer_language_school_link WHERE oid = " . $_GET['oid'] 
        . " AND lsid = " . $_GET['lsid'];
        mysql_query($query);
    }
    include('templates/core/tpl_header.php');
    include('templates/content/tpl_content_modify_offers.php');
    include('templates/core/tpl_footer.php');
    
    Database::closeConnection();
    
    function printOffers(){
        $out = '<table>';
        $out .= '<tr>';
            $out .= '<td align="left" valign="top" style="padding-top: 20px;">';
                $out .= '<b>OID</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                $out .= '<b>Name</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                $out .= '<b>Beschreibung</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                $out .= '<b>Lernpakete</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                $out .= '<b>Sprachschulen</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                $out .= '<b></b>';
            $out .= '</td>';
        $out .= '</tr>';
        $query = "SELECT * FROM go2stuko_offer o ORDER BY o.oid DESC";
        $offers = Database::getDatasetFromQuery($query);
        foreach($offers as $offer){
            $query = "SELECT * FROM go2stuko_offer_package_link opl, go2stuko_test_package tp WHERE tp.tpid = opl.tpid AND opl.oid = " . $offer->oid;
            $packages = Database::getDatasetFromQuery($query);
            $query = "SELECT * FROM go2stuko_offer_language_school_link olsl, go2stuko_language_school ls WHERE ls.lsid = olsl.lsid AND olsl.oid = " . $offer->oid;
            $language_schools = Database::getDatasetFromQuery($query);
            $out .= '<tr>';
                $out .= '<td align="left" valign="top" style="padding-top: 20px;">';
                    $out .= $offer->oid;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                    $out .= '' . $offer->offer_name . '</a>';
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                    $out .= $offer->offer_description ;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                    foreach($packages as $package){
                        $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?action=delete_offer_package_link&oid=' . $offer->oid . '&tpid=' . $package->tpid . '"><img src="src/imgs/delete.gif" border="0"></a> ';
                        $out .= $package->description . "</a><br>";
                    }
                    $out .= '<form action="' . $_SERVER['PHP_SELF'] . '?action=add_package_to_offer&oid=' . $offer->oid . '" method="post" 
                                name="add_package_to_offer">';
                    $out .= '<br><img src="src/imgs/create.gif"> ';
                    $out .= DynamicFormElements::getPackages('tpid', NULL, 
                                ($errors['tpid'] != null ? "register_error" : ""));
                    $out .= '<br><input style="margin-left:30px;" class="" type="submit" name="submit" value="OK"/>';
                    $out .= '</form>';
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                    foreach($language_schools as $language_school){
                        $query = "SELECT * FROM go2stuko_discount_code WHERE oid = " . $offer->oid . " AND lsid = " . $language_school->lsid;
                        $discount_codes = Database::getDatasetFromQuery($query);
                        $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?action=delete_offer_language_school_link&oid=' . $offer->oid . '&lsid=' . $language_school->lsid . '"><img src="src/imgs/delete.gif" border="0"></a> ';
                        $out .= '<a href="show_school_details.php?lsid=' . $language_school->lsid . '">' . $language_school->school_name . "</a> (<a href=\"content_manage_discount_codes.php?oid=" . $offer->oid . "&lsid=" . $language_school->lsid . "\">" . count($discount_codes) . " Lizenzen</a>)<br>";
                    }
                    $out .= '<form action="' . $_SERVER['PHP_SELF'] . '?action=add_language_school_to_offer&oid=' . $offer->oid . '" method="post" 
                                name="add_package_to_offer">';
                    $out .= '<br><img src="src/imgs/create.gif"> ';
                    $out .= DynamicFormElements::getLanguageSchools('lsid', NULL, 
                                ($errors['lsid'] != null ? "register_error" : ""));
                    $out .= '<br><input style="margin-left:30px;" class="" type="submit" name="submit" value="OK"/>';
                    $out .= '</form>';
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                    $out .= '<a href="content_modify_offer.php?oid=' . $offer->oid . '"><img src="src/imgs/edit.gif" border="0"></a>';
                    $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?action=delete_offer&oid=' . $offer->oid . '"><img src="src/imgs/delete.gif" border="0"></a>';
                $out .= '</td>';
            $out .= '</tr>';
        } 
        $out .= '</table>';
        return $out; 
    }
    
?>
