<?php


require_once './classes/utils/class.phpmailer.php';
require_once './classes/utils/class.smtp.php';

function globalMailSend($mail_subject, $mail_body, $user_data){

    //error_reporting(E_ALL);
    //ini_set("display_errors", 1);
    $mail = new PHPMailer(true);
    try {
        $mail->CharSet = "UTF-8";
        $mail->IsSMTP();
        $mail->Host       = "ms-srv.com"; // SMTP server
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->Port       = 25;                    // set the SMTP port for the GMAIL server
        $mail->Username   = "buchhaltung@go2studienkolleg.de"; // SMTP account username
        $mail->Password   = "okuts2gobuchhaltung";        // SMTP account password
        $mail->AddReplyTo('buchhaltung@go2studienkolleg.de', 'Go2 Buchhaltung');

        $mail->AddAddress($user_data->email_address, $user_data->forename . ' ' . $user_data->surname);
        $mail->AddBCC('info@go2studienkolleg.de', 'Go 2 Studienkolleg');

        $mail->SetFrom('buchhaltung@go2studienkolleg.de', 'Go2Studienkolleg Buchhaltung');
        $mail->Subject = $mail_subject;
        $mail->AltBody = 'Bitte nutzen Sie ein HTML-kompatibles Email-Programm um diese Nachricht zu lesen!'; // optional - MsgHTML will create an alternate automatically
        $mail->MsgHTML($mail_body);
//        echo "Testmail Gesendet!<br><br>";
//        echo $newsletter_mail_body . "<br>-------------------------";
        $success_state = $mail->Send();
    } catch (phpmailerException $e) {
        echo $e->errorMessage(); //Pretty error messages from PHPMailer
    } catch (Exception $e) {
        echo $e->getMessage(); //Boring error messages from anything else!
    }

}