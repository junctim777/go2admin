<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    include("./database/Database.php");
    include("./classes/utils/DynamicFormElements.php");
    $skid = $_GET['skid'];
    if(empty($skid)){
        $skid = $_POST['skid'];
    }
    
    Database::establishConnection();
    
    if(isset($_GET['action']) && $_GET['action'] == 'delete_test_package' && !empty($_GET['tpid'])){
        $query = "DELETE FROM go2stuko_test_package WHERE tpid = " . $_GET['tpid'];
        mysql_query($query);
    }
    if(isset($_GET['action']) && $_GET['action'] == 'move_test_up' && !empty($_GET['tpid'])
            && !empty($_GET['tid'])){
        $query = "SELECT package_position FROM go2stuko_package_test_link WHERE tpid = " . $_GET['tpid'] . " AND tid = " . $_GET['tid'];
        $new_position = Database::getDatasetFromQuery($query);
        $new_position = $new_position[0];
        $query = "UPDATE go2stuko_package_test_link SET package_position = package_position+1 WHERE tpid = " . $_GET['tpid'] 
        . " AND package_position = (" . $new_position->package_position . "-1)";
        mysql_query($query);
        $query = "UPDATE go2stuko_package_test_link SET package_position = package_position-1 WHERE tpid = " . $_GET['tpid'] 
        . " AND tid = " . $_GET['tid'];
        mysql_query($query);
        $location = "Location: " . $_SERVER['PHP_SELF'] . "?skid=" . $skid . "#" . $_GET['tpid']; 
        header($location);
    }
    if(isset($_GET['action']) && $_GET['action'] == 'move_test_down' && !empty($_GET['tpid'])
            && !empty($_GET['tid'])){
        $query = "SELECT package_position FROM go2stuko_package_test_link WHERE tpid = " . $_GET['tpid'] . " AND tid = " . $_GET['tid'];
        $new_position = Database::getDatasetFromQuery($query);
        $new_position = $new_position[0];
        $query = "UPDATE go2stuko_package_test_link SET package_position = package_position-1 WHERE tpid = " . $_GET['tpid'] 
        . " AND test_position = (" . $new_position->test_position . "+1)";
        mysql_query($query);
        $query = "UPDATE go2stuko_package_test_link SET package_position = package_position+1 WHERE tpid = " . $_GET['tpid'] 
        . " AND tid = " . $_GET['tid'];
        mysql_query($query);
        $location = "Location: " . $_SERVER['PHP_SELF'] . "?skid=" . $skid . "#" . $_GET['tpid'];
        header($location);
    }
    
    include('templates/core/tpl_header.php');
    include('templates/content/tpl_content_modify_packages.php');
    include('templates/core/tpl_footer.php');

    Database::closeConnection();
    
    function printPackages($skid){
        $out = '<table>';
        $out .= '<tr>';
            $out .= '<td align="left" valign="top" style="padding-top: 20px;">';
                $out .= '<b>ID</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                $out .= '<b>Paketname</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                $out .= '<b>Studienkolleg</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                $out .= '<b>Tests</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                $out .= '<b></b>';
            $out .= '</td>';
        $out .= '</tr>';
        $query = "SELECT * FROM go2stuko_test_package tp, go2stuko_studienkolleg sk " 
                . "WHERE tp.skid = sk.skid AND sk.skid = " . $skid . " ORDER BY tp.tpid";
        $test_packages = Database::getDatasetFromQuery($query);
        foreach($test_packages as $test_packages){
            $query = "SELECT t.*, ptl.* FROM go2stuko_test t, go2stuko_test_package tp, go2stuko_package_test_link ptl " 
                    . "WHERE ptl.tpid = tp.tpid AND ptl.tid = t.tid AND tp.tpid = " . $test_packages->tpid . " ORDER BY ptl.package_position";
            $tests = Database::getDatasetFromQuery($query);
            $out .= '<tr>';
                $out .= '<td align="left" valign="top" style="padding-top: 20px;">';
                    $out .= $test_packages->tpid;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                    $out .= '<a name="' . $test_packages->tpid . '">' . $test_packages->description . '</a>';
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                    $out .= $test_packages->studienkolleg_name;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                    foreach($tests as $test){
                        $query = "SELECT * FROM go2stuko_test_exercise_link tel, go2stuko_exercise_type et, go2stuko_exercise e WHERE e.etid = et.etid AND tel.eid = e.eid AND tel.tid = " . $test->tid . " ORDER BY tel.test_position";
                        $exercises = Database::getDatasetFromQuery($query);
                        $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?skid=' . $skid . '&action=move_test_up&tpid=' . $test_packages->tpid . '&tid=' . $test->tid . '"><img src="src/imgs/arrow_up.gif" border="0"></a> ';
                        $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?skid=' . $skid . '&action=move_test_down&tpid=' . $test_packages->tpid . '&tid=' . $test->tid . '"><img src="src/imgs/arrow_down.gif" border="0"></a> ';
                        //$out .= '<a href="' . $_SERVER['PHP_SELF'] . '?skid=' . $skid . '&action=delete_package_test_link&tid=' . $test_packages->tpid . '&tid=' . $test->tid . '"><img src="src/imgs/delete.gif" border="0"></a> ';
                        $out .= "<b>" . $test->description . " (" . $test->package_position . ")</b><br><div style=\"font-size:11px; padding-left:60px;\"><ul>";
                        foreach($exercises as $exercise){
                            $out .= "<li>" . $exercise->heading . " (" . $exercise->etname . ")</li>";
                        }
                        $out .= '<ul></div><br>';
                    }
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                    $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?skid=' . $skid . '&action=delete_test_packages&tpid=' . $test_packages->tpid . '"><img src="src/imgs/delete.gif" border="0"></a>';
                $out .= '</td>';
            $out .= '</tr>';
        } 
        $out .= '</table>';
        return $out; 
    }
    
?>
