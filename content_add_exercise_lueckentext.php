<?php

include("./database/Database.php");
include("./classes/Exercise.php");
include("./classes/utils/Helper.php");

$extype = $_GET['extype'];
$action = $_GET['action'];
$eid = $_GET['eid'];
$print_mode = $_GET['print_mode'];
$qid = $_POST['qid'];
$ssid = $_POST['ssid'];
$eeid = $_GET['eeid'];
$word = $_GET['word'];
$wordno = $_GET['wordno'];
$action = $_GET['action'];
$new_slot = $_GET['new_slot'];
$update_slot = $_GET['update_slot'];
$exmodus = $_GET['exmodus'];
$newSolution = $_POST['new_solution'];
$has_content_point = $_POST['has_content_point'];


if (empty($eid) && $exmodus != "test" && $exmodus != "training") {
    $errors[modus] = "Es ist kein korrekter &Uuml;bungsmodus gesetzt (Training oder Test)";
}
if (empty($eid) && $extype != "Lueckentext") {
    $errors[type] = "Es ist kein korrekter &Uuml;bungstyp gesetzt (Lueckentext)";
}

Database::establishConnection();

if(!empty($action)){
    if($action == "deleteQuestion"){
        if(!empty($qid)){
            $query = "DELETE FROM go2stuko_question WHERE qid = " . $qid;
            echo $query;
        }
    }
}

if (count($errors) == 0) {
    if (!empty($action) && isset($wordno) && !empty($eeid)) {
        if ($action == 'addAsQuestion') {
            if (!empty($word)) {
                include('./templates/content/add_exercise/lueckentext/dynamic_addWordAsQuestion.php');
            }
        }
        if ($action == 'deleteAsQuestion') {
            include('./templates/content/add_exercise/lueckentext/dynamic_deleteWordAsQuestion.php');
        }
    }
}

if (isset($_POST['submit_base_data']) || isset($_POST['update_base_data'])) {
    $heading = addslashes(strip_tags(trim($_POST['heading'])));
    $introduction_text = addslashes(nl2br(trim($_POST['introduction_text'])));
    if (empty($heading)) {
        $errors[heading] = "Die Ueberschrift muss korrekt eingegeben werden!";
    }
    if (empty($introduction_text)) {
        $errors[introduction_text] = "Der Aufgabentext muss korrekt eingegeben werden!";
    }
}

if (isset($_POST['submit_text_data']) || isset($_POST['update_text_data'])) {
    $lueckentext = addslashes(nl2br(strip_tags(trim($_POST['lueckentext']))));
    $lueckentext = str_replace("<br>", "", $lueckentext);
    $lueckentext = str_replace("<br />", "", $lueckentext);
    $lueckentext = preg_replace('/\s{2,}/sm', ' ', $lueckentext);
    if (empty($lueckentext)) {
        $errors[heading] = "Der Luecken-Text (Aufgaben-Satz) muss korrekt eingegeben werden!";
    }
}

if (count($errors) == 0) {
    if (isset($_POST['submit_base_data'])) {
        include('./templates/content/add_exercise/dynamic_submit_base_data.php');
    } else if (isset($_POST['update_base_data']) && isset($eid)) {
        include('./templates/content/add_exercise/dynamic_update_base_data.php');
    }
    if (isset($_POST['submit_text_data'])) {
        include('./templates/content/add_exercise/lueckentext/dynamic_submit_text_data.php');
    }
    if (isset($_POST['update_text_data'])) {
        include('./templates/content/add_exercise/lueckentext/dynamic_update_element_data.php');
    }
    if (!empty($newSolution) && !empty($qid)){
        include('./templates/content/add_exercise/lueckentext/dynamic_addNewSolution.php');
    }
    if (isset($has_content_point) && !empty($ssid) && !empty($qid)){
        include('./templates/content/add_exercise/lueckentext/dynamic_updateContentPoints.php');
    }
}

if (!empty($eid)) {
    $exercise = new Exercise($eid);
    if (!isset($_POST['update_base_data']) || $update_slot != 1) {
        $heading = $exercise->getHeading();
        $introduction_text = $exercise->getIntroductionText();
    }
} else{
    if($extype == "Lueckentext"){
        $introduction_text = '<b>Setzen Sie bitte in jede Lücke <u>ein</u> Wort ein, das inhaltlich und grammatisch passt!</b>
<i>Beachten Sie, dass es sich um einen zusammenhängenden Text handelt.</i>';
    }
}

Database::closeConnection();

if ($extype != null) {
    $extype_str = "- " . $extype;
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

include('./templates/core/tpl_header.php');

include('./templates/content/add_exercise/tpl_add_exercise_header.php');
$slot_no = 1;
include('./templates/content/add_exercise/tpl_new_base_data.php');
echo "<br clear=\"all\"><h3>Inhalt</h3>";

if (!empty($eid)) {
    if (count($exercise->exerciseElements) == 0) {
        include('./templates/content/add_exercise/lueckentext/tpl_new_text.php');
    }
    foreach ($exercise->exerciseElements as $exerciseElement) {
        $slot_no++;
        $eeid = $exerciseElement->getEeid();
        $element_text = $exerciseElement->getElementText();
        $sentence_no = $exerciseElement->getExercisePosition();
        $words = split(" ", $element_text);
        $sentence = '';
        $cnt = 0;
        foreach ($words as $word) {
            if (isWordAQuestion($cnt, $exerciseElement->questions)) {
                $sentence .= '<a href="' . $_SERVER['PHP_SELF'] . '?eid=' . $eid
                        . '&eeid=' . $eeid . '&wordno=' . $cnt++
                        . '&action=deleteAsQuestion#' . $eeid .'">';
                $sentence .= '___________ ';
                $sentence .= '</a>';
            } else {
                $sentence .= '<a href="' . $_SERVER['PHP_SELF'] . '?eid=' . $eid
                        . '&eeid=' . $eeid . '&wordno=' . $cnt++
                        . '&word=' . Helper::quote2entities($word) . '&action=addAsQuestion#' . $eeid .'">';
                $sentence .= $word . ' ';
                $sentence .= '</a>';
            }
        }
        //$question_list = '<ol>';
        $question_list = '';
        $quest_cnt = 1;
        foreach($exerciseElement->questions as $question){
            //$question_list .= '<li>';
               $question_list  .= $quest_cnt++ . ')';
               $question_list .= '<table width="200" cellpadding="0" cellspacing="0">'; 
               $sol_cnt = 0; 
               foreach($question->sampleSolutions as $sampleSolution){
                    $solution_word = $sampleSolution->wordByWordSolutions[0];
                    $question_list .= '<tr>'; 
                    $question_list .= '<td valign="top" align="left" width="180">'; 
                    $question_list .= '<a href="' . $_SERVER['PHP_SELF'] . '?eid=' . $eid .'&action=delete&qid=' . $question->getQid() . '&ssid=' . $sampleSolution->getSsid() . '">-</a>&nbsp;&nbsp;';
                    if($sampleSolution->getIsBestAnswer()){
                        $question_list .= '' . trim($sampleSolution->getSolutionText()) . ' ';
                    } else{
                        $question_list .= '<a href="">' . trim($sampleSolution->getSolutionText()) . '</a> ';
                    } 
                    $question_list .= '</td><td valign="top" align="left" width="20" style="padding-left: 5px;">'; 
                    $question_list .= '<form action="' . $_SERVER['PHP_SELF'] . '?eid=' . $eid .'#' . $eeid .'" 
                                     method="post" name="content_add_exercise_content_point_' . $sol_cnt++ . '">';
                    $question_list .= '<input type="hidden" name="ssid" value="' . $sampleSolution->getSsid() . '">';
                    $question_list .= '<input type="hidden" name="qid" value="' . $question->getQid() . '">';
                    $question_list .= '<input type="hidden" value="0" name="has_content_point">';
                    $question_list .= '<input type="checkbox" value="1" name="has_content_point" ' . ($solution_word->getContentPoints() == 1 ? 'checked="true"' : '') . ' onClick="this.form.submit()" />';
                    $question_list .= '</form>';
                    $question_list .= '</td></tr>'; 
                }
                $question_list .= '<tr><td valign="top" align="left" width="200" colspan="2">';
                $question_list .= '<form action="' . $_SERVER['PHP_SELF'] . '?eid=' . $eid .'#' . $eeid .'" 
                                     method="post" name="content_add_exercise_solution_data_' . $tabindex . '">';
                $question_list .= '<input type="hidden" name="qid" value="' . $question->getQid() . '">';
                if(empty($print_mode)){
                    $question_list .= '<input type="text" tabindex="' . ++$tabindex . '" class="" name="new_solution" onKeyPress="return submitenter(this,event)">';
                }
                $question_list .= '</form>';
            $question_list .= '</td></tr></table>';
            //$question_list .= '</li>';
        }
        //$question_list .= '</ol>';
        if(isset($update_slot) && $update_slot == $slot_no){
            $lueckentext = $element_text;
            include('./templates/content/add_exercise/lueckentext/tpl_new_text.php');
        } else{
            include('./templates/content/add_exercise/lueckentext/tpl_show_text.php');
        }
    }
}

include('./templates/content/add_exercise/tpl_add_exercise_footer.php');
include('./templates/core/tpl_footer.php');

function isWordAQuestion($word_index, $questions) {
    foreach ($questions as $question) {
        if ($question->getExerciseElementPosition() == $word_index)
            return true;
    }
    return false;
}

?>
