<?php

    include('database/Database.php');
    include('classes/User.php');
    require_once './classes/utils/class.phpmailer.php';
    require_once './classes/utils/class.smtp.php';
    include('classes/utils/DynamicFormElements.php');
    
    $skid = $_POST['skid'];
    if(empty($skid)){
        $skid = $_GET['skid'];
    }

    Database::establishConnection();
    
    $email_subject = $_POST['email_subject'];
    $email_body = $_POST['email_body'];  
    $email_adresses = "";
     
    if(!empty($skid)){
        $query = "SELECT *, DATEDIFF(exam_date, NOW()) AS days_to_exam FROM go2stuko_studienkolleg_exam ske WHERE ske.skid = " . $skid . " AND ske.exam_date > date(NOW()) ORDER BY ske.exam_date";
        $stk_exams = Database::getDatasetFromQuery($query);
        if(count($stk_exams > 0)){
            $next_exam = $stk_exams[0];
            $next_exam_date = new DateTime($next_exam->exam_date);
            $days_to_next_exam = $next_exam->days_to_exam;
            $weeks_to_next_exam = floor(($days_to_next_exam / 7));
        }
    }
    
    if(empty($email_body)){
 
        $mail_body = "";
        $mail_body_tmp = file("src/tpl_emails/newsletter.php");
        foreach ($mail_body_tmp as $mail_body_line) {
            $mail_body .= $mail_body_line;
        }
        
    } else{
        $mail_body = $email_body;
    }
    if(empty($email_subject)){
        $mail_subject = "Go2Studienkolleg Newsletter";
        if(!empty($next_exam)){
            $mail_subject .= ": Noch " . $days_to_next_exam . " Tage bis zur Aufnahmepr&uuml;fung";
        }
    } else{
        $mail_subject = $email_subject;
    }

    if(!empty($skid)){
        $query = "SELECT * FROM go2stuko_user u, go2stuko_student_information si " 
                    . "WHERE u.uid = si.uid AND si.skid = " .$skid . " AND u.created>date_sub(now(),interval 547 day)";
        $query_stk = "SELECT * FROM go2stuko_studienkolleg sk, go2stuko_address a WHERE a.aid = sk.aid AND sk.skid = " . $skid;
        $stk_infos = Database::getDatasetFromQuery($query_stk);
        $stk_infos = $stk_infos[0];
    } else{
        $query = "SELECT * FROM go2stuko_user u";
    }
    $users_data = Database::getDatasetFromQuery($query);
    
    foreach($users_data AS $user_data){
        $newsletter_mail_body = "";
        $newsletter_mail_body = $mail_body;

        $query = "SELECT * FROM go2stuko_user_contact_details cd, go2stuko_user u WHERE u.uid = " . $user_data->uid
                    . " AND cd.cdid = u.study_cdid";
        $user_study_contact_details_data_tmp = Database::getDatasetFromQuery($query);
        
        
        if (count($user_study_contact_details_data_tmp) > 0) {

            $user_study_contact_details_data = $user_study_contact_details_data_tmp[0];
            
            if($user_study_contact_details_data->receive_newsletter == 1){

                if($user_data->sex == "Herr"){
                    $newsletter_mail_body = str_replace('[sex]', "Lieber", $newsletter_mail_body);
                } else if($user_data->sex == "Frau") {
                    $newsletter_mail_body = str_replace('[sex]', "Liebe", $newsletter_mail_body);
                }

                $newsletter_mail_body = str_replace('[prename]', $user_data->forename, $newsletter_mail_body);
                $newsletter_mail_body = str_replace('[surname]', $user_data->surname, $newsletter_mail_body);
                
                $newsletter_mail_body = str_replace('[uid]', ($user_data->uid+45234), $newsletter_mail_body);
                $newsletter_mail_body = str_replace('[ruid]', ($user_data->uid), $newsletter_mail_body);
                if(!empty($stk_infos)){
                    $newsletter_mail_body = str_replace('[stk_city]', $stk_infos->city, $newsletter_mail_body);
                }
                if(!empty($next_exam)){
                    $newsletter_mail_body = str_replace('[date_next_exam]', $next_exam_date->format("d.m.Y"), $newsletter_mail_body);
                    $newsletter_mail_body = str_replace('[days_to_next_exam]', $days_to_next_exam, $newsletter_mail_body);
                    $newsletter_mail_body = str_replace('[weeks_to_next_exam]', $weeks_to_next_exam, $newsletter_mail_body);
                }
                $email_adresses .= ++$cnt. ". Name: " . $user_data->forename . " " . $user_data->surname . "<br>";
                $email_adresses .= "Email: " .$user_study_contact_details_data->email_address . "<br><br>";
                
                if(isset ($_POST['send_newsletter_mail'])){
                    //error_reporting(E_ALL); 
                    //ini_set("display_errors", 1);
                    $mail = new PHPMailer(true);
                    try {
                      $mail->CharSet = "UTF-8";
                      $mail->IsSMTP();
                      $mail->Host       = "ms-srv.com"; // SMTP server
                      $mail->SMTPAuth   = true;                  // enable SMTP authentication
                      $mail->Port       = 25;                    // set the SMTP port for the GMAIL server
                      $mail->Username   = "info@go2studienkolleg.de"; // SMTP account username
                      $mail->Password   = "okuts2goinfo";        // SMTP account password
                      $mail->AddReplyTo('info@go2studienkolleg.de', 'Go2Studienkolleg');
                      $mail->AddAddress($user_study_contact_details_data->email_address, $user_data->forename . ' ' . $user_data->surname);
                      $mail->SetFrom('info@go2studienkolleg.de', 'Go2Studienkolleg');
                      $mail->Subject = $mail_subject;
                      $mail->AltBody = 'Bitte nutzen Sie ein HTML-kompatibles Email-Programm um diese Nachricht zu lesen!'; // optional - MsgHTML will create an alternate automatically
                      $mail->MsgHTML($newsletter_mail_body);
                      //echo "Gesendet!<br><br>";
                      //echo $newsletter_mail_body . "<br>-------------------------";
                      $success_state = $mail->Send();
                      if(!empty($next_exam)){
                        $query = "INSERT INTO go2stuko_mail_log (skexid, uid, sending_date, success_state) VALUES(" . $next_exam->skexid . ", " . $user_data->uid . ", NOW(), " . $success_state . ")";
                        mysql_query($query);
                      }
                    } catch (phpmailerException $e) {
                      echo $e->errorMessage(); //Pretty error messages from PHPMailer
                    } catch (Exception $e) {
                      echo $e->getMessage(); //Boring error messages from anything else!
                    }
                } else if(isset ($_POST['send_test_mail'])){
                    //error_reporting(E_ALL); 
                    //ini_set("display_errors", 1);
                    $mail = new PHPMailer(true);
                    try {
                      $mail->CharSet = "UTF-8";
                      $mail->IsSMTP();
                      $mail->Host       = "ms-srv.com"; // SMTP server
                      $mail->SMTPAuth   = true;                  // enable SMTP authentication
                      $mail->Port       = 25;                    // set the SMTP port for the GMAIL server
                      $mail->Username   = "info@go2studienkolleg.de"; // SMTP account username
                      $mail->Password   = "okuts2goinfo";        // SMTP account password
                      $mail->AddReplyTo('info@go2studienkolleg.de', 'Go2Studienkolleg');
                      $mail->AddAddress("info@go2studienkolleg.de", $user_data->forename . ' ' . $user_data->surname);
                      $mail->SetFrom('info@go2studienkolleg.de', 'Go2Studienkolleg');
                      $mail->Subject = $mail_subject;
                      $mail->AltBody = 'Bitte nutzen Sie ein HTML-kompatibles Email-Programm um diese Nachricht zu lesen!'; // optional - MsgHTML will create an alternate automatically
                      $mail->MsgHTML($newsletter_mail_body);
                      echo "Testmail Gesendet!<br><br>";
                      echo $newsletter_mail_body . "<br>-------------------------";
                      $success_state = $mail->Send();
                      if(!empty($next_exam)){
                        $query = "INSERT INTO go2stuko_mail_log (skexid, uid, sending_date, success_state) VALUES(" . $next_exam->skexid . ", " . $user_data->uid . ", NOW(), " . $success_state . ")";
                        mysql_query($query);
                      }
                    } catch (phpmailerException $e) {
                      echo $e->errorMessage(); //Pretty error messages from PHPMailer
                    } catch (Exception $e) {
                      echo $e->getMessage(); //Boring error messages from anything else!
                    }
                    break;
                } 
            }
        }
    }
    
    if(isset ($_POST['send_newsletter_mail'])){
        header("Location: send_newsletter.php");
    }
    
    include('templates/core/tpl_header.php');
    include('templates/tpl_send_newsletter.php');
    include('templates/core/tpl_footer.php');
    
    Database::closeConnection();
    
?>
