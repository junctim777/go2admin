<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    include('database/Database.php');
    include('classes/User.php');
    include("./classes/utils/DynamicFormElements.php");
    
    $lsid = $_GET['lsid'];
    if(empty($lsid)){
        $lsid = $_POST['lsid'];
    }
     
    $teid = $_GET['teid'];
    $showClassRooms = $_GET['showClassRooms'];
    $date_choose = $_POST['date_choose'];
    if(empty($date_choose)) $date_choose = "today";
    
    Database::establishConnection();
    
    $dataset = printUserInputs($date_choose);
    $no_of_tests_tmp = split("\[", $dataset);
    $no_of_tests = count($no_of_tests_tmp);
    $chart_js_code = '<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawTestsChart);
      function drawTestsChart() {
        var data = google.visualization.arrayToDataTable([
          [\'User & Testname\', \'%\'],' . $dataset . 
          '
        ]);

        var options = {
          title: \'User Tests\',
          chartArea:{left:400,top:50, width:"53%",height:"90%"},
          height: ' . (30 * $no_of_tests) . ',
          vAxis: {textStyle: {color: \'black\', fontSize: 10}}
        };

        var chart = new google.visualization.BarChart(document.getElementById(\'tests_chart_div\'));
        chart.draw(data, options);
      }
    </script>';
    
    include('templates/core/tpl_header.php');
    if(!empty($teid)){
        include('templates/tpl_show_user_inputs.php');
    } else{
        if(!empty($showClassRooms)){
            include('templates/tpl_show_class_rooms.php');
        } else{
            include('templates/tpl_overview.php');
        }
    }
    include('templates/core/tpl_footer.php');
    
    Database::closeConnection();
    
    function printUserInput($teid){
        $out = '<table>';
        $out .= '<tr>';
            $out .= '<td align="left" valign="top">';
                $out .= '<b>QID</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>Antwort</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>Inhaltspunkte</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>Sprachpunkte</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>Absendezeit</b>';
            $out .= '</td>';
        $out .= '</tr>';
        $query = 'SELECT * FROM go2stuko_user_input WHERE teid = ' . $teid;
        $user_inputs_tmp = Database::getDatasetFromQuery($query);
        foreach($user_inputs_tmp as $user_input){
            $out .= '<tr>';
                $out .= '<td align="left" valign="top">';
                    $out .= $user_input->qid;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $user_input->user_answer;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $user_input->content_points;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $user_input->linguistic_points;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $user_input->created;
                $out .= '</td>';
            $out .= '</tr>';
        }
        $out .= '</table>';
        return $out;
    }
    
    function printUserInputs($date_choose){
        $dataset = '';
        $out = '<table>';
        $out .= '<tr>';
            $out .= '<td align="left" valign="top">';
                $out .= '<b>Test</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>User</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>Punkte</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>Start-Zeit</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>End-Zeit</b>';
            $out .= '</td>';
        $out .= '</tr>';
        if($date_choose == "today"){
            $query = "SELECT * FROM go2stuko_test_examination te, go2stuko_test t, go2stuko_user u "
                    . "WHERE te.tid = t.tid AND te.uid = u.uid AND DATE(te.starting_time) = DATE(NOW()) ORDER BY t.is_demo, u.uid, te.starting_time DESC";
        } else{
            $query = "SELECT * FROM go2stuko_test_examination te, go2stuko_test t, go2stuko_user u "
                    . "WHERE te.tid = t.tid AND te.uid = u.uid AND te.starting_time >= DATE_SUB(NOW(), INTERVAL " . $date_choose . " DAY) ORDER BY t.is_demo, u.uid, te.starting_time DESC";
        }
        $user_inputs_tmp = Database::getDatasetFromQuery($query);
        $cnt = 0;
        foreach($user_inputs_tmp as $user_input){
            $query = 'SELECT * FROM go2stuko_language_school_user_link lsul, go2stuko_language_school ls WHERE lsul.lsid = ls.lsid AND lsul.uid = ' . $user_input->uid;
            $language_school = Database::getDatasetFromQuery($query);
            if(count($language_school) == 1){
                $language_school_data = $language_school[0];
            }
            $user_content_points = 0;
            $user_linguistic_points = 0;
            $test_content_points = 0;
            $test_linguistic_points = 0;
            $query = 'SELECT SUM(content_points) AS content_points, SUM(linguistic_points) AS linguistic_points FROM go2stuko_user_input WHERE teid = ' . $user_input->teid;
            $user_points_tmp = Database::getDatasetFromQuery($query);
            $user_points = $user_points_tmp[0];
            $query = 'SELECT SUM(q.content_points) AS content_points, SUM(q.linguistic_points) AS linguistic_points FROM go2stuko_question q, go2stuko_exercise_element ee, go2stuko_exercise e, go2stuko_test_exercise_link tel WHERE tel.eid = e.eid AND ee.eid = e.eid AND q.eeid = ee.eeid AND tel.tid = ' . $user_input->tid;
            $test_points_tmp = Database::getDatasetFromQuery($query);
            $test_points = $test_points_tmp[0];
            if(!empty($user_points->content_points))
                    $user_content_points += $user_points->content_points;
            if(!empty($user_points->linguistic_points))
                    $user_linguistic_points += $user_points->linguistic_points;
            if(!empty($test_points->content_points))
                    $test_content_points += $test_points->content_points;
            if(!empty($test_points->linguistic_points))
                    $test_linguistic_points += $test_points->linguistic_points;

            $user_total_points = $user_content_points + $user_linguistic_points;
            $test_total_points = $test_content_points + $test_linguistic_points;
            $out .= '<tr>';
                $out .= '<td align="left" valign="top">';
                    $dataset .= "['" . substr($user_input->description, 0, 21);
                    $out .= $user_input->description;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= '<a href="' . $_SERVER['SCRIPT_NAME'] . '?teid=' . $user_input->teid . '">';
                    $out .= $user_input->sex . ' ' . $user_input->forename . ' ' . $user_input->surname;
                    $dataset .= " | " . substr(($user_input->forename . ' ' . $user_input->surname), 0, 21);
                    $out .= '</a>';
                    if(count($language_school) == 1){
                        $out .= '<br>' . $language_school_data->school_name;
                    }
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $user_total_points . ' von ' . $test_total_points . ' Punkten (' . round(($user_total_points * 100 / $test_total_points), 0) . '%)';
                    
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $start_time = new DateTime($user_input->starting_time);
                    $out .= $start_time->format("d.m.Y");
                    $dataset .= " | " . $start_time->format("d.m.Y | H:i");
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $user_input->ending_time;
                    $ending_time = new DateTime($user_input->ending_time);
                    $ending_time_str= "";
                    if($user_input->ending_time != null){
                        $ending_time_str = $ending_time->format("H:i");
                    } else{
                        $ending_time_str = "XX:xx";
                    }
                    $dataset .= " - " . $ending_time_str;
                    $dataset .= "',";
                $out .= '</td>';
            $out .= '</tr>';
            $dataset .= round(($user_total_points * 100 / $test_total_points), 0) . "]";
            if(++$cnt != count($user_inputs_tmp)){
                $dataset .= ",";
            }
            
        }
        //echo $dataset;
        $out .= '</table>';
        return $dataset;
    }
    
    function printIrregularVerbInputs($date_choose){
        $out = '<table>';
        $out .= '<tr>';
            $out .= '<td align="left" valign="top">';
                $out .= '<b>User</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>Anzahl</b>';
            $out .= '</td>';
        $out .= '</tr>';
        if($date_choose == "today"){
            $query = "SELECT *, COUNT(*) AS count FROM go2stuko_vocabulary_user_log ul, go2stuko_user u "
                    . "WHERE ul.ivid <> 'NULL' AND ul.uid = u.uid AND DATE(ul.date_last_training) = DATE(NOW()) GROUP BY u.uid";
        } else{
             $query = "SELECT *, COUNT(*) AS count FROM go2stuko_vocabulary_user_log ul, go2stuko_user u "
                . "WHERE ul.ivid <> 'NULL' AND ul.uid = u.uid AND ul.date_last_training >= DATE_SUB(NOW(), INTERVAL " . $date_choose . " DAY) GROUP BY u.uid";
        }
        $user_inputs_tmp = Database::getDatasetFromQuery($query);
        foreach($user_inputs_tmp as $user_input){
            $out .= '<tr>';
                $out .= '<td align="left" valign="top">';
                    $out .= $user_input->sex . ' ' . $user_input->forename . ' ' . $user_input->surname;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $user_input->count;
                $out .= '</td>';
            $out .= '</tr>';
        }
        $out .= '</table>';
        return $out;
    }
    
    function printPrepositionInputs($date_choose){
        $out = '<table>';
        $out .= '<tr>';
            $out .= '<td align="left" valign="top">';
                $out .= '<b>User</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>Anzahl</b>';
            $out .= '</td>';
        $out .= '</tr>';
        if($date_choose == "today"){
            $query = "SELECT *, COUNT(*) AS count FROM go2stuko_vocabulary_user_log ul, go2stuko_user u "
                    . "WHERE ul.pqid <> 'NULL' AND ul.uid = u.uid AND DATE(ul.date_last_training) = DATE(NOW()) GROUP BY u.uid";
        } else{
            $query = "SELECT *, COUNT(*) AS count FROM go2stuko_vocabulary_user_log ul, go2stuko_user u "
                    . "WHERE ul.pqid <> 'NULL' AND ul.uid = u.uid AND ul.date_last_training >= DATE_SUB(NOW(), INTERVAL " . $date_choose . " DAY) GROUP BY u.uid";
        }
        $user_inputs_tmp = Database::getDatasetFromQuery($query);
        foreach($user_inputs_tmp as $user_input){
            $out .= '<tr>';
                $out .= '<td align="left" valign="top">';
                    $out .= $user_input->sex . ' ' . $user_input->forename . ' ' . $user_input->surname;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $user_input->count;
                $out .= '</td>';
            $out .= '</tr>';
        }
        $out .= '</table>';
        return $out;
    }
    
    function printTrainingInputs($date_choose){
        $out = '<table>';
        $out .= '<tr>';
            $out .= '<td align="left" valign="top">';
                $out .= '<b>User</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<b>Anzahl</b>';
            $out .= '</td>';
        $out .= '</tr>';
        if($date_choose == "today"){
            $query = "SELECT *, COUNT(*) AS count FROM go2stuko_vocabulary_user_log ul, go2stuko_user u "
                    . "WHERE ul.eid <> 'NULL' AND ul.uid = u.uid AND DATE(ul.date_last_training) = DATE(NOW()) GROUP BY u.uid";
        } else{
            $query = "SELECT *, COUNT(*) AS count FROM go2stuko_vocabulary_user_log ul, go2stuko_user u "
                    . "WHERE ul.eid <> 'NULL' AND ul.uid = u.uid AND ul.date_last_training >= DATE_SUB(NOW(), INTERVAL " . $date_choose . " DAY) GROUP BY u.uid";
        }
        $user_inputs_tmp = Database::getDatasetFromQuery($query);
        foreach($user_inputs_tmp as $user_input){
            $out .= '<tr>';
                $out .= '<td align="left" valign="top">';
                    $out .= $user_input->sex . ' ' . $user_input->forename . ' ' . $user_input->surname;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $user_input->count;
                $out .= '</td>';
            $out .= '</tr>';
        }
        $out .= '</table>';
        return $out;
    }
    
    function printClassRooms($lsid){
        $query = "SELECT * FROM go2stuko_language_school WHERE lsid = " . $lsid;
        $language_school = Database::getDatasetFromQuery($query);
        $language_school = $language_school[0];
        $query = "SELECT *, DATE_ADD(cr.start_date, INTERVAL o.number_of_weeks WEEK) AS end_date FROM go2stuko_class_room cr, go2stuko_offer o WHERE cr.oid = o.oid AND cr.lsid = " . $lsid . " ORDER BY cr.start_date DESC";
        $class_rooms = Database::getDatasetFromQuery($query);
        foreach($class_rooms as $class_room){
            $query = "SELECT * FROM go2stuko_class_room_user_link crul, go2stuko_user u WHERE u.uid = crul.uid AND crul.crid = " . $class_room->crid;
            $class_room_users = Database::getDatasetFromQuery($query);
            $no_of_logins = 0;
            $no_of_tests = 0;
            $average_percentage_of_tests = 0;
            $total_mins_of_tests = 0;
            $average_mins_of_tests = 0;
            $no_of_demo_tests = 0;
            $average_percentage_of_demo_tests = 0;
            $total_mins_of_demo_tests = 0;
            $average_mins_of_demo_tests = 0;
            $no_of_trainings = 0;
            $no_of_irregular_verbs = 0;
            $no_of_prepositions = 0;
            foreach($class_room_users as $class_room_user){
                $query = "SELECT *, TIME_TO_SEC(TIMEDIFF(te.ending_time, te.starting_time)) AS time_diff_secs FROM go2stuko_test_examination te, go2stuko_test t WHERE te.tid = t.tid AND t.is_demo = 0 AND te.uid = " . $class_room_user->uid;
                $test_examinations = Database::getDatasetFromQuery($query);
                foreach($test_examinations as $test_examination){
                    $user_content_points = 0;
                    $user_linguistic_points = 0;
                    $test_content_points = 0;
                    $test_linguistic_points = 0;
                    $query = 'SELECT SUM(content_points) AS content_points, SUM(linguistic_points) AS linguistic_points FROM go2stuko_user_input WHERE teid = ' . $test_examination->teid;
                    $user_points_tmp = Database::getDatasetFromQuery($query);
                    $user_points = $user_points_tmp[0];
                    $query = 'SELECT SUM(q.content_points) AS content_points, SUM(q.linguistic_points) AS linguistic_points FROM go2stuko_question q, go2stuko_exercise_element ee, go2stuko_exercise e, go2stuko_test_exercise_link tel WHERE tel.eid = e.eid AND ee.eid = e.eid AND q.eeid = ee.eeid AND tel.tid = ' . $test_examination->tid;
                    $test_points_tmp = Database::getDatasetFromQuery($query);
                    $test_points = $test_points_tmp[0];
                    if(!empty($user_points->content_points))
                            $user_content_points += $user_points->content_points;
                    if(!empty($user_points->linguistic_points))
                            $user_linguistic_points += $user_points->linguistic_points;
                    if(!empty($test_points->content_points))
                            $test_content_points += $test_points->content_points;
                    if(!empty($test_points->linguistic_points))
                            $test_linguistic_points += $test_points->linguistic_points;

                    $user_total_points = $user_content_points + $user_linguistic_points;
                    $test_total_points = $test_content_points + $test_linguistic_points;
                    $percentage = $user_total_points * 100 / $test_total_points;
                    $test_duration_mins = $test_examination->time_diff_secs / 60;
                    if($test_duration_mins > 1){
                        $total_mins_of_tests += $test_duration_mins;
                    }
                    if($percentage > 0.2){
                        $no_of_tests++;
                        $average_percentage_of_tests = ($average_percentage_of_tests * ($no_of_tests-1) + $percentage) / $no_of_tests;
                        $average_mins_of_tests = ($average_mins_of_tests * ($no_of_tests-1) + $test_duration_mins) / $no_of_tests;
                    }
                }
                $query = "SELECT *, TIME_TO_SEC(TIMEDIFF(te.ending_time, te.starting_time)) AS time_diff_secs FROM go2stuko_test_examination te, go2stuko_test t WHERE te.tid = t.tid AND t.is_demo = 1 AND te.uid = " . $class_room_user->uid;
                $test_examinations = Database::getDatasetFromQuery($query);
                foreach($test_examinations as $test_examination){
                    $user_content_points = 0;
                    $user_linguistic_points = 0;
                    $test_content_points = 0;
                    $test_linguistic_points = 0;
                    $query = 'SELECT SUM(content_points) AS content_points, SUM(linguistic_points) AS linguistic_points FROM go2stuko_user_input WHERE teid = ' . $test_examination->teid;
                    $user_points_tmp = Database::getDatasetFromQuery($query);
                    $user_points = $user_points_tmp[0];
                    $query = 'SELECT SUM(q.content_points) AS content_points, SUM(q.linguistic_points) AS linguistic_points FROM go2stuko_question q, go2stuko_exercise_element ee, go2stuko_exercise e, go2stuko_test_exercise_link tel WHERE tel.eid = e.eid AND ee.eid = e.eid AND q.eeid = ee.eeid AND tel.tid = ' . $test_examination->tid;
                    $test_points_tmp = Database::getDatasetFromQuery($query);
                    $test_points = $test_points_tmp[0];
                    if(!empty($user_points->content_points))
                            $user_content_points += $user_points->content_points;
                    if(!empty($user_points->linguistic_points))
                            $user_linguistic_points += $user_points->linguistic_points;
                    if(!empty($test_points->content_points))
                            $test_content_points += $test_points->content_points;
                    if(!empty($test_points->linguistic_points))
                            $test_linguistic_points += $test_points->linguistic_points;

                    $user_total_points = $user_content_points + $user_linguistic_points;
                    $test_total_points = $test_content_points + $test_linguistic_points;
                    $percentage = $user_total_points * 100 / $test_total_points;
                    $test_duration_mins = $test_examination->time_diff_secs / 60;
                    if($test_duration_mins > 1){
                        $total_mins_of_demo_tests += $test_duration_mins;
                    }
                    if($percentage > 0.2){
                        $no_of_demo_tests++;
                        $average_percentage_of_demo_tests = ($average_percentage_of_demo_tests * ($no_of_demo_tests-1) + $percentage) / $no_of_demo_tests;
                        $average_mins_of_demo_tests = ($average_mins_of_demo_tests * ($no_of_demo_tests-1) + $test_duration_mins) / $no_of_demo_tests;
                    }
                }
                $query = "SELECT *, COUNT(*) AS count FROM go2stuko_vocabulary_user_log ul, go2stuko_user u "
                    . "WHERE ul.ivid <> 'NULL' AND ul.uid = u.uid AND u.uid = " . $class_room_user->uid;
                $user_irregular_verb_trainings = Database::getDatasetFromQuery($query);
                $user_irregular_verb_trainings = $user_irregular_verb_trainings[0];
                $no_of_irregular_verbs += $user_irregular_verb_trainings->count;
                
                $query = "SELECT *, COUNT(*) AS count FROM go2stuko_vocabulary_user_log ul, go2stuko_user u "
                    . "WHERE ul.pqid <> 'NULL' AND ul.uid = u.uid AND u.uid = " . $class_room_user->uid;
                $user_pq_trainings = Database::getDatasetFromQuery($query);
                $user_pq_trainings = $user_pq_trainings[0];
                $no_of_prepositions += $user_pq_trainings->count;
                
                $query = "SELECT *, COUNT(*) AS count FROM go2stuko_vocabulary_user_log ul, go2stuko_user u "
                    . "WHERE ul.eid <> 'NULL' AND ul.uid = u.uid AND u.uid = " . $class_room_user->uid;
                $user_e_trainings = Database::getDatasetFromQuery($query);
                $user_e_trainings = $user_e_trainings[0];
                $no_of_trainings += $user_e_trainings->count;
                $query = "SELECT *, COUNT(*) AS count FROM go2stuko_user_session WHERE uid = " . $class_room_user->uid;
                $user_sessions = Database::getDatasetFromQuery($query);
                $user_sessions = $user_sessions[0];
                $no_of_logins += $user_sessions->count;
            }
            echo '<b><a target="_blank" href="http://lehrer.go2studienkolleg.de/login.php?admin_login=Go2AdminLogin&admin_uid=' . $language_school->headofschool_uid . '">' . $class_room->class_room_name . '</a></b><br>';
            echo '<table>';
                echo '<tr><td><i>Start-Datum:</i></td><td style="padding-left: 30px;">' . $class_room->start_date . '</td></tr>';
                echo '<tr><td><i>End-Datum:</i></td><td style="padding-left: 30px;">' . $class_room->end_date . '</td></tr>';
                echo '<tr><td><i>Angebot:</i></td><td style="padding-left: 30px;"><a target="_blank" href="content_modify_offers.php">' . $class_room->offer_name . '</a> - ' . $class_room->number_of_weeks . ' Wochen - ' . $class_room->tests_per_week . ' Test(s)/Woche</td></tr>';
                echo '<tr><td><i>Sch&uuml;ler:</i></td><td style="padding-left: 30px;">' . count($class_room_users) . '</td></tr>';
                echo '<tr><td><i>Bearbeitete Tests:</i></td><td style="padding-left: 30px;">' . $no_of_tests . ' (&Oslash; ' . round($no_of_tests/count($class_room_users), 1) . ', &Oslash; ' . round($average_percentage_of_tests, 1) . '%, &Oslash; ' . round($average_mins_of_tests,1) . 'mins, Total: ' . round($total_mins_of_tests/60,1) . 'std)</td></tr>';
                echo '<tr><td><i>Bearbeitete Demo-Tests:</i></td><td style="padding-left: 30px;">' . $no_of_demo_tests . ' (&Oslash; ' . round($no_of_demo_tests/count($class_room_users), 1) . ', &Oslash; ' . round($average_percentage_of_demo_tests, 1) . '%, &Oslash; ' . round($average_mins_of_demo_tests,1) . 'mins, Total: ' . round($total_mins_of_demo_tests/60,1) . 'std)</td></tr>';
                echo '<tr><td><i>Trainings:</i></td><td style="padding-left: 30px;">' . $no_of_trainings . ' (&Oslash; ' . round($no_of_trainings/count($class_room_users), 1) . ')</td></tr>';
                echo '<tr><td><i>Irregul&auml;re Verben:</i></td><td style="padding-left: 30px;">' . $no_of_irregular_verbs . ' (&Oslash; ' . round($no_of_irregular_verbs/count($class_room_users), 1) . ')</td></tr>';
                echo '<tr><td><i>Pr&auml;positionen:</i></td><td style="padding-left: 30px;">' . $no_of_prepositions . ' (&Oslash; ' . round($no_of_prepositions/count($class_room_users), 1) . ')</td></tr>';
                echo '<tr><td><i>Logins:</i></td><td style="padding-left: 30px;">' . $no_of_logins . ' (&Oslash; ' . round($no_of_logins/count($class_room_users), 1) . ')</td></tr>';
            echo '</table>';
            echo '<br><br>';
        }
    }
    
?>
