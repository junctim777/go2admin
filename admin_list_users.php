<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function printUsers() {

    if (isset($_GET['validateUser'])) {
        validateUser();
    }

    $out = '<a href="administration.php?showAll=true">show all</a><br>';
    $out .= '<table>';
    $out .= '<tr>';
    $out .= '<td align="left" valign="top">';
    $out .= '<b>UID</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Buchungen</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Name</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Email-Adresse</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>News-<br>letter</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Studienkolleg</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Registrierungs-Datum</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Best&auml;tigung</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Login</b>';
    $out .= '</td>';
    $out .= '</tr>';
    if ($_GET['showAll'] == "true") {
        $query = "SELECT * FROM go2stuko_user "
                . "ORDER BY created DESC";
    } else {
        $query = "SELECT * FROM go2stuko_user "
                . "ORDER BY created DESC LIMIT 50";
    }
    $users_tmp = Database::getDatasetFromQuery($query);
    foreach ($users_tmp as $user_tmp) {
        $user = new User($user_tmp->uid);
        $user_data = $user->getUserData();
        $contact_data = $user->getContactDetails();
        $login_data = $user->getLoginData();
        $study_address = $user->getStudyAddressData();
        $home_address = $user->getHomeAddressData();
        $studentInfos = $user->getStudentInformation();
        $booking_datas = $user->getUserBookingData();
        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= '<a href="administration.php?showUserDetails=true&uid=' . $user_data->uid . '">' . $user_data->uid . "</a>";
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
        foreach ($booking_datas as $booking_data) {
            $out .= "<a href=\"" . $_SERVER['PHP_SELF'] . "?bid=" . $booking_data->bid . "\">" . $booking_data->bid . "</a><br>";
        }
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
        $out .= "<a href=\"" . $_SERVER['PHP_SELF'] . "?showTests=true&uid=" . $user_data->uid
                . "\">" . $user_data->sex . ' ' . $user_data->forename . ' '
                . $user_data->surname . "</a><br>-> " . $login_data->login_nick_name;
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
        $out .= '<a href="mailto:' . $contact_data->email_address . '">mail</a>';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
        $out .= $contact_data->receive_newsletter;
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 15px;">';
        $out .= $studentInfos->studienkolleg_name . "<br>";
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
        $out .= $user_data->created;
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px;">';

        if ($user_data->validated != null)
            $out .= $user_data->validated;
        else {
            if (isset($_GET['showAll'])) {
                $out .= "<a href=\"" . $_SERVER['PHP_SELF'] . "?showAll=true&validateUser=true&uid=" . $user_data->uid . " \">bestätigen</a>";
            } else {
                $out .= "<a href=\"" . $_SERVER['PHP_SELF'] . "?validateUser=true&uid=" . $user_data->uid . " \">bestätigen</a>";
            }
        }

        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
        $out .= '<a target="blank" href="http://campus.go2studienkolleg.de?uid=' . $user_data->uid . '&isPayingUser=yes"><img src="src/imgs/page_right.gif"></a>';
        $out .= '</td>';
        $out .= '</tr>';
    }
    $out .= '</table>';
    return $out;
}

?>