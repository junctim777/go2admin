<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    include("./database/Database.php");
    include("./classes/Exercise.php");
    include("./classes/utils/Helper.php");

    $eid = $_GET['eid'];
    $ssid = $_GET['ssid'];
    $sentence_position = $_GET['sentence_position'];
    $ssid_parent = $_GET['ssid_parent'];
    $sentence_position_parent = $_GET['sentence_position_parent'];
    $action = $_GET['action'];
    $qid = $_GET['qid'];
    
    if(empty($eid)){
        $errors[noExercise] = "Es ist keine &Uuml;bung zur Punktevergabe gesetzt";
    }
    
    Database::establishConnection();
    
    if(!empty($action) && !empty($ssid) && !empty($sentence_position)){
        if($action == 'addpoint_content'){
            $query = "UPDATE go2stuko_word_by_word_solution" .
                " SET " .
                "content_points = content_points+1 " . 
                " WHERE ssid = " . $ssid . " AND sentence_position = " . $sentence_position;
            $success = mysql_query($query);
            if(! $success){
                $errors['database_failure_add_content_point'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Fuege Content-Punkt hinzu - go2stuko_word_by_word_solution)";
            } else{
                unset($_GET['ssid']);
                unset($_GET['sentence_position']);
            }
        }
        if($action == 'removepoint_content'){
            $query = "UPDATE go2stuko_word_by_word_solution" .
                " SET " .
                "content_points = content_points-1 " . 
                " WHERE ssid = " . $ssid . " AND sentence_position = " . $sentence_position;
            $success = mysql_query($query);
            if(! $success){
                $errors['database_failure_remove_content_point'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Reduziere Content-Punkt - go2stuko_word_by_word_solution)";
            } else{
                unset($_GET['ssid']);
                unset($_GET['sentence_position']);
            }
        }
        if($action == 'addpoint_linguistic'){
            $query = "UPDATE go2stuko_word_by_word_solution" .
                " SET " .
                "linguistic_points = linguistic_points+1 " . 
                " WHERE ssid = " . $ssid . " AND sentence_position = " . $sentence_position;
            $success = mysql_query($query);
            if(! $success){
                $errors['database_failure_add_linguistic_point'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Fuege Sprach-Punkt hinzu - go2stuko_word_by_word_solution)";
            } else{
                unset($_GET['ssid']);
                unset($_GET['sentence_position']);
            }
        }
        if($action == 'removepoint_linguistic'){
            $query = "UPDATE go2stuko_word_by_word_solution" .
                " SET " .
                "linguistic_points = linguistic_points-1 " . 
                " WHERE ssid = " . $ssid . " AND sentence_position = " . $sentence_position;
            $success = mysql_query($query);
            if(! $success){
                $errors['database_failure_remove_linguistic_point'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Reduziere Sprach-Punkt - go2stuko_word_by_word_solution)";
            } else{
                unset($_GET['ssid']);
                unset($_GET['sentence_position']);
            }
        }
        if($action == 'addparentlink'){
            if(!empty($ssid_parent) && !empty($sentence_position_parent)){
                if($ssid_parent != $ssid || $sentence_position != $sentence_position_parent){
                    $query = "UPDATE go2stuko_word_by_word_solution" .
                        " SET " .
                        "parent_link_sentence_position = " . $sentence_position_parent . 
                        " WHERE ssid = " . $ssid . " AND sentence_position = " . $sentence_position;
                    $success = mysql_query($query);
                    if(! $success){
                        $errors['database_failure_add_parent_link'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Fuege Parent-Link hinzu - go2stuko_word_by_word_solution)";
                    } else{
                        unset($_GET['ssid']);
                        unset($_GET['sentence_position']);
                    }
                } else{
                    $errors['database_failure_add_parent_link'] = "Bitte ein anderes Wort aus dem selben L&ouml;sungssatz ausw&auml;hlen!";
                }
            }
        }
        if($action == 'removeparentlink'){
            $query = "UPDATE go2stuko_word_by_word_solution" .
                " SET " .
                "parent_link_sentence_position = NULL " . 
                " WHERE ssid = " . $ssid . " AND sentence_position = " . $sentence_position;
            $success = mysql_query($query);
            if(! $success){
                $errors['database_failure_remove_parent_link'] = "Sorry. Es ist ein Problem mit der Datenbank-Eingabe aufgetreten (Entferne Parent-Link - go2stuko_word_by_word_solution)";
            } else{
                unset($_GET['ssid']);
                unset($_GET['sentence_position']);
            }
        }
    }
    
    

    include('./templates/core/tpl_header.php');
    include('./templates/content/add_exercise/tpl_assign_credit_points.php');
    include('./templates/core/tpl_footer.php');
    
    Database::closeConnection();
    
    /**
     *
     * @param type $eid
     * @return string 
     */
    function printSolutions($eid){
        $exercise = new Exercise($eid);
        $out = '<tr><td><table width="800" cellpadding="0" cellspacing="0">';
        foreach($exercise->exerciseElements as $exerciseElement){
            $question = $exerciseElement->getFirstQuestion();
            $introduction_text = $exerciseElement->getElementText();
            $question_text = $question->getQuestionText();
            $content_points = $question->getContentPoints();
            $linguistic_points = $question->getLinguisticPoints();
            
            $out .= '<tr><td colspan="2" align="left" valign="top" style="padding-top: 30px;">';
                $out .= '<a name="' . $question->getQid() . '"><b>' . $question_text . '</b></a>';
            $out .= '</td></tr>';
            $out .= '<tr><td width="100" align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<u>Inhaltspunkte:</u></td>';
                $out .= '<td align="left" valign="top">' . $content_points . "</td>";
            $out .= "</tr>";
            $out .= '<tr><td width="100" align="left" valign="top" style="padding-left: 20px;">';
                $out .= '<u>Sprachpunkte:</u></td>';
                $out .= '<td align="left" valign="top" style="padding-bottom: 10px;">' . $linguistic_points . "</td>";
            $out .= "</tr>";
            foreach($question->sampleSolutions as $samleSolution){
                $out .= '<tr><td colspan="2" align="left" valign="top" style="padding-left: 40px;">';
                    $out .= '<table cellpadding="0" cellspacing="0">';
                        $out .= '<tr><td align="center" valign="top" style="padding-right: 10px;">IP:</td>';
                            foreach($samleSolution->wordByWordSolutions as $word){
                                $out .= '<td align="center" valign="top" style="padding-right: 5px;">';
                                    $out .= $word->getContentPoints() . ' ';
                                    $out .= '<a href="' . $_SERVER['PHP_SELF'] 
                                    . '?eid=' . $eid . '&ssid=' . $word->getSsid() 
                                    . '&sentence_position=' . $word->getSentencePosition() 
                                    . '&action=addpoint_content#' . $question->getQid() . '">+</a>';
                                    $out .= '<a href="' . $_SERVER['PHP_SELF'] 
                                    . '?eid=' . $eid . '&ssid=' . $word->getSsid() 
                                    . '&sentence_position=' . $word->getSentencePosition() 
                                    . '&action=removepoint_content#' . $question->getQid() . '">-</a>';
                                $out .= '</td>';
                            }
                        $out .= '</tr>';
                        $out .= '<tr><td align="center" valign="top" style="padding-right: 10px;">SP:</td>';
                            foreach($samleSolution->wordByWordSolutions as $word){
                                $out .= '<td align="center" valign="top" style="padding-right: 5px;">';
                                    $out .= $word->getLinguisticPoints() . ' ';
                                    $out .= '<a href="' . $_SERVER['PHP_SELF'] 
                                    . '?eid=' . $eid . '&ssid=' . $word->getSsid() 
                                    . '&sentence_position=' . $word->getSentencePosition() 
                                    . '&action=addpoint_linguistic#' . $question->getQid() . '">+</a>';
                                    $out .= '<a href="' . $_SERVER['PHP_SELF'] 
                                    . '?eid=' . $eid . '&ssid=' . $word->getSsid() 
                                    . '&sentence_position=' . $word->getSentencePosition() 
                                    . '&action=removepoint_linguistic#' . $question->getQid() . '">-</a>';
                                $out .= '</td>';
                            }
                        $out .= '</tr>';
                        $out .= '<tr><td align="center" valign="top" style="padding-right: 10px;"></td>';
                            foreach($samleSolution->wordByWordSolutions as $word){
                                $out .= '<td align="left" valign="top" style="padding-right: 5px;">';
                                    $out .= '<a style="color: black;" href="' . $_SERVER['PHP_SELF'] 
                                    . '?eid=' . $eid . '&ssid_parent=' . $_GET['ssid'] 
                                    . '&sentence_position_parent=' . $_GET['sentence_position'] . '&ssid=' . $word->getSsid() 
                                    . '&sentence_position=' . $word->getSentencePosition() 
                                    . '&action=addparentlink#' . $question->getQid() . '">';
                                    $out .= '<font color=';
                                    $out .= ($_GET['ssid'] == $word->getSsid() 
                                    && $_GET['sentence_position'] == $word->getSentencePosition())
                                            ? '"red">' : '"black">';
                                    $out .= $word->getValue() . '</a>';
                                    $out .= '</font>';
                                $out .= '</td>';
                            }
                        $out .= '</tr>';
                        for($i=1; $i<=count($samleSolution->wordByWordSolutions); $i++){
                            $word = $samleSolution->wordByWordSolutions[$i-1];
                            if($word->getParentLinkSentencePosition() != null){
                                $out .= '<tr><td align="center" valign="top" style="padding-right: 10px;">';
                                $out .= '<a href="' . $_SERVER['PHP_SELF'] 
                                    . '?eid=' . $eid . '&ssid=' . $word->getSsid() 
                                    . '&sentence_position=' . $word->getSentencePosition() 
                                    . '&action=removeparentlink#' . $question->getQid() . '">-</a>';
                                $out .= '</td>';
                                for($j=1; $j<=count($samleSolution->wordByWordSolutions); $j++){
                                    $out .= '<td align="left" valign="top" style="padding-right: 5px;">';
                                    if($j == $i || $j == $word->getParentLinkSentencePosition()){
                                        $out .= 'X';
                                    }
                                    $out .= '</td>';
                                }
                                $out .= '</tr>';
                            }
                        }
                    $out .= '</table>';
                $out .= '</td></tr>';
            }
        }
        $out .= '</table></td></tr>';
        return $out;
    }
    
?>
