<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include("./database/Database.php");
    include("./classes/utils/DynamicFormElements.php");

    Database::establishConnection();
    
    if(isset($_POST['submit']) && $_GET['oid']
            && !empty($_POST['offer_name']) && !empty($_POST['offer_description'])
            && !empty($_POST['number_of_weeks']) && !empty($_POST['total_sum'])
            && !empty($_POST['tests_per_week']) && !empty($_POST['redemption_notice'])){
        $query = "UPDATE go2stuko_offer SET "
                . "offer_name = '" . $_POST['offer_name'] . "', "
                . "offer_description = '" . $_POST['offer_description'] . "', "
                . "number_of_weeks = " . $_POST['number_of_weeks'] . ", "
                . "tests_per_week = " . $_POST['tests_per_week'] . ", "
                . "total_sum = '" . $_POST['total_sum'] . "', "
                . "redemption_notice = '" . $_POST['redemption_notice'] . "' "
                . "WHERE oid = " . $_GET['oid']; 
        mysql_query($query);
        header('Location: content_modify_offers.php');
    }
    
    $query = "SELECT * FROM go2stuko_offer o WHERE oid = " . $_GET['oid'];
    $offer = Database::getDatasetFromQuery($query);
    $offer = $offer[0];
    
    include('templates/core/tpl_header.php');
    include('templates/content/tpl_content_modify_offer.php');
    include('templates/core/tpl_footer.php');
    
    Database::closeConnection();