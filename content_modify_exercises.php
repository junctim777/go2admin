<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    include("./database/Database.php");
    include("./classes/utils/DynamicFormElements.php");
    
    //error_reporting(E_ALL);
    
    Database::establishConnection();
    
    if(isset($_GET['action']) && $_GET['action'] == 'delete_exercise' && !empty($_GET['eid'])){
        $query = "DELETE FROM go2stuko_exercise WHERE eid = " . $_GET['eid'];
        mysql_query($query);
    }
    
    include('templates/core/tpl_header.php');
    include('templates/content/tpl_content_modify_exercises.php');
    include('templates/core/tpl_footer.php');
    
    Database::closeConnection();
    
    function printExercises(){
        $out = '<table>';
        $query = "SELECT * FROM go2stuko_exercise e, go2stuko_exercise_type et "
                ."WHERE e.etid = et.etid " . (empty($_POST['etid']) ? '' : 'AND e.etid = ' . $_POST['etid']) . " ORDER BY e.created DESC";
        $exercises = Database::getDatasetFromQuery($query);
        $ex_type = $exercises[0]->etid;
        $out .= '<tr><td colspan="7">Aufgabenanzahl: ' . count($exercises) . '<br><br></td></tr>';
        if($ex_type == 8){
            $twenty_point_cnt = 0;
            foreach($exercises as $exercise){
                $query = 'SELECT SUM(q.content_points) AS content_points, SUM(q.linguistic_points) AS linguistic_points FROM go2stuko_question q, go2stuko_exercise_element ee, go2stuko_exercise e WHERE ee.eid = e.eid AND q.eeid = ee.eeid AND e.eid = ' . $exercise->eid;
                $exercise_points_tmp = Database::getDatasetFromQuery($query);
                $exercise_points = $exercise_points_tmp[0];
                if(($exercise_points->content_points + $exercise_points->linguistic_points) == 20){
                    $twenty_point_cnt++;
                }
            }
            $out .= '<tr><td colspan="7">Davon : ' . $twenty_point_cnt . ' mit 20 Punkten.<br><br></td></tr>';
        }
        foreach($exercises as $exercise){
            $query = 'SELECT SUM(q.content_points) AS content_points, SUM(q.linguistic_points) AS linguistic_points FROM go2stuko_question q, go2stuko_exercise_element ee, go2stuko_exercise e WHERE ee.eid = e.eid AND q.eeid = ee.eeid AND e.eid = ' . $exercise->eid;
            $exercise_points_tmp = Database::getDatasetFromQuery($query);
            $exercise_points = $exercise_points_tmp[0];
            $exercise_total_points = $exercise_points->content_points + $exercise_points->linguistic_points;
            switch($exercise->etid){
                case(5):
                    $url = 'content_add_exercise_bildbeschreibung.php';
                    break;
                case(6):
                    $url = 'content_add_exercise_lueckentext.php';
                    break;
                case(7):
                    $url = 'content_add_exercise_leseverstehen.php';
                    break;
                case(8):
                    $url = 'content_add_exercise_ctest.php';
                    break;
                case(9):
                    $url = 'content_add_exercise_hoerverstehen.php';
                    break;
                default:
                    $url = 'content_add_exercise_multi.php';
            }
            $out .= '<tr>';
                $out .= '<td align="left" valign="top">';
                    $out .= $exercise->eid;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= '<a href="' . $url . '?eid=' . $exercise->eid . '&extype=' . $exercise->etid . '&exmodus=' . $exercise->exmodus . '">' . $exercise->heading . '</a>';
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $exercise->etname;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $exercise->exmodus;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $exercise_total_points;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="width: 70px;padding-left: 20px;">';
                    if($exercise->exmodus == "test"){
                        $query = "SELECT * FROM go2stuko_test_exercise_link  tel "
                                ."WHERE tel.eid = ". $exercise->eid; 
                        $tests = Database::getDatasetFromQuery($query);
                        foreach($tests as $test){
                            $out .= $test->tid . "<br>";
                        }
                        
                    }
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= $exercise->created;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
                    $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?action=delete_exercise&eid=' . $exercise->eid . '"><img src="src/imgs/delete.gif" border="0"></a>';
                $out .= '</td>';
            $out .= '</tr>';
        }
        $out .= '</table>';
        return $out;
    }
    
?>
