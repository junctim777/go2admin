<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function printUserDetails($uid) {
    if (isset($_POST['submit_new_user_demo_booking']) && !empty($_POST['oid'])) {
        $query = "SELECT * FROM go2stuko_offer WHERE oid = " . $_POST['oid'];
        $offer_data_tmp = Database::getDatasetFromQuery($query);
        $offer_data = $offer_data_tmp[0];
        $tmp_query = 'starting_date = NOW(), ending_date = DATE_ADD(NOW(), INTERVAL ' . $offer_data->number_of_weeks . ' WEEK), ';
        $query = "SELECT * FROM go2stuko_offer_package_link WHERE oid = " . $_POST['oid'];
        $test_package_data_tmp = Database::getDatasetFromQuery($query);
        $test_package_data = $test_package_data_tmp[0];
        $query = "INSERT INTO go2stuko_booking "
                . "(uid, oid, tpid, pdid, paid, payment_method, "
                . "booking_date, ending_date, cash_receipt_date, "
                . "starting_date, all_tests_instantly, "
                . "terms_of_use_accepted, is_demo) "
                . "VALUES (" . $uid . ", " . $_POST['oid'] . ", " . $test_package_data->tpid
                . ", 63, 164, 'automatic_debit_transfer', NOW(), "
                . "DATE_ADD(NOW(), INTERVAL " . $offer_data->number_of_weeks . " WEEK), NOW(), NOW(), 1, 1, 1)";
        mysql_query($query);
    }
    $u = new User($uid);
    $out = '<table>';
    $out .= '<tr>';
    $out .= '<td align="left" valign="top">';
    $out .= '<b>Private Daten:</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
    $out .= $u->printUserData();
    $out .= '</td>';
    $out .= '</tr>';
    $out .= '<tr>';
    $out .= '<td align="left" valign="top">';
    $out .= '<b>Demo-Buchung:</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
    $out .= '<form action="administration.php?showUserDetails=true&uid=' . $uid . '" method="post">';
    $out .= 'Angebots-ID: <input type="text" name="oid"> <input type="submit" name="submit_new_user_demo_booking">';
    $out .= '</form>';
    $out .= '<br><br>';
    $out .= '</td>';
    $out .= '</tr>';
    $out .= '<tr>';
    $out .= '<td align="left" valign="top">';
    $out .= '<a href="administration.php">Zur&uuml;ck</a>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-bottom: 20px;">';
    $out .= '</td>';
    $out .= '</tr>';
    $out .= '</table>';
    return $out;
}

?>