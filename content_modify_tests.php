<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    include("./database/Database.php");
    include("./classes/utils/DynamicFormElements.php");
    $skid = $_GET['skid'];
    if(empty($skid)){
        $skid = $_POST['skid'];
    }
    
    Database::establishConnection();
    
    if(isset($_GET['action']) && $_GET['action'] == 'delete_test' && !empty($_GET['tid'])){
        $query = "DELETE FROM go2stuko_test WHERE tid = " . $_GET['tid'];
        mysql_query($query);
    }
    if(isset($_GET['action']) && $_GET['action'] == 'activate_test' && !empty($_GET['tid'])){
        $query = "UPDATE go2stuko_test SET is_active = 1 WHERE tid = " . $_GET['tid'];
        mysql_query($query);
    }
    if(isset($_GET['action']) && $_GET['action'] == 'inactivate_test' && !empty($_GET['tid'])){
        $query = "UPDATE go2stuko_test SET is_active = 0 WHERE tid = " . $_GET['tid'];
        mysql_query($query);
    }
    if(isset($_GET['action']) && $_GET['action'] == 'modify_test' && !empty($_GET['tid'])){
        header('Location: content_modify_test.php?tid=' . $_GET['tid'] . '&skid=' . $skid);
    }
    if(isset($_GET['action']) && $_GET['action'] == 'move_ex_up' && !empty($_GET['tid'])
            && !empty($_GET['eid'])){
        $query = "SELECT test_position FROM go2stuko_test_exercise_link WHERE tid = " . $_GET['tid'] . " AND eid = " . $_GET['eid'];
        $new_position = Database::getDatasetFromQuery($query);
        $new_position = $new_position[0];
        $query = "UPDATE go2stuko_test_exercise_link SET test_position = test_position+1 WHERE tid = " . $_GET['tid'] 
        . " AND test_position = (" . $new_position->test_position . "-1)";
        mysql_query($query);
        $query = "UPDATE go2stuko_test_exercise_link SET test_position = test_position-1 WHERE tid = " . $_GET['tid'] 
        . " AND eid = " . $_GET['eid'];
        mysql_query($query);
        $location = "Location: " . $_SERVER['PHP_SELF'] . "?skid=" . $skid . "#" . $_GET['tid']; 
        header($location);
    }
    if(isset($_GET['action']) && $_GET['action'] == 'move_ex_down' && !empty($_GET['tid'])
            && !empty($_GET['eid'])){
        $query = "SELECT test_position FROM go2stuko_test_exercise_link WHERE tid = " . $_GET['tid'] . " AND eid = " . $_GET['eid'];
        $new_position = Database::getDatasetFromQuery($query);
        $new_position = $new_position[0];
        $query = "UPDATE go2stuko_test_exercise_link SET test_position = test_position-1 WHERE tid = " . $_GET['tid'] 
        . " AND test_position = (" . $new_position->test_position . "+1)";
        mysql_query($query);
        $query = "UPDATE go2stuko_test_exercise_link SET test_position = test_position+1 WHERE tid = " . $_GET['tid'] 
        . " AND eid = " . $_GET['eid'];
        mysql_query($query);
        $location = "Location: " . $_SERVER['PHP_SELF'] . "?skid=" . $skid . "#" . $_GET['tid'];
        header($location);
    }
    if(isset($_GET['action']) && $_GET['action'] == 'delete_test_ex_link' && !empty($_GET['tid'])
            && !empty($_GET['eid'])){
        $query = "DELETE FROM go2stuko_test_exercise_link WHERE tid = " . $_GET['tid'] 
        . " AND eid = " . $_GET['eid'];
        mysql_query($query);
        $location = "Location: " . $_SERVER['PHP_SELF'] . "?skid=" . $skid . "#" . $_GET['tid'];
        header($location);
    }
    if(isset($_GET['action']) && $_GET['action'] == 'add_ex_to_test' && !empty($_GET['tid'])
            && !empty($_POST['eid'])){
        $query = "INSERT INTO go2stuko_test_exercise_link (tid, eid, test_position) " . 
                    "VALUES (" . $_GET['tid'] . "," . $_POST['eid'] . ", (SELECT IFNULL(MAX(test_position),0) FROM go2stuko_test_exercise_link tel WHERE tid=" . $_GET['tid'] . ")+1)";
        mysql_query($query);
        $location = "Location: " . $_SERVER['PHP_SELF'] . "?skid=" . $skid . "#" . $_GET['tid'];
        header($location);
    }
    if(isset($_GET['action']) && $_GET['action'] == 'add_new_test' && !empty($_POST['description_new_test'])){
        $query = "INSERT INTO go2stuko_test (skid, description, is_demo, created) " . 
                    "VALUES (" . $_GET['skid'] . ",'" . $_POST['description_new_test'] . "'," . (empty($_POST['is_demo_new_test'])?'0':'1') . ", NOW())";
        mysql_query($query);
    }
    if(isset($_GET['action']) && $_GET['action'] == 'delete_package_test_link' && !empty($_GET['tid'])
            && !empty($_GET['tpid'])){
        $query = "DELETE FROM go2stuko_package_test_link WHERE tpid = " . $_GET['tpid'] 
        . " AND tid = " . $_GET['tid'];
        mysql_query($query);
        $location = "Location: " . $_SERVER['PHP_SELF'] . "?skid=" . $skid . "#" . $_GET['tid'];
        header($location);
    }  
    if(isset($_GET['action']) && $_GET['action'] == 'add_test_to_package' && !empty($_GET['tid'])
            && !empty($_POST['tpid'])){
        $query = "INSERT INTO go2stuko_package_test_link (tpid, tid, package_position) " . 
                    "VALUES (" . $_POST['tpid'] . "," . $_GET['tid'] . ", (SELECT IFNULL(MAX(package_position),0) FROM go2stuko_package_test_link ptl WHERE ptl.tpid = " . $_POST['tpid'] . ")+1)";
        //echo $query;
        mysql_query($query);
        $location = "Location: " . $_SERVER['PHP_SELF'] . "?skid=" . $skid . "#" . $_GET['tid'];
        header($location);
    }
    if(isset($_GET['action']) && $_GET['action'] == 'add_new_package' && !empty($_POST['description_new_package'])){
        $query = "INSERT INTO go2stuko_test_package (skid, description) " . 
                    "VALUES (" . $_GET['skid'] . ",'" . $_POST['description_new_package'] . "')";
        mysql_query($query);
    }
    if(isset($_GET['action']) && $_GET['action'] == 'add_new_package_auto' && !empty($_POST['auto_package_no_of_tests'])){
        $auto_package_no_of_tests = $_POST['auto_package_no_of_tests'];
        $query = "SELECT * FROM go2stuko_test WHERE skid = " . $_GET['skid'] . " AND is_demo=1";
        $demo_tests = Database::getDatasetFromQuery($query);
        if(count($demo_tests) != 2){
            echo "FEHLER: Erst 2 Demo-Tests als Muster anlegen!";
        }else{
            $query = "SELECT * FROM go2stuko_exercise e, go2stuko_test_exercise_link tel WHERE e.eid = tel.eid AND tel.tid = " . $demo_tests[0]->tid . " ORDER BY test_position";
            $demo_test_1_exercises = Database::getDatasetFromQuery($query);
            $query = "SELECT * FROM go2stuko_exercise e, go2stuko_test_exercise_link tel WHERE e.eid = tel.eid AND tel.tid = " . $demo_tests[1]->tid . " ORDER BY test_position";
            $demo_test_2_exercises = Database::getDatasetFromQuery($query);
            
            $query = "SELECT * FROM go2stuko_studienkolleg WHERE skid = " . $_GET['skid'];
            $stk_info = Database::getDatasetFromQuery($query);
            $stk_info = $stk_info[0];
            
            $description = $stk_info->studienkolleg_name . " - " . $auto_package_no_of_tests . " Wochen - " . $auto_package_no_of_tests . " Tests";
            $query = "INSERT INTO go2stuko_test_package (skid, description) " . 
                        "VALUES (" . $_GET['skid'] . ",'" . $description . "')";
            mysql_query($query);
            $tpid_tmp = mysql_insert_id(Database::$link);
            
            $query = "SELECT * FROM go2stuko_test WHERE skid = " . $_GET['skid'] . " AND is_demo=0 LIMIT " . $auto_package_no_of_tests;
            $available_tests = Database::getDatasetFromQuery($query);
            $needed_tests = $auto_package_no_of_tests - count($available_tests);
            
            foreach($available_tests as $available_test){
                $query = "INSERT INTO go2stuko_package_test_link (tpid, tid, package_position) " . 
                        "VALUES (" . $tpid_tmp . "," . $available_test->tid . ", (SELECT IFNULL(MAX(package_position),0) FROM go2stuko_package_test_link ptl WHERE ptl.tpid = " . $tpid_tmp . ")+1)";
                mysql_query($query);
            }

            $new_test_description = $stk_info->studienkolleg_name . " Go2-Aufnahmepr&uuml;fung";
            for($i=0;$i<$needed_tests;$i++){
                $query = "INSERT INTO go2stuko_test (skid, description, is_demo, created) " . 
                    "VALUES (" . $_GET['skid'] . ",'" . $new_test_description . "',0, NOW())";
                mysql_query($query);
                $tid_tmp = mysql_insert_id(Database::$link);
                $query = "INSERT INTO go2stuko_package_test_link (tpid, tid, package_position) " . 
                        "VALUES (" . $tpid_tmp . "," . $tid_tmp . ", (SELECT IFNULL(MAX(package_position),0) FROM go2stuko_package_test_link ptl WHERE ptl.tpid = " . $tpid_tmp . ")+1)";
                mysql_query($query);
                $ex_types_template = null;
                if($i%2==0){
                    $ex_types_template = $demo_test_1_exercises;
                }else{ 
                    $ex_types_template = $demo_test_2_exercises;
                }
                for($j=0;$j<count($ex_types_template);$j++){
                    $ex_type = $ex_types_template[$j];
                    $ex_type = $ex_type->etid;
                    $query_yet_not_used = "AND e.eid NOT IN (SELECT e_sub.eid FROM go2stuko_exercise e_sub, go2stuko_test_exercise_link tel_sub, go2stuko_test t_sub WHERE tel_sub.eid = e_sub.eid AND tel_sub.tid = t_sub.tid AND t_sub.skid = " . $_GET['skid'] . ")";
                    $query_leseverstehen = "";
                    if($ex_type == 7 && $j<count($ex_types_template)-1 && $ex_types_template[$j+1]->etid == 7){
                        $query_leseverstehen = " AND e.heading LIKE '%1%' ";
                    }
                    $query = "SELECT * FROM go2stuko_exercise e, go2stuko_exercise_type et, go2stuko_studienkolleg_exercise_type_link sketl WHERE e.etid = " . $ex_type . $query_leseverstehen . " AND e.exmodus='test' " . $query_yet_not_used . " ORDER BY RAND() LIMIT 1";
                    //$query = "SELECT * FROM go2stuko_exercise e, go2stuko_exercise_type et WHERE e.etid = et.etid";
                    //echo $query;
                    $exercises = Database::getDatasetFromQuery($query);
                    if(count($exercises) < 1){
                        echo "FEHLER: Nicht genug Aufgabentypen!";
                    } else{
                        $exercise = $exercises[0];
                        $query = "INSERT INTO go2stuko_test_exercise_link (tid, eid, test_position) " . 
                                    "VALUES (" . $tid_tmp . "," . $exercise->eid . ", (SELECT IFNULL(MAX(test_position),0) FROM go2stuko_test_exercise_link tel WHERE tid=" . $tid_tmp . ")+1)";
                        mysql_query($query);
                        // Leseverstehen
                        if($ex_type == 7 && $ex_types_template[$j+1]->etid == 7){
                            if(strpos($exercise->heading, "1") == false){
                                echo "Das Leseverstehen hat keinen zweiten Teil!";
                            } else{
                                $heading = str_replace("1", "2", $exercise->heading);
                                $query = "SELECT * FROM go2stuko_exercise WHERE heading LIKE '" . $heading . "'";
                                $leseverstehen_second = Database::getDatasetFromQuery($query);
                                if(count($leseverstehen_second) != 1){
                                    echo "Es wurde kein zweiter Teil fuer das Leseverstehen gefunden";
                                } else{
                                    $leseverstehen_second = $leseverstehen_second[0];
                                    $query = "INSERT INTO go2stuko_test_exercise_link (tid, eid, test_position) " . 
                                                "VALUES (" . $tid_tmp . "," . $leseverstehen_second->eid . ", (SELECT IFNULL(MAX(test_position),0) FROM go2stuko_test_exercise_link tel WHERE tid=" . $tid_tmp . ")+1)";
                                    mysql_query($query);
                                    $j++;
                                }
                            }
                        }
                    }
                }
            }   
        }
    }
    
    
    include('templates/core/tpl_header.php');
    include('templates/content/tpl_content_modify_tests.php');
    include('templates/core/tpl_footer.php');
    Database::closeConnection();
    
    function printTests($skid){
        $out = '<table>';
        $out .= '<tr>';
            $out .= '<td align="left" valign="top" style="padding-top: 20px;">';
                $out .= '<b>ID</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                $out .= '<b>Test-Name</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                $out .= '<b>Demo?</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                $out .= '<b>Aktiv?</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                $out .= '<b>Aufgaben</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                $out .= '<b>Punkte</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                $out .= '<b>Enthalten in Lern-Paket?</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                $out .= '<b>Datum</b>';
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                $out .= '<b></b>';
            $out .= '</td>';
        $out .= '</tr>';
        $query = "SELECT * FROM go2stuko_test t WHERE skid = " . $skid . " ORDER BY t.created DESC";
        $tests = Database::getDatasetFromQuery($query);
        foreach($tests as $test){
            $query = "SELECT * FROM go2stuko_test_exercise_link tel, go2stuko_exercise_type et, go2stuko_exercise e WHERE e.etid = et.etid AND tel.eid = e.eid AND tel.tid = " . $test->tid . " ORDER BY tel.test_position";
            $exercises = Database::getDatasetFromQuery($query);
            $query = "SELECT * FROM go2stuko_package_test_link ptl, go2stuko_test_package tp WHERE tp.tpid = ptl.tpid AND ptl.tid = " . $test->tid;
            $packages = Database::getDatasetFromQuery($query);
            $query = 'SELECT SUM(q.content_points) AS content_points, SUM(q.linguistic_points) AS linguistic_points FROM go2stuko_question q, go2stuko_exercise_element ee, go2stuko_exercise e, go2stuko_test_exercise_link tel WHERE tel.eid = e.eid AND ee.eid = e.eid AND q.eeid = ee.eeid AND tel.tid = ' . $test->tid;
            $test_points_tmp = Database::getDatasetFromQuery($query);
            $test_points = $test_points_tmp[0];
            $test_total_points = $test_points->content_points + $test_points->linguistic_points;
            $out .= '<tr>';
                $out .= '<td align="left" valign="top" style="padding-top: 20px;">';
                    $out .= $test->tid;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                    $out .= '<a name="' . $test->tid . '">' . $test->description . '</a>';
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                    $out .= $test->is_demo;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                    if($test->is_active == 0){
                        $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?skid=' . $skid . '&action=activate_test&tid=' . $test->tid . '">' . $test->is_active . '</a>';
                    } else{
                        $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?skid=' . $skid . '&action=inactivate_test&tid=' . $test->tid . '">' . $test->is_active . '</a>';
                    }
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                    foreach($exercises as $exercise){
                        switch($exercise->etid){
                            case(5):
                                $url = 'content_add_exercise_bildbeschreibung.php';
                                break;
                            case(6):
                                $url = 'content_add_exercise_lueckentext.php';
                                break;
                            case(7):
                                $url = 'content_add_exercise_leseverstehen.php';
                                break;
                            case(8):
                                $url = 'content_add_exercise_ctest.php';
                                break;
                            case(8):
                                $url = 'content_add_exercise_hoerverstehen.php';
                                break;
                            default: 
                                $url = 'content_add_exercise_multi.php';
                        }
                        $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?skid=' . $skid . '&action=move_ex_up&tid=' . $test->tid . '&eid=' . $exercise->eid . '"><img src="src/imgs/arrow_up.gif" border="0"></a> ';
                        $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?skid=' . $skid . '&action=move_ex_down&tid=' . $test->tid . '&eid=' . $exercise->eid . '"><img src="src/imgs/arrow_down.gif" border="0"></a> ';
                        $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?skid=' . $skid . '&action=delete_test_ex_link&tid=' . $test->tid . '&eid=' . $exercise->eid . '"><img src="src/imgs/delete.gif" border="0"></a> ';
                        $out .= $exercise->heading . "<br>" . $exercise->etname . ' (<a href="' . $url . '?eid=' . $exercise->eid . '&extype=' . $exercise->etid . '&exmodus=' . $exercise->exmodus . '">' . $exercise->eid . "</a>)<br>";
                    }
                    $out .= '<form action="' . $_SERVER['PHP_SELF'] . '?action=add_ex_to_test&skid=' . $skid . '&tid=' . $test->tid . '" method="post" 
                                name="add_ex_to_test">';
                    $out .= '<br><img src="src/imgs/create.gif"> ';
                    $out .= DynamicFormElements::getTestExercises('eid', NULL, 
                                ($errors['eid'] != null ? "register_error" : ""), $skid);
                    $out .= '<br><input style="margin-left:30px;" class="" type="submit" name="submit" value="OK"/>';
                    $out .= '</form>';
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                    $out .= $test_total_points . " Punkte";
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                    foreach($packages as $package){
                        $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?skid=' . $skid . '&action=delete_package_test_link&tid=' . $test->tid . '&tpid=' . $package->tpid . '"><img src="src/imgs/delete.gif" border="0"></a> ';
                        $out .= $package->description . "</a><br>";
                    }
                    $out .= '<form action="' . $_SERVER['PHP_SELF'] . '?action=add_test_to_package&skid=' . $skid . '&tid=' . $test->tid . '" method="post" 
                                name="add_test_to_package">'; 
                    $out .= '<br><img src="src/imgs/create.gif"> ';
                    $out .= DynamicFormElements::getPackagesOfStk($skid,'tpid', NULL, 
                                ($errors['tpid'] != null ? "register_error" : ""));
                    $out .= '<br><input style="margin-left:30px;" class="" type="submit" name="submit" value="OK"/>';
                    $out .= '</form>';
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                    $out .= $test->created;
                $out .= '</td>';
                $out .= '<td align="left" valign="top" style="padding-left: 20px; padding-top: 20px;">';
                    $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?skid=' . $skid . '&action=modify_test&tid=' . $test->tid . '"><img src="src/imgs/icons/page_edit.gif" border="0"></a>';
                    $out .= '<a href="' . $_SERVER['PHP_SELF'] . '?skid=' . $skid . '&action=delete_test&tid=' . $test->tid . '"><img src="src/imgs/delete.gif" border="0"></a>';
                $out .= '</td>';
            $out .= '</tr>';
        } 
        $out .= '</table>';
        return $out; 
    }
    
?>
