<?php

    include('database/Database.php');
    include('classes/User.php');
    include('classes/utils/DynamicFormElements.php');
   
    // school name
    $school_name = $_POST['school_name'];
    
    // school adress
    $school_adress_street = $_POST['school_adress_street'];
    $school_adress_housenr = $_POST['school_adress_housenr'];
    $school_adress_plz = $_POST['school_adress_plz'];
    $school_adress_city = $_POST['school_adress_city'];
    
    // school contact information
    $school_subdomain = $_POST['school_subdomain'];
    $school_email = $_POST['school_email'];
    $school_phone = $_POST['school_phone'];
    $school_url = $_POST['school_url'];
    $skid = $_POST['skid'];
    
    // school contact person
    $contactPerson_sex = $_POST['contactPerson_sex'];
    $contactPerson_forename = $_POST['contactPerson_forename'];
    $contactPerson_surname = $_POST['contactPerson_surname'];
    
    // school account information
    $school_comment = $_POST['school_comment'];
    $loginData_username = $_POST['loginData_username'];
    $loginData_password = $_POST['loginData_password'];
    
    
    Database::establishConnection(); 
    
    if(isset ($_POST['submit_school_data'])){
        if(!empty($contactPerson_forename) && !empty($contactPerson_surname)
                && !empty($loginData_username) && !empty($loginData_password)){
         
            if(!empty($school_name)){
                if(!empty($school_email) || !empty($school_phone) || !empty($school_url)){
                    $query = "INSERT INTO go2stuko_user_contact_details (email_address, " 
                                . "receive_newsletter, phone_number, url) VALUES ('" .$school_email . "'," 
                                . "0, '" . $school_phone . "', '" . $school_url . "')";
                    $success = mysql_query($query);
                    $cdid = mysql_insert_id(Database::$link);
                } else {
                    $errors[school_contact] = "Bitte gebe die Email-Adresse, die Telefonummer oder die Homepage der Sprachschule ein!";
                }
                if(!empty($school_adress_street) && !empty($school_adress_housenr) 
                        && !empty($school_adress_plz) && !empty($school_adress_city)){
                    $query = "INSERT INTO go2stuko_address (road, " 
                                . "house_number, postal_code, city, cid) VALUES ('" .$school_adress_street . "'," 
                                . "'" . $school_adress_housenr . "', '" . $school_adress_plz . "', '" . $school_adress_city . "', 1)";
                    $success = mysql_query($query);
                    $aid = mysql_insert_id(Database::$link);
                }
                if(!empty($contactPerson_sex) && !empty($contactPerson_forename) 
                        && !empty($contactPerson_surname) && !empty($cdid)){
                    $query = "INSERT INTO go2stuko_user (sex, " 
                                . "surname, forename, " . (!empty($aid) ? "study_aid," : "") . " study_cdid, created, validated, isAdmin, isCorrector) VALUES ('" .$contactPerson_sex . "'," 
                                . "'" . $contactPerson_surname . "', '" . $contactPerson_forename . "'," . (!empty($aid) ? $aid . "," : "") . $cdid . ", NOW(), NOW(), 1, 1)";
                    $success = mysql_query($query);
                    $uid = mysql_insert_id(Database::$link);
                    if(!empty($loginData_username) && !empty($loginData_password)){
                        $query = "INSERT INTO go2stuko_login_data (uid, " 
                                . "login_nick_name, login_password) VALUES (" . $uid . "," 
                                . "'" . $loginData_username . "', '" . hash("sha256", $loginData_password) . "')";
                        $success = mysql_query($query);
                    } else {
                        $errors[school_login] = "Bitte gebe die Login-Daten ein!";
                    }
                } else {
                    $errors[school_contactPerson] = "Bitte gebe den Vor- und Nachnamen der kontaktperson an!";
                }
                $query = "INSERT INTO go2stuko_language_school (" . (empty($cdid) ? "" : "cdid,") 
                            . (empty($aid) ? "" : "aid,") . " school_name, subdomain, headofschool_uid) VALUES (" . (empty($cdid) ? "" : $cdid . ",") 
                            . (empty($aid) ? "" : $aid . ",") . "'" . $school_name . "', '" . $school_subdomain . "', " . $uid . ")";
                $success = mysql_query($query);
                $lsid = mysql_insert_id(Database::$link);
                if(!$success || empty($lsid)){
                    $errors[language_school] = "Es ist ein Fehler beim Eintragen der Sprachschule in die Datenbank aufgetreten.";
                } else{
                    if(!empty($school_comment)){
                        $query = "INSERT INTO go2stuko_language_school_comment (lsid, comment_text, insert_date) " 
                                    . " VALUES (" .$lsid . ",'" . $school_comment . "', NOW())";
                        $success = mysql_query($query);
                    }
                    if(!empty($skid)){
                        $query = "INSERT INTO go2stuko_language_school_studienkolleg_link (lsid, skid) " 
                                    . " VALUES (" .$lsid . "," . $skid . ")";
                        $success = mysql_query($query);
                    }
                    header('Location: administration.php?adminLanguageSchools=true');
                }
            } else{
                $errors[school_name] = "Bitte gebe einen Schul-Namen an!";
            }
        } else {
            $errors[school_initialData] = "Bitte den Vor- und Nachnamen der Kontaktperson und die Logindaten an!";
        }
    }
    
    include('templates/core/tpl_header.php');
    include('templates/tpl_add_new_school.php');
    include('templates/core/tpl_footer.php');
    
    Database::closeConnection();


?>
