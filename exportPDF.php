#!/usr/bin/php5
<?php

$out_path = '/var/www/vhosts/go2studienkolleg.de/teachers/src/pdfs/';

require_once('/var/www/vhosts/go2studienkolleg.de/teaching/src/tcpdf/config/lang/eng.php');
require_once('/var/www/vhosts/go2studienkolleg.de/teaching/src/tcpdf/tcpdf.php');

//error_reporting(E_ALL); 
//ini_set("display_errors", 1);

$uid = $argv[1];
$tid = $argv[2];
$teid = $argv[3];

function __autoload($class_name) {
    if($class_name == 'Database'){
        include ("/var/www/vhosts/go2studienkolleg.de/teaching/database/Database.php");
    } else if($class_name == "DynamicFormElements"){
        include ('/var/www/vhosts/go2studienkolleg.de/teaching/classes/utils/DynamicFormElements.php');
    } else if($class_name == "Helper"){
        include("/var/www/vhosts/go2studienkolleg.de/teaching/classes/utils/Helper.php");
    } else{
        include ('/var/www/vhosts/go2studienkolleg.de/teaching/classes/campus/' . $class_name . '.php');
    }
}

Database::establishConnection();

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    private $user;
    private $test;
    
    public function setUser($user){
        $this->user = $user;
    }
    
    public function setTest($test){
        $this->test = $test;
    }

    //Page header
    public function Header() {
        $query = "SELECT SUBTIME(TIME(ending_time), TIME(starting_time)) AS working_time FROM go2stuko_test_examination WHERE teid = " . $this->test->getTeid();
        $text_examinations = Database::getDatasetFromQuery($query);
        $text_examination = $text_examinations[0];
        
        $user_data = $this->user->getUserData();

        $this->SetFont('', 'B', 10);
        $this->Cell(45, 0, 'Go2 Lösungsansicht', 0, 1, 'L', 0, '', 0);
        
        $this->SetFont('', '', 8);
        $this->Cell(45, 5, 'Schüler: '. $user_data->forename . ' ' . $user_data->surname . ' | Bearbeitungsdauer: '. $text_examination->working_time . " (h)", 0, 1, 'L', 0, '', 0);
    }
 
    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(45, 0, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, 1, 'L', 0, '', 0);
        
        $this->SetFont('', '', 6);
        $this->Cell(45, 5, '© 2012 Go2Studienkolleg - www.Go2Studienkolleg.de - info@go2studienkolleg.de', 0, 1, 'L', 0, '', 0);
    }
}

$file_path = '/var/www/vhosts/go2studienkolleg.de/teachers/src/pdfs/Loesungsansicht_' . $tid . '_' . $uid . '_' . $teid . '.pdf';


if(!file_exists($file_path)){
$user = new User($uid);
$user_data = $user->getUserData();
if(!empty($tid)){
    $query = 'SELECT COUNT(*) AS count FROM go2stuko_booking WHERE terms_of_use_accepted = 1 AND starting_date <> \'NULL\' AND uid = ' . $user->getUid();
    $validate_booking_count_tmp = Database::getDatasetFromQuery($query);
    $validate_booking_count = $validate_booking_count_tmp[0];
    $query = 'SELECT is_demo FROM go2stuko_test WHERE tid = ' . $tid;
    $is_test_demo = Database::getDatasetFromQuery($query);
    $is_test_demo = $is_test_demo[0];
    if($validate_booking_count->count > 0 || $is_test_demo->is_demo == 1){
        $test = new Test($tid, $uid, $teid);
    }
} else{
    echo "FEHLER!";
}

$pages = $test->printTest();

$css_code = "<style>";
$css_code_tmp = file("src/tcpdf/css/style.css");
foreach($css_code_tmp as $line){
    $css_code .= $line;
}
$css_code_tmp = file("src/tcpdf/css/exam.css");
foreach($css_code_tmp as $line){
    $css_code .= $line;
}
$css_code_tmp = file("src/tcpdf/css/exercise_picture.css");
foreach($css_code_tmp as $line){
    $css_code .= $line;
}
$css_code .= "</style>";


// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setUser($user);
$pdf->setTest($test);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Go2 Studienkolleg');
$pdf->SetTitle('Lösungsansicht');
$pdf->SetSubject('Export Lösungsansicht');
$pdf->SetKeywords('Go2Studienkollef, PDF, Lösung');


//set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// remove default header/footer
$pdf->setPrintHeader(true);
//$pdf->Header();
$pdf->setPrintFooter(true);

$pdf->SetHeaderMargin(5);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(13, 15, PDF_MARGIN_RIGHT);

//set auto page breaks
//$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// set font
$pdf->SetFont('', '', 10);

// print a block of text
$pdf->writeHTML($txt, false, false, true, true, 'C');
//echo $page;

foreach ($pages as $page){
// add a page
$pdf->AddPage();

$page = $css_code . $page;
// set some text to print
$txt = <<<EOF
$page
EOF;

// print a block of text
$pdf->writeHTML($txt, false, false, true, true, 'C');

}

//Close and output PDF document
$pdf->Output('/var/www/vhosts/go2studienkolleg.de/teachers/src/pdfs/Loesungsansicht_' . $tid . '_' . $uid . '_' . $teid . '.pdf', 'F');
//============================================================+
// END OF FILE                                                
//============================================================+
}

Database::closeConnection();

?>