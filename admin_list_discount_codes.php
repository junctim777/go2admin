<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function printDiscountCodes() {
    $query = "SELECT * FROM go2stuko_tmp ORDER BY inserted DESC";
    $discount_codes = Database::getDatasetFromQuery($query);
    $out = '<table>';
    $out .= '<tr>';
    $out .= '<td align="left" valign="top">';
    $out .= '<b>Discount-Code</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Akzeptiert</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
    $out .= '<b>Datum</b>';
    $out .= '</td>';
    $out .= '</tr>';
    foreach ($discount_codes as $discount_code) {
        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $out .= $discount_code->discount_code;
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
        $out .= $discount_code->accepted;
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left: 20px;">';
        $out .= $discount_code->inserted;
        $out .= '</td>';
        $out .= '</tr>';
    }
    $out .= '</table>';
    $out .= '<br>';
    $out .= '<a href="administration.php">Zur&uuml;ck</a>';
    return $out;
}

?>