<?php
/**
 * Stellt eine Verbindung zu der Datenbank her
 * und bietet diverse Schnittstellen zur Datenbank
 * @author Marco Armbruster
 * @date 14.06.2007
 */

include("db_settings.php");

final class Database{

    static $link;

	/**
	* Stellt die Verbindung zur Datenbank her.
	* Muss vor allen Queries ausgeführt werden!
	*/
    public static function establishConnection(){
        $link = self::$link = mysql_connect($GLOBALS['db_host'],
					$GLOBALS['db_user'], $GLOBALS['db_password']);
        if(!$link){
                die("MySQL-Verbinung fehlgeschlagen:" + mysql_error());
        } else{
            $db_selected = mysql_select_db($GLOBALS['db_name'], $link);
            if(!$db_selected){
                die("Datenbank nicht verwendbar:" + mysql_error());
            } else{
                mysql_query('set character set utf8;');
                return true;
            }
        }
    }

	/**
	* Gibt einen Datensatz als Objekt-Array aus der Datenbank zurück
	* @param Tabellen-Namen
	* @param Where-Statement: "WHERE (X = Y AND A != B)
	* @return eindimensionales Objekt-Array, direkter Zugriff über
	*  Spaltennamen möglich
	*/
	public static function getDataset($table, $whereStatement){
		$dataset = null;
		$query = "SELECT * FROM " . $table . " " . $whereStatement . ";";
		$result = mysql_query($query);
		if($result == false){
			 die("Fehler in der Select-Abfrage: Database - getDataset");
		} else{
			$zeile = 0;
			while($row = mysql_fetch_object ($result))
				$dataset[$zeile++] = $row;
		}
		return $dataset;
	}

        /**
	* Gibt einen Datensatz als Objekt-Array aus der Datenbank zurück
	* @param Query-Statement: "SELECT * FROM ...
	* @return eindimensionales Objekt-Array, direkter Zugriff über
	*  Spaltennamen möglich
	*/
	public static function getDatasetFromQuery($query){
		$dataset = null;
		$result = mysql_query($query);
		if($result == false){
			 die("Fehler in der Select-Abfrage: Database - getDataset");
		} else{ 
			$zeile = 0;
			while($row = mysql_fetch_object ($result))
				$dataset[$zeile++] = $row;
		}
		return $dataset;
	}

	/**
	* Schreibt einen Datensatz in die Tabelle
	* @param Tabellen-Namen
	* @param zwei dimensionaler Datensatz
	* @param Ist die erste Spalte der Tabelle eine autoincrement ID?
	*/
	public static function writeDataset($table, $dataset, $id){
		if(count($dataset) <= 0)
			 die ( "Dataset - writeDataset - Der Datensatz ist leer" );
		$rowInfos = self::getRowInfos($table);
		if($rowInfos == null)
			die ( "Ein unerwarteter Fehler ist aufgetreten:
					Database - writeDataset - rowInfos > NullPointerException" );
		$valueDesc = "";
		$start = 0;
		if($id) $start = 1;
		for($x = $start; $x < count($rowInfos); $x++){
			$valueDesc .= $rowInfos[$x];
			if($x < count($rowInfos) - 1)
				$valueDesc .= ", ";
		}
		for($x = 0; $x < count($dataset); $x++){
			$query = "INSERT INTO " . $table . " (" . $valueDesc . ") VALUES (";
				for($y = 0; $y < count($dataset[0]); $y++){
					$insertValue = $dataset[$x][$y];
					if($insertValue != "NOW()")
						 $query .= "'";
					$query .= $insertValue;
					if($insertValue != "NOW()")
						 $query .= "'";
					if($y < count($dataset[0]) - 1)
						$query .= ", ";
				}
			$query .= ");";
			mysql_query($query);
		}
	}

	public static function deleteFromTable($table, $where_statement){
		$query = "DELETE FROM " . $table . " " . $where_statement .";";
		return(mysql_query($query) != false);
	}

	public static function countFromTable($table, $where_statement){
		$zeilen = 0;
		$query = "SELECT COUNT(*) FROM " . $table . " " . $where_statement .";";
		$result = mysql_query($query);
		if($row = mysql_fetch_row($result))
			 $zeilen = $row[0];
		return $zeilen;
	}

	public static function updateDataset($table, $set_statement, $where_statement){
		$query = "UPDATE " . $table . " " . $set_statement . " " . $where_statement .";";
		return(mysql_query($query) != false);
	}

	/**
	* Gibt die Spalten-Namen einer Tabelle als Array zurück
	* @param Tabellen-Namen
	* @return Spaltennamen der Tabelle
	*/
	public static function getRowInfos($table){
		$rowInfos = null;
		$query = "SELECT * FROM " . $table . ";";
		$result = mysql_query ( $query );
		$spaltenAnzahl = mysql_num_fields ( $result );
		for ( $x = 0; $x < $spaltenAnzahl; $x++ ){
		  $rowInfos[$x] = mysql_field_name ( $result, $x );
		}
		return $rowInfos;
	}

	/**
	* Schließt eine bestehende Verbindung zur Datenbank
	*/
    public static function closeConnection(){
        mysql_close(self::$link);
    }

}

?>
