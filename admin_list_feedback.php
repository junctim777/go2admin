<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function printFeedback() {
    $query = "SELECT * FROM go2stuko_feedback f, go2stuko_user u WHERE f.uid = u.uid ORDER BY insert_date DESC";
    $feedback_datas = Database::getDatasetFromQuery($query);
    $csv = 'UID;Name;Buchung;$no_of_logins;$no_of_tests;$average_percentage_of_tests;$total_mins_of_tests;$average_mins_of_tests;$no_of_demo_tests;';
    $csv .= '$average_percentage_of_demo_tests;$total_mins_of_demo_tests;$average_mins_of_demo_tests;$no_of_trainings;$no_of_irregular_verbs;$no_of_prepositions;';
    $csv .= 'Bestanden (Link);Bestanden (Umfrage);Geholfen?|C-Test?|Leseverstehen?|Textproduktion?|Freitext?;Datum;<br>';
    $out = '<table>';
    $out .= '<tr>';
    $out .= '<td align="left" valign="top">';
    $out .= '<b>UID</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left:20px;">';
    $out .= '<b>Name</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left:20px;">';
    $out .= '<b>Buchung</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left:20px;">';
    $out .= '<b>$no_of_logins | $no_of_tests | $average_percentage_of_tests | ';
    $out .= '$total_mins_of_tests | $average_mins_of_tests | $no_of_demo_tests | ';
    $out .= '$average_percentage_of_demo_tests | $total_mins_of_demo_tests | ';
    $out .= '$average_mins_of_demo_tests | $no_of_trainings | $no_of_irregular_verbs | $no_of_prepositions</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left:20px;">';
    $out .= '<b>Bestanden<br>(Link)</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left:20px;">';
    $out .= '<b>Bestanden<br>(Umfrage)</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left:20px;">';
    $out .= '<b>Geholfen? C-Test? Leseverstehen?<br>Textproduktion? Freitext</b>';
    $out .= '</td>';
    $out .= '<td align="left" valign="top" style="padding-left:20px;">';
    $out .= '<b>Datum</b>';
    $out .= '</td>';
    $out .= '</tr>';
    foreach ($feedback_datas as $feedback_data) {
        $query = "SELECT * FROM go2stuko_booking b, go2stuko_offer o WHERE b.uid = " . $feedback_data->uid . " AND b.oid = o.oid";
        $bookings = Database::getDatasetFromQuery($query);

        $no_of_logins = 0;
        $no_of_tests = 0;
        $average_percentage_of_tests = 0;
        $total_mins_of_tests = 0;
        $average_mins_of_tests = 0;
        $no_of_demo_tests = 0;
        $average_percentage_of_demo_tests = 0;
        $total_mins_of_demo_tests = 0;
        $average_mins_of_demo_tests = 0;
        $no_of_trainings = 0;
        $no_of_irregular_verbs = 0;
        $no_of_prepositions = 0;
        $query = "SELECT *, TIME_TO_SEC(TIMEDIFF(te.ending_time, te.starting_time)) AS time_diff_secs FROM go2stuko_test_examination te, go2stuko_test t WHERE te.tid = t.tid AND t.is_demo = 0 AND te.uid = " . $feedback_data->uid;
        $test_examinations = Database::getDatasetFromQuery($query);
        foreach ($test_examinations as $test_examination) {
            $user_content_points = 0;
            $user_linguistic_points = 0;
            $test_content_points = 0;
            $test_linguistic_points = 0;
            $query = 'SELECT SUM(content_points) AS content_points, SUM(linguistic_points) AS linguistic_points FROM go2stuko_user_input WHERE teid = ' . $test_examination->teid;
            $user_points_tmp = Database::getDatasetFromQuery($query);
            $user_points = $user_points_tmp[0];
            $query = 'SELECT SUM(q.content_points) AS content_points, SUM(q.linguistic_points) AS linguistic_points FROM go2stuko_question q, go2stuko_exercise_element ee, go2stuko_exercise e, go2stuko_test_exercise_link tel WHERE tel.eid = e.eid AND ee.eid = e.eid AND q.eeid = ee.eeid AND tel.tid = ' . $test_examination->tid;
            $test_points_tmp = Database::getDatasetFromQuery($query);
            $test_points = $test_points_tmp[0];
            if (!empty($user_points->content_points))
                $user_content_points += $user_points->content_points;
            if (!empty($user_points->linguistic_points))
                $user_linguistic_points += $user_points->linguistic_points;
            if (!empty($test_points->content_points))
                $test_content_points += $test_points->content_points;
            if (!empty($test_points->linguistic_points))
                $test_linguistic_points += $test_points->linguistic_points;

            $user_total_points = $user_content_points + $user_linguistic_points;
            $test_total_points = $test_content_points + $test_linguistic_points;
            $percentage = $user_total_points * 100 / $test_total_points;
            $test_duration_mins = $test_examination->time_diff_secs / 60;
            if ($test_duration_mins > 1) {
                $total_mins_of_tests += $test_duration_mins;
            }
            if ($percentage > 0.2) {
                $no_of_tests++;
                $average_percentage_of_tests = ($average_percentage_of_tests * ($no_of_tests - 1) + $percentage) / $no_of_tests;
                $average_mins_of_tests = ($average_mins_of_tests * ($no_of_tests - 1) + $test_duration_mins) / $no_of_tests;
            }
        }
        $query = "SELECT *, TIME_TO_SEC(TIMEDIFF(te.ending_time, te.starting_time)) AS time_diff_secs FROM go2stuko_test_examination te, go2stuko_test t WHERE te.tid = t.tid AND t.is_demo = 1 AND te.uid = " . $feedback_data->uid;
        $test_examinations = Database::getDatasetFromQuery($query);
        foreach ($test_examinations as $test_examination) {
            $user_content_points = 0;
            $user_linguistic_points = 0;
            $test_content_points = 0;
            $test_linguistic_points = 0;
            $query = 'SELECT SUM(content_points) AS content_points, SUM(linguistic_points) AS linguistic_points FROM go2stuko_user_input WHERE teid = ' . $test_examination->teid;
            $user_points_tmp = Database::getDatasetFromQuery($query);
            $user_points = $user_points_tmp[0];
            $query = 'SELECT SUM(q.content_points) AS content_points, SUM(q.linguistic_points) AS linguistic_points FROM go2stuko_question q, go2stuko_exercise_element ee, go2stuko_exercise e, go2stuko_test_exercise_link tel WHERE tel.eid = e.eid AND ee.eid = e.eid AND q.eeid = ee.eeid AND tel.tid = ' . $test_examination->tid;
            $test_points_tmp = Database::getDatasetFromQuery($query);
            $test_points = $test_points_tmp[0];
            if (!empty($user_points->content_points))
                $user_content_points += $user_points->content_points;
            if (!empty($user_points->linguistic_points))
                $user_linguistic_points += $user_points->linguistic_points;
            if (!empty($test_points->content_points))
                $test_content_points += $test_points->content_points;
            if (!empty($test_points->linguistic_points))
                $test_linguistic_points += $test_points->linguistic_points;

            $user_total_points = $user_content_points + $user_linguistic_points;
            $test_total_points = $test_content_points + $test_linguistic_points;
            $percentage = $user_total_points * 100 / $test_total_points;
            $test_duration_mins = $test_examination->time_diff_secs / 60;
            if ($test_duration_mins > 1) {
                $total_mins_of_demo_tests += $test_duration_mins;
            }
            if ($percentage > 0.2) {
                $no_of_demo_tests++;
                $average_percentage_of_demo_tests = ($average_percentage_of_demo_tests * ($no_of_demo_tests - 1) + $percentage) / $no_of_demo_tests;
                $average_mins_of_demo_tests = ($average_mins_of_demo_tests * ($no_of_demo_tests - 1) + $test_duration_mins) / $no_of_demo_tests;
            }
        }
        $query = "SELECT *, COUNT(*) AS count FROM go2stuko_vocabulary_user_log ul, go2stuko_user u "
                . "WHERE ul.ivid <> 'NULL' AND ul.uid = u.uid AND u.uid = " . $feedback_data->uid;
        $user_irregular_verb_trainings = Database::getDatasetFromQuery($query);
        $user_irregular_verb_trainings = $user_irregular_verb_trainings[0];
        $no_of_irregular_verbs += $user_irregular_verb_trainings->count;

        $query = "SELECT *, COUNT(*) AS count FROM go2stuko_vocabulary_user_log ul, go2stuko_user u "
                . "WHERE ul.pqid <> 'NULL' AND ul.uid = u.uid AND u.uid = " . $feedback_data->uid;
        $user_pq_trainings = Database::getDatasetFromQuery($query);
        $user_pq_trainings = $user_pq_trainings[0];
        $no_of_prepositions += $user_pq_trainings->count;

        $query = "SELECT *, COUNT(*) AS count FROM go2stuko_vocabulary_user_log ul, go2stuko_user u "
                . "WHERE ul.eid <> 'NULL' AND ul.uid = u.uid AND u.uid = " . $feedback_data->uid;
        $user_e_trainings = Database::getDatasetFromQuery($query);
        $user_e_trainings = $user_e_trainings[0];
        $no_of_trainings += $user_e_trainings->count;
        $query = "SELECT *, COUNT(*) AS count FROM go2stuko_user_session WHERE uid = " . $feedback_data->uid;
        $user_sessions = Database::getDatasetFromQuery($query);
        $user_sessions = $user_sessions[0];
        $no_of_logins += $user_sessions->count;

        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        $csv .= $feedback_data->uid . ";";
        $out .= '<a href="administration.php?showUserDetails=true&uid=' . $feedback_data->uid . '">' . $feedback_data->uid . '</a>';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left:20px;">';
        $csv .= $feedback_data->forename . " " . $feedback_data->surname . ";";
        $out .= $feedback_data->forename . '<br>' . $feedback_data->surname;
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left:20px;">';
        foreach ($bookings as $booking) {
            $csv .= $booking->offer_name . " | ";
            $out .= $booking->offer_name . "<br>";
        }
        $csv .= ';';
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left:20px;">';
        $out .= $no_of_logins . " | " .
                $no_of_tests . " | " .
                $average_percentage_of_tests . " | " .
                $total_mins_of_tests . " | " .
                $average_mins_of_tests . " | " .
                $no_of_demo_tests . " | " .
                $average_percentage_of_demo_tests . " | " .
                $total_mins_of_demo_tests . " | " .
                $average_mins_of_demo_tests . " | " .
                $no_of_trainings . " | " .
                $no_of_irregular_verbs . " | " .
                $no_of_prepositions;
        $csv .= $no_of_logins . ";" .
                $no_of_tests . ";" .
                round($average_percentage_of_tests, 1) . ";" .
                round($total_mins_of_tests, 1) . ";" .
                round($average_mins_of_tests, 1) . ";" .
                round($no_of_demo_tests, 1) . ";" .
                round($average_percentage_of_demo_tests, 1) . ";" .
                round($total_mins_of_demo_tests, 1) . ";" .
                round($average_mins_of_demo_tests, 1) . ";" .
                $no_of_trainings . ";" .
                $no_of_irregular_verbs . ";" .
                $no_of_prepositions . ";";
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left:20px;">';
        $csv .= $feedback_data->passed_unsure . ";";
        $out .= $feedback_data->passed_unsure;
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left:20px;">';
        $csv .= $feedback_data->passed_sure . ";";
        $out .= $feedback_data->passed_sure;
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left:20px;">';
        $csv .= $feedback_data->answers . ";";
        $out .= $feedback_data->answers;
        $out .= '</td>';
        $out .= '<td align="left" valign="top" style="padding-left:20px;">';
        $csv .= $feedback_data->insert_date . ";<br>";
        $out .= $feedback_data->insert_date;
        $out .= '</td>';
        $out .= '</tr>';
    }
    $out .= '</table>';
    $out .= '<br>';
    $out .= '<a href="administration.php">Zur&uuml;ck</a>';
    //echo $csv;
    return $out;
}

?>