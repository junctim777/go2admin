<?php

include ("./classes/ExerciseElement.php");

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Exercise
 *
 * @author Marco Armbruster
 */
class Exercise {
    //put your code here
    
    var $dbExercise;
    var $exerciseElements;
    var $eid;

    function Exercise($eid) {
        $this->eid = $eid;
        $this->getFromDatabase();
    }
    
    private function getFromDatabase(){
        $query = "SELECT * FROM go2stuko_exercise WHERE eid = " . $this->eid;
        $exercise_data_tmp = Database::getDatasetFromQuery($query);
        $this->dbExercise = $exercise_data_tmp[0];
        
        $query = "SELECT * FROM go2stuko_exercise_element WHERE " 
                ."eid = " . $this->eid . " "
                ."ORDER BY exercise_position";
        $exerciseElements_data_tmp = Database::getDatasetFromQuery($query);
        foreach($exerciseElements_data_tmp as $exerciseElement_data_tmp){
            $this->exerciseElements[count($this->exerciseElements)] = 
                    new ExerciseElement($exerciseElement_data_tmp);
        }
    }
    
    public function getEid(){
        return $this->dbExercise->eid;
    }
    
    public function getEtid(){
        return $this->dbExercise->etid;
    }
    
    public function getHeading(){
        return $this->dbExercise->heading;
    }
    
    public function getIntroductionText(){
        return $this->dbExercise->introduction_text;
    }
    
    public function getAuthorisedSince(){
        return $this->dbExercise->authorised_since;
    }
    
    public function getCreated(){
        return $this->dbExercise->created;
    }
    
    public function getChanged(){
        return $this->dbExercise->changed;
    }
}

?>
