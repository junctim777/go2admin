<?php

include ("./classes/Question.php");
include ("./classes/File.php");

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ExerciseElement
 *
 * @author Marco Armbruster
 */
class ExerciseElement {
    //put your code here
    
    var $dbExerciseElement;
    var $questions;
    var $file;
    
    function ExerciseElement($dbExerciseElement) {
        $this->dbExerciseElement = $dbExerciseElement;
        $this->getFromDatabase();
    }
    
    private function getFromDatabase(){
        $query = "SELECT * FROM go2stuko_question Q, go2stuko_question_type QT WHERE " 
                ."Q.qtid = QT.qtid AND Q.eeid = " . $this->dbExerciseElement->eeid . " "
                ."ORDER BY Q.exercise_element_position";
        $questions_data_tmp = Database::getDatasetFromQuery($query);
        foreach($questions_data_tmp as $question_data_tmp){
            $this->questions[count($this->questions)] = 
                    new Question($question_data_tmp);
        }
        if($this->dbExerciseElement->fid != null){
            $query = "SELECT * FROM go2stuko_file WHERE " 
                    ."fid = " . $this->dbExerciseElement->fid;
            $file_data_tmp = Database::getDatasetFromQuery($query);
            if(count($file_data_tmp) > 0){
                $this->file = new File($file_data_tmp[0]);
            }
        }
    }
    
    public function getFirstQuestion(){
        return $this->questions[0];
    }
    
    public function getQuestionFromId($qid){
        foreach($this->questions as $question){
            if($question->getQid() == $qid){
                return $question;
            }
        }
        return false;
    }
    
    public function getQuestions(){
        return $this->questions;
    }
    
    public function getEeid(){
        return $this->dbExerciseElement->eeid;
    }
    
    public function getEid(){
        return $this->dbExerciseElement->eid;
    }
    
    public function getElementType(){
        return $this->dbExerciseElement->element_type;
    }
    
    public function getExercisePosition(){
        return $this->dbExerciseElement->exercise_position;
    }
    
    public function getElementText(){
        return $this->dbExerciseElement->element_text;
    }
    
    public function getFid(){
        return $this->dbExerciseElement->fid;
    }
    
    public function getCreated(){
        return $this->dbExerciseElement->created;
    }
    
    public function getChanged(){
        return $this->dbExerciseElement->changed;
    }
    
}

?>
