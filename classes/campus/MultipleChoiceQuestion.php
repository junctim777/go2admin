<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MultipleChoiceQuestion
 *
 * @author Marco Armbruster
 */
class MultipleChoiceQuestion extends Question{
    //put your code here
    
    private $question_introduction;
    private $mc_answers;
    
    public function MultipleChoiceQuestion($dbQuestion) {
        parent::Question($dbQuestion);
        
        $xml = new SimpleXMLElement($this->getQuestionText());
        $this->question_introduction = (string)$xml->question_introduction;
        foreach($xml->answer as $answer){
            $isCorrect = $answer->attributes()->correct == "true";
            $this->mc_answers[count($this->mc_answers)] = new MultipleChoiceAnswer((string)$answer, $isCorrect);
        }
    }
    
    public function getQuestionIntroductionText(){
        return $this->question_introduction;
    }
    
    public function getMCAnswers(){
        return $this->mc_answers;
    }
    
    public function getNumberOfCorrectAnswers(){
        $no = 0;
        foreach($this->mc_answers as $mc_answer){
            if($mc_answer->isCorrectAnswer()){
                $no++;
            }
        }
        return $no;
    }
    
    public function getContentPointsPerAnswer(){
        return $this->getContentPoints() / $this->getNumberOfCorrectAnswers();
    }
    
    public function isUserAnswerCorrect($user_char){
        $correct = false;
        $cnt = 0;
        for($i=65; $i<(65+count($this->mc_answers)); $i++){
            $answer_char = strtolower(chr($i));
            $user_char = strtolower($user_char);
            $mc_answer = $this->mc_answers[$cnt++];
            if($answer_char == $user_char && $mc_answer->isCorrectAnswer()){
                return true;
            }
        }
        return false;
    }
    
    public function getCorrectAnswerCharacters(){
        $correctCharacters = "";
        $cnt = 0;
        $foundMatches = 0;
        for($i=65; $i<(65+count($this->mc_answers)); $i++){
            $mc_answer = $this->mc_answers[$cnt];
            if($mc_answer->isCorrectAnswer()){
                if($foundMatches > 0){
                    $correctCharacters .= ', ';
                }
                $correctCharacters .= strtolower(chr($i));
                $foundMatches++;
            }
            $cnt++;
        }
        return $correctCharacters;
    }
    
}

?>
