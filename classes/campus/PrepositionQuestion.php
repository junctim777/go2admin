<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrepositionQuestion
 *
 * @author cmon
 */
class PrepositionQuestion extends VocabularyQuestion{
    //put your code here
    
    private $dbPreposition;
    
    public function PrepositionQuestion($dbPreposition) {
        parent::VocabularyQuestion();
        $this->dbPreposition = $dbPreposition;
    }
    
    public function getDbPreposition(){
        return $this->dbPreposition;
    }
    
    public function printVocabularyQuestion($showSolution) {
        $out = '<h2>Pr&auml;positionen - Trainer</h2><br /><br />';
        $out .= '<form action="' . $_SERVER['PHP_SELF'] . '" method="post" name="userInputForm">';
            $out .= '<table class="irregular-verbs" width="850" cellpadding="" cellspacing="" border="0">';
                $out .= '<tr>';
                    $out .= ($showSolution ? '<td class="" align="LEFT" valign="top" width="120"></td>' : '');
                    $out .= '<td class="" align="LEFT" valign="top" width=""><b>Voranstehendes<br>Objekt:</b></td>';
                    $out .= '<td class="" align="LEFT" valign="top" width=""><b>Verb:</b></td>';
                    $out .= '<td class="" align="LEFT" valign="top" width=""><b>Pr&auml;position:</b></td>';
                    $out .= '<td class="" align="LEFT" valign="top" width=""><b>Nachstehendes<br>Objekt:</b></td>';
                $out .= '</tr>';
                if($showSolution){
                    $query = "SELECT * FROM go2stuko_preposition_question WHERE verb = \"" . $this -> dbPreposition -> verb . "\""
                            . " AND pqid != " . $this -> dbPreposition -> pqid;
                    Database::establishConnection();
                    $otherCorrectAnswersRaw = Database::getDatasetFromQuery($query);
                    $out .= '<tr>';
                    $out .= '<td class="list_input" align="LEFT" valign="top" width="120">';
                        $out .= 'Deine Antwort:';
                    $out .= '</td>';
                    $out .= '<td class="list_input" align="LEFT" valign="top" width="120">';
                        $out .= '<font class="' . (($_POST['answerPre'] == 
                            $this -> dbPreposition -> pre) ? 'correct' : 'incorrect') . '">';
                        $out .= '<b>' . $_POST['answerPre'] .'</b></font>&nbsp;';
                    $out .= '</td>';
                    $out .= '<td class="list_input" align="LEFT" valign="top" width="120">';
                        $out .= '<font class="highlighted_text"><b>' . $this -> dbPreposition -> verb . '</b></font>';
                    $out .= '</td>';
                    $out .= '<td class="list_input" align="LEFT" valign="top" width="120">';
                        $out .= '<font class="' . (($_POST['answerPreposition'] == 
                            $this -> dbPreposition -> preposition) ? 'correct' : 'incorrect') . '">';
                        $out .= '<b>' . $_POST['answerPreposition'] .'</b></font>&nbsp;';
                    $out .= '</td>';
                    $out .= '<td class="list_input" align="LEFT" valign="top" width="120">';
                        $out .= '<font class="' . (($_POST['answerPost'] == 
                            $this -> dbPreposition -> post) ? 'correct' : 'incorrect') . '">';
                        $out .= '<b>' . $_POST['answerPost'] .'</b></font>&nbsp;';
                   $out .= '</td>';  
                   $out .= '</tr>';
                   $out .= '<tr>';
                        $out .= '<td class="list_input" align="LEFT" valign="top" width="120">';
                            $out .= 'L&ouml;sung:';
                        $out .= '</td>';
                        $out .= '<td class="list_input" align="LEFT" valign="top" width="120">';
                           $out .= '<font class="correct"><b>' . $this -> dbPreposition -> pre . '</b></font>&nbsp;';
                        $out .= '</td>';
                        $out .= '<td class="list_input" align="LEFT" valign="top" width="120">';
                            $out .= '<font class="highlighted_text"><b>' . $this -> dbPreposition -> verb . '</b></font>&nbsp;';
                        $out .= '</td>';
                        $out .= '<td class="list_input" align="LEFT" valign="top" width="120">';
                            $out .= '<font class="correct"><b>' . $this -> dbPreposition -> preposition . '</b></font>&nbsp;';
                        $out .= '</td>';
                        $out .= '<td class="list_input" align="LEFT" valign="top" width="120">';
                            $out .= '<font class="correct"><b>' . $this -> dbPreposition -> post . '</b></font>&nbsp;';
                        $out .= '</td>';
                    $out .= '</tr>';
                    if($otherCorrectAnswersRaw != null){
                        foreach($otherCorrectAnswersRaw AS $otherCorrectAnswerRaw){
                            $out .= '<tr>';
                                $out .= '<td class="list_input" align="LEFT" valign="top" width="120">';
                                    $out .= 'L&ouml;sung:';
                                $out .= '</td>';
                                $out .= '<td class="list_input" align="LEFT" valign="top" width="120">';
                                   $out .= '<font class="correct"><b>' . $otherCorrectAnswerRaw -> pre . '</b></font>&nbsp;';
                                $out .= '</td>';
                                $out .= '<td class="list_input" align="LEFT" valign="top" width="120">';
                                    $out .= '<font class="highlighted_text"><b>' . $otherCorrectAnswerRaw -> verb . '</b></font>&nbsp;';
                                $out .= '</td>';
                                $out .= '<td class="list_input" align="LEFT" valign="top" width="120">';
                                    $out .= '<font class="correct"><b>' . $otherCorrectAnswerRaw -> preposition . '</b></font>&nbsp;';
                                $out .= '</td>';
                                $out .= '<td class="list_input" align="LEFT" valign="top" width="120">';
                                    $out .= '<font class="correct"><b>' . $otherCorrectAnswerRaw -> post . '</b></font>&nbsp;';
                                $out .= '</td>';
                            $out .= '</tr>';
                        }
                    }
                } else {
                    $out .= '<tr>';
                    $out .= '<td class="list_input" align="LEFT" valign="top" width="120">';
                        $out .= '<select tabindex="' . ++$tabindex . '" class="" name="answerPre" size="9">';
                            $out .= '<option selected></option>';
                            $out .= '<option>sich (Reflexiv)</option>';
                            $out .= '<option>jemanden (Akk.)</option>';
                            $out .= '<option>etwas (Akk.)</option>';
                            $out .= '<option>jn | etw (Akk.)</option>';
                            $out .= '<option>jm etw (Dat. + Akk.)</option>';
                            $out .= '<option>jemandem (Dat.)</option>';
                            $out .= '<option>s | jn (Reflexiv | Akk.)</option>';
                            $out .= '<option>s | jn | etw (Reflexiv | Akk.)</option>';
                        $out .= '</select>';
                    $out .= '</td>';
                    $out .= '<td class="list_input" align="LEFT" valign="top" width="120">';
                        $out .= '<font class="highlighted_text"><b>' . $this -> dbPreposition -> verb . '</b></font>';
                    $out .= '</td>';
                    $out .= '<td class="list_input" align="LEFT" valign="top" width="120">';
                        $out .= '<input tabindex="' . ++$tabindex . '" class="" name="answerPreposition" type="text" size="10" maxlength="20"';
                               $out .='value="' . $_POST['answerPreposition'] . '">';
                   $out .= '</td>';
                    $out .= '<td class="list_input" align="LEFT" valign="top" width="120">';
                        $out .= '<select tabindex="' . ++$tabindex . '" class="highlighted_text" name="answerPost" size="7">';
                            $out .= '<option selected></option>';
                            $out .= '<option>jemanden(Akk.)</option>';
                            $out .= '<option>etwas (Akk.)</option>';
                            $out .= '<option>jemanden | etwas (Akk.)</option>';
                            $out .= '<option>jemandem (Dat.)</option>';
                            $out .= '<option>etwas (Dat.)</option>';
                            $out .= '<option>jemandem | etwas (Dat.)</option>';
                        $out .= '</select>';
                    $out .= '</td>';
                $out .= '</tr>';
                }
                $out .= '<tr>';
                    $out .= '<td class="list_input" align="left" valign="top" width="120" colspan="2">';
                        $out .= '<input tabindex="' . ($tabindex+2) . '" class="cancel" type="submit" name="cancel" value="">';
                    $out .= '</td>';
                if($showSolution){
                      $out .= '<td class="list_input" align="LEFT" valign="bottom" width="170" colspan="3">';
                      $out .= 'Meine Antwort war: <a href="' . $_SERVER['PHP_SELF'] . '?successChoice=true">RICHTIG</a> | <a href="' . $_SERVER['PHP_SELF'] .'?successChoice=false">FALSCH</a>';     
                      $out .= '</td>';
                } else {
                    $out .= '<td class="list_input" align="right" valign="top" width="120" colspan="2">';
                        $out .= '<input tabindex="' . ($tabindex+1) . '" class="next" type="submit" name="submitAnswer" value="">';
                    $out .= '</td>';
                }
                $out .= '</tr>';
            $out .= '</table>';
        $out .= '</form>';
        return $out;
    }
    
    public function updateInDatabase($success){
        $user = $_SESSION['user'];
        $uid = $user->getUid();
        $query = "SELECT COUNT(*) AS count FROM go2stuko_vocabulary_user_log WHERE uid = " . $uid . " AND pqid = " . $this->dbPreposition->pqid;
        $noOfUserLogsTmp = Database::getDatasetFromQuery($query);
        $noOfUserLogsTmp = $noOfUserLogsTmp[0];
        $noOfUserLogsTmp = $noOfUserLogsTmp->count;
        if($noOfUserLogsTmp == 0){
            if($success)
                $card_index = 2;
            else
                $card_index = 1;
            $query = "INSERT INTO go2stuko_vocabulary_user_log" .
                    " (uid, pqid, date_last_training, card_index)" .
                    " VALUES (" .
                    $uid . "," .
                    $this->dbPreposition->pqid . "," .
                    "NOW()," .
                    $card_index . 
                    ")";
            $dbSuccess = mysql_query($query);
        } else{
            if($success)
                $query = "UPDATE go2stuko_vocabulary_user_log SET date_last_training = NOW(), card_index = card_index + 1 WHERE uid = " . $uid . " AND pqid = " . $this->dbPreposition->pqid;
            else
                $query = "UPDATE go2stuko_vocabulary_user_log SET date_last_training = NOW(), card_index = 1 WHERE uid = " . $uid . " AND pqid = " . $this->dbPreposition->pqid;
            mysql_query($query);
        }
    }
    
}

?>
