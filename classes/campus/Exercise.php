<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Exercise
 *
 * @author Marco Armbruster
 */
abstract class Exercise extends VocabularyQuestion{
    
    protected $eid;
    protected $heading;
    protected $introductionText;
    protected $exerciseElements;
    
    protected $userContentPoints;
    protected $userLinguisticPoints;
    
    protected $incorrect_answer = false;
    
    public function VocabularyQuestion() {
    }

    public function wasAnsweredIncorrectly(){
        $this -> incorrect_answer = true;
    }
    
    public function wasIncorrect(){
        return $this->incorrect_answer;
    }
    
    public function printVocabularyQuestion($showSolution){
        return $this->printExercise($showSolution, -1, false);
    }
    
    public function updateInDatabase($success){
        $user = $_SESSION['user'];
        $uid = $user->getUid();
        $query = "SELECT COUNT(*) AS count FROM go2stuko_vocabulary_user_log WHERE uid = " . $uid . " AND eid = " . $this->eid;
        $noOfUserLogsTmp = Database::getDatasetFromQuery($query);
        $noOfUserLogsTmp = $noOfUserLogsTmp[0];
        $noOfUserLogsTmp = $noOfUserLogsTmp->count;
        if($noOfUserLogsTmp == 0){
            if($success)
                $card_index = 2;
            else
                $card_index = 1;
            $query = "INSERT INTO go2stuko_vocabulary_user_log" .
                    " (uid, eid, date_last_training, card_index)" .
                    " VALUES (" .
                    $uid . "," .
                    $this->eid . "," .
                    "NOW()," .
                    $card_index . 
                    ")";
            $dbSuccess = mysql_query($query);
        } else{
            if($success)
                $query = "UPDATE go2stuko_vocabulary_user_log SET date_last_training = NOW(), card_index = card_index + 1 WHERE uid = " . $uid . " AND eid = " . $this->eid;
            else
                $query = "UPDATE go2stuko_vocabulary_user_log SET date_last_training = NOW(), card_index = 1 WHERE uid = " . $uid . " AND eid = " . $this->eid;
            mysql_query($query);
        }
    }
    
    public function Exercise($eid, $heading, $introductionText) {
        $this->eid = $eid;
        $this->heading = $heading;
        $this->introductionText = $introductionText;
    }
    
    public function addNewExerciseElement($exerciseElement){
        $this->exerciseElements[count($this->exerciseElements)] = $exerciseElement;
    }
    
    public function getEid(){
        return $this->eid;
    }
    
    public function getHeading(){
        return $this->heading;
    }
    
    public function getIntroductionText(){
        return $this->introductionText;
    }
    
    public function getUserContentPoints(){
        return $this->userContentPoints;
    }
    
    public function getUserLinguisticPoints(){
        return $this->userLinguisticPoints;
    }
    
    abstract public function getAllUserContentPoints();
    
    public function getAllContentPoints(){
        $cntContenPoints = 0;
        foreach($this->exerciseElements as $exerciseElement){
            $cntContenPoints += $exerciseElement->getAllContentPoints();
        }
        return $cntContenPoints;
    }
    
    abstract public function getAllUserLinguisticPoints();
    
    public function getAllLinguisticPoints(){
        $cntContenPoints = 0;
        foreach($this->exerciseElements as $exerciseElement){
            $cntContenPoints += $exerciseElement->getAllLinguisticPoints();
        }
        return $cntContenPoints;
    }
    
    public function setUserContentPoints($cps){
        $this->userContentPoints = $cps;
    }
    
    public function setUserLinguisticPoints($lps){
        $this->userLinguisticPoints = $lps;
    }
    
    abstract public function printStatistic();
    
    abstract public function printExercise($showSolution, $teid, $showBackButton);
    
    abstract public function handleUserInput($teid);
    
}


?>
