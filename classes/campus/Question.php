<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Question
 *
 * @author Marco Armbruster
 */
class Question {
    //put your code here
     
    var $dbQuestion;
    var $sampleSolutions;
    
    public static function getUserInput($teid, $qid){
        $query = "SELECT * FROM go2stuko_user_input WHERE " 
                . "teid = " . $teid . " AND qid = " . $qid;
        $user_input_data_tmp = Database::getDatasetFromQuery($query);
        // NUR TEMPORAER: ansonsten == 1
        if(count($user_input_data_tmp) > 0){
            return $user_input_data_tmp[0];
        }
        else return false;
    }
    
    function Question($dbQuestion) {
        $this->dbQuestion = $dbQuestion;
        $this->getFromDatabase();
    }
    
    private function getFromDatabase(){
        $query = "SELECT * FROM go2stuko_sample_solution WHERE " 
                ."qid = " . $this->dbQuestion->qid . " "
                ."ORDER BY ssid";
        $sampleSolutions_data_tmp = Database::getDatasetFromQuery($query);
        foreach($sampleSolutions_data_tmp as $sampleSolution_data_tmp){
            $this->sampleSolutions[count($this->sampleSolutions)] = 
                    new SampleSolution($sampleSolution_data_tmp);
        }
    }
    
    public function getSampleSolutions(){
        return $this->sampleSolutions;
    }
    
    public function getBestSampleSolution(){
        foreach($this->sampleSolutions as $sampleSolution){
            if($sampleSolution->getIsBestAnswer() == 1){
                return $sampleSolution;
            }
        }
        return null;
    }
    
    public function getQid(){
        return $this->dbQuestion->qid;
    }
    
    public function getEeid(){
        return $this->dbQuestion->eeid;
    }
    
    public function getQtid(){
        return $this->dbQuestion->qtid;
    }
    
    public function getExerciseElementPosition(){
        return $this->dbQuestion->exercise_element_position;
    }
    
    public function getDifficulty(){
        return $this->dbQuestion->difficulty;
    }
    
    public function getEstimatedTime(){
        return $this->dbQuestion->estimated_time;
    }
    
    public function getQuestionText(){
        return $this->dbQuestion->question_text;
    }
    
    public function getIsFreeTextQuestion(){
        return $this->dbQuestion->is_free_text_question;
    }
    
    public function getContentPoints(){
        return $this->dbQuestion->content_points;
    }
    
    public function getLinguisticPoints(){
        return $this->dbQuestion->linguistic_points;
    }
    
    public function getAnswerNote(){
        return $this->dbQuestion->answer_note;
    }
    
    public function getCreated(){
        return $this->dbQuestion->created;
    }
    
    public function getChanged(){
        return $this->dbQuestion->changed;
    }
    
}

?>
