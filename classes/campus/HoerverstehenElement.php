<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HoerverstehenElement
 *
 * @author Marco Armbruster
 */
class HoerverstehenElement extends ExerciseElement{
    //put your code here
    
    private $listening_text;
    private $audio_data;
    private $fid;
    private $questions;
    private $userInput;
    
    public function HoerverstehenElement($eeid, $exercisePosition, $listening_text, $fid) {
        parent::ExerciseElement($eeid, $exercisePosition);
        $this->listening_text = $listening_text;
        $this->fid = $fid;
        $this->getFromDatabase();
    }

    protected function getFromDatabase() {
        $query = "SELECT * FROM go2stuko_question WHERE " 
                ."eeid = " . $this->eeid . " "
                ."ORDER BY exercise_element_position";
        $questions_data_tmp = Database::getDatasetFromQuery($query);
        foreach($questions_data_tmp as $question_data_tmp){

            if($question_data_tmp->qtid == 4){
                $this->questions[$qcnt++] = new MultipleChoiceQuestion($question_data_tmp);

            } else if($question_data_tmp->qtid == 6){
                
               $this->questions[$qcnt++] = new YesNoQuestion($question_data_tmp);
                
            } else {
                
                $this->questions[$qcnt++] = new Question($question_data_tmp);
            }
        }
        $query = "SELECT * FROM go2stuko_file WHERE " 
                ."fid = " . $this->fid;
        $audio_data_tmp = Database::getDatasetFromQuery($query);
        if(count($audio_data_tmp) > 0){
            $this->audio_data= new Go2File($audio_data_tmp[0]);
        }
    }
    
    public function getQuestions() {
        return $this->questions;
    }
    
    public function getUserInput() {
        return $this->userInput;
    }
    
    public function handleUserInput($teid) {
        foreach ($this->questions as $question) {
            $inputString = Question::getUserInput($teid, $question->getQid());
            
            if($question->getQtid() == 4){
                $inputString = preg_replace("/[^a-zA-Z]/", "", $inputString);
                $userContentPoints = 0;
                $userLinguisticPoints = 0;
                $noOfCorrectAnswers = $question->getNumberOfCorrectAnswers();
                $cpPerAnswer = $question->getContentPointsPerAnswer();
                $mcAnswers = $question->getMCAnswers();
                $user_char_array = str_split($inputString);
                foreach($user_char_array as $user_char){
                   if($question->isUserAnswerCorrect($user_char)){
                       $userContentPoints += $cpPerAnswer;
                   }
                }
                if(count($user_char_array) > $noOfCorrectAnswers){
                    $userContentPoints -= $cpPerAnswer * (count($user_char_array) - $noOfCorrectAnswers);
                }
                $userContentPoints = max(array($userContentPoints, 0));
                $bestSolution = $question->getCorrectAnswerCharacters();
            } else if($question->getQtid() == 6){
                $usr_answer_complete = "";
                $userContentPoints = 0;
                $userLinguisticPoints = 0;
                for($i=0;$i<count($inputString);$i++){
                    $usr_answer = $inputString[$i];
                    $usr_answer_complete .= $usr_answer . "-";
                    $yn_questions = $question->getYNQuestions();
                    $matching_yn_question = $yn_questions[$i];
                    if($usr_answer == ($matching_yn_question->isCorrectAnswer()?'true':'false')){
                        $userContentPoints += 1;
                    }
                }
                $inputString = $usr_answer_complete;
                $bestSolution = $question->getBestSolutionSerialized();
            }
            $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][0] = $inputString;
            $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][1] = $bestSolution;
            $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][2] = $userContentPoints;
            $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][3] = $userLinguisticPoints;
        }
    }
    
    public function printExerciseElement($showSolution, $teid) {
        $audio_path = 'src/exercise_imgs/' . $this->audio_data->getFid() . '.' . $this->audio_data->getEnding();
        $out = '<tr>';
            $out .= '<td colspan="" class="" align="left" valign="top" style="padding: 10px; text-align:center;" >';
                $out .= 'Text abspielen:<br>';
                $out .= '<object type="application/x-shockwave-flash" data="src/flash/emff_easy_glaze.swf" width="110" height="34">';
                    $out .= '<param name="movie" value="src/flash/emff_easy_glaze.swf">';
                    $out .= '<param name="FlashVars" value="src=' . $audio_path . '&amp;shortcuts=yes">';
                $out .= '</object>';
            $out .='</td>';
            $out .= '<td class="leftPoints" style="border-bottom-width: 0px;" width="25"></td><td class="rightPoints" style="border-bottom-width: 0px;" width="25"></td>';
        $out .= '</tr>';
        $out .= '<tr>';
            $out .= '<td class="" width="796" align="left" valign="top" colspan="3">';
                $out .= '<table cellpadding="0" cellspacing="0" class="" width="796" border="0">';
                    
                    foreach($this->questions as $question){
                        $out .= '<tr>';
                            if($question->getQtid() == 4){
                                
                                $out .= '<td class="question" colspan="" width="721" align="left" valign="center" style="padding: 30px 5px 5px 5px;">';    
                                    $out .= '<b>' . $question->getQuestionIntroductionText() . '</b>';
                                $out .= '</td>';
                                $out .= '<td class="leftPoints" style="border-bottom-width: 0px;" width="25"></td><td class="rightPoints" style="border-bottom-width: 0px;" width="25"></td>';
                                $out .= '</tr>';
                                $out .= '<tr>';
                                $out .= '<td class="" colspan="" width="" align="left" valign="top" style="padding: 25px 5px;">';
                                $out .= '<ol type="a" style="padding-left: 30px;">';
                                foreach($question->getMCAnswers() as $mc_answer){
                                    $out .= '<li>' . $mc_answer->getAnswerText() . '</li>';
                                }
                                $out .= '</ol>';
                                if (!$showSolution) {
                                    if($teid != -1){
                                        $user_input_so_far = Question::getUserInput($teid, $question->getQid());
                                    }
                                    $out .= '<br>Buchstaben der L&ouml;sung: ';
                                    $out .= '<input class="exam-answer" style="width: 150px;" type="text"';
                                    $out .= ' onkeydown="return nextElementOnEnter(this, event);"';
                                    $out .= ' name="user_inputs[\'' . ($this->eeid) . '\'][\'' . ($question->getQid()) . '\']"';
                                    $out .= ' value="' . (!empty($user_input_so_far->user_answer) 
                                                ? $user_input_so_far->user_answer : '') . '"';
                                    $out .= ' />';
                                } else{
                                    $userInput = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][0];
                                    $bestSolution = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][1];
                                    $userContentPoints = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][2];
                                    $userLinguisticPoints = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][3];
                                    $answeredCorrectly = $userContentPoints + $userLinguisticPoints == $question->getContentPoints() + $question->getLinguisticPoints();
                                    if ($answeredCorrectly) {
                                        $out .= '<br>Deine Antwort war korrekt: ';
                                        $out .= '<b><font class="correctAnswer">' . $userInput . '</font></b>';
                                    } else{
                                        $out .= '<br>Deine Antwort: ';
                                        $out .= '<b>'. $userInput .'</b>';
                                        $out .= '<br>Buchstaben der L&ouml;sung: ';
                                        $out .= '<b>'. $bestSolution .'</b>';
                                    }
                                }
                             }else if($question->getQtid() == 6){
                                $out .= '<td class="" colspan="" width="711">';   
                                    $out .= '<table class="hoerverstehen" width="731" cellpadding="0" cellspacing="0">';
                                        $out .= '<tr>';
                                            $out .= '<td class="ynquesions" width="681" align="left" valign="bottom" style="padding-left:5px; padding-top:25px;">';
                                                $out .= '<b>Beantworten Sie folgenden Fragen mit Ja oder Nein:</b> <br>';
                                            $out .= '</td>';
                                            $out .= '<td class="ynquesions" width="45" align="center" valign="bottom">';
                                                $out .= '<b>Ja</b>';
                                            $out .= '</td>';
                                            $out .= '<td width="45" align="center" valign="bottom" style="border-bottom-style: solid;border-bottom-width: 1px;">';
                                                $out .= '<b>Nein</b>';
                                            $out .= '</td>';
                                        $out .= '</tr>';
                                if(!$showSolution){
                                    if($teid != -1){
                                        $user_input_so_far = Question::getUserInput($teid, $question->getQid());
                                    }
                                    if(!empty($user_input_so_far)){
                                        $usr_input_so_far_answers = split("\-", $user_input_so_far->user_answer);
                                    }
                                        $question_cnt=0;
                                        $answer_cnt = 0;
                                        foreach($question->getYNQuestions() as $yn_question){
                                            $true_selection = '';
                                            $false_selection = '';
                                            if(!empty ($usr_input_so_far_answers)){
                                                $true_selection = ($usr_input_so_far_answers[$answer_cnt] == 'true')?'checked="checked"':'';
                                                $false_selection = ($usr_input_so_far_answers[$answer_cnt] == 'false')?'checked="checked"':'';
                                            }
                                            $out .= '<tr>';
                                                $out .= '<td class="ynquesions" width="681" align="left" style="padding-left:10px;">';
                                                    $out .= ++$question_cnt . '. ' . $yn_question->getYNQuestionText();
                                                $out .= '</td>';
                                                $out .= '<td class="ynquesions" width="45" align="center" style="padding:10px;">';
                                                    $out .= '<input name=user_inputs[\'' . ($this->eeid) . '\'][\'' . ($question->getQid()) . '\'][' . $answer_cnt . '] type="radio" ' . $true_selection . ' value="true">';
                                                $out .= '</td>';
                                                $out .= '<td width="45" align="center" style="padding:10px; border-bottom-style: solid;border-bottom-width: 1px;">';
                                                    $out .= '<input name=user_inputs[\'' . ($this->eeid) . '\'][\'' . ($question->getQid()) . '\'][' . $answer_cnt++ . '] type="radio" ' . $false_selection . ' value="false">';;
                                                $out .= '</td>';
                                            $out .= '</tr>';

                                        }
                                    $out .= '</table>';
                                } else if($showSolution){
                                    $userInput = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][0];
                                    $bestSolution = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][1];
                                    $userContentPoints = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][2];
                                    $userLinguisticPoints = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][3];
                                    $user_answers = split("\-", $userInput);
                                    $solutions = split("\|", $bestSolution);
                                    $answer_cnt = 0;
                                    $question_cnt = 0;
                                    foreach($question->getYNQuestions() as $yn_question){
                                        $out .= '<tr>';
                                            $out .= '<td class="ynquesions" width="681" align="left" style="padding-left:10px;">';
                                                $out .= ++$question_cnt . '. ' . $yn_question->getYNQuestionText();
                                            $out .= '</td>';
                                            
                                           if ($user_answers[$answer_cnt] == $solutions[$answer_cnt]){
                                                if ($user_answers[$answer_cnt] == "true"){
                                                    $out .= '<td class="ynquesions" width="45" align="center" style="padding:10px;">';
                                                        $out .= '<b><font class="correctAnswer">JA</font></b>';
                                                    $out .= '</td>';
                                                    $out .= '<td width="45" align="center" style="padding:10px; border-bottom-style: solid;border-bottom-width: 1px;">';
                                                        $out .= ' ';
                                                    $out .= '</td>';                         
                                                }
                                                else if ($user_answers[$answer_cnt] == "false"){
                                                    $out .= '<td class="ynquesions" width="45" align="center" style="padding:10px;">';
                                                        $out .= ' ';
                                                    $out .= '</td>';
                                                    $out .= '<td width="45" align="center" style="padding:10px; border-bottom-style: solid;border-bottom-width: 1px;">';
                                                        $out .= '<b><font class="correctAnswer">Nein</font></b>';
                                                    $out .= '</td>'; 
                                                } 
                                            }
                                            else if($user_answers[$answer_cnt] != null){
                                                if ($user_answers[$answer_cnt] == "true"){
                                                    $out .= '<td class="ynquesions" width="45" align="center" style="padding:10px;">';
                                                        $out .= '<b><font class="falseAnswer">JA</font></b>';
                                                    $out .= '</td>';
                                                    $out .= '<td width="45" align="center" style="padding:10px; border-bottom-style: solid;border-bottom-width: 1px;">';
                                                        $out .= ' ';
                                                    $out .= '</td>';                         
                                                }
                                                else if ($user_answers[$answer_cnt] == "false"){
                                                    $out .= '<td class="ynquesions" width="45" align="center" style="padding:10px;">';
                                                        $out .= ' ';
                                                    $out .= '</td>';
                                                    $out .= '<td width="45" align="center" style="padding:10px; border-bottom-style: solid;border-bottom-width: 1px;">';
                                                        $out .= '<b><font class="falseAnswer">Nein</font></b>';
                                                    $out .= '</td>'; 
                                                } 
                                            }else if($user_answers[$answer_cnt] == null){
                                                if ($solutions[$answer_cnt] == "true"){
                                                    $out .= '<td class="ynquesions" width="45" align="center" style="padding:10px;">';
                                                        $out .= 'JA';
                                                    $out .= '</td>';
                                                    $out .= '<td width="45" align="center" style="padding:10px; border-bottom-style: solid;border-bottom-width: 1px;">';
                                                        $out .= ' ';
                                                    $out .= '</td>';                         
                                                }
                                                else if ($solutions[$answer_cnt] == "false"){
                                                    $out .= '<td class="ynquesions" width="45" align="center" style="padding:10px;">';
                                                        $out .= ' ';
                                                    $out .= '</td>';
                                                    $out .= '<td width="45" align="center" style="padding:10px; border-bottom-style: solid;border-bottom-width: 1px;">';
                                                        $out .= 'Nein';
                                                    $out .= '</td>'; 
                                                } 
                                            }
                                            $answer_cnt++;
                                            $out .= '</tr>';
                                            
                                        }
                                        $out .= '</table>';
                                    } 
                            }
                             
                            $out .= '</td>';
                            $out .= '<td class="leftPoints" width="25" align="center" valign="bottom" style="border-bottom-width: 0px;">';
                            if ($showSolution) {
                                for ($i = 0; $i < $userContentPoints; $i++) {
                                    $out .= '<img class="points" src="src/imgs/content_green.png" alt="cp"><br />';
                                }
                                for ($i = $userContentPoints; $i < $question->getContentPoints(); $i++) {
                                    $out .= '<img class="points" src="src/imgs/content.png" alt="cp"><br />';
                                }
                            } else {
                                for ($i = 0; $i < $question->getContentPoints(); $i++) {
                                    $out .= '<img class="points" src="src/imgs/content.png" alt="cp"><br />';
                                }
                            }
                            $out .= '</td>';
                            $out .= '<td class="rightPoints" width="25" align="center" valign="bottom" style="border-bottom-width: 0px;">';
                            if ($showSolution) {
                                for ($i = 0; $i < $userLinguisticPoints; $i++) {
                                    $out .= '<img class="points" src="src/imgs/lingu_green.png" alt="cp"><br />';
                                }
                                for ($i = $userLinguisticPoints; $i < $question->getLinguisticPoints(); $i++) {
                                    $out .= '<img class="points" src="src/imgs/lingu.png" alt="cp"><br />';
                                }
                            } else {
                                for ($i = 0; $i < $question->getLinguisticPoints(); $i++) {
                                    $out .= '<img class="points" src="src/imgs/lingu.png" alt="cp"><br />';
                                }
                            }
                            $out .= '</td>';
                    $out .= '</tr>';    
                    }
                    
                $out .= '</table>';
            $out .= '</td>';
        $out .= '</tr>';
        
        return $out;
        
    }
    
    public function getAllContentPoints() {
        $contentPoints = 0;
        foreach ($this->questions as $question) {
            $contentPoints += $question->getContentPoints();
        }
        return $contentPoints;
    }

    public function getAllLinguisticPoints() {
        $linguisticPoints = 0;
        foreach ($this->questions as $question) {
            $linguisticPoints += $question->getLinguisticPoints();
        }
        return $linguisticPoints;
    }
    
}

?>
