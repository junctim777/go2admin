<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LueckentextElement
 *
 * @author Marco Armbruster
 */
class LueckentextElement extends ExerciseElement {

    //put your code here

    private $sentenceText;
    private $questions;
    private $userInput;

    public function LueckentextElement($eeid, $exercisePosition, $sentenceText) {
        parent::ExerciseElement($eeid, $exercisePosition);
        $this->sentenceText = $sentenceText;
        $this->getFromDatabase();
    }

    protected function getFromDatabase() {
        $query = "SELECT * FROM go2stuko_question WHERE "
                . "eeid = " . $this->eeid . " "
                . "ORDER BY exercise_element_position";
        $questions_data_tmp = Database::getDatasetFromQuery($query);
        foreach ($questions_data_tmp as $question_data_tmp) {
            $this->questions[count($this->questions)] =
                    new Question($question_data_tmp);
        }
    }

    public function getQuestions() {
        return $this->questions;
    }

    public function getUserInput() {
        return $this->userInput;
    }

    private function findBestMatchingSampleSolution($inputWord, $question) {
        $sampleSolutions = $question->getSampleSolutions();
        $maxContentPoints = 0;
        $maxLinguisticPoints = 0;
        $bestSolution = null;
        $punctuations = array(',', '.', '!', '?', '!');
        foreach ($sampleSolutions as $sampleSolution) {
            $contentPoints = 0;
            $linguisticPoints = 0;
            $wordByWordSolutions = $sampleSolution->getSolutionWords();
            $linkedWordGroups = null;
            foreach ($wordByWordSolutions as $wordByWordSolution) {
                $solutionWord = $wordByWordSolution->getValue();
                $solutionWord = str_replace($punctuations, '', $solutionWord);
                $solutionWord = trim($solutionWord);
                $solutionWord = preg_replace('/\s{2,}/sm', ' ', $solutionWord);
                $inputWord = str_replace($punctuations, '', $inputWord);
                $inputWord = trim($inputWord);
                $inputWord = preg_replace('/\s{2,}/sm', ' ', $inputWord);
                if ($inputWord == $solutionWord) {
                    $contentPoints += $wordByWordSolution->getContentPoints();
                    $linguisticPoints += $wordByWordSolution->getLinguisticPoints();
                }
            }
            if (($contentPoints + $linguisticPoints) > ($maxContentPoints + $maxLinguisticPoints)) {
                $maxContentPoints = $contentPoints;
                $maxLinguisticPoints = $linguisticPoints;
                $bestSolution = $sampleSolution;
            }
        }
        return array($sampleSolution, $maxContentPoints, $maxLinguisticPoints);
    }

    public function handleUserInput($teid) {
        foreach ($this->questions as $question) {
            $inputWord = Question::getUserInput($teid, $question->getQid());
            $userContentPoints = 0;
            $userLinguisticPoints = 0;
            $bestMatch = $this->findBestMatchingSampleSolution($inputWord, $question);
            $bestSolution = $bestMatch[0];
            $userContentPoints = $bestMatch[1];
            $userLinguisticPoints = $bestMatch[2];
            $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][0] = $inputWord;
            $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][1] = $bestSolution;
            $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][2] = $userContentPoints;
            $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][3] = $userLinguisticPoints;
        }
    }

    public function printExerciseElement($showSolution, $teid) {
        $out .= '<tr><td align="left" valign="bottom" width="710">';
        $out .= '<p class="luekentext">';
        $charCnt = 0;
        $this->sentenceText = str_replace("<br>", " ", $this->sentenceText);
        $this->sentenceText = wordwrap($this->sentenceText, 85, " <br> ");
        $this->sentenceText = preg_replace('/\s{2,}/sm', ' ', $this->sentenceText);
        $words = split(" ", $this->sentenceText);
        $cnt = 0;
        $index_from = 0;
        $index_to = 0;
        $tmpUserContentPoints = 0;
        $tmpUserLinguisticPoints = 0;
        foreach ($words as $word) {
            if ($word == "<br>") {
                $out .= '</p></td>';
                $out .= '<td class="contentPoints" style="min-width: 25px" align="center" valign="" width="25">';
                    $out .= $this->printContentPoints($showSolution, $index_from, $index_to, $tmpUserContentPoints);
                $out .= '</td><td class="linguisticPoints" style="min-width: 25px" align="center" valign="" width="25">';
                    $out .= $this->printLinguisticPoints($showSolution, $index_from, $index_to, $tmpUserLinguisticPoints);
                $index_from = $index_to;
                $out .= '</td></tr>';
                $out .= '<tr><td align="left" valign="bottom" width="710">';
                $out .= '<p class="luekentext">';
                $tmpUserContentPoints = 0;
                $tmpUserLinguisticPoints = 0;
            } else {
                $question = $this->isWordAQuestion($cnt++, $this->questions);
                if ($question != null) {
                    $index_to++;
                    if (!$showSolution) {
                        if($teid != -1){
                            $user_input_so_far = Question::getUserInput($teid, $question->getQid());
                        }
                        $out .= '<input tabindex="' . ++LueckentextExercise::$tabindex . '" class="exam-answer-luekentext" type="text"';
                        $out .= ' onkeydown="return nextElementOnEnter(this, event);"';
                        $out .= ' name="user_inputs[\'' . ($this->eeid) . '\'][\'' . ($question->getQid()) . '\']"';
                        $out .= ' value="' . (!empty($user_input_so_far->user_answer) 
                                ? $user_input_so_far->user_answer : '') . '" /> ';
                        $charCnt += 12;
                    } else {
                        $inputWord = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][0];
                        $bestSolution = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][1];
                        $userContentPoints = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][2];
                        $userLinguisticPoints = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][3];
                        $answeredCorrectly = $userContentPoints + $userLinguisticPoints == $question->getContentPoints() + $question->getLinguisticPoints();
                        $out .= '<font class="correctAnswer"><b>' . (($answeredCorrectly) ? $inputWord : $bestSolution->getSolutionText()) . '</b></font> ';
                        $out .= '<font class="falseAnswer"><b>' . (($answeredCorrectly) ? '' : '(' . $inputWord . ')') . '</b></font> ';
                        $out .= '<a href="user_solution_veto.php?ssid=' . $bestSolution->getSsid() . '" target="_blank">?</a> ';
                        $tmpUserContentPoints += $userContentPoints;
                        $tmpUserLinguisticPoints += $userLinguisticPoints;
                    }
                } else {
                    $out .= $word . " ";
                    $charCnt += strlen($word) + 1;
                }
            }
        }
        $out .= '</p>';
        $out .= '</td>';

        $out .= '<td class="contentPoints" style="min-width: 25px" align="center" valign="" width="25">';
            $out .= $this->printContentPoints($showSolution, $index_from, $index_to, $tmpUserContentPoints);
        $out .= '</td><td class="linguisticPoints" style="min-width: 25px" align="center" valign="" width="25">';
            $out .= $this->printLinguisticPoints($showSolution, $index_from, $index_to, $tmpUserLinguisticPoints);
        $out .= '</td></tr>';
        return $out;
    }
    
    private function printContentPoints($showSolution, $from_index, $to_index, $tmpUserContentPoints){
        $out = "";
        if ($showSolution) {
            for ($i = 0; $i < $tmpUserContentPoints; $i++) {
                $out .= '<img class="points" src="src/imgs/content_green.png" alt="cp"><br />';
            }
            for ($i = $tmpUserContentPoints; $i < $this->getAllContentPointsForQuestionRange($from_index, $to_index); $i++) {
                $out .= '<img class="points" src="src/imgs/content.png" alt="cp"><br />';
            }
        } else {
            for ($i = 0; $i < $this->getAllContentPointsForQuestionRange($from_index, $to_index); $i++) {
                $out .= '<img class="points" src="src/imgs/content.png" alt="cp"><br />';
            }
        }
        return $out;
    }
    
    private function printLinguisticPoints($showSolution, $from_index, $to_index, $tmpUserLinguisticPoints){
        $out = "";
        if ($showSolution) {
            for ($i = 0; $i < $tmpUserLinguisticPoints; $i++) {
                $out .= '<img class="points" src="src/imgs/lingu_green.png" alt="cp"><br />';
            }
            for ($i = $tmpUserLinguisticPoints; $i < $this->getAllLinguisticPointsForQuestionRange($from_index, $to_index); $i++) {
                $out .= '<img class="points" src="src/imgs/lingu.png" alt="cp"><br />';
            }
        } else {
            for ($i = 0; $i < $this->getAllLinguisticPointsForQuestionRange($from_index, $to_index); $i++) {
                $out .= '<img class="points" src="src/imgs/lingu.png" alt="cp"><br />';
            }
        }
        return $out;
    }
    
    private function getAllContentPointsForQuestionRange($from_index, $to_index) {
        $contentPoints = 0;
        for ($i = $from_index; $i < $to_index; $i++) {
            $question = $this->questions[$i];
            $contentPoints += $question->getContentPoints();
        }
        return $contentPoints;
    }

    private function getAllLinguisticPointsForQuestionRange($from_index, $to_index) {
        $linguisticPoints = 0;
        for ($i = $from_index; $i < $to_index; $i++) {
            $question = $this->questions[$i];
            $linguisticPoints += $question->getLinguisticPoints();
        }
        return $linguisticPoints;
    }

    public function getAllContentPoints() {
        $contentPoints = 0;
        foreach ($this->questions as $question) {
            $contentPoints += $question->getContentPoints();
        }
        return $contentPoints;
    }

    public function getAllLinguisticPoints() {
        $linguisticPoints = 0;
        foreach ($this->questions as $question) {
            $linguisticPoints += $question->getLinguisticPoints();
        }
        return $linguisticPoints;
    }

    private function isWordAQuestion($word_index, $questions) {
        foreach ($questions as $question) {
            if ($question->getExerciseElementPosition() == $word_index)
                return $question;
        }
        return null;
    }

}

?>
