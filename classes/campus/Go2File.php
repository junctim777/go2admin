<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Go2File
 *
 * @author Marco Armbruster
 */
class Go2File {
    //put your code here
    var $dbGo2File;
    
    function Go2File($dbGo2File) {
        $this->dbGo2File = $dbGo2File;
    }
    
    public function getFid(){
        return $this->dbGo2File->fid;
    }
    
    public function getEnding(){
        return $this->dbGo2File->ending;
    }
    
    public function getFileType(){
        return $this->dbGo2File->file_type;
    }
    
    public function getDescription(){
        return $this->dbGo2File->description;
    }
    
}

?>
