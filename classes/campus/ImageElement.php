<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TextproductionElement
 *
 * @author Marco Armbruster
 */
class ImageElement extends ExerciseElement{
    //put your code here
    
    private $picture;
    public $questions;
    private $fid;
    private $userInput;
    
    /**
     *
     * @param type $eeid
     * @param type $exercisePosition
     * @param type $prolog
     * @param type $question 
     */
    function ImageElement($eeid, $exercisePosition, $fid) {
        parent::ExerciseElement($eeid, $exercisePosition);
        $this->fid = $fid;
        $this->getFromDatabase();
    }
    
    public function getQuestions(){
        return $this->questions;
    }
    
    protected function getFromDatabase(){
        $query = "SELECT * FROM go2stuko_question WHERE " 
                ."eeid = " . $this->eeid . " "
                ."ORDER BY exercise_element_position";
        $questions_data_tmp = Database::getDatasetFromQuery($query);
        foreach($questions_data_tmp as $question_data_tmp){
            $this->questions[count($this->questions)] = 
                    new Question($question_data_tmp);
        }
        if(!empty($this->fid)){
            $query = "SELECT * FROM go2stuko_file WHERE " 
                    ."fid = " . $this->fid;
            $picture_data_tmp = Database::getDatasetFromQuery($query);
            if(count($picture_data_tmp) > 0){
                $this->picture = new Go2File($picture_data_tmp[0]);
            }
        }
    }
    
    public function handleUserInput($teid) {
        $question_id = $this->questions[0]->getQid();
        $this->userInput = Question::getUserInput($teid, $question_id)->user_answer;
        
    }
    
    private function getAnswerNote(){
        $answerNote = '';
        foreach($this->questions as $question){
            $answerNote .= $question->getAnswerNote() . "<br>";
        }
        return $answerNote;
    }
    
    /**
     * 
     */
    public function printExerciseElement($showSolution, $teid) {
        if(!empty($this->picture)){
            $picture_path = 'src/exercise_imgs/' . $this->picture->getFid() . '.' . $this->picture->getEnding();
        }
        $out = '<tr><td align="left" valign="bottom" width="560">';
        $out .= '<table cellpadding="0" cellspacing="0" width="560"><tr><td>';
        $cnt = 1;
        foreach($this->questions as $question){
            $question_id = $question->getQid();
            $out .= '<tr>';
                $out .= '<td class="question-picture" align="left" valign="top">';
                    $out .= $cnt++ . ") ";
                    $out .= $question->getQuestionText();
                $out .= '</td>';
            $out .= '</tr>';
        }
        $out .= '<tr>';
            $out .= '<td class="question-picture" align="center" valign="top" style="padding-bottom: 20px;">';
                if(!empty($this->picture)){
                    $out .= '<img src="' . $picture_path . '" alt="' . $this->picture->getDescription() . '">';
                }
            $out .= '</td>';
        $out .= '</tr>';        
        $out .= '</td></tr></table>';
        $out .= '</td><td class="contentPoints" style="min-width: 25px" align="center" valign="top" width="25">';
            for($i=0; $i<$this->getAllContentPoints(); $i++){
                $out .= '<img class="points" src="src/imgs/content.png" alt="cp"><br />';
            }
        $out .= '</td>';
        $out .= '<td class="linguisticPoints" style="min-width: 25px" align="center" valign="top" width="25">';
            for($i=0; $i<$this->getAllLinguisticPoints(); $i++){
                $out .= '<img class="points" src="src/imgs/lingu.png" alt="cp"><br />';
            }
        $out .= '</td></tr>';
        if(!$showSolution){
            if($teid != -1){
                $tmp_question = $this->questions[0];
                $user_input_so_far = Question::getUserInput($teid, $tmp_question->getQid());
                $user_input_so_far = split("<br>", $user_input_so_far->user_answer);
            }
            for($i=0; $i < 10; $i++){
                $out .= '<tr>';
                $out .= '<td class="answer-picture" align="left" valign="top">';
                    $out .= '<input class="exam-answer" type="text"';
                    $out .= ' onkeydown="return nextElementOnEnter(this, event);"';
                    $out .= ' name="user_inputs[' . $i .']"';
                    $out .= ' value="' . (!empty($user_input_so_far[$i]) 
                                ? $user_input_so_far[$i] : '') . '"';
                    $out .= ' />';
                    $out .= '<br />';
                $out .= '</td align="left" valign="top">';
                $out .= '<td class="contentPoints" align="center" valign="bottom" width="25">';
                $out .= '</td>';
                $out .= '<td class="linguisticPoints" align="center" valign="bottom" width="25">'; 
                $out .= '</td>';
                $out .= '</tr>';
            }
        } else{
            $userInput = $this->userInput;
            $out .= '<tr>';
            $out .= '<td width="560" class="answer-picture" align="left" valign="top">';
                $out .= '<b>Musterl&ouml;sung:</b><br>';
                $out .= '<font class="correctAnswer">' . $this->getAnswerNote() . '</font><br><br>';
                $out .= '<b>Deine Antwort:</b><br>';
                $out .= '<font class="">' . $userInput . '</font> ';
            $out .= '</td align="left" valign="top">';
            $out .= '<td class="contentPoints" align="center" valign="bottom" width="25">';
            $out .= '</td>';
            $out .= '<td class="linguisticPoints" align="center" valign="bottom" width="25">'; 
            $out .= '</td>';   
            $out .= '</tr>';           
        }
        return $out;
    }
    
    public function getAllContentPoints() {
        $contentPoints = 0;
        foreach($this->questions as $question){
            $contentPoints += $question->getContentPoints();
        }
        return $contentPoints;
    }
    
    public function getAllLinguisticPoints() {
        $linguisticPoints = 0;
        foreach($this->questions as $question){
            $linguisticPoints += $question->getLinguisticPoints();
        }
        return $linguisticPoints;
    }
}

?>
