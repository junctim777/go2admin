<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PointedWordGroup
 *
 * @author Marco Armbruster
 */
class LinkedWordGroup {
    //put your code here
    
    private $wordSolutions;
    
    function LinkedWordGroup($firstSolution) {
        $index = count($this->wordSolutions);
        $this->wordSolutions[$index][0] = $firstSolution;
        $this->wordSolutions[$index][1] = false;
    }
    
    public function addWordSolutionIfItIsWithin($wordSolution){
        foreach($this->wordSolutions as $groupSolution){
            if($groupSolution[0]->getSentencePosition() == $wordSolution->getParentLinkSentencePosition()){
                $index = count($this->wordSolutions);
                $this->wordSolutions[$index][0] = $wordSolution;
                $this->wordSolutions[$index][1] = false;
            }
        }
    }
    
    public function wasAnsweredCorretly($inputWords){
        $punctuations = array(',', '.', '!', '?', '!');
        for($i=0; $i < count($this->wordSolutions); $i++){
            $groupSolution = $this->wordSolutions[$i];
            foreach($inputWords as $inputWord){
                $wordSolution = $groupSolution[0];
                $word = $wordSolution->getValue();
                $word = str_replace($punctuations, '', $word);
                $word = trim($word);
                $word = preg_replace('/\s{2,}/sm', ' ', $word);
                //echo "RICHTIG: " . $word . " | USER: " . $inputWord. "<br>";
                if(strtolower($word) == strtolower($inputWord)){
                    $this->wordSolutions[$i][1] = true;
                    break;
                }
            }
            //echo "<br>";
        }
        //echo "<br><br><br>";
        foreach($this->wordSolutions as $groupSolution){
            $wordSolution = $groupSolution[0];
            $word = $wordSolution->getValue();
            //echo $word . " - " . $groupSolution[1] . "<br>";
            if(!$groupSolution[1]) return false;
        }
        //echo "<br><br>";
        return true;
    }
    
    public function printWordGroup(){
        $out = '';
        foreach($this->wordSolutions as $wordSolution){
            $wordSolution = $wordSolution[0];
            $out .= $wordSolution->getValue() . " ";
        }
        return $out;
    }
    
    public function getWordSolutions(){
        return $this->wordSolutions;
    }

    public function getAllContentPoints(){
        $contentPoints = 0;
        foreach($this->wordSolutions as $wordSolution){
            $wordSolution = $wordSolution[0];
            $contentPoints += $wordSolution->getContentPoints();
        }
        return $contentPoints;
    }
    
    public function getAllLinguisticPoints(){
        $linguisticPoints = 0;
        foreach($this->wordSolutions as $wordSolution){
            $wordSolution = $wordSolution[0];
            $linguisticPoints += $wordSolution->getLinguisticPoints();
        }
        return $linguisticPoints;
    }

}

?>
