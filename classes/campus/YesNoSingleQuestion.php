<?php

class YesNoSingleQuestion {
    //put your code here
    
    private $yn_question_text;
    private $isCorrect;
    
    public function YesNoSingleQuestion($yn_question_text, $isCorrect){
        $this->yn_question_text = $yn_question_text;
        $this->isCorrect = $isCorrect;
    }
    
    public function getYNQuestionText(){
        return $this->yn_question_text;
    }
    
    public function isCorrectAnswer(){
        return $this->isCorrect;
    }
}

?>
