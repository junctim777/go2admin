<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MultipleChoiceQuestion
 *
 * @author Marco Armbruster
 */
class YesNoQuestion extends Question{
    //put your code here
    
    private $yn_question;
    
    public function YesNoQuestion($dbQuestion) {
        parent::Question($dbQuestion);
        
        $xml = new SimpleXMLElement($this->getQuestionText());
        foreach($xml->question as $question){
            $isCorrect = $question->attributes()->correct == "true";
            $this->yn_question[count($this->yn_question)] = new YesNoSingleQuestion((string)$question, $isCorrect);
        }
        
    }
    
    public function getQuestionIntroductionText(){
        return $this->question_introduction;
    }
                   
    public function getYNQuestions(){
        return $this->yn_question;
    }
    
    public function getBestSolutionSerialized(){
        $best_answer = "";
        foreach($this->yn_question as $yn_quest){
            $best_answer .= ($yn_quest->isCorrectAnswer() ? 'true' : 'false') . "|";
        }
        return $best_answer;
    }
    
    public function getNumberOfCorrectAnswers(){
        $no = 0;
        foreach($this->yn_question as $yn_quest){
            if($yn_quest->isCorrectAnswer()){
                $no++;
            }
        }
        return $no;
    }
    
    public function getContentPointsPerAnswer(){
        return $this->getContentPoints() / $this->getNumberOfCorrectAnswers();
    }
    /*
    public function isUserAnswerCorrect($user_char){
        $correct = false;
        $cnt = 0;
        for($i=65; $i<(65+count($this->yn_question)); $i++){
            $answer_char = strtolower(chr($i));
            $user_char = strtolower($user_char);
            $mc_answer = $this->yn_question[$cnt++];
            if($answer_char == $user_char && $mc_answer->isCorrectAnswer()){
                return true;
            }
        }
        return false;
    }
*/
    
}

?>
