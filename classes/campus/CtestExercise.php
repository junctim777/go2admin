<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CtestExercise
 *
 * @author Marco Armbruster
 */
class CtestExercise extends Exercise{
    //put your code here
    
    public static $tabindex = 0;
    
    public function CtestExercise($eid, $heading, $introduction_text) {
        parent::Exercise($eid, $heading, $introduction_text);
    }
    
    public function handleUserInput($teid) {
        foreach($this->exerciseElements as $exerciseElement){
            $exerciseElement -> handleUserInput($teid);
        }
    }
    
    public function printStatistic() {
        $out = '<h3>C-Test</h3>';
        $out .= '<b>' . $this->heading."</b><br>";
        $out .= 'Inhaltspunkte: ' . $this->getAllUserContentPoints() . ' von ' . $this->getAllContentPoints() . "<br>";
        $out .= 'Sprachpunkte: ' . $this->getAllUserLinguisticPoints() . ' von ' . $this->getAllLinguisticPoints() . "<br>";
        return $out;
    }
    
    public function printExercise($showSolution, $teid, $showBackButton){
        $out .= '<table class="luekentext" cellpadding="0" cellspacing="0">';
        $out .= '<tr><td width="10"></td><td width="500" class="questions"><br><br>';
        foreach($this->exerciseElements as $exerciseElement){
            $out .= $exerciseElement->printExerciseElement($showSolution, $teid);
        }
        $out .= '<br></td><td width="10"></td><td width="10" class="questions_right_border"></td><td width="60" valign="bottom" class="points">';
        $out .= '<br><br><i>Go2:</i><br>' . ($showSolution ? $this->getAllUserLinguisticPoints() . ' / ' . parent::getAllLinguisticPoints() : parent::getAllLinguisticPoints());
        $out .= '<br><br><i>Lehrer:</i><br>____ / ' . parent::getAllLinguisticPoints();
        $out .= '</td>'; 
        $out .= '</tr>';
        $out .= '</table>';
        return $out;
    }
    
    public function getAllUserContentPoints() {
        $contentPoints = 0;
        foreach($this->exerciseElements as $exerciseElement){
            $userInput = $exerciseElement->getUserInput();
            foreach($exerciseElement->getQuestions() as $question){
                $contentPoints += $userInput['\'' . $exerciseElement->getEeid() . '\'']['\'' . $question->getQid() . '\''][2];
            }
        }
        return $contentPoints;
    }
    
    public function getAllUserLinguisticPoints() {
        $linguisticPoints = 0;
        foreach($this->exerciseElements as $exerciseElement){
            $userInput = $exerciseElement->getUserInput();
            foreach($exerciseElement->getQuestions() as $question){
                $linguisticPoints += $userInput['\'' . $exerciseElement->getEeid() . '\'']['\'' . $question->getQid() . '\''][3];
            }
        }
        return $linguisticPoints;
    }
    
}

?>
