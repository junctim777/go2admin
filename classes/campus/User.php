<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author Marco Armbruster
 */
class User {

    var $uid;

    function User($uid) {
        $this->uid = $uid;
    }

    public function getUid(){
     return $this->uid;
    }
    
    public function getUserData() {
        $query = "SELECT * FROM go2stuko_user WHERE uid = " . $this->uid;
        $user_data_tmp = Database::getDatasetFromQuery($query);
        $user_data = $user_data_tmp[0];
        return $user_data;
    }

    public function showDiscountBox() {
        $query = "SELECT (DATE(NOW()) > end_date) AS demo_finished FROM go2stuko_user u, go2stuko_demo_access da WHERE u.uid = da.uid AND u.uid = " . $this->uid;
        $user_demo_access_data_tmp = Database::getDatasetFromQuery($query);
        if(count($user_demo_access_data_tmp) == 0) return false;
        else{
            foreach($user_demo_access_data_tmp as $user_demo_access_data){
                if($user_demo_access_data->demo_finished == 1) return true;
            }
        }
        return false;
    }
    
    public function printUserData(){
        $userData = $this->getUserData();
        $studyAddress = $this->getStudyAddressData();
        $homeAddress = $this->getHomeAddressData();
        $contactDetails = $this->getContactDetails();
        $studentInformations = $this->getStudentInformation();
        
        $out = "Nachname: " . $userData->surname . "<br>";
        $out .= "Vorname: " . $userData->forename . "<br>";
        $out .= "Registriert seit: " . $userData->created . "<br><br>";
        
        $out .= "Studienkolleg: " . $studentInformations->studienkolleg_name . "<br><br>";
        
        $out .= "<b>Kontaktdaten:</b><br>";
        $out .= "Email: " . $contactDetails->email_address . "<br>";
        $out .= "Telefon: " . $contactDetails->telephone_area_code . " " . $contactDetails->phone_number . "<br>";
        $out .= "Handy: " . $contactDetails->mobile_phone_area_code . " " . $contactDetails->mobile_phone_number . "<br><br>";
        
        if($studyAddress != null){
            $out .= "<b>Studienadresse:</b><br>";
            $out .= $studyAddress->road . " " . $studyAddress->house_number . "<br>";
            $out .= $studyAddress->postal_code . " " . $studyAddress->city . "<br><br>";
        }
        
        if($homeAddress != null){
            $out .= "<b>Heimatadresse:</b><br>";
            $out .= $homeAddress->road . " " . $homeAddress->house_number . "<br>";
            $out .= $homeAddress->postal_code . " " . $homeAddress->city . "<br>";
            $out .= $homeAddress->country_name . "<br><br>";
        }
        return $out;
    }
    
    public function getUserEmailAddress(){
        return $this->getContactDetails()->email_address;
    }
    
    public function getUserForename(){
        return $this->getUserData()->forename;
    }
    
    public function getUserSurname(){
        return $this->getUserData()->surname;
    }
    
    public function getLoginData() {
        $query = "SELECT * FROM go2stuko_login_data WHERE uid = " . $this->uid;
        $user_login_data_tmp = Database::getDatasetFromQuery($query);
        $user_login_data = $user_login_data_tmp[0];
        return $user_login_data;
    }

    public function getStudyAddressData() {
        $query = "SELECT * FROM go2stuko_address a, go2stuko_user u, go2stuko_country c 
                    WHERE u.uid = " . $this->uid
                    . " AND a.aid = u.study_aid AND a.cid = c.cid";
        $user_study_address_data_tmp = Database::getDatasetFromQuery($query);
        if (count($user_study_address_data_tmp) > 0) {
            $user_study_address_data = $user_study_address_data_tmp[0];
            return $user_study_address_data;
        } else return null;
    }

    public function getHomeAddressData() {
        $query = "SELECT * FROM go2stuko_address a, go2stuko_user u, go2stuko_country c 
                    WHERE u.uid = " . $this->uid
                    . " AND a.aid = u.home_country_aid AND a.cid = c.cid";
        $user_home_address_data_tmp = Database::getDatasetFromQuery($query);
        if (count($user_home_address_data_tmp) > 0) {
            $user_home_address_data = $user_home_address_data_tmp[0];
            return $user_home_address_data;
        } else return null;
    }

    public function getContactDetails() {
        $query = "SELECT * FROM go2stuko_user_contact_details cd, go2stuko_user u WHERE u.uid = " . $this->uid
                . " AND cd.cdid = u.study_cdid";
        $user_study_contact_details_data_tmp = Database::getDatasetFromQuery($query);
        if (count($user_study_contact_details_data_tmp) > 0) {
            $user_study_contact_details_data = $user_study_contact_details_data_tmp[0];
            return $user_study_contact_details_data;
        } else return null;
    }

    public function getStudentInformation() {
        $query = "SELECT * FROM go2stuko_student_information si, "
                . " go2stuko_university_studies us, "
                . " go2stuko_studienkolleg sk "
                . "WHERE si.uid = " . $this->uid
                . " AND si.usid = us.usid AND si.skid = sk.skid";
        $user_student_information_data_tmp = Database::getDatasetFromQuery($query);
        if (count($user_student_information_data_tmp) > 0) {
            $user_student_information_data = $user_student_information_data_tmp[0];
            return $user_student_information_data;
        } else return null;
    }
    
}

?>
