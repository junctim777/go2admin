<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SentenceElement
 *
 * @author Marco Armbruster
 */
class SentenceElement extends ExerciseElement {

    //put your code here

    private $prolog;
    private $question;
    private $userInput;

    /**
     *
     * @param type $eeid
     * @param type $exercisePosition
     * @param type $prolog
     * @param type $question 
     */
    function SentenceElement($eeid, $exercisePosition, $prolog) {
        parent::ExerciseElement($eeid, $exercisePosition);
        $this->prolog = $prolog;
        $this->getFromDatabase();
    }

    public function getUserInput() {
        return $this->userInput;
    }

    protected function getFromDatabase() {
        $query = "SELECT * FROM go2stuko_question WHERE "
                . "eeid = " . $this->eeid . " "
                . "ORDER BY exercise_element_position";
        $questions_data_tmp = Database::getDatasetFromQuery($query);
        $this->question =
                new Question($questions_data_tmp[0]);
    }

    private function findBestMatchingSampleSolution($inputWords) {
        $sampleSolutions = $this->question->getSampleSolutions();
        $maxContentPoints = 0;
        $maxLinguisticPoints = 0;
        $bestSolution = null;
        foreach ($sampleSolutions as $sampleSolution) {
            $contentPoints = 0;
            $linguisticPoints = 0;
            $wordByWordSolutions = $sampleSolution->getSolutionWords();
            $linkedWordGroups = null;
            foreach ($wordByWordSolutions as $wordByWordSolution) {
                if ($wordByWordSolution->getParentLinkSentencePosition() == null) {
                    $linkedWordGroups[count($linkedWordGroups)] = new LinkedWordGroup($wordByWordSolution);
                } else {
                    foreach ($linkedWordGroups as $linkedWordGroup) {
                        $linkedWordGroup->addWordSolutionIfItIsWithin($wordByWordSolution);
                    }
                }
            }
            foreach ($linkedWordGroups as $linkedWordGroup) {
                //echo "<font class=\"incorrect\">" . $linkedWordGroup->printWordGroup() . "</font><br>";
                if ($linkedWordGroup->wasAnsweredCorretly($inputWords)) {
                    $contentPoints += $linkedWordGroup->getAllContentPoints();
                    $linguisticPoints += $linkedWordGroup->getAllLinguisticPoints();
                }
            }
            //echo "<br><br>";
            //echo $contentPoints . " | " . $linguisticPoints . "<br>";
            //echo "MAX: " . $maxContentPoints . " | " . $maxLinguisticPoints . "<br>";
            if (($contentPoints + $linguisticPoints) > ($maxContentPoints + $maxLinguisticPoints)) {
                $maxContentPoints = $contentPoints;
                $maxLinguisticPoints = $linguisticPoints;
                $bestSolution = $sampleSolution;
            }
        }
        return array($sampleSolution, $maxContentPoints, $maxLinguisticPoints);
    }
    
    public function handleUserInput($teid) {
        $punctuations = array(',', '.', '!', '?', '!');
        $userInput = str_replace($punctuations, '', Question::getUserInput($teid, $this->question->getQid()));
        $userInput = trim($userInput);
        $userInput = preg_replace('/\s{2,}/sm', ' ', $userInput);
        $inputWords = split(' ', $userInput);
        $bestMatch = $this->findBestMatchingSampleSolution($inputWords);
        $bestSolution = $bestMatch[0];
        $userContentPoints = $bestMatch[1];
        $userLinguisticPoints = $bestMatch[2];
        $this->userInput['\'' . $this->eeid . '\''][0] = $userInput;
        $this->userInput['\'' . $this->eeid . '\''][1] = $bestSolution;
        $this->userInput['\'' . $this->eeid . '\''][2] = $userContentPoints;
        $this->userInput['\'' . $this->eeid . '\''][3] = $userLinguisticPoints;
    }

    /**
     * 
     */
    public function printExerciseElement($showSolution, $teid) {
        $out = '<tr><td class="questions" align="left" valign="bottom" width="710">';
        $out .= '<table cellpadding="0" cellspacing="0" width="710"><tr><td>';
        if (!empty($this->prolog)) {
            $out .= '<tr>';
            $out .= '<td align="left" valign="top">';
            $out .= $this->prolog . "<br>";
            $out .= '</td align="left" valign="top">';
            $out .= '</tr>';
        }
        $out .= '<tr>';
        $out .= '<td align="left" valign="top">' . (($showSolution) ? '<b>' : '');
        $out .= $this->exercisePosition . ") ";
        $out .= $this->question->getQuestionText();
        $out .= ( ($showSolution) ? '</b>' : '') . '</td align="left" valign="top">';
        $out .= '</tr>';
        $out .= '<tr>';
        $out .= '<td align="left" valign="top">';
        if (!$showSolution) {
            if($teid != -1){
                $user_input_so_far = Question::getUserInput($teid, $this->question->getQid());
            }
            $out .= '<input class="exam-answer" type="text"';
            $out .= ' onkeydown="return nextElementOnEnter(this, event);"';
            $out .= ' name="user_inputs[\'' . ($this->eeid) . '\']"';
            $out .= ' value="' . (!empty($user_input_so_far->user_answer) ? $user_input_so_far->user_answer : '') . '" />';
            $out .= '<br />';
            $charCnt += 12;
        } else {
            $userInput = $this->userInput['\'' . $this->eeid . '\''][0];
            $bestSolution = $this->userInput['\'' . $this->eeid . '\''][1];
            $userContentPoints = $this->userInput['\'' . $this->eeid . '\''][2];
            $userLinguisticPoints = $this->userInput['\'' . $this->eeid . '\''][3];
            $answeredCorrectly = $userContentPoints + $userLinguisticPoints == $this->question->getContentPoints() + $this->question->getLinguisticPoints();

            if ($answeredCorrectly) {
                $out .= '<font class="correctAnswer">' . $userInput . '</font>';
            } else {
                $out .= '<table><tr><td><u>Musterl&ouml;sung:</u></td><td style="padding-left: 10px;"><font class="correctAnswer">';
                $solutionWords = split(" ", $bestSolution->getSolutionText());
                $inputWords = split(" ", $userInput);
                $punctuations = array(',', '.', '!', '?', '!');
                foreach ($solutionWords as $solutionWord) {
                    $solutionWord = str_replace($punctuations, '', $solutionWord);
                    $solutionWord = trim($solutionWord);
                    $solutionWord = preg_replace('/\s{2,}/sm', ' ', $solutionWord);
                    $correct = false;
                    foreach ($inputWords as $inputWord) {
                        $inputWord = str_replace($punctuations, '', $inputWord);
                        $inputWord = trim($inputWord);
                        $inputWord = preg_replace('/\s{2,}/sm', ' ', $inputWord);
                        if ($inputWord == $solutionWord) {
                            $correct = true;
                            break;
                        }
                    }
                    if ($correct) {
                        $out .= $solutionWord . " ";
                    } else {
                        $out .= '<b><font class="highlighted">';
                        $out .= $solutionWord . "</font></b> ";
                    }
                }
                $out .= '</font></td></tr>';
                $out .= '<tr><td><u>Deine Antwort:</u></td><td style="padding-left: 10px;">';
                $solutionWords = split(" ", $bestSolution->getSolutionText());
                $inputWords = split(" ", $userInput);
                $punctuations = array(',', '.', '!', '?', '!');
                foreach ($inputWords as $inputWord) {
                    $inputWord = str_replace($punctuations, '', $inputWord);
                    $inputWord = trim($inputWord);
                    $inputWord = preg_replace('/\s{2,}/sm', ' ', $inputWord);
                    $correct = false;
                    $finished = false;
                    foreach ($this->question->getSampleSolutions() as $sampleSolution) {
                        $solutionWordsTmp = split(" ", $sampleSolution->getSolutionText());
                        foreach ($solutionWordsTmp as $solutionWordTmp) {
                            $solutionWordTmp = str_replace($punctuations, '', $solutionWordTmp);
                            $solutionWordTmp = trim($solutionWordTmp);
                            $solutionWordTmp = preg_replace('/\s{2,}/sm', ' ', $solutionWordTmp);
                            if ($inputWord == $solutionWordTmp) {
                                $correct = true;
                                $finished = true;
                                break;
                            }
                        }
                        if ($finished)
                            break;
                    }
                    if ($correct) {
                        $out .= '<font class="correctAnswer">';
                        $out .= $inputWord . "</font> ";
                    } else {
                        $out .= '<b>';
                        $out .= '<font class="falseAnswer">';
                        $out .= $inputWord . "</b></font> ";
                    }
                }
                $out .= '</td></tr></table>';
            }
        }

        $out .= '</td>';
        $out .= '</tr>';
        $out .= '</td></tr></table>';
        $out .= '</td><td class="contentPoints" style="min-width: 25px" align="center" valign="bottom" width="25">';
        if ($showSolution) {
            for ($i = 0; $i < $userContentPoints; $i++) {
                $out .= '<img class="points" src="src/imgs/content_green.png" alt="cp"><br />';
            }
            for ($i = $userContentPoints; $i < $this->question->getContentPoints(); $i++) {
                $out .= '<img class="points" src="src/imgs/content.png" alt="cp"><br />';
            }
        } else {
            for ($i = 0; $i < $this->question->getContentPoints(); $i++) {
                $out .= '<img class="points" src="src/imgs/content.png" alt="cp"><br />';
            }
        }
        $out .= '</td>';
        $out .= '<td class="linguisticPoints" style="min-width: 25px" align="center" valign="bottom" width="25">';
        if ($showSolution) {
            for ($i = 0; $i < $userLinguisticPoints; $i++) {
                $out .= '<img class="points" src="src/imgs/lingu_green.png" alt="cp"><br />';
            }
            for ($i = $userLinguisticPoints; $i < $this->question->getLinguisticPoints(); $i++) {
                $out .= '<img class="points" src="src/imgs/lingu.png" alt="cp"><br />';
            }
        } else {
            for ($i = 0; $i < $this->question->getLinguisticPoints(); $i++) {
                $out .= '<img class="points" src="src/imgs/lingu.png" alt="cp"><br />';
            }
        }
        $out .= '</td></tr>';
        if ($showSolution) {
            $out .= '<tr>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; ">> <a href="user_solution_veto.php?ssid=' . $bestSolution->getSsid() . '" target="_blank">Ich bin anderer Meinung</a></td>';
            $out .= '<td class="contentPoints" style="min-width: 25px" align="center" valign="bottom" width="25"></td>';
            $out .= '<td class="linguisticPoints" style="min-width: 25px" align="center" valign="bottom" width="25"></td>';
            $out .= '</tr>';
        }
        return $out;
    }

    public function getAllContentPoints() {
        return $this->question->getContentPoints();
    }

    public function getAllLinguisticPoints() {
        return $this->question->getLinguisticPoints();
    }

}

?>
