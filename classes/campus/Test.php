<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Test
 *
 * @author Marco Armbruster
 */
class Test {
    
    private $TEST_DURATION = 65;
    
    //put your code here
    private $tid;
    private $teid;
    private $actualExerciseIndex = 0;
    private $exercises;
    private $isFinished = false;
    private $showStatistic = true;
    private $startTime;
    
    function Test($tid, $uid, $teid) {
        
        $this->tid = $tid;
        $this->teid = $teid;
        $this->getFromDatabase();
        $this->startTime = new DateTime();
    }
    
    public function getTid(){
        return $this->tid;
    }
    
    public function getTeid(){
        return $this->teid;
    }
    
    public function getExerciseFromEid($eid){
        foreach($this->exercises as $exercise){
            if($exercise->getEid() == $eid) return $exercise;
        }
        return null;
    }
    
    public function moveOneExerciseBack(){
        if($this->actualExerciseIndex > 0)
            $this->actualExerciseIndex-=2;
    }
    
    private function getActualExercise(){
        if($this->actualExerciseIndex < count($this->exercises)){
            return $this->exercises[$this->actualExerciseIndex];
        } else return null;
    }
    
    public function printActualTestPart(){
        $isFinished = $this->getActualExercise() == null;
        if($isFinished){
            if(!$this->showStatistic){
                include ('templates/test/tpl_finished.php');
                $this->actualExerciseIndex = 0;
                $this->showStatistic = true;
            } else{
                return $this->showStatistic();
            }
        }
        $showBackButton = $this->actualExerciseIndex > 0;
        $actualExercise = $this->getActualExercise();
        //$actualExercise->handleUserInput($this->getTeid());
        $actualExercise->handleUserInput($this->getTeid());
        return ($isFinished) ? 
                "" : $actualExercise->printExercise(true, $this->getTeid(), $showBackButton);
    }
    
    public function printTest(){
        $out = null;
        $cnt = 0;
        for($i=0; $i<count($this->exercises); $i++){
            $exercise = $this->exercises[$i];
            $exercise->handleUserInput($this->getTeid());
            $ex_html = $exercise->printExercise(true, $this->getTeid(), $showBackButton);
            $out[$cnt] .= $ex_html;
            if($i < count($this->exercises) - 1){
                if(get_class($exercise) != get_class($this->exercises[$i+1])){
                    $cnt++;
                }else{
                    if(!$exercise instanceof CtestExercise){
                        $cnt++;
                    }
                }
            }
        }
        return $out;
    }
    
    private function showStatistic(){
        $contentPoints = 0;
        $userContentPoints = 0;
        $linguisticPoints = 0;
        $userLinguisticPoints = 0;
        $out = '<br><table><tr><td align="center">';
        $out .= '<h1>Deine Ergebnisse</h1><br>';
        foreach($this->exercises as $exercise){
            $contentPoints += $exercise->getAllContentPoints();
            $userContentPoints += $exercise->getAllUserContentPoints();
            $linguisticPoints += $exercise->getAllLinguisticPoints();
            $userLinguisticPoints += $exercise->getAllUserLinguisticPoints();
        }
        $totalPoints = ($contentPoints + $linguisticPoints);
        $totalUserPoints = ($userContentPoints+$userLinguisticPoints);
        $out .= '<font class="correctAnswer"><b>' . $totalUserPoints . " von " . $totalPoints;
        $out .= ' (' . round(($totalUserPoints * 100 / $totalPoints), 1) . '%)</b></font><br><br>';
        foreach($this->exercises as $exercise){
            $out .= $exercise->printStatistic() . "<br>";
        }
        $out .= '<br><br><a href="test_start.php">WEITER</a><br><br><br><br><br><br></td></tr></table>';
        return $out;
    }
    
    public function handleUserInput($userInput){
        $actualExercise = $this->getActualExercise();
        if($actualExercise != null){
            $this->actualExerciseIndex++;
        } else echo "Fehler";
    }
    
    private function getFromDatabase(){       
        $query = "SELECT e.* FROM go2stuko_test_exercise_link tel, go2stuko_exercise e WHERE " 
                ."tel.tid = " . $this->tid . " AND tel.eid = e.eid "
                ."ORDER BY tel.test_position";
        $exercises_data_tmp = Database::getDatasetFromQuery($query);
        foreach($exercises_data_tmp as $exercise_data_tmp){
            $eid = $exercise_data_tmp->eid;
            $heading = $exercise_data_tmp->heading;
            $introduction_text = $exercise_data_tmp->introduction_text;
            
            $query = "SELECT * FROM go2stuko_exercise_element WHERE " 
                ."eid = " . $eid . " "
                ."ORDER BY exercise_position";
            $exerciseElements_data_tmp = Database::getDatasetFromQuery($query);
            
            if($exercise_data_tmp->etid <= 4){
                $exercise = new SentenceExcercise($eid, $heading, $introduction_text);
                foreach($exerciseElements_data_tmp as $exerciseElement_data_tmp){
                    $eeid = $exerciseElement_data_tmp->eeid;
                    $exercisePosition = $exerciseElement_data_tmp->exercise_position;
                    $prolog = $exerciseElement_data_tmp->element_text;
                    $exercise->addNewExerciseElement(
                            new SentenceElement($eeid, $exercisePosition, $prolog));
                }
            } else if($exercise_data_tmp->etid == 5){
                $exercise = new ImageExercise($eid, $heading, $introduction_text);
                $exerciseElement_data_tmp = $exerciseElements_data_tmp[0];
                $eeid = $exerciseElement_data_tmp->eeid;
                $exercisePosition = $exerciseElement_data_tmp->exercise_position;
                $fid = $exerciseElement_data_tmp->fid;
                $exercise->addNewExerciseElement(
                        new ImageElement($eeid, $exercisePosition, $fid));
            } else if($exercise_data_tmp->etid == 6){
                $exercise = new LueckentextExercise($eid, $heading, $introduction_text);
                foreach($exerciseElements_data_tmp as $exerciseElement_data_tmp){
                    $eeid = $exerciseElement_data_tmp->eeid;
                    $exercisePosition = $exerciseElement_data_tmp->exercise_position;
                    $sentenceText = $exerciseElement_data_tmp->element_text; 
                    $exercise->addNewExerciseElement(
                                new LueckentextElement($eeid, $exercisePosition, $sentenceText));
                }
            } else if($exercise_data_tmp->etid == 7){
                $exercise = new LeseverstehenExercise($eid, $heading, $introduction_text);
                foreach($exerciseElements_data_tmp as $exerciseElement_data_tmp){
                    $eeid = $exerciseElement_data_tmp->eeid;
                    $exercisePosition = $exerciseElement_data_tmp->exercise_position;
                    $chapter_text = $exerciseElement_data_tmp->element_text; 
                    $exercise->addNewExerciseElement(
                                new LeseverstehenElement($eeid, $exercisePosition, $chapter_text));
                }
            } else if($exercise_data_tmp->etid == 8){
                $exercise = new CtestExercise($eid, $heading, $introduction_text);
                foreach($exerciseElements_data_tmp as $exerciseElement_data_tmp){
                    $eeid = $exerciseElement_data_tmp->eeid;
                    $exercisePosition = $exerciseElement_data_tmp->exercise_position;
                    $sentenceText = $exerciseElement_data_tmp->element_text; 
                    $exercise->addNewExerciseElement(
                                new CtestElement($eeid, $exercisePosition, $sentenceText));
                }
            } else if($exercise_data_tmp->etid == 9){
                $exercise = new HoerverstehenExercise($eid, $heading, $introduction_text);
                $exerciseElement_data_tmp = $exerciseElements_data_tmp[0];
                $eeid = $exerciseElement_data_tmp->eeid;
                $exercisePosition = $exerciseElement_data_tmp->exercise_position;
                $listening_text = $exerciseElement_data_tmp->element_text; 
                $fid = $exerciseElement_data_tmp->fid;
                
                $exercise->addNewExerciseElement(
                        new HoerverstehenElement($eeid, $exercisePosition, $listening_text, $fid));
            }
            $this->exercises[count($this->exercises)] = $exercise;
            //print_r($exercise);
        }
    }
    
}

?>
