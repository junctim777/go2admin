<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LueckentextExercise
 *
 * @author Marco Armbruster
 */
class LueckentextExercise extends Exercise{
    //put your code here
    
    public static $tabindex = 0;
    
    public function LueckentextExercise($eid, $heading, $introduction_text) {
        parent::Exercise($eid, $heading, $introduction_text);
    }
    
    public function handleUserInput($teid) {
        foreach($this->exerciseElements as $exerciseElement){
            $exerciseElement -> handleUserInput($teid);
        }
    }
    
    public function printStatistic() {
        $out = '<h3>L&uuml;ckentext</h3>';
        $out .= '<b>' . $this->heading."</b><br>";
        $out .= 'Inhaltspunkte: ' . $this->getAllUserContentPoints() . ' von ' . $this->getAllContentPoints() . "<br>";
        $out .= 'Sprachpunkte: ' . $this->getAllUserLinguisticPoints() . ' von ' . $this->getAllLinguisticPoints() . "<br>";
        return $out;
    }
    
    public function printExercise($showSolution, $teid, $showBackButton){
        LueckentextExercise::$tabindex = 0;
        $out = '<form action="' . $_SERVER[($teid == -1) ? 'SCRIPT_NAME' : 'SELF'] . '" method="post" name="userInputForm">';
        $out .= '<table cellpadding="0" cellspacing="0" class="pointLegend" width="462"><td class="questions" width="711"></td><td class="contentPoints" width="25"><b>In</b></td><td class="linguisticPoints" width="25"><b>Sp</b></td></tr></table>';
        $out .= '<table class="luekentext" cellpadding="0" cellspacing="0">';
        $out .= '<tr><td class="questions" align="left" valign="bottom" width="710">' .  $this->introductionText .'</td><td class="leftPoints" width="25"></td><td class="rightPoints" width="25"></td></tr>';
        foreach($this->exerciseElements as $exerciseElement){
            $out .= $exerciseElement->printExerciseElement($showSolution, $teid);
        }
        $out .= '</table>';
        $out .= '<table cellpadding="0" cellspacing="0" class="credit-points" width="796">';
        $out .= '<tr><td class="questions" width="711"></td>';
        $out .= '<td class="content-credit" width="25">';
        $out .= ($showSolution ? $this->getAllUserContentPoints() . '<br>---<br>' . parent::getAllContentPoints() : parent::getAllContentPoints());
        $out .= '</td>';
        $out .= '<td class="linguistic-credit" width="25">';
        $out .= ($showSolution ? $this->getAllUserLinguisticPoints() . '<br>---<br>' . parent::getAllLinguisticPoints() : parent::getAllLinguisticPoints());
        $out .=  '</td>';
        $out .= '</tr>';
        $out .= '</table>';
        $out .= '<table cellpadding="0" cellspacing="0" class="credit-points" width="796">';
        $out .= '<tr>';
            $out .= '<td align="left" valign="top" class="questions">';
                if(!$showSolution && $showBackButton)
                    $out .= '<input tabindex="' . (LueckentextExercise::$tabindex+2) . '" class="back" type="submit" name="back" value="">';
                else if($teid == -1)
                    $out .= '<input tabindex="' . (LueckentextExercise::$tabindex+2) . '" class="cancel" type="submit" name="cancel" value="">';
            $out .= '</td>';
            $out .= '<td>';
                if($showSolution && $teid == -1)
                    $out .= 'Meine Antwort war: <a href="' . $_SERVER['PHP_SELF'] . '?successChoice=true">RICHTIG</a> | <a href="' . $_SERVER['PHP_SELF'] .'?successChoice=false">FALSCH</a>';  
            $out .= '</td>' ;
            $out .= '<td align="right" valign="top" class="questions">';
                if(!$showSolution || $teid != -1)
                    $out .= '<input tabindex="' . (LueckentextExercise::$tabindex+1) . '" class="next" type="submit" name="submit" value="">';
            $out .= '</td>';
        $out .= '</tr>';
        $out .=  '</table>';
        $out .= '</form>';
        return $out;
    }
    
    public function getAllUserContentPoints() {
        $contentPoints = 0;
        foreach($this->exerciseElements as $exerciseElement){
            $userInput = $exerciseElement->getUserInput();
            foreach($exerciseElement->getQuestions() as $question){
                $contentPoints += $userInput['\'' . $exerciseElement->getEeid() . '\'']['\'' . $question->getQid() . '\''][2];
            }
        }
        return $contentPoints;
    }
    
    public function getAllUserLinguisticPoints() {
        $linguisticPoints = 0;
        foreach($this->exerciseElements as $exerciseElement){
            $userInput = $exerciseElement->getUserInput();
            foreach($exerciseElement->getQuestions() as $question){
                $linguisticPoints += $userInput['\'' . $exerciseElement->getEeid() . '\'']['\'' . $question->getQid() . '\''][3];
            }
        }
        return $linguisticPoints;
    }
    
}

?>
