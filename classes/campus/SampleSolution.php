<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SampleSolution
 *
 * @author Marco Armbruster
 */
class SampleSolution {
    //put your code here
    var $dbSampleSolution;
    var $wordByWordSolutions;
    
    function SampleSolution($dbSampleSolution) {
        $this->dbSampleSolution = $dbSampleSolution;
        $this->getFromDatabase();
    }
    
    private function getFromDatabase(){
        $query = "SELECT * FROM go2stuko_word_by_word_solution WHERE " 
                ."ssid = " . $this->dbSampleSolution->ssid . " "
                ."ORDER BY sentence_position";
        $wordByWordSolutions_data_tmp = Database::getDatasetFromQuery($query);
        foreach($wordByWordSolutions_data_tmp as $wordByWordSolution_data_tmp){
            $this->wordByWordSolutions[count($this->wordByWordSolutions)] = 
                    new WordByWordSolution($wordByWordSolution_data_tmp);
        }
    }
    
    public function getSolutionWords(){
        return $this->wordByWordSolutions;
    }
    
    public function getSolutionText(){
        $solutionText = "";
        foreach($this->wordByWordSolutions as $wordByWordSolution){
            $solutionText .= $wordByWordSolution->getValue() . " ";
        }
        return trim($solutionText);
    }
    
    public function getSsid(){
        return $this->dbSampleSolution->ssid;
    }
    
    public function getQid(){
        return $this->dbSampleSolution->qid;
    }
    
    public function getIsBestAnswer(){
        return $this->dbSampleSolution->is_best_answer;
    }
    
    public function getCreated(){
        return $this->dbSampleSolution->created;
    }
    
    public function getChanged(){
        return $this->dbSampleSolution->changed;
    }
    
}

?>
