<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MultipleChoiceAnswer
 *
 * @author Marco Armbruster
 */
class MultipleChoiceAnswer {
    //put your code here
    
    private $answer_text;
    private $isCorrect;
    
    public function MultipleChoiceAnswer($answer_text, $isCorrect){
        $this->answer_text = $answer_text;
        $this->isCorrect = $isCorrect;
    }
    
    public function getAnswerText(){
        return $this->answer_text;
    }
    
    public function isCorrectAnswer(){
        return $this->isCorrect;
    }
    
    
}

?>
