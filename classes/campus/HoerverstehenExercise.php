<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HoerverstehenExercise
 *
 * @author Marco Armbruster
 */
class HoerverstehenExercise extends Exercise{
    //put your code here
    
    public static $tabindex = 0;
    
    public function HoerverstehenExercise($eid, $heading, $introductionText) {
        parent::Exercise($eid, $heading, $introductionText);
    }
    
    public function handleUserInput($teid) {
        foreach($this->exerciseElements as $exerciseElement){
            $exerciseElement -> handleUserInput($teid);
        }
    }
    
    public function printStatistic() {
        $out = '<h3>H&ouml;rverstehen</h3>';
        $out .= '<b>' . $this->heading."</b><br>";
        $out .= 'Inhaltspunkte: ' .$this->getAllUserContentPoints() . ' von ' . $this->getAllContentPoints() . "<br>";
        $out .= 'Sprachpunkte: ' .$this->getAllUserLinguisticPoints() . ' von ' . $this->getAllLinguisticPoints() . "<br>";
        return $out;
    }
    
    public function printExercise($showSolution, $teid, $showBackButton) {
        HoerverstehenExercise::$tabindex = 0;
        $out = '<form action="' . $_SERVER[($teid == -1) ? 'SCRIPT_NAME' : 'SELF'] . '" method="post" name="userInputForm">';
        $out .= '&nbsp;';
        $out .= '<table class="luekentext" cellpadding="0" cellspacing="0" width="796" border="0">';
        $out .= '<tr>';
            $out .= '<td colspan="" class="questions" align="left" valign="top" style="padding-bottom: 20px;">';
                $out .= '<b>H&ouml;ren Sie sich den Text <u>zweimal</u> koplett an und beantworten Sie anschlie&szlig;end die folgenden Fragen.</b>
                         <br>Um ein realistisches Ergebnis zu erzielen, empfehlen wir Ihnen den Text maximal zweimal zu hören.
                         <br>Sie können den Text starten, in dem Sie auf den "Abspielen" Button klicken.';
            $out .='</td>';
            $out .= '<td class="leftPoints" width="25"></td><td class="rightPoints" width="25"></td>';
        $out .= '</tr>';
        $out .= '<tr>';
            $out .= '<td colspan="" class="" align="center" valign="top"><br><b>' . $this->heading . '</b></td>';
            $out .= '<td class="leftPoints" style="border-bottom-width: 0px;" width="25"></td><td class="rightPoints" style="border-bottom-width: 0px;" width="25"></td>';
        $out .= '</tr>';
        foreach($this->exerciseElements as $exerciseElement){
            $out .= $exerciseElement->printExerciseElement($showSolution, $teid);
        }
        $out .= '</table>';
        $out .= '<table cellpadding="0" cellspacing="0" class="credit-points" width="796">';
        $out .= '<tr><td class="questions" width="711"></td>';
        $out .= '<td class="content-credit" width="25">';
        $out .= ($showSolution ? $this->getAllUserContentPoints() . '<br>---<br>' . parent::getAllContentPoints() : parent::getAllContentPoints());
        $out .= '</td>';
        $out .= '<td class="linguistic-credit" width="25">';
        $out .= ($showSolution ? $this->getAllUserLinguisticPoints() . '<br>---<br>' . parent::getAllLinguisticPoints() : parent::getAllLinguisticPoints());
        $out .=  '</td>';
        $out .= '</tr>';
        $out .= '</table>';
        $out .= '<table cellpadding="0" cellspacing="0" class="credit-points" width="796">';
        $out .= '<tr>';
            $out .= '<td align="left" valign="top" class="questions">';
                if(!$showSolution && $showBackButton)
                    $out .= '<input tabindex="' . (HoerverstehenExercise::$tabindex+2) . '" class="back" type="submit" name="back" value="">';
                else if($teid == -1)
                    $out .= '<input tabindex="' . (HoerverstehenExercise::$tabindex+2) . '" class="cancel" type="submit" name="cancel" value="">';
            $out .= '</td>';
            $out .= '<td>';
                if($showSolution && $teid == -1)
                    $out .= 'Meine Antwort war: <a href="' . $_SERVER['PHP_SELF'] . '?successChoice=true">RICHTIG</a> | <a href="' . $_SERVER['PHP_SELF'] .'?successChoice=false">FALSCH</a>';  
            $out .= '</td>' ;
            $out .= '<td align="right" valign="top" class="questions">';
                if(!$showSolution || $teid != -1)
                    $out .= '<input tabindex="' . (HoerverstehenExercise::$tabindex+1) . '" class="next" type="submit" name="submit" value="">';
            $out .= '</td>';
        $out .= '</tr>';
        $out .=  '</table>';
        $out .= '</form>';
        return $out;
    }
    
    public function getAllUserContentPoints() {
        $contentPoints = 0;
        foreach($this->exerciseElements as $exerciseElement){
            $userInput = $exerciseElement->getUserInput();
            foreach($exerciseElement->getQuestions() as $question){
                $contentPoints += $userInput['\'' . $exerciseElement->getEeid() . '\'']['\'' . $question->getQid() . '\''][2];
            }
        }
        return $contentPoints;
    }
    
    public function getAllUserLinguisticPoints() {
        $linguisticPoints = 0;
        foreach($this->exerciseElements as $exerciseElement){
            $userInput = $exerciseElement->getUserInput();
            foreach($exerciseElement->getQuestions() as $question){
                $linguisticPoints += $userInput['\'' . $exerciseElement->getEeid() . '\'']['\'' . $question->getQid() . '\''][3];
            }
        }
        return $linguisticPoints;
    }
    
}

?>
