<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LeseverstehenExercise
 *
 * @author Marco Armbruster
 */
class LeseverstehenExercise extends Exercise{
    //put your code here
    
    public static $tabindex = 0;
    
    public function LeseverstehenExercise($eid, $heading, $introductionText) {
        parent::Exercise($eid, $heading, $introductionText);
    }
    
    public function handleUserInput($teid) {
        foreach($this->exerciseElements as $exerciseElement){
            $exerciseElement -> handleUserInput($teid);
        }
    }
    
    public function printStatistic() {
        $out = '<h3>Leseverstehen</h3>';
        $out .= '<b>' . $this->heading."</b><br>";
        $out .= 'Inhaltspunkte: ' .$this->getAllUserContentPoints() . ' von ' . $this->getAllContentPoints() . "<br>";
        $out .= 'Sprachpunkte: ' .$this->getAllUserLinguisticPoints() . ' von ' . $this->getAllLinguisticPoints() . "<br>";
        return $out;
    }
    
    public function printExercise($showSolution, $teid, $showBackButton) {
        $out .= '<table cellpadding="0" cellspacing="0" class="credit-points" width="646" border="1">';
        $out .= '<tr>';
            $out .= '<td class="questions" width="20" align="left" valign="top" style="padding-bottom: 40px;"></td>';
            $out .= '<td class="questions" width="281" align="left" valign="top">Text: <u>' . $this->heading . '</u></td>';
            $out .= '<td class="questions" width="295" align="left" valign="top"><b><i>Aufgaben</i></b></td>';
            $out .= '<td class="contentPoints" width="25" align="left" valign="top"><b>In</b></td>';
            $out .= '<td class="linguisticPoints" width="25" align="left" valign="top"><b>Sp</b></td>';
        $out .= '</tr>';
        foreach($this->exerciseElements as $exerciseElement){
            $out .= $exerciseElement->printExerciseElement($showSolution, $teid);
        }
        $out .= '</table>';
        $out .= '<table cellpadding="0" cellspacing="0" class="credit-points" width="796">';
        $out .= '<tr><td class="questions" width="561"></td>';
        $out .= '<td class="content-credit" width="25">';
        $out .= ($showSolution ? $this->getAllUserContentPoints() . '<br>---<br>' . parent::getAllContentPoints() : parent::getAllContentPoints());
        $out .= '</td>';
        $out .= '<td class="linguistic-credit" width="25">';
        $out .= ($showSolution ? $this->getAllUserLinguisticPoints() . '<br>---<br>' . parent::getAllLinguisticPoints() : parent::getAllLinguisticPoints());
        $out .=  '</td>';
        $out .= '</tr>';
        $out .= '</table>';
        $out .= '<table cellpadding="0" cellspacing="0" class="credit-points" width="646">';
        $out .= '<tr>';
            $out .= '<td align="left" valign="top" class="questions">';
                if(!$showSolution && $showBackButton)
                    $out .= '<input tabindex="' . (LeseverstehenExercise::$tabindex+2) . '" class="back" type="submit" name="back" value="">';
                else if($teid == -1)
                    $out .= '<input tabindex="' . (LeseverstehenExercise::$tabindex+2) . '" class="cancel" type="submit" name="cancel" value="">';
            $out .= '</td>';
            $out .= '<td>';
                if($showSolution && $teid == -1)
                    $out .= 'Meine Antwort war: <a href="' . $_SERVER['PHP_SELF'] . '?successChoice=true">RICHTIG</a> | <a href="' . $_SERVER['PHP_SELF'] .'?successChoice=false">FALSCH</a>';  
            $out .= '</td>' ;
            $out .= '<td align="right" valign="top" class="questions">';
                if(!$showSolution || $teid != -1)
                    $out .= '<input tabindex="' . (LeseverstehenExercise::$tabindex+1) . '" class="next" type="submit" name="submit" value="">';
            $out .= '</td>';
        $out .= '</tr>';
        $out .=  '</table>';
        return $out;
        
    }
    
    public function getAllUserContentPoints() {
        $contentPoints = 0;
        foreach($this->exerciseElements as $exerciseElement){
            $userInput = $exerciseElement->getUserInput();
            foreach($exerciseElement->getQuestions() as $question){
                $contentPoints += $userInput['\'' . $exerciseElement->getEeid() . '\'']['\'' . $question->getQid() . '\''][2];
            }
        }
        return $contentPoints;
    }
    
    public function getAllUserLinguisticPoints() {
        $linguisticPoints = 0;
        foreach($this->exerciseElements as $exerciseElement){
            $userInput = $exerciseElement->getUserInput();
            foreach($exerciseElement->getQuestions() as $question){
                $linguisticPoints += $userInput['\'' . $exerciseElement->getEeid() . '\'']['\'' . $question->getQid() . '\''][3];
            }
        }
        return $linguisticPoints;
    }
    
}

?>
