<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DailyTraining
 *
 * @author Marco Armbruster
 */
class DailyTraining {
    //put your code here
    
    var $startTime;
    var $leftQuestions;
    var $doneQuestions;
    
    public function __construct() {
    }
    
    public function addQuestion($question){
        $this -> leftQuestions[count($this -> leftQuestions)] = $question;
    }
    
    public function getNoOfCorrectAnswers(){
        $noOfCorrectAnswers = 0;
        foreach($this -> doneQuestions AS $question){
            if($question -> wasIncorrect() == false)
                    $noOfCorrectAnswers++;
        }
        return $noOfCorrectAnswers;
    }

    public function getNoOfFalseAnswers(){
        $noOfFalseAnswers = 0;
        foreach($this -> doneQuestions AS $question){
            if($question -> wasIncorrect() == true)
                    $noOfFalseAnswers++;
        }
        return $noOfFalseAnswers;
    }
    
    public function getNoOfLeftQuestions(){
        return count($this -> leftQuestions);
    }

    public function getNoOfAllQuestions(){
        return count($this -> leftQuestions) + count($this -> doneQuestions);
    }

    public function getActualQuestion(){
        return $this -> leftQuestions[0];
    }
    
    public function getAllFalseQuestions(){
        $falseQuestions = array();
        foreach($this -> doneQuestions AS $question){
            if($question -> wasIncorrect() == true)
                    $falseQuestions[count($falseQuestions)] = $question;
        }
        return $falseQuestions;
    }
    
    public function getNextQuestion(){
        shuffle($this -> leftQuestions);
        return $this -> leftQuestions[0];
    }
    
    public function actualQuestionCorrect(){
        $actualQuestion = $this -> getActualQuestion();
        $actualQuestion -> updateInDatabase(true);
        $this -> doneQuestions[count($this -> doneQuestions)] = $actualQuestion;
        $this -> removeActualQuestion();
    }

    public function actualQuestionFalse(){
        $actualQuestion = $this -> getActualQuestion();
        $actualQuestion -> updateInDatabase(false);
        $actualQuestion -> wasAnsweredIncorrectly();
    }

    private  function removeActualQuestion(){
        if($this ->getNoOfLeftQuestions() >= 2){
            $this -> leftQuestions = $this -> array_trim ($this -> leftQuestions, 0);
        } else{
            unset ( $this -> leftQuestions[0] );
        }
    }
    
    /* Usage:
    $array : Array
    $indey : Integer

    The value of $array at the index $index will be
    deleted by the function.
    */
    private function array_trim ( $array, $index ) {
        if ( is_array ( $array ) ) {
            unset ( $array[$index] );
            array_unshift ( $array, array_shift ( $array ) );
            return $array;
        }
        else {
            return false;
        }
    }
    
}

?>
