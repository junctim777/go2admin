<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LeseverstehenElement
 *
 * @author Marco Armbruster
 */
class LeseverstehenElement extends ExerciseElement{
    //put your code here
    
    private $chapter_text;
    public $questions;
    private $userInput;
    private $unsortedIndices;
    private $sortingSolutionPositions;
    
    public function LeseverstehenElement($eeid, $exercisePosition, $chapter_text) {
        parent::ExerciseElement($eeid, $exercisePosition);
        $this->chapter_text = $chapter_text;
        $this->getFromDatabase();
        if($this->hasSortingQuestion()){
            $regEx = '/([.!?] )/';
            $splitted_sentences_tmp = preg_split($regEx, $this->chapter_text, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
            $this->chapter_text = null;
            $cnt = 0;
            for($i=0; $i<count($splitted_sentences_tmp); $i+=2){
                $this->chapter_text[$cnt++] = $splitted_sentences_tmp[$i] . $splitted_sentences_tmp[$i+1];
            }
            $this->unsortedIndices = range(0, count($this->chapter_text) - 1);
            shuffle($this->unsortedIndices);
            for($i=0; $i<count($this->chapter_text); $i++){
                for($j=0; $j<count($this->unsortedIndices); $j++){
                    if($this->unsortedIndices[$j] == $i){
                        $this->sortingSolutionPositions[$i] = $j;
                        break;
                    }
                }
            }
        }
    }

    protected function getFromDatabase() {
        $query = "SELECT * FROM go2stuko_question WHERE " 
                ."eeid = " . $this->eeid . " "
                ."ORDER BY exercise_element_position";
        $questions_data_tmp = Database::getDatasetFromQuery($query);
        foreach($questions_data_tmp as $question_data_tmp){
            if($question_data_tmp->qtid == 4){
                $this->questions[count($this->questions)] = 
                    new MultipleChoiceQuestion($question_data_tmp);
            } else{
                $this->questions[count($this->questions)] = 
                    new Question($question_data_tmp);
            }
        }
    }
    
    public function getQuestions() {
        return $this->questions;
    }
    
    public function getUserInput() {
        return $this->userInput;
    }
    
    public function handleUserInput($teid) {
        foreach ($this->questions as $question) {
            $inputString = Question::getUserInput($teid, $question->getQid())->user_answer;
            if($question->getQtid() == 4){
                $inputString = preg_replace("/[^a-zA-Z]/", "", $inputString);
                $userContentPoints = 0;
                $userLinguisticPoints = 0;
                $noOfCorrectAnswers = $question->getNumberOfCorrectAnswers();
                $cpPerAnswer = $question->getContentPointsPerAnswer();
                $mcAnswers = $question->getMCAnswers();
                $user_char_array = str_split($inputString);
                foreach($user_char_array as $user_char){
                   if($question->isUserAnswerCorrect($user_char)){
                       $userContentPoints += $cpPerAnswer;
                   }
                }
                if(count($user_char_array) > $noOfCorrectAnswers){
                    $userContentPoints -= $cpPerAnswer * (count($user_char_array) - $noOfCorrectAnswers);
                }
                $userContentPoints = max(array($userContentPoints, 0));
                $bestSolution = $question->getCorrectAnswerCharacters();
            }
            if($question->getQtid() == 5){
                $inputString = preg_replace("/[^a-zA-Z]/", "", $inputString);
                $userContentPoints = 0;
                $userLinguisticPoints = 0;
                $bestSolution = '';
                $user_char_array = str_split($inputString);
                for($i=0; $i<count($this->sortingSolutionPositions)-1; $i++){
                    $actual_solution_char = strtolower(chr(65+$this->sortingSolutionPositions[$i]));
                    $next_solution_char = strtolower(chr(65+$this->sortingSolutionPositions[$i+1]));
                    if($i>0){
                        $bestSolution .= '-';
                    }
                    $bestSolution .= $actual_solution_char;
                    if($i == count($this->sortingSolutionPositions)-2){
                        $bestSolution .= '-' . $next_solution_char;
                    }
                    $corresponding_user_char = '';
                    $next_corresponding_user_char = '';
                    for($j=0; $j<count($user_char_array)-1; $j++){
                        if($user_char_array[$j] == $actual_solution_char){
                            $corresponding_user_char = strtolower($user_char_array[$j]);
                            $next_corresponding_user_char = strtolower($user_char_array[$j+1]);
                            break;
                        }
                    }
                    if($next_solution_char == $next_corresponding_user_char){
                        $userContentPoints++;
                    }
                    if($i==0){
                        $first_user_char = $user_char_array[0];
                        if($first_user_char == $actual_solution_char){
                            $userContentPoints++;
                        }
                    }
                }
            }
            $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][0] = $inputString;
            $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][1] = $bestSolution;
            $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][2] = $userContentPoints;
            $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][3] = $userLinguisticPoints;
        }
    }
    
    public function printExerciseElement($showSolution, $teid) {
        $out = '<tr>';
            $out .= '<td class="questions" width="20" align="left" valign="top">' . $this->exercisePosition . '</td>';
            $out .= '<td class="questions" width="281" align="left" valign="top" style="text-align:justify;">';
                if(!$this->hasSortingQuestion()){
                    $out .= $this->chapter_text;
                } else{
                    $out .= '<ol type="a" style="padding-left: 30px;">';
                    foreach($this->unsortedIndices as $unsortedIndex){
                        $out .= '<li>' . $this->chapter_text[$unsortedIndex] . '</li>';
                    }
                    $out .= '</ol>';
                }
            $out .= '</td>';
            $out .= '<td class="" width="345" align="left" valign="top" colspan="3">';
                $out .= '<table cellpadding="0" cellspacing="0" class="" width="369" border="1">';
                    foreach($this->questions as $question){
                        $out .= '<tr>';
                            $out .= '<td class="" width="295" align="left" valign="top" style="padding: 5px 5px 5px 5px;">';
                                if($question->getQtid() == 4){
                                    $out .= $question->getQuestionIntroductionText();
                                    $out .= '<ol type="a" style="padding-left: 30px;">';
                                    foreach($question->getMCAnswers() as $mc_answer){
                                        $out .= '<li>' . $mc_answer->getAnswerText() . '</li>';
                                    }
                                    $out .= '</ol>';
                                    if (!$showSolution) {
                                        if($teid != -1){
                                            $user_input_so_far = Question::getUserInput($teid, $question->getQid());
                                        }
                                        $out .= '<br>Buchstaben der L&ouml;sung: ';
                                        $out .= '<input class="exam-answer" style="width: 150px;" type="text"';
                                        $out .= ' onkeydown="return nextElementOnEnter(this, event);"';
                                        $out .= ' name="user_inputs[\'' . ($this->eeid) . '\'][\'' . ($question->getQid()) . '\']"';
                                        $out .= ' value="' . (!empty($user_input_so_far->user_answer) 
                                                    ? $user_input_so_far->user_answer : '') . '"';
                                        $out .= ' />';
                                    } else{
                                        $userInput = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][0];
                                        $bestSolution = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][1];
                                        $userContentPoints = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][2];
                                        $userLinguisticPoints = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][3];
                                        $answeredCorrectly = $userContentPoints + $userLinguisticPoints == $question->getContentPoints() + $question->getLinguisticPoints();
                                        if ($answeredCorrectly) {
                                            $out .= '<br>Buchstaben der L&ouml;sung: ';
                                            $out .= '<font class="correctAnswer">' . $userInput . '</font>';
                                        } else {
                                            $out .= '<table>';
                                                $out .= '<tr><td><u>Musterl&ouml;sung:</u></td><td style="padding-left: 10px;"><font class="correctAnswer">' . $bestSolution . '</font></td></tr>';
                                                $out .= '<tr><td><u>Deine Antwort:</u></td><td style="padding-left: 10px;"><font class="falseAnswer">' . $userInput . '</font></td></tr>';
                                            $out .= '</table>';
                                        }
                                    }
                                }
                                if($question->getQtid() == 5){
                                    $out .= $question->getQuestionText();
                                    $out .= '<br>';
                                    if (!$showSolution) {
                                        if($teid != -1){
                                            $user_input_so_far = Question::getUserInput($teid, $question->getQid());
                                        }
                                        $out .= '<br>Richtige Reihenfolge: ';
                                        $out .= '<input class="exam-answer" style="width: 150px;" type="text"';
                                        $out .= ' onkeydown="return nextElementOnEnter(this, event);"';
                                        $out .= ' name="user_inputs[\'' . ($this->eeid) . '\'][\'' . ($question->getQid()) . '\']"';
                                        $out .= ' value="' . (!empty($user_input_so_far->user_answer) 
                                                    ? $user_input_so_far->user_answer : '') . '"';
                                        $out .= ' />';
                                    } else{
                                        $userInput = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][0];
                                        $bestSolution = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][1];
                                        $userContentPoints = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][2];
                                        $userLinguisticPoints = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][3];
                                        $answeredCorrectly = $userContentPoints + $userLinguisticPoints == $question->getContentPoints() + $question->getLinguisticPoints();
                                        if ($answeredCorrectly) {
                                            $out .= '<br>Richtige Reihenfolge: ';
                                            $out .= '<font class="correctAnswer">' . $userInput . '</font>';
                                        } else {
                                            $out .= '<table>';
                                                $out .= '<tr><td><u>Musterl&ouml;sung:</u></td><td style="padding-left: 10px;"><font class="correctAnswer">' . $bestSolution . '</font></td></tr>';
                                                $out .= '<tr><td><u>Deine Antwort:</u></td><td style="padding-left: 10px;"><font class="falseAnswer">' . $userInput . '</font></td></tr>';
                                            $out .= '</table>';
                                        }
                                    }
                                }
                            $out .= '</td>';
                            $out .= '<td class="contentPoints" width="25" align="center" valign="top">';
                            if ($showSolution) {
                                for ($i = 0; $i < $userContentPoints; $i++) {
                                    $out .= '<img class="points" src="src/imgs/content_green.png" alt="cp"><br />';
                                }
                                for ($i = $userContentPoints; $i < $question->getContentPoints(); $i++) {
                                    $out .= '<img class="points" src="src/imgs/content.png" alt="cp"><br />';
                                }
                            } else {
                                for ($i = 0; $i < $question->getContentPoints(); $i++) {
                                    $out .= '<img class="points" src="src/imgs/content.png" alt="cp"><br />';
                                }
                            }
                            $out .= '</td>';
                            $out .= '<td class="linguisticPoints" width="25" align="center" valign="bottom">';
                            if ($showSolution) {
                                for ($i = 0; $i < $userLinguisticPoints; $i++) {
                                    $out .= '<img class="points" src="src/imgs/lingu_green.png" alt="cp"><br />';
                                }
                                for ($i = $userLinguisticPoints; $i < $question->getLinguisticPoints(); $i++) {
                                    $out .= '<img class="points" src="src/imgs/lingu.png" alt="cp"><br />';
                                }
                            } else {
                                for ($i = 0; $i < $question->getLinguisticPoints(); $i++) {
                                    $out .= '<img class="points" src="src/imgs/lingu.png" alt="cp"><br />';
                                }
                            }
                            $out .= '</td>';
                        $out .= '</tr>';
                    }
                $out .= '</table>';
            $out .= '</td>';
        $out .= '</tr>';
        
        return $out;
    }
    
    private function hasSortingQuestion(){
        foreach ($this->questions as $question) {
            if($question->getQtid() == 5)
                return true;
        }
        return false;
    }
    
    public function getAllContentPoints() {
        $contentPoints = 0;
        foreach ($this->questions as $question) {
            $contentPoints += $question->getContentPoints();
        }
        return $contentPoints;
    }

    public function getAllLinguisticPoints() {
        $linguisticPoints = 0;
        foreach ($this->questions as $question) {
            $linguisticPoints += $question->getLinguisticPoints();
        }
        return $linguisticPoints;
    }
    
}

?>
