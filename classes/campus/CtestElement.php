<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CtestElement
 *
 * @author Marco Armbruster
 */
class CtestElement extends ExerciseElement {

    //put your code here

    private $sentenceText;
    public $questions;
    private $userInput;

    public function CtestElement($eeid, $exercisePosition, $sentenceText) {
        parent::ExerciseElement($eeid, $exercisePosition);
        $this->sentenceText = $sentenceText;
        $this->getFromDatabase();
    }

    protected function getFromDatabase() {
        $query = "SELECT * FROM go2stuko_question WHERE "
                . "eeid = " . $this->eeid . " "
                . "ORDER BY exercise_element_position";
        $questions_data_tmp = Database::getDatasetFromQuery($query);
        foreach ($questions_data_tmp as $question_data_tmp) {
            $this->questions[count($this->questions)] =
                    new Question($question_data_tmp);
        }
    }

    public function getQuestions() {
        return $this->questions;
    }

    public function getUserInput() {
        return $this->userInput;
    }

    private function findBestMatchingSampleSolution($inputWord, $question) {
        $sampleSolutions = $question->getSampleSolutions();
        $maxContentPoints = 0;
        $maxLinguisticPoints = 0;
        $bestSolution = null;
        $punctuations = array(',', '.', '!', '?', '!');
        foreach ($sampleSolutions as $sampleSolution) {
            $contentPoints = 0;
            $linguisticPoints = 0;
            $wordByWordSolutions = $sampleSolution->getSolutionWords();
            $linkedWordGroups = null;
            foreach ($wordByWordSolutions as $wordByWordSolution) {
                // Hier steht explizit die Silbe des Users
                $solutionWord = $wordByWordSolution->getValue();
                $solutionWord = str_replace($punctuations, '', $solutionWord);
                $solutionWord = trim($solutionWord);
                $solutionWord = preg_replace('/\s{2,}/sm', ' ', $solutionWord);
                $inputWord = str_replace($punctuations, '', $inputWord);
                $inputWord = trim($inputWord);
                $inputWord = preg_replace('/\s{2,}/sm', ' ', $inputWord);
                // Hier wird ueberprueft ob das eingegeben Wort gleich dem Loesungsstring ist
                if ($inputWord == $solutionWord) {
                    $contentPoints += $wordByWordSolution->getContentPoints();
                    $linguisticPoints += $wordByWordSolution->getLinguisticPoints();
                }
            }
            if (($contentPoints + $linguisticPoints) > ($maxContentPoints + $maxLinguisticPoints)) {
                $maxContentPoints = $contentPoints;
                $maxLinguisticPoints = $linguisticPoints;
                $bestSolution = $sampleSolution;
            }
        }
        return array($sampleSolution, $maxContentPoints, $maxLinguisticPoints);
    }

    public function handleUserInput($teid) {
        foreach ($this->questions as $question) {
            $inputWord = Question::getUserInput($teid, $question->getQid())->user_answer;
            $userContentPoints = 0;
            $userLinguisticPoints = 0;
            $bestMatch = $this->findBestMatchingSampleSolution($inputWord, $question);
            $bestSolution = $bestMatch[0];
            $userContentPoints = $bestMatch[1];
            $userLinguisticPoints = $bestMatch[2];
            $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][0] = $inputWord;
            $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][1] = $bestSolution;
            $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][2] = $userContentPoints;
            $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][3] = $userLinguisticPoints;
        }
    }

    public function printExerciseElement($showSolution, $teid) {
        $out = "";
        $charCnt = 0;
        $this->sentenceText = str_replace("<br>", " ", $this->sentenceText);
        if(count($this->questions) > 0 && $showSolution && false){
            $this->sentenceText = wordwrap($this->sentenceText, 55, " <br> ");
        }
        $this->sentenceText = preg_replace('/\s{2,}/sm', ' ', $this->sentenceText);
        // Hier stehen die einzelnen Worte des Satzes
        $words = split(" ", $this->sentenceText);
        $cnt = 0;
        $index_from = 0;
        $index_to = 0;
        $tmpUserContentPoints = 0;
        $tmpUserLinguisticPoints = 0;
        foreach ($words as $word) {
            $question = $this->isWordAQuestion($cnt++, $this->questions);
            if ($question != null) {
                // !!!!!
                $index_to++;
                $inputWord = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][0];
                $bestSolution = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][1];
                $userContentPoints = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][2];
                $userLinguisticPoints = $this->userInput['\'' . $this->eeid . '\'']['\'' . $question->getQid() . '\''][3];
                $answeredCorrectly = $userContentPoints + $userLinguisticPoints == $question->getContentPoints() + $question->getLinguisticPoints();

                $tmp = $question->getQuestionText();
                $tmp = split("-", $tmp);
                $wordLength = $tmp[1];
                $word = substr($word, 0, $wordLength);
                $out .= $word;
                $out .= '<font class="correctAnswer"><b>' . (($answeredCorrectly) ? $inputWord : $bestSolution->getSolutionText()) . '</b></font> ';
                $out .= '<font class="falseAnswer"><b>' . (($answeredCorrectly) ? '' : '(' . $inputWord . ')') . '</b></font> ';
                //$out .= '<a href="user_solution_veto.php?ssid=' . $bestSolution->getSsid() . '" target="_blank">?</a> ';
                $tmpUserContentPoints += $userContentPoints;
                $tmpUserLinguisticPoints += $userLinguisticPoints;
            } else {
                $out .= $word . " ";
                $charCnt += strlen($word) + 1;
            }
        }
        return $out;
    }
    
    private function printContentPoints($showSolution, $from_index, $to_index, $tmpUserContentPoints){
        $out = "";
        if ($showSolution) {
            for ($i = 0; $i < $tmpUserContentPoints; $i++) {
                $out .= '<img class="points" src="src/imgs/content_green.png" alt="cp"><br />';
            }
            for ($i = $tmpUserContentPoints; $i < $this->getAllContentPointsForQuestionRange($from_index, $to_index); $i++) {
                $out .= '<img class="points" src="src/imgs/content.png" alt="cp"><br />';
            }
        } else {
            for ($i = 0; $i < $this->getAllContentPointsForQuestionRange($from_index, $to_index); $i++) {
                $out .= '<img class="points" src="src/imgs/content.png" alt="cp"><br />';
            }
        }
        return $out;
    }
    
    private function printLinguisticPoints($showSolution, $from_index, $to_index, $tmpUserLinguisticPoints){
        $out = "";
        if ($showSolution) {
            for ($i = 0; $i < $tmpUserLinguisticPoints; $i++) {
                $out .= '<img class="points" src="src/imgs/lingu_green.png" alt="cp"><br />';
            }
            for ($i = $tmpUserLinguisticPoints; $i < $this->getAllLinguisticPointsForQuestionRange($from_index, $to_index); $i++) {
                $out .= '<img class="points" src="src/imgs/lingu.png" alt="cp"><br />';
            }
        } else {
            for ($i = 0; $i < $this->getAllLinguisticPointsForQuestionRange($from_index, $to_index); $i++) {
                $out .= '<img class="points" src="src/imgs/lingu.png" alt="cp"><br />';
            }
        }
        return $out;
    }
    
    private function getAllContentPointsForQuestionRange($from_index, $to_index) {
        $contentPoints = 0;
        for ($i = $from_index; $i < $to_index; $i++) {
            $question = $this->questions[$i];
            $contentPoints += $question->getContentPoints();
        }
        return $contentPoints;
    }

    private function getAllLinguisticPointsForQuestionRange($from_index, $to_index) {
        $linguisticPoints = 0;
        for ($i = $from_index; $i < $to_index; $i++) {
            $question = $this->questions[$i];
            $linguisticPoints += $question->getLinguisticPoints();
        }
        return $linguisticPoints;
    }

    public function getAllContentPoints() {
        $contentPoints = 0;
        foreach ($this->questions as $question) {
            $contentPoints += $question->getContentPoints();
        }
        return $contentPoints;
    }

    public function getAllLinguisticPoints() {
        $linguisticPoints = 0;
        foreach ($this->questions as $question) {
            $linguisticPoints += $question->getLinguisticPoints();
        }
        return $linguisticPoints;
    }

    private function isWordAQuestion($word_index, $questions) {
        foreach ($questions as $question) {
            if ($question->getExerciseElementPosition() == $word_index)
                return $question;
        }
        return null;
    }

}

?>
