<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VocabularyQuestion
 *
 * @author Marco Armbruster
 */
abstract class VocabularyQuestion {
    //put your code here
    
    private $incorrect_answer = false;
    
    public function VocabularyQuestion() {
    }

    public function wasAnsweredIncorrectly(){
        $this -> incorrect_answer = true;
    }
    
    public function wasIncorrect(){
        return $this->incorrect_answer;
    }
    
    abstract public function printVocabularyQuestion($showSolution);
    
    abstract public function updateInDatabase($success);
}

?>
