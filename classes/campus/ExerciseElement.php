<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ExerciseElement
 *
 * @author Marco Armbruster
 */
abstract class ExerciseElement {
    //put your code here
    
    protected $eeid;
    protected $exercisePosition;
    
    public function ExerciseElement($eeid, $exercisePosition) {
        $this->eeid = $eeid;
        $this->exercisePosition = $exercisePosition;
    }
    
    public function getEeid(){
        return $this->eeid;
    }
    
    abstract public function handleUserInput($teid);
    
    abstract public function printExerciseElement($showSolution, $teid);
    
    abstract protected function getFromDatabase();
    
    abstract public function getAllContentPoints();

    abstract public function getAllLinguisticPoints();
    
    
    
}

?>
