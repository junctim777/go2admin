<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IrregularVerbExerciseElement
 *
 * @author Marco Armbruster
 */
class IrregularVerbQuestion extends VocabularyQuestion{
    //put your code here
    
    private $dbIrregularVerb;
    
    public function IrregularVerbQuestion($dbIrregularVerb) {
        parent::VocabularyQuestion();
        $this->dbIrregularVerb = $dbIrregularVerb;
    }
    
    public function getDbIrregularVerb(){
        return $this->dbIrregularVerb;
    }
    
    public function printVocabularyQuestion($showSolution) {
        $out = '<h2>Unregelm&auml;&szlig;ige Verben</h2><br /><br />';
        $out .= '<form action="' . $_SERVER['PHP_SELF'] .'" method="post" name="userInputForm">';
        $out .= '<table style="margin:0px 0px 90px 0px" class="irregular-verbs" cellpadding="0" cellspacing="0" width="850" border="0">';
        $out .= '<tr>';
            $out .= ($showSolution ? '<td class="" align="LEFT" valign="top" width="160"></td>' : '');
            $out .= '<td class="" align="LEFT" valign="top" width="' . ($showSolution ? '160' : '160') . '"><b>Infinitiv:</b></td>';
            $out .= '<td class="" align="LEFT" valign="top" width="' . ($showSolution ? '160' : '160') . '"><b>3. Person Sg. Pr&auml;sens:</b></td>';
            $out .= '<td class="" align="LEFT" valign="top" width="' . ($showSolution ? '160' : '160') . '"><b>3. Person Sg. Pr&auml;teritum:</b></td>';
            $out .= '<td class="" align="LEFT" valign="top" width="' . ($showSolution ? '160' : '160') . '"><b>Partizip II:</b></td>';
        $out .= '</tr>';
        if($showSolution){
            $out .= '<tr>';
                $out .= '<td class="" align="LEFT" valign="top" width="160">';
                    $out .= 'Deine Antwort:';
                $out .= '</td>';
                $out .= '<td class="" align="LEFT" valign="top" width="160">';
                    $out .= '<font class="highlighted_text"><b>' . $this -> dbIrregularVerb -> infinitive .'</b></font>';
                $out .= '</td>';
                $out .= '<td class="" align="LEFT" valign="top" width="160">';
                    $out .= '<font class="' . (($_POST['answerThirdPersonPresent'] == 
                            $this -> dbIrregularVerb -> third_person_present) ? 'correct' : 'incorrect') . '">
                            <b>' . $_POST['answerThirdPersonPresent'] .'</b></font>&nbsp;';
                $out .= '</td>';
                $out .= '<td class="" align="LEFT" valign="top" width="160">';
                    $out .= '<font class="' . (($_POST['answerThirdPersonPastTense'] == 
                            $this -> dbIrregularVerb -> third_person_past_tense) ? 'correct' : 'incorrect') . '">
                            <b>' . $_POST['answerThirdPersonPastTense'] .'</b></font>&nbsp;';
                $out .= '</td>';
                $out .= '<td class="" align="LEFT" valign="top" width="160">';
                    $out .= '<font class="' . (($_POST['answerParticipTwo'] == 
                            $this -> dbIrregularVerb -> particip_two) ? 'correct' : 'incorrect') . '">
                            <b>' . $_POST['answerParticipTwo'] .'</b></font>&nbsp;';
                $out .= '</td>';
            $out .= '</tr>';
            $out .= '<tr>';
                $out .= '<td class="" align="LEFT" valign="top" >';
                    $out .= 'L&ouml;sung:';
                $out .= '</td>';
                $out .= '<td class="" align="LEFT" valign="top" >';
                    $out .= '<font class="highlighted_text"><b>' . $this -> dbIrregularVerb -> infinitive .'</b></font>&nbsp;';
                $out .= '</td>';
                $out .= '<td class="" align="LEFT" valign="top" >';
                    $out .= '<font class="correct"><b>' . $this -> dbIrregularVerb -> third_person_present .'</b></font>&nbsp;';
                $out .= '</td>';
                $out .= '<td class="" align="LEFT" valign="top" >';
                    $out .= '<font class="correct"><b>' . $this -> dbIrregularVerb -> third_person_past_tense .'</b></font>&nbsp;';
                $out .= '</td>';
                $out .= '<td class="" align="LEFT" valign="top" >';
                    $out .= '<font class="correct"><b>' . $this -> dbIrregularVerb -> particip_two .'</b></font>&nbsp;';
                $out .= '</td>';
            $out .= '</tr>';
        } else{
            $out .= '<tr>';
                $out .= '<td class="" align="LEFT" valign="top" width="160">';
                    $out .= '<font class="highlighted_text"><b>' . $this -> dbIrregularVerb -> infinitive . '</b></font>';
                $out .= '</td>';
                $out .= '<td class="" align="LEFT" valign="top" width="160">';
                    $out .= '<input tabindex="' . ++$tabindex . '" class="" name="answerThirdPersonPresent" type="text" size="15" maxlength="30"
                           value="' . $_POST['answerThirdPersonPresent'] . '">';
                $out .= '</td>';
                $out .= '<td class="" align="LEFT" valign="top" width="160">';
                    $out .= '<input tabindex="' . ++$tabindex . '" class="" name="answerThirdPersonPastTense" type="text" size="15" maxlength="30"
                           value="' . $_POST['answerThirdPersonPastTense'] . '">';
                $out .= '</td>';
                $out .= '<td class="" align="LEFT" valign="top" width="160">';
                    $out .= '<input tabindex="' . ++$tabindex . '" class="" name="answerParticipTwo" type="text" size="15" maxlength="30"
                           value="' . $_POST['answerParticipTwo'] . '">';
                $out .= '</td>';
            $out .= '</tr>';
        }
        $out .= '<tr>';
            $out .= '<td class="list_input" style="padding-top: 30px;" align="left" valign="top" width="120" colspan="2">';
                $out .= '<input tabindex="' . ($tabindex+2) . '" class="cancel" type="submit" name="cancel" value="">';
            $out .= '</td>';
            $out .= '<td align="right" valign="top" style="padding-top: 30px;" class="questions" colspan="' . ($showSolution? '4' : '2') . '">';
            if($showSolution){
                $out .= 'Meine Antwort war: <a href="' . $_SERVER['PHP_SELF'] . '?successChoice=true">RICHTIG</a>
                | <a href="' . $_SERVER['PHP_SELF'] .'?successChoice=false">FALSCH</a>';
            } else{
                $out .= '<input tabindex="' . ($tabindex+1) . '" class="next" type="submit" name="submitAnswer" value="">';
            }
        $out .= '</td>';
        $out .= '</tr>';
        $out .=  '</table>';
        $out .= '</form>';
        return $out;
    }
    
    public function updateInDatabase($success){
        $user = $_SESSION['user'];
        $uid = $user->getUid();
        $query = "SELECT COUNT(*) AS count FROM go2stuko_vocabulary_user_log WHERE uid = " . $uid . " AND ivid = " . $this->dbIrregularVerb->ivid;
        $noOfUserLogsTmp = Database::getDatasetFromQuery($query);
        $noOfUserLogsTmp = $noOfUserLogsTmp[0];
        $noOfUserLogsTmp = $noOfUserLogsTmp->count;
        if($noOfUserLogsTmp == 0){
            if($success)
                $card_index = 2;
            else
                $card_index = 1;
            $query = "INSERT INTO go2stuko_vocabulary_user_log" .
                    " (uid, ivid, date_last_training, card_index)" .
                    " VALUES (" .
                    $uid . "," .
                    $this->dbIrregularVerb->ivid . "," .
                    "NOW()," .
                    $card_index . 
                    ")";
            $dbSuccess = mysql_query($query);
        } else{
            if($success)
                $query = "UPDATE go2stuko_vocabulary_user_log SET date_last_training = NOW(), card_index = card_index + 1 WHERE uid = " . $uid . " AND ivid = " . $this->dbIrregularVerb->ivid;
            else
                $query = "UPDATE go2stuko_vocabulary_user_log SET date_last_training = NOW(), card_index = 1 WHERE uid = " . $uid . " AND ivid = " . $this->dbIrregularVerb->ivid;
            mysql_query($query);
        }
    }
    
}

?>
