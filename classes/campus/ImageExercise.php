<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ImageExercise
 *
 * @author Marco Armbruster
 */
class ImageExercise extends Exercise{
    //put your code here
    
    public function ImageExercise($eid, $heading, $introduction_text) {
        parent::Exercise($eid, $heading, $introduction_text);
    }
    
    public function handleUserInput($teid) {
        foreach($this->exerciseElements as $exerciseElement){
            $exerciseElement -> handleUserInput($teid);
        }
    }
    
    public function printStatistic() {
        $out = '<h3>Textproduktion</h3>';
        $out .= '<b>' . $this->heading."</b><br>";
        $out .= 'Inhaltspunkte: ' . $this->getAllUserContentPoints() . ' von ' . $this->getAllContentPoints() . "<br>";
        $out .= 'Sprachpunkte: ' . $this->getAllUserLinguisticPoints() . ' von ' . $this->getAllLinguisticPoints() . "<br>";
        return $out;
    }
    
    public function printExercise($showSolution, $teid, $showBackButton){
        $out = '<table cellpadding="0" cellspacing="0" class="pointLegend" width="646">';
        $out .= '<td class="questions" width="561"></td><td class="contentPoints" width="25"><b>In</b></td><td class="linguisticPoints" width="25"><b>Sp</b></td></tr></table>';
        $out .= '<table class="exam" cellpadding="0" cellspacing="0">';
        $out .= '<tr><td class="questions" align="left" valign="bottom" width="560">' .  $this->introductionText .'</td><td class="contentPoints" width="25"></td><td class="linguisticPoints" width="25"></td></tr>';
        foreach($this->exerciseElements as $exerciseElement){
            $out .= $exerciseElement->printExerciseElement($showSolution, $teid);
        }
        $out .= '</table>';
        $out .= '<table cellpadding="0" cellspacing="0" class="credit-points" width="646">';
        $out .= '<tr><td class="questions" width="561"></td>';
        $out .= '<td class="content-credit" width="25">';
        $out .=  parent::getAllContentPoints();
        $out .= '</td>';
        $out .= '<td class="linguistic-credit" width="25">';
        $out .=  parent::getAllLinguisticPoints();
        $out .=  '</td>';
        $out .= '</tr>';
        $out .= '</table>';
        $out .= '<table cellpadding="0" cellspacing="0" class="credit-points" width="646">';
        $out .= '<tr>';
            $out .= '<td align="left" valign="top" class="questions">';
                if(!$showSolution && $showBackButton)
                    $out .= '<input class="back" type="submit" name="back" value="">';
                 else if($teid == -1)
                    $out .= '<input class="cancel" type="submit" name="cancel" value="">';
            $out .= '</td>';
            $out .= '<td>';
                if($showSolution && $teid == -1)
                    $out .= 'Meine Antwort war: <a href="' . $_SERVER['PHP_SELF'] . '?successChoice=true">RICHTIG</a> | <a href="' . $_SERVER['PHP_SELF'] .'?successChoice=false">FALSCH</a>';  
            $out .= '</td>' ;
            $out .= '<td align="right" valign="top" class="questions">';
                if(!$showSolution || $teid != -1)
                    $out .= '<input class="next" type="submit" name="submit" value="">';
            $out .= '</td>';
        $out .= '</tr>';
        $out .=  '</table>';
        $out .= '</form>';
        return $out;
    }

    public function getAllUserContentPoints(){
        return $this->getUserContentPoints();
    }
    
    public function getAllUserLinguisticPoints() {
        return $this->getUserLinguisticPoints();
    }
}

?>
