<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WordByWordSolution
 *
 * @author Marco Armbruster
 */
class WordByWordSolution {
    //put your code here
    var $dbWordByWordSolution;
    
    function WordByWordSolution($dbWordByWordSolution) {
        $this->dbWordByWordSolution = $dbWordByWordSolution;
    }
    
    public function getSsid(){
        return $this->dbWordByWordSolution->ssid;
    }
    
    public function getSentencePosition(){
        return $this->dbWordByWordSolution->sentence_position;
    }
    
    public function getValue(){
        return $this->dbWordByWordSolution->value;
    }
    
    public function getContentPoints(){
        return $this->dbWordByWordSolution->content_points;
    }
    
    public function getLinguisticPoints(){
        return $this->dbWordByWordSolution->linguistic_points;
    }
    
    public function getParentLinkSentencePosition(){
        return $this->dbWordByWordSolution->parent_link_sentence_position;
    }
}

?>
