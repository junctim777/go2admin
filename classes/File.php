<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Picture
 *
 * @author Marco Armbruster
 */
class File {
    //put your code here
    var $dbFile;
    
    function File($dbFile) {
        $this->dbFile = $dbFile;
    }
    
    public function getFid(){
        return $this->dbFile->fid;
    }
    
    public function getEnding(){
        return $this->dbFile->ending;
    }
    
    public function getFileType(){
        return $this->dbFile->file_type;
    }
    
    public function getDescription(){
        return $this->dbFile->description;
    }
    
}

?>
