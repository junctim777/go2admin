<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DynamicFormElements
 *
 * @author Marco Armbruster
 */
final class DynamicFormElements {
    //put your code here

    public static function getAllCountries($form_name, $tabindex, $select_index, $css_class){
        $query = "SELECT * FROM go2stuko_country";
        $countries = Database::getDatasetFromQuery($query);
        $html_form = '<select tabindex="' . $tabindex .'" class="' . $css_class . '" name="' . $form_name . '" style="width: 155px;">';
        $html_form .= '<option value="0">Bitte ausw&auml;hlen...</option>';
        foreach($countries as $country){
            $html_form .= '<option value="' . $country->cid .'"';
            if(isset($_POST[$form_name])){
                $html_form .= $_POST[$form_name] == $country->cid ? ' selected="selected">' : '>';
            } else{
                $html_form .= $select_index == $country->cid ? ' selected="selected">' : '>';
            }
            $html_form .= $country->country_name . '</option>';
        }
        $html_form .= '</select';
        return $html_form;
    }
    
    public static function getPackageTypes($form_name, $select_index, $css_class){
        
        $query = "SELECT * FROM go2stuko_test_package";
        $testPackages = Database::getDatasetFromQuery($query);
        
        $html_form = '<select tabindex="29" class="' . $css_class . '" name="' . $form_name . '" style="width: 215px;">';
        $html_form .= '<option value="0">Bitte ausw&auml;hlen...</option>';
        foreach($testPackages as $testPackage){
            $html_form .= '<option value="' . $testPackage->tpid .'"';
            if(isset($_POST[$form_name])){
                $html_form .= $_POST[$form_name] == $testPackage->tpid ? ' selected="selected">' : '>';
            }else{
                $html_form .= $select_index == $testPackage->tpid ? ' selected="selected">' : '>';
            }
            $html_form .= $testPackage->description . '</option>';
        }
        $html_form .= '</select';
        return $html_form;
    }
    
    public static function getExerciseTypes($form_name){
        
        $query = "SELECT * FROM go2stuko_exercise_type";
        $exerciseTypes = Database::getDatasetFromQuery($query);
        
        $html_form = '<select class="" name="etid" style="width: 155px;">';
        $html_form .= '<option value="0">Bitte ausw&auml;hlen...</option>';
        foreach($exerciseTypes as $exerciseType){
            $html_form .= '<option value="' . $exerciseType->etid .'"';
            if(isset($_POST[$form_name])){
                $html_form .= $_POST[$form_name] == $exerciseType->etid ? ' selected="selected">' : '>';
            } else{
                $html_form .= $select_index == $exerciseType->etid ? ' selected="selected">' : '>';
            }
            $html_form .= $exerciseType->etname . '</option>';
        }
        $html_form .= '</select';
        return $html_form;
    }
    
    public static function getUniversitySubjects($form_name, $select_index, $css_class){
        $query = "SELECT * FROM go2stuko_university_studies";
        $university_studies = Database::getDatasetFromQuery($query);
        $html_form = '<select tabindex="28" class="' . $css_class . '" name="' . $form_name . '" style="width: 155px;">';
        $html_form .= '<option value="0">Bitte ausw&auml;hlen...</option>';
        foreach($university_studies as $university_studie){
            $html_form .= '<option value="' . $university_studie->usid .'"';
            if(isset($_POST[$form_name])){
                $html_form .= $_POST[$form_name] == $university_studie->usid ? ' selected="selected">' : '>';
            } else{
                $html_form .= $select_index == $university_studie->usid ? ' selected="selected">' : '>';
            }
            $html_form .= $university_studie->subject_name . '</option>';
        }
        $html_form .= '</select';
        return $html_form;
    }
    
    public static function getStudienkollegs($form_name, $select_index, $css_class){
        $query = "SELECT * FROM go2stuko_studienkolleg";
        $studienkollegs = Database::getDatasetFromQuery($query);
        $html_form = '<select tabindex="29" class="' . $css_class . '" name="' . $form_name . '" style="width: 215px;">';
        $html_form .= '<option value="0">Bitte ausw&auml;hlen...</option>';
        foreach($studienkollegs as $studienkolleg){
            $html_form .= '<option value="' . $studienkolleg->skid .'"';
            if(isset($_POST[$form_name])){
                $html_form .= $_POST[$form_name] == $studienkolleg->skid ? ' selected="selected">' : '>';
            }else{
                $html_form .= $select_index == $studienkolleg->skid ? ' selected="selected">' : '>';
            }
            $html_form .= $studienkolleg->studienkolleg_name . '</option>';
        }
        $html_form .= '</select';
        return $html_form;
    }

    public static function getLogLevels($form_name, $select_index, $css_class){
        $logLevels = array(LOG_EMERG, LOG_ALERT, LOG_CRIT, LOG_ERR, LOG_WARNING, LOG_NOTICE, LOG_INFO, LOG_DEBUG, 8);
        $logLevelNames = array('EMERG', 'ALERT', 'CRIT', 'ERR', 'WARNING', 'NOTICE', 'INFO', 'DEBUG', 'Alle Logs');

        $html_form = '<select tabindex="" class="' . $css_class . '" name="' . $form_name . '" style="width: 215px;">';
        foreach($logLevels as $logLevel){
            $html_form .= '<option value="' . $logLevel .'"';
            if(isset($_POST[$form_name])){
                $html_form .= $_POST[$form_name] == $logLevel ? ' selected="selected">' : '>';
            }else{
                $html_form .= $select_index == $logLevel ? ' selected="selected">' : '>';
            }
            $html_form .= $logLevelNames[$logLevel] . '</option>';
        }
        $html_form .= '</select';
        return $html_form;
    }
    
    public static function getTestExercises($form_name, $select_index, $css_class, $skid){
        $query_yet_not_used = "AND e.eid NOT IN (SELECT e_sub.eid FROM go2stuko_exercise e_sub, go2stuko_test_exercise_link tel_sub, go2stuko_test t_sub WHERE tel_sub.eid = e_sub.eid AND tel_sub.tid = t_sub.tid AND t_sub.tid IN (378, 377, 375, 374, 379, 380, 381, 382, 383, 384, 385, 386) AND t_sub.skid = " . $skid . ")";
        $query_only_types_of_stk = "e.etid = sketl.etid AND sketl.skid = " . $skid . " AND e.etid = et.etid AND ";
        $query = "SELECT * FROM go2stuko_exercise e, go2stuko_exercise_type et, go2stuko_studienkolleg_exercise_type_link sketl WHERE " . $query_only_types_of_stk . " e.exmodus='test' " . $query_yet_not_used . " ORDER BY e.etid, e.created DESC";
        //$query = "SELECT * FROM go2stuko_exercise e, go2stuko_exercise_type et WHERE e.etid = et.etid";
        $exercises = Database::getDatasetFromQuery($query);
        $html_form = '<select class="' . $css_class . '" name="' . $form_name . '" style="width: 155px;">';
        $html_form .= '<option value="0">Bitte ausw&auml;hlen...</option>';
        foreach($exercises as $exercise){
            $html_form .= '<option value="' . $exercise->eid .'"';
            if(isset($_POST[$form_name])){
                $html_form .= $_POST[$form_name] == $exercise->eid ? ' selected="selected">' : '>';
            }else{
                $html_form .= $select_index == $exercise->eid ? ' selected="selected">' : '>';
            }
            $html_form .= $exercise->etname . ': ' . $exercise->heading . '</option>';
        }
        $html_form .= '</select';
        return $html_form;
    }
    
    public static function getPackagesOfStk($skid, $form_name, $select_index, $css_class){
        $query = "SELECT * FROM go2stuko_test_package tp WHERE tp.skid = " . $skid;
        $packages = Database::getDatasetFromQuery($query);
        $html_form = '<select class="' . $css_class . '" name="' . $form_name . '" style="width: 155px;">';
        $html_form .= '<option value="0">Bitte ausw&auml;hlen...</option>';
        foreach($packages as $package){
            $html_form .= '<option value="' . $package->tpid .'"';
            if(isset($_POST[$form_name])){
                $html_form .= $_POST[$form_name] == $package->tpid ? ' selected="selected">' : '>';
            }else{
                $html_form .= $select_index == $package->tpid ? ' selected="selected">' : '>';
            }
            $html_form .= $package->description . '</option>';
        }
        $html_form .= '</select';
        return $html_form;
    }

    public static function getPackages($form_name, $select_index, $css_class){
        $query = "SELECT * FROM go2stuko_test_package";
        $packages = Database::getDatasetFromQuery($query);
        $html_form = '<select class="' . $css_class . '" name="' . $form_name . '" style="width: 155px;">';
        $html_form .= '<option value="0">Bitte ausw&auml;hlen...</option>';
        foreach($packages as $package){
            $html_form .= '<option value="' . $package->tpid .'"';
            if(isset($_POST[$form_name])){
                $html_form .= $_POST[$form_name] == $package->tpid ? ' selected="selected">' : '>';
            }else{
                $html_form .= $select_index == $package->tpid ? ' selected="selected">' : '>';
            }
            $html_form .= $package->description . '</option>';
        }
        $html_form .= '</select';
        return $html_form;
    }
    
    public static function getLanguageSchools($form_name, $select_index, $css_class){
        $query = "SELECT * FROM go2stuko_language_school";
        $language_schools = Database::getDatasetFromQuery($query);
        $html_form = '<select class="' . $css_class . '" name="' . $form_name . '" style="width: 155px;">';
        $html_form .= '<option value="0">Bitte ausw&auml;hlen...</option>';
        foreach($language_schools as $language_school){
            $html_form .= '<option value="' . $language_school->lsid .'"';
            if(isset($_POST[$form_name])){
                $html_form .= $_POST[$form_name] == $language_school->lsid ? ' selected="selected">' : '>';
            }else{
                $html_form .= $select_index == $language_school->lsid ? ' selected="selected">' : '>';
            }
            $html_form .= $language_school->school_name . '</option>';
        }
        $html_form .= '</select';
        return $html_form;
    }

}

?>
