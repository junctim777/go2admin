<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Helper
 *
 * @author Marco Armbruster
 */
final class Helper {

    //put your code here

    public static function isArrayEmpty($array) {
        if (empty($array))
            return true;
        else {
            $array = array_unique($array);
            return (count($array) == 1 && $array[0] == '');
        }
    }

    /**
     * replcae quotes to HTML entities by names or numbers
     *
     * @param (string) escaped string value
     * @param (string) default ='number' will be return to number entities you can use ='name' to return name entities
     * Note : don't use ='name' coz (&apos;) (does not work in IE)
     */
    public static function quote2entities($string, $entities_type='number') {
        $search = array("\"", "'");
        $replace_by_entities_name = array("&quot;", "&apos;");
        $replace_by_entities_number = array("&#34;", "&#39;");
        $do = null;
        if ($entities_type == 'number') {
            $do = str_replace($search, $replace_by_entities_number, $string);
        } else if ($entities_type == 'name') {
            $do = str_replace($search, $replace_by_entities_name, $string);
        } else {
            $do = addslashes($string);
        }
        return $do;
    }

}

?>
