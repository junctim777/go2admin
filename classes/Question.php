<?php

include ("./classes/SampleSolution.php");

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Question
 *
 * @author Marco Armbruster
 */
class Question {
    //put your code here
    
    var $dbQuestion;
    var $sampleSolutions;
    
    function Question($dbQuestion) {
        $this->dbQuestion = $dbQuestion;
        $this->getFromDatabase();
    }
    
    private function getFromDatabase(){
        $query = "SELECT * FROM go2stuko_sample_solution WHERE " 
                ."qid = " . $this->dbQuestion->qid . " "
                ."ORDER BY ssid";
        $sampleSolutions_data_tmp = Database::getDatasetFromQuery($query);
        foreach($sampleSolutions_data_tmp as $sampleSolution_data_tmp){
            $this->sampleSolutions[count($this->sampleSolutions)] = 
                    new SampleSolution($sampleSolution_data_tmp);
        }
    }
    
    public function getQid(){
        return $this->dbQuestion->qid;
    }
    
    public function getEeid(){
        return $this->dbQuestion->eeid;
    }
    
    public function getQtid(){
        return $this->dbQuestion->qtid;
    }
    
    public function getQtName(){
        return $this->dbQuestion->qtname;
    }
    
    public function getExerciseElementPosition(){
        return $this->dbQuestion->exercise_element_position;
    }
    
    public function getDifficulty(){
        return $this->dbQuestion->difficulty;
    }
    
    public function getEstimatedTime(){
        return $this->dbQuestion->estimated_time;
    }
    
    public function getQuestionText(){
        return $this->dbQuestion->question_text;
    }
    
    public function getIsFreeTextQuestion(){
        return $this->dbQuestion->is_free_text_question;
    }
    
    public function getContentPoints(){
        return $this->dbQuestion->content_points;
    }
    
    public function getLinguisticPoints(){
        return $this->dbQuestion->linguistic_points;
    }
    
    public function getAnswerNote(){
        return $this->dbQuestion->answer_note;
    }
    
    public function getCreated(){
        return $this->dbQuestion->created;
    }
    
    public function getChanged(){
        return $this->dbQuestion->changed;
    }
    
}

?>
