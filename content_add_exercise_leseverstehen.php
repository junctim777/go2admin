<?php

    include("./database/Database.php");
    include("./classes/Exercise.php");
    include("./classes/utils/Helper.php");
    
    $action = $_GET['action'];
    $extype = $_GET['extype'];
    $eid = $_GET['eid'];
    $eeid = $_GET['eeid'];
    $new_slot = $_GET['new_slot'];
    $update_slot = $_GET['update_slot'];
    $exmodus = $_GET['exmodus'];
    
    
    if(empty($eid) && $exmodus != "test" && $exmodus != "training"){
        $errors[modus] = "Es ist kein korrekter &Uuml;bungsmodus gesetzt (Training oder Test)";
    }
    if(empty($eid) && $extype != "Leseverstehen"){
        $errors[type] = "Es ist kein korrekter &Uuml;bungstyp gesetzt (Leseverstehen)";
    }
    
    Database::establishConnection();
    
    if($_GET['action'] == "delete_question" && !empty($_GET['qid'])){
        $query = "DELETE FROM go2stuko_question WHERE qid = " . $_GET['qid'];
        $success = mysql_query($query);
    }
    
    
    if(isset($_POST['submit_base_data']) || isset($_POST['update_base_data'])){
        $heading = addslashes(strip_tags(trim($_POST['heading'])));
        if(empty($heading)){
            $errors[heading] = "Die Ueberschrift muss korrekt eingegeben werden!";
        }
    }
    
    if(isset($_POST['submit_element_data']) || isset($_POST['update_element_data'])){
        $element_text = addslashes(nl2br(strip_tags(trim($_POST['element_text']))));
        if(empty($element_text)){
            $errors[heading] = "Der Text des Abschnitts muss korrekt eingegeben werden!";
        }
    }
    
    if(isset($_POST['submit_mc_question_data']) || isset($_POST['update_mc_question_data'])
            || isset($_POST['submit_sorting_question_data']) || isset($_POST['update_sorting_question_data'])){
        $question_introduction = addslashes(nl2br(trim($_POST['question_introduction'])));
        $answer_note = addslashes(nl2br(trim($_POST['answer_note'])));
        $difficulty = nl2br(strip_tags(trim($_POST['difficulty'])));
        $estimated_time = nl2br(strip_tags(trim($_POST['estimated_time'])));
        $content_points = nl2br(strip_tags(trim($_POST['content_points'])));
        $linguistic_points = nl2br(strip_tags(trim($_POST['linguistic_points'])));
        if(empty($answer_note)){
            $answer_note = 'NULL';
        }
        if(($content_points + $linguistic_points) == 0){
            $errors[no_points] = "Es muss zumindestens 1 Inhalts- oder Sprachpunkt f&uuml;r diese Aufgabe vergeben werden!";
        }
    }
    
    if(isset($_POST['submit_mc_question_data']) || isset($_POST['update_mc_question_data'])){
        $answers = $_POST['answers'];
        $is_correct = $_POST['is_correct'];
        if(empty($question_introduction)){
            $errors[question_introduction] = "Der Frage-Text (Aufgaben-Text) muss korrekt eingegeben werden!";
        }
        if(Helper::isArrayEmpty($answers)){
            $errors[solution_texts] = "Es muss zumindestens eine Antwort angegeben werden!";
        }
    }

    if(count($errors) == 0){
        if(isset($_POST['submit_base_data'])){
            include('./templates/content/add_exercise/dynamic_submit_base_data.php');
        }
        else if(isset($_POST['update_base_data']) && isset($eid)){
            include('./templates/content/add_exercise/dynamic_update_base_data.php');
        }
        if(isset($_POST['submit_element_data'])){
            include('./templates/content/add_exercise/leseverstehen/dynamic_submit_element_data.php');
        }
        if(isset($_POST['update_element_data'])){
            include('./templates/content/add_exercise/leseverstehen/dynamic_update_element_data.php');
        }
        if(isset($_POST['submit_mc_question_data'])){
            include('./templates/content/add_exercise/leseverstehen/dynamic_submit_mc_question_data.php');
        }
        if(isset($_POST['update_mc_question_data'])){
            include('./templates/content/add_exercise/leseverstehen/dynamic_update_mc_question_data.php');
        }
        if(isset($_POST['submit_sorting_question_data'])){
            include('./templates/content/add_exercise/leseverstehen/dynamic_submit_sorting_question_data.php');
        }
        if(isset($_POST['update_sorting_question_data'])){
            include('./templates/content/add_exercise/leseverstehen/dynamic_update_sorting_question_data.php');
        }
    }
    
    if(!empty($eid)){
        $exercise = new Exercise($eid);
        if(!isset($_POST['update_base_data']) || $update_slot != 1){
            $heading = $exercise->getHeading();
        }
    }
    
    Database::closeConnection();
    
    if($extype != null){
        $extype_str = "- " . $extype;
    }

    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */

    include('./templates/core/tpl_header.php');
    
    include('./templates/content/add_exercise/tpl_add_exercise_header.php');
    $slot_no = 1;
    include('./templates/content/add_exercise/tpl_new_base_data.php');
    echo "<br clear=\"all\"><h3>Inhalt</h3>";
    if(!empty($eid)){
        foreach($exercise->exerciseElements as $exerciseElement){
            echo "<br clear=\"all\">";
            $slot_no++;
            $sentence_no = $exerciseElement->getExercisePosition();
            if(!isset($_POST['update_element_data']) || $update_slot != $slot_no){
                $element_text = $exerciseElement->getElementText();
            }
            if(isset($update_slot) && $update_slot == $slot_no)
                include('./templates/content/add_exercise/leseverstehen/tpl_new_element.php');
            else
                include('./templates/content/add_exercise/leseverstehen/tpl_show_element.php');
            if($eeid == $exerciseElement->dbExerciseElement->eeid){
                 if($action == "show_sorting_question" || $action == "show_mc_question"){
                        $question = $exerciseElement->getQuestionFromId($_GET['qid']);
                        $answer_note = $question->getAnswerNote();
                        $difficulty = $question->getDifficulty();
                        $estimated_time = $question->getEstimatedTime();
                        $content_points = $question->getContentPoints();
                        $linguistic_points = $question->getLinguisticPoints();
                 }
                if($action == "new_mc_question" || $action == "show_mc_question"){
                    if($action == "show_mc_question"){
                        $xml = new SimpleXMLElement($question->getQuestionText());
                        $question_introduction = $xml->question_introduction;
                        $answer = $xml->answer;

                    }
                    include('./templates/content/add_exercise/leseverstehen/tpl_new_mc_question.php');
                }
                if($action == "new_sorting_question" || $action == "show_sorting_question"){
                    if($action == "show_sorting_question"){
                        $question_introduction = $question->getQuestionText();
                    } else{
                        $content_points = 5;
                        $question_introduction = '<b>Die Sätze des XXX Abschnitts sind durcheinander geraten.</b>
Ordnen Sie die Sätze in der richtigen Reihenfolge (z.B. e-d-c-b-a). Der erste richtige Satz schließt an den Abschnitt X an!';
                    }
                    include('./templates/content/add_exercise/leseverstehen/tpl_new_sorting_question.php');
                }
            }
        }
        if(!$update_slot){
            $slot_no++;
            $sentence_no = $slot_no-1;
            if(isset($_POST['submit_element_data']) && ($new_slot == $slot_no)){
                $element_text = nl2br(strip_tags(trim($_POST['element_text'])));
                
            } else{
                $element_text = null;
            }
            echo "<br clear=\"all\">";
            include('./templates/content/add_exercise/leseverstehen/tpl_new_element.php');
        }
    }
    include('./templates/content/add_exercise/tpl_add_exercise_footer.php');
    include('./templates/core/tpl_footer.php');
    
    function printQuestionsOfExerciseElement($exerciseElement){
        $out = '<ul>';
        foreach($exerciseElement->getQuestions() as $question){
            switch($question->getQtid()){
                case(4): $action = "show_mc_question"; break;
                case(5): $action = "show_sorting_question"; break;
            }
            $out .= '<li>';
                $out .= '<a href="' . $_SERVER['PHP_SELF'] .'?action=' . $action . '&eid=' . $_GET['eid'] .'&eeid=' . $exerciseElement->dbExerciseElement->eeid .'&qid=' . $question->getQid() . '">' . $question->getQtName() . '</a>';
                $out .= '&nbsp;&nbsp;&nbsp;&nbsp;<a href="' . $_SERVER['PHP_SELF'] .'?action=delete_question&eid=' . $_GET['eid'] .'&eeid=' . $exerciseElement->dbExerciseElement->eeid .'&qid=' . $question->getQid() . '">-</a>';
            $out .= '</li>';
        }
        $out .= '</ul>';
        return $out;
    }
    
?>
