<?php

    include('database/Database.php');
    include('classes/User.php');
    require_once './classes/utils/class.phpmailer.php';
    require_once './classes/utils/class.smtp.php';
    include('classes/utils/DynamicFormElements.php');
    
    $lsid = $_GET['lsid'];
     
    // school adress
    $school_adress_street = $_POST['school_adress_street'];
    $school_adress_housenr = $_POST['school_adress_housenr'];
    $school_adress_plz = $_POST['school_adress_plz'];
    $school_adress_city = $_POST['school_adress_city'];
    
    $school_name = $_POST['school_name'];
    $school_subdomain = $_POST['school_subdomain'];
    $school_email = $_POST['school_email'];
    $school_phone = $_POST['school_phone'];
    $school_url = $_POST['school_url'];
    $contactPerson_sex = $_POST['contactPerson_sex'];
    $contactPerson_forename = $_POST['contactPerson_forename'];
    $contactPerson_surname = $_POST['contactPerson_surname'];
    $school_comment = $_POST['school_comment'];
    $loginData_username = $_POST['loginData_username'];
    $loginData_password = $_POST['loginData_password'];
    $skid = $_POST['skid'];
    
    Database::establishConnection();
    
    if(isset($_GET['action']) && $_GET['action'] == 'deleteStudienkolleg'){
        if(!empty($_GET['skid'])){
            $query = "DELETE FROM go2stuko_language_school_studienkolleg_link WHERE lsid = " . $lsid 
                        . " AND skid = " . $_GET['skid'];
            mysql_query($query);
        }
    }
    
    $query = "SELECT * FROM go2stuko_language_school WHERE lsid = " . $lsid;
    $language_school = Database::getDatasetFromQuery($query);
    $language_school = $language_school[0];
    
    if(isset($_GET['action']) && $_GET['action'] == 'activateTeachersAccount'){
        $query = "INSERT INTO go2stuko_language_school_user_link (lsid, uid, rid) VALUES (" . $language_school->lsid . "," . $language_school->headofschool_uid . ", 2)";
        mysql_query($query);
    }
    
    $query = "SELECT ucd.* FROM go2stuko_language_school ls, go2stuko_user_contact_details ucd WHERE ls.cdid = ucd.cdid AND ls.lsid = " . $lsid;
    $contact_details = Database::getDatasetFromQuery($query);
    $contact_details = $contact_details[0];
    
    $query = "SELECT a.* FROM go2stuko_language_school ls, go2stuko_address a WHERE ls.aid = a.aid AND ls.lsid = " . $lsid;
    $address = Database::getDatasetFromQuery($query);
    if(!empty($address)){
        $address = $address[0];
    }
    
    $query = "SELECT u.*, ld.* FROM go2stuko_language_school ls, go2stuko_login_data ld, go2stuko_user u WHERE u.uid = ld.uid AND ls.headofschool_uid = u.uid AND ls.lsid = " . $lsid;
    $headofschool = Database::getDatasetFromQuery($query);
    $headofschool = $headofschool[0];
    
    $query = "SELECT * FROM go2stuko_language_school_comment WHERE lsid = " . $lsid . " ORDER BY insert_date DESC";
    $comments = Database::getDatasetFromQuery($query);
    
    $query = "SELECT sk.* FROM go2stuko_language_school_studienkolleg_link lsskl, go2stuko_studienkolleg sk WHERE lsskl.lsid = " . $lsid . " AND lsskl.skid = sk.skid";
    $studienkollegs = Database::getDatasetFromQuery($query);
    
    $query = "SELECT * FROM go2stuko_language_school_user_link lsul, go2stuko_role r, go2stuko_user u WHERE lsul.uid = u.uid AND lsul.rid = r.rid AND lsul.lsid = " . $lsid;
    $linkedUsers = Database::getDatasetFromQuery($query);
    
    if(isset ($_POST['submit_school_data'])){
        if(!empty($contactPerson_forename) && !empty($contactPerson_surname)
                && ((!empty($loginData_username) && !empty($loginData_password)) || (count($linkedUsers) > 0))){
        
            if(!empty($school_name)){
                if(!empty($school_email) || !empty($school_phone) || !empty($school_url)){
                    $query = "UPDATE go2stuko_user_contact_details  " 
                                . "SET email_address = '" . $school_email . "',"
                                . "phone_number = '" . $school_phone . "'," 
                                . "url = '" . $school_url . "' " 
                                . "WHERE cdid = " . $contact_details->cdid;
                    $success = mysql_query($query);
                } else {
                    $errors[school_contact] = "Bitte gebe die Email-Adresse, die Telefonummer oder die Homepage der Sprachschule ein!";
                }
                if(!empty($school_adress_street) && !empty($school_adress_housenr) 
                        && !empty($school_adress_plz) && !empty($school_adress_city)){
                    if(empty($address)){
                        echo "INSERT";
                        $query = "INSERT INTO go2stuko_address (road, " 
                                    . "house_number, postal_code, city, cid) VALUES ('" .$school_adress_street . "'," 
                                    . "'" . $school_adress_housenr . "', '" . $school_adress_plz . "', '" . $school_adress_city . "', 1)";
                        $success = mysql_query($query);
                        $aid = mysql_insert_id(Database::$link);
                    } else{
                        echo "UPDATE";
                        $query = "UPDATE go2stuko_address  " 
                                . "SET road = '" . $school_adress_street . "',"
                                . "house_number = '" . $school_adress_housenr . "',"
                                . "postal_code = '" . $school_adress_plz . "'," 
                                . "city = '" . $school_adress_city . "' " 
                                . "WHERE aid = " . $address->aid;
                        $success = mysql_query($query);
                    }
                }
                $query = "UPDATE go2stuko_language_school  " 
                                . "SET school_name = '" . $school_name . "',"
                                . (!empty($aid) ? "aid = " . $aid . "," : "")
                                . "subdomain = '" . $school_subdomain . "' " 
                                . "WHERE lsid = " . $lsid;
                $success = mysql_query($query);
                if(!empty($school_comment)){
                    $query = "INSERT INTO go2stuko_language_school_comment (lsid, comment_text, insert_date) " 
                                . " VALUES (" .$lsid . ",'" . $school_comment . "', NOW())";
                    $success = mysql_query($query);
                }
                if(!empty($skid)){
                    $query = "INSERT INTO go2stuko_language_school_studienkolleg_link (lsid, skid) " 
                                . " VALUES (" .$lsid . "," . $skid . ")";
                    $success = mysql_query($query);
                }
                if(!empty($contactPerson_sex) && !empty($contactPerson_forename) 
                        && !empty($contactPerson_surname)){
                    $query = "UPDATE go2stuko_user " 
                                . "SET sex = '" . $contactPerson_sex . "',"
                                . "surname = '" . $contactPerson_surname . "'," 
                                . "forename = '" . $contactPerson_forename . "' " 
                                . "WHERE uid = " . $headofschool->uid;
                    $success = mysql_query($query);
                    if(!empty($loginData_username) && !empty($loginData_password)){
                        $query = "UPDATE go2stuko_login_data  " 
                                . "SET login_nick_name = '" . $loginData_username . "'," 
                                . "login_password = '" . hash("sha256", $loginData_password) . "' " 
                                . "WHERE ldid = " . $headofschool->ldid;
                        $success = mysql_query($query);
                    } else if(count($linkedUsers) == 0){
                        $errors[school_login] = "Bitte gebe die Login-Daten ein!";
                    } 
                } else{
                    $errors[school_contactPerson] = "Bitte gebe den Vor- und Nachnamen der kontaktperson an!";
                }
                header('Location: administration.php?adminLanguageSchools=true');
            } else{
                $errors[school_name] = "Bitte gebe einen Schul-Namen an!";
            }
        } else{
            $errors[school_initialData] = "Bitte den Vor- und Nachnamen der Kontaktperson und die Logindaten an!";
        }
    }

    include('templates/core/tpl_header.php');
    include('templates/tpl_show_school_details.php');
    include('templates/core/tpl_footer.php');
    
    Database::closeConnection();


?>
