<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    $oid = $_GET[oid];
    $lsid = $_GET[lsid];

    include("./database/Database.php");
    
    Database::establishConnection();
    
    function mnemonischesPasswort($lenght = 3, $digits = 2){
    
        // Variablen definieren
        $consonant = "bcdfghjklmnprstvwxzBCDFGHJKLMNPRSTVWXZ";
        $vowels    = "aeiouyAEIUY";
        $password  = '';

        mt_srand((double)microtime()*1000000);

        // Vokal/Konsonant-Paare erzeugen
        for($i = 1; $i <= $lenght; $i++)
        {
          $password .= substr($consonant, mt_rand(0, strlen($consonant)-1), 1);
          $password .= substr($vowels, mt_rand(0, strlen($vowels)-1), 1);
        }

        // Zahlen anhängen
        for($i = 1; $i <= $digits; $i++)
          $password .= mt_rand(0, 9);

        RETURN $password;
      }
    
    if(isset($_GET['action']) && $_GET['action'] == 'add_new_codes_already_payed' && !empty($_GET['oid'])
            && !empty($_GET['lsid']) && !empty($_POST['number'])){
        $cnt_secure = 0;
        $discount_code_cnt = 0;
        while($discount_code_cnt<$_POST['number']){
            $new_password = mnemonischesPasswort();
            $query = "INSERT INTO go2stuko_discount_code" .
                        " (lsid, oid, already_payed, discount_code)" .
                        " VALUES (" .
                        "" . $_GET['lsid'] . "," .
                        "" . $_GET['oid'] . "," .
                        "1," .
                        "'" . $new_password . "'" .
                        ")";
            $success = mysql_query($query);
            if($success != false){
                $discount_code_cnt++;
            }
            if($cnt_secure++ >= 10000)
                break;
        }
    }
    
    if(isset($_GET['action']) && $_GET['action'] == 'add_new_codes' && !empty($_GET['oid'])
            && !empty($_GET['lsid']) && !empty($_POST['number'])){
        $cnt_secure = 0;
        $discount_code_cnt = 0;
        while($discount_code_cnt<$_POST['number']){
            $new_password = mnemonischesPasswort();
            $query = "INSERT INTO go2stuko_discount_code" .
                        " (lsid, oid, already_payed, discount_code)" .
                        " VALUES (" .
                        "" . $_GET['lsid'] . "," .
                        "" . $_GET['oid'] . "," .
                        "0," .
                        "'" . $new_password . "'" .
                        ")";
            $success = mysql_query($query);
            if($success != false){
                $discount_code_cnt++;
            }
            if($cnt_secure++ >= 10000)
                break;
        }
    }
    
    if(isset($_GET['action']) && $_GET['action'] == 'export_codes_already_payed' && !empty($_GET['oid'])
            && !empty($_GET['lsid'])){
        $query = "SELECT * FROM go2stuko_discount_code WHERE already_payed = 1 AND bid IS NULL AND oid = " . $_GET['oid'] . " AND lsid = " . $_GET['lsid'];
        $free_discount_codes_already_payed = Database::getDatasetFromQuery($query);
        $out = "Rabatt-Codes\n";
        foreach($free_discount_codes_already_payed as $free_discount_code){
            $out .= $free_discount_code->discount_code . "\n";
        }
        $myFile = "./src/tmp/tmp.txt";
        $fh = fopen($myFile, 'w') or die("can't open file");
        fwrite($fh, $out);
        fclose($fh);
        header('Content-type: text/plain');
        header('Content-Disposition: attachment; filename="' . basename($myFile) . '"');
        header('Content-Transfer-Encoding: binary');
        readfile($myFile);
    }
    
    if(isset($_GET['action']) && $_GET['action'] == 'export_codes' && !empty($_GET['oid'])
            && !empty($_GET['lsid'])){
        $query = "SELECT * FROM go2stuko_discount_code WHERE already_payed = 0 AND bid IS NULL AND oid = " . $_GET['oid'] . " AND lsid = " . $_GET['lsid'];
        $free_discount_codes_already_payed = Database::getDatasetFromQuery($query);
        $out = "";
        foreach($free_discount_codes_already_payed as $free_discount_code){
            $out .= $free_discount_code->discount_code . "\\n";
        }
        echo $out;
    }
    
    if(isset($_GET['action']) && $_GET['action'] == 'delete_code' && !empty($_GET['dcid'])){
        $query = "DELETE FROM go2stuko_discount_code WHERE dcid = " . $_GET['dcid'];
        $success = mysql_query($query);
    }
    
    
    $query = "SELECT * FROM go2stuko_language_school WHERE lsid = " . $lsid;
    $language_school = Database::getDatasetFromQuery($query);
    $language_school = $language_school[0];
    
    $query = "SELECT * FROM go2stuko_offer WHERE oid = " . $oid;
    $offer = Database::getDatasetFromQuery($query);
    $offer = $offer[0];
    
    $query = "SELECT * FROM go2stuko_discount_code WHERE already_payed = 1 AND oid = " . $oid . " AND lsid = " . $lsid;
    $discount_codes_already_payed = Database::getDatasetFromQuery($query);
    
    $query = "SELECT * FROM go2stuko_discount_code WHERE already_payed = 0 AND oid = " . $oid . " AND lsid = " . $lsid;
    $discount_codes = Database::getDatasetFromQuery($query);
    
    include('templates/core/tpl_header.php');
    include('templates/content/tpl_content_manage_discount_codes.php');
    include('templates/core/tpl_footer.php');
    
    Database::closeConnection();
    
?>
