<?php

    function printLogging(){

        $query = "SELECT * FROM go2stuko_general_log ";
        if(isset($_POST['loglevel']) && $_POST['loglevel'] != 8 ) {
            $query .= " WHERE type LIKE " . $_POST['loglevel'] . " ";
        }
        $query .= "ORDER BY date DESC ";

        if(isset($_GET['limit'])) {
            $query .= "LIMIT " . $_GET['limit'];
        }

        $log_messages = Database::getDatasetFromQuery($query);

        $out = '<form action="' . $_SERVER['PHP_SELF'] . '?showLogging=true" method="post" 
                name="choose_loglevel"><table><tr><td>LogLevel:</td><td style="padding-left:30px;">'
            . DynamicFormElements::getLogLevels(
                'loglevel',
                (isset($_POST['loglevel']) ? $_POST['loglevel'] : 8),
                "")
            . '</td><td style="padding-left:20px;"><input class="" type="submit" name="submit" value="OK"/></td></tr></table></form><br>';


        $out .= '<a href="administration.php?showLogging=true&limit=25">show 25 logs</a><br>';
        $out .= '<a href="administration.php?showLogging=true&limit=50">show 50 logs</a><br>';
        $out .= '<a href="administration.php?showLogging=true&limit=150">show 150 logs</a><br>';
        $out .= '<a href="administration.php?showLogging=true">show ALL logs</a><br>';

        $out .= '<table border="0">';
        $out .= '<tr>';
        $out .= '<th align="left" valign="top" style="padding-left: 10px; padding-right: 10px; width: 200px;">';
        $out .= '<b>Datum</b>';
        $out .= '</th>';
        $out .= '<th align="left" valign="top" style="padding-left: 10px; padding-right: 10px;">';
        $out .= '<b>LogLevel</b>';
        $out .= '</th>';
        $out .= '<th align="left" valign="top" style="padding-left: 10px; padding-right: 10px;">';
        $out .= '<b>Message</b>';
        $out .= '</th>';
        $out .= '</tr>';

        foreach ($log_messages as $message){
            $out .= '<tr>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;">';
            $out .= $message->date;
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;">';
            $out .= $message->type;
            $out .= '</td>';
            $out .= '<td align="left" valign="top" style="padding-left: 10px; padding-right: 10px;">';
            $out .= $message->event;
            $out .= '</td>';
            $out .= '</tr>';
        }

        $out .= '</table>';
        $out .= '<br>';
        $out .= '<a href="administration.php">Zur&uuml;ck</a>';

        return $out;

    }

?>