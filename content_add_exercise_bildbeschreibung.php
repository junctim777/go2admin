<?php

    include("./database/Database.php");
    include("./classes/Exercise.php");
    include("./classes/utils/Helper.php");
    
    $extype = $_GET['extype'];
    $eid = $_GET['eid'];
    $new_slot = $_GET['new_slot'];
    $update_slot = $_GET['update_slot'];
    $exmodus = $_GET['exmodus'];
    
    
    if(empty($eid) && $exmodus != "test" && $exmodus != "training"){
        $errors[modus] = "Es ist kein korrekter &Uuml;bungsmodus gesetzt (Training oder Test)";
    }
    
    if(empty($eid) && $extype != "Textproduktion"){
        $errors[type] = "Es ist kein korrekter &Uuml;bungstyp gesetzt (Textproduktion)";
    }
    
    Database::establishConnection();
    
    if(isset($_POST['submit_base_data']) || isset($_POST['update_base_data'])){
        $heading = addslashes(strip_tags(trim($_POST['heading'])));
        $introduction_text = addslashes(nl2br(trim($_POST['introduction_text'])));
        if(empty($heading)){
            $errors[heading] = "Die Ueberschrift muss korrekt eingegeben werden!";
        }
        if(empty($introduction_text)){
            $errors[introduction_text] = "Der Aufgabentext muss korrekt eingegeben werden!";
        }
    }
    
    if(isset($_POST['update_image_data']) || isset($_POST['submit_image_data'])){
        $filetype = GetImageSize($_FILES['new_image']['tmp_name']);
        $filesize = $_FILES['new_image']['size'];
        if($filetype[1] == 0 && $filetype[2] == 0 && $filetype[3] == 0){
            $errors[no_points] = "Bitte w&auml;hle ein g&uuml;ltiges Bildaus (gif, jpg oder png)!";
        }
        if($filesize > 1000000){
            $errors[no_points] = "Das Bild darf nicht gr&ouml;&szlig;er als 1MB sein!";
        }
    }
    
    if(isset($_POST['submit_question_data']) || isset($_POST['update_question_data'])){
        $question_text = addslashes(nl2br(strip_tags(trim($_POST['question_text']))));
        $answer_note = addslashes(nl2br(trim($_POST['answer_note'])));
        $difficulty = nl2br(strip_tags(trim($_POST['difficulty'])));
        $estimated_time = nl2br(strip_tags(trim($_POST['estimated_time'])));
        $content_points = nl2br(strip_tags(trim($_POST['content_points'])));
        $linguistic_points = nl2br(strip_tags(trim($_POST['linguistic_points'])));
        if(empty($question_text)){
            $errors[heading] = "Der Frage-Text (Aufgaben-Satz) muss korrekt eingegeben werden!";
        }
        if(empty($answer_note)){
            $errors[answer_note] = "Es muss ein L&ouml;sungshinweis angegeben werden!";
        }
        if(($content_points + $linguistic_points) == 0){
            $errors[no_points] = "Es muss zumindestens 1 Inhalts- oder Sprachpunkt f&uuml;r diese Aufgabe vergeben werden!";
        }
    }
    
    if(count($errors) == 0){
        if(isset($_POST['submit_base_data'])){
            include('./templates/content/add_exercise/dynamic_submit_base_data.php');
        }
        else if(isset($_POST['update_base_data']) && isset($eid)){
            include('./templates/content/add_exercise/dynamic_update_base_data.php');
        }
        if(isset($_POST['no_image'])){
            include('./templates/content/add_exercise/bildbeschreibung/dynamic_no_image.php');
        }
        if(isset($_POST['submit_image_data'])){
            include('./templates/content/add_exercise/bildbeschreibung/dynamic_upload_image.php');
        }
        if(isset($_POST['delete_image_data'])){
            //include('./templates/content/add_exercise/satzbildung/dynamic_update_element_data.php');
        }
        if(isset($_POST['submit_question_data'])){
            include('./templates/content/add_exercise/bildbeschreibung/dynamic_submit_question_data.php');
        }
        if(isset($_POST['update_question_data'])){
            include('./templates/content/add_exercise/bildbeschreibung/dynamic_update_question_data.php');
        }
        
    }
    
    if(!empty($eid)){
        $exercise = new Exercise($eid);
        if(!isset($_POST['update_base_data']) || $update_slot != 1){
            $heading = $exercise->getHeading();
            $introduction_text = $exercise->getIntroductionText();
        }
    } else{
        if($extype == "Textproduktion"){
        $introduction_text = '<b>Schreiben Sie zu dem Bild einen <u>zusammenhängenden Text</u> von mindestens <u>5 Sätzen</u>.</b>
<i>Folgende Inhalte müssen dabei vorkommen:</i>';
        }
    }
    
    Database::closeConnection();
    
    if($extype != null){
        $extype_str = "- " . $extype;
    }

    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */

    include('./templates/core/tpl_header.php');
    
    include('./templates/content/add_exercise/tpl_add_exercise_header.php');
    $slot_no = 1;
    include('./templates/content/add_exercise/tpl_new_base_data.php');
    echo "<br clear=\"all\"><h3>Inhalt</h3>";
    if(!empty($eid)){
        include('./templates/content/add_exercise/bildbeschreibung/tpl_upload_image.php');
        if(count($exercise->exerciseElements) > 0){
            $exerciseElement = $exercise->exerciseElements[0];
            $eeid = $exerciseElement->getEeid();
            foreach($exerciseElement->questions as $question){
                echo "<br clear=\"all\">";
                $slot_no++;
                $sentence_no = $question->getExerciseElementPosition();
                if(!isset($_POST['update_question_data']) || $update_slot != $slot_no){
                    $question_text = $question->getQuestionText();
                    $answer_note = $question->getAnswerNote();
                    $difficulty = $question->getDifficulty();
                    $estimated_time = $question->getEstimatedTime();
                    $content_points = $question->getContentPoints();
                    $linguistic_points = $question->getLinguisticPoints();
                }
                if(isset($update_slot) && $update_slot == $slot_no)
                    include('./templates/content/add_exercise/bildbeschreibung/tpl_new_question.php');
                else
                    include('./templates/content/add_exercise/bildbeschreibung/tpl_show_question.php');
            }
            if(!$update_slot){
                $slot_no++;
                $sentence_no = $slot_no-1;
                if(isset($_POST['submit_question_data']) && ($new_slot == $slot_no)){
                    $question_text = nl2br(strip_tags(trim($_POST['question_text'])));
                    $answer_note = nl2br(strip_tags(trim($_POST['answer_note'])));
                    $difficulty = nl2br(strip_tags(trim($_POST['difficulty'])));
                    $estimated_time = nl2br(strip_tags(trim($_POST['estimated_time'])));
                    $content_points = nl2br(strip_tags(trim($_POST['content_points'])));
                    $linguistic_points = nl2br(strip_tags(trim($_POST['linguistic_points'])));

                } else{
                    $question_text = null;
                    $answer_note = null;
                    $difficulty = null;
                    $estimated_time = null;
                    $content_points = null;
                    $linguistic_points = null;
                }
                echo "<br clear=\"all\">";
                include('./templates/content/add_exercise/bildbeschreibung/tpl_new_question.php');
            }
        }
    }
    include('./templates/content/add_exercise/tpl_add_exercise_footer.php');
    include('./templates/core/tpl_footer.php');
    
?>
